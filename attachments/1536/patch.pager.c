--- pager.c.orig	Mon May 26 16:56:10 2003
+++ pager.c	Mon May 26 19:32:25 2003
@@ -971,7 +971,11 @@
{
  unsigned char *p;
  static int b_read;
-
+  
+  size_t k, n;
+  wchar_t wc;
+  mbstate_t mbstate;
+  
  if (*buf_ready == 0)
  {
    buf[blen - 1] = 0;
@@ -986,6 +990,15 @@
    b_read = (int) (*last_pos - offset);
    *buf_ready = 1;

+    /* trim tail of buf so that it contains complete multibyte characters */
+    memset (&mbstate, 0, sizeof (mbstate));
+    for (n = b_read, p = buf;
+         n > 0 && (k = mbrtowc (&wc, (char *) p, n, &mbstate)); 
+         p += k, n -= k) if (k == -1) k = 1; 
+                         else if (k == -2) break;
+    b_read -= n;
+    buf[b_read] = 0; 
+    
    /* copy "buf" to "fmt", but without bold and underline controls */
    p = buf;
    while (*p)
