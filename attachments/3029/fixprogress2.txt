# HG changeset patch
# User Anders Helmersson <anders.helmersson.utsikt@wasadata.net>
# Date 1202626900 -3600
# Branch HEAD
# Node ID 26eb1f9d395d824c77eae16bc02a9c1b3bd3d774
# Parent  fd741c1922f303d3cef89d642b9ed4d0b0fcdc44
fix in progress message for large (>22M) messages)

diff -r fd741c1922f3 -r 26eb1f9d395d curs_lib.c
--- a/curs_lib.c	Tue Jan 29 21:06:38 2008 -0800
+++ b/curs_lib.c	Sun Feb 10 08:01:40 2008 +0100
@@ -426,7 +426,8 @@ void mutt_progress_update (progress_t* p
     if (progress->size > 0)
     {
       mutt_message ("%s %s/%s (%d%%)", progress->msg, posstr, progress->sizestr,
-		    percent > 0 ? percent : progress->pos * 100 / progress->size);
+		    percent > 0 ? percent :
+		   	(int) (100.0 * (double) progress->pos / progress->size));
     }
     else
     {
