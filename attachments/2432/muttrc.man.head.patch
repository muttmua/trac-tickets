Index: muttrc.man.head
===================================================================
RCS file: /cvsroots/mutt/doc/muttrc.man.head,v
retrieving revision 3.22
diff -u -3 -p -u -r3.22 muttrc.man.head
--- muttrc.man.head	20 Jul 2006 00:12:52 -0000	3.22
+++ muttrc.man.head	21 Aug 2006 00:09:57 -0000
@@ -67,16 +67,33 @@ like sh and bash: Prepend the name of th
 .SH COMMANDS
 .PP
 .nf
-\fBalias\fP \fIkey\fP \fIaddress\fP [\fB,\fP \fIaddress\fP [ ... ]]
+\fBalias\fP [\fB-group\fP \fIname\fP [...]] \fIkey\fP \fIaddress\fP [\fB,\fP \fIaddress\fP [ ... ]]
 \fBunalias\fP [\fB * \fP | \fIkey\fP ]
 .fi
 .IP
 \fBalias\fP defines an alias \fIkey\fP for the given addresses.
 \fBunalias\fP removes the alias corresponding to the given \fIkey\fP or
-all aliases when \(lq\fB*\fP\(rq is used as an argument.
+all aliases when \(lq\fB*\fP\(rq is used as an argument. The optional
+\fB-group\fP argument to \fBalias\fP causes the aliased address(es) to be
+added to the named \fIgroup\fP.
 .PP
 .nf
-\fBalternates\fP \fIregexp\fP [ \fB,\fP \fIregexp\fP [ ... ]]
+\fBgroup\fP [\fB-group\fP \fIname\fP] [\fB-rx\fP \fIEXPR\fP [ \fI...\fP ]] [\fB-addr\fP \fIaddress\fP [ \fI...\fP ]]
+\fBungroup\fP [\fB-group\fP \fIname\fP ] [ \fB*\fP | [[\fB-rx\fP \fIEXPR\fP [ \fI...\fP ]] [\fB-addr\fP \fIaddress\fP [ \fI...\fP ]]]
+.fi
+.IP
+\fBgroup\fP is used to directly add either addresses or regular expressions to
+the specified group or groups. The different categories of arguments to the
+\fBgroup\fP command can be in any order. The flags \fB-rx\fP and \fB-addr\fP
+specify what the following strings (that cannot begin with a hyphen) should be
+interpreted as: either a regular expression or an email address, respectively.
+\fBungroup\fP is used to remove addresses or regular expressions from the
+specified group or groups. The syntax is similar to the \fBgroup\fP command,
+however the special character \fB*\fP can be used to empty a group of all of
+its contents.
+.PP
+.nf
+\fBalternates\fP [\fB-group\fP \fIname\fP] \fIregexp\fP [ \fB,\fP \fIregexp\fP [ ... ]]
 \fBunalternates\fP [\fB * \fP | \fIregexp\fP [ \fB,\fP \fIregexp\fP [ ... ]] ]
 .fi
 .IP
@@ -84,7 +101,8 @@ all aliases when \(lq\fB*\fP\(rq is used
 where you receive mail; you can use regular expressions to specify
 alternate addresses.  This affects mutt's idea about messages
 from you, and messages addressed to you.  \fBunalternates\fP removes
-a regular expression from the list of known alternates.
+a regular expression from the list of known alternates. The \fB-group\fP flag
+causes all of the subsequent regular expressions to be added to the named group.
 .PP
 .nf
 \fBalternative_order\fP \fItype\fP[\fB/\fP\fIsubtype\fP] [ ... ]
@@ -226,9 +244,9 @@ The \fBunignore\fP command permits you t
 the above mentioned list of ignored headers.
 .PP
 .nf
-\fBlists\fP \fIregexp\fP [ \fIregexp\fP ... ]
+\fBlists\fP [\fB-group\fP \fIname\fP] \fIregexp\fP [ \fIregexp\fP ... ]
 \fBunlists\fP \fIregexp\fP [ \fIregexp\fP ... ]
-\fBsubscribe\fP \fIregexp\fP [ \fIregexp\fP ... ]
+\fBsubscribe\fP [\fB-group\fP \fIname\fP] \fIregexp\fP [ \fIregexp\fP ... ]
 \fBunsubscribe\fP \fIregexp\fP [ \fIregexp\fP ... ]
 .fi
 .IP
@@ -241,7 +259,8 @@ known mailing lists.  The \fBunlists\fP 
 list from the lists of known and subscribed mailing lists.  The
 \fBsubscribe\fP command adds a mailing list to the lists of known
 and subscribed mailing lists.  The \fBunsubscribe\fP command removes
-it from the list of subscribed mailing lists.
+it from the list of subscribed mailing lists. The \fb-group\fP flag
+adds all of the subsequent regular expressions to the named group.
 .TP
 \fBmbox-hook\fP [\fB!\fP]\fIpattern\fP \fImailbox\fP
 When mutt changes to a mail folder which matches \fIpattern\fP,
@@ -378,7 +397,9 @@ In various places with mutt, including s
 A simple pattern consists of an operator of the form
 \(lq\fB~\fP\fIcharacter\fP\(rq, possibly followed by a parameter
 against which mutt is supposed to match the object specified by
-this operator.  (For a list of operators, see below.)
+this operator.  For some \fIcharacter\fPs, the \fB~\fP may be
+replaced by another character to alter the behavior of the match.
+These are described in the list of operators, below.
 .PP
 With some of these operators, the object to be matched consists of
 several e-mail addresses.  In these cases, the object is matched if
@@ -397,52 +418,154 @@ Additionally, you can negate a pattern b
 .SS Simple Patterns
 .PP
 Mutt understands the following simple patterns:
-.PP
-.TS
-l l.
-~A	all messages
-~b \fIEXPR\fP	messages which contain \fIEXPR\fP in the message body
-~B \fIEXPR\fP	messages which contain \fIEXPR\fP in the whole message
-~c \fIEXPR\fP	messages carbon-copied to \fIEXPR\fP
-~C \fIEXPR\fP	message is either to: or cc: \fIEXPR\fP
-~d \fIMIN\fP-\fIMAX\fP	messages with \(lqdate-sent\(rq in a Date range
-~D	deleted messages
-~e \fIEXPR\fP	message which contains \fIEXPR\fP in the \(lqSender\(rq field
-~E	expired messages
-~f \fIEXPR\fP	messages originating from \fIEXPR\fP
-~F	flagged messages
-~g	PGP signed messages
-~G	PGP encrypted messages
-~h \fIEXPR\fP	messages which contain \fIEXPR\fP in the message header
-~H \fIEXPR\fP	messages with spam tags matching \fIEXPR\fP
-~i \fIEXPR\fP	message which match \fIEXPR\fP in the \(lqMessage-ID\(rq field
-~k	message contains PGP key material
-~l	message is addressed to a known mailing list
-~L \fIEXPR\fP	message is either originated or received by \fIEXPR\fP
-~m \fIMIN\fP-\fIMAX\fP	message in the range \fIMIN\fP to \fIMAX\fP
-~n \fIMIN\fP-\fIMAX\fP	messages with a score in the range \fIMIN\fP to \fIMAX\fP
-~N	new messages
-~O	old messages
-~p	message is addressed to you (consults $alternates)
-~P	message is from you (consults $alternates)
-~Q	messages which have been replied to
-~r \fIMIN\fP-\fIMAX\fP	messages with \(lqdate-received\(rq in a Date range
-~R	read messages
-~s \fIEXPR\fP	messages having \fIEXPR\fP in the \(lqSubject\(rq field.
-~S	superseded messages
-~t \fIEXPR\fP	messages addressed to \fIEXPR\fP
-~T	tagged messages
-~u	message is addressed to a subscribed mailing list
-~U	unread messages
-~v	message is part of a collapsed thread.
-~V	cryptographically verified messages
-~x \fIEXPR\fP	messages which contain \fIEXPR\fP in the \(lqReferences\(rq field
-~X \fIMIN\fP-\fIMAX\fP	messages with MIN - MAX attachments
-~y \fIEXPR\fP	messages which contain \fIEXPR\fP in the \(lqX-Label\(rq field
-~z \fIMIN\fP-\fIMAX\fP	messages with a size in the range \fIMIN\fP to \fIMAX\fP
-~=	duplicated messages (see $duplicate_threads)
-~$	unreferenced message (requries threaded view)
-.TE
+.P
+.PD 0
+.TP 12
+~A
+all messages
+.TP
+~b \fIEXPR\fP
+messages which contain \fIEXPR\fP in the message body.
+.TP
+=b \fISTRING\fP
+messages which contain \fISTRING\fP in the message body. If IMAP is enabled, searches for \fISTRING\fP on the server, rather than downloading each message and searching it locally.
+.TP
+~B \fIEXPR\fP
+messages which contain \fIEXPR\fP in the whole message.
+.TP
+~c \fIEXPR\fP
+messages carbon-copied to \fIEXPR\fP
+.TP
+%c \fIgroup\fP
+messages carbon-copied to any member of \fIgroup\fP
+.TP
+~C \fIEXPR\fP
+messages either to: or cc: \fIEXPR\fP
+.TP
+%C \fIgroup\fP
+messages either to: or cc: to any member of \fIgroup\fP
+.TP
+~d \fIMIN\fP-\fIMAX\fP
+messages with \(lqdate-sent\(rq in a Date range
+.TP
+~D
+deleted messages
+.TP
+~e \fIEXPR\fP
+messages which contain \fIEXPR\fP in the \(lqSender\(rq field
+.TP
+%e \fIgroup\fP
+messages which contain a member of \fIgroup\fP in the \(lqSender\(rq field
+.TP
+~E
+expired messages
+.TP
+~f \fIEXPR\fP
+messages originating from \fIEXPR\fP
+.TP
+%f \fIgroup\fP
+messages originating form any member of \fIgroup\fP
+.TP
+~F
+flagged messages
+.TP
+~g
+PGP signed messages
+.TP
+~G
+PGP encrypted messages
+.TP
+~h \fIEXPR\fP
+messages which contain \fIEXPR\fP in the message header
+.TP
+~H \fIEXPR\fP
+messages with spam tags matching \fIEXPR\fP
+.TP
+~i \fIEXPR\fP
+messages which match \fIEXPR\fP in the \(lqMessage-ID\(rq field
+.TP
+~k
+messages containing PGP key material
+.TP
+~l
+messages addressed to a known mailing list (defined by either \fBsubscribe\fP or \fBlist\fP)
+.TP
+~L \fIEXPR\fP
+messages either originated or received by \fIEXPR\fP
+.TP
+%L \fIgroup\fP
+messages either originated or received by any member of \fIgroup\fP
+.TP
+~m \fIMIN\fP-\fIMAX\fP
+message in the range \fIMIN\fP to \fIMAX\fP
+.TP
+~n \fIMIN\fP-\fIMAX\fP
+messages with a score in the range \fIMIN\fP to \fIMAX\fP
+.TP
+~N
+new messages
+.TP
+~O
+old messages
+.TP
+~p
+messages addressed to you (as defined by \fBalternates\fP)
+.TP
+~P
+messages from you (as defined by \fBalternates\fP)
+.TP
+~Q
+messages which have been replied to
+.TP
+~r \fIMIN\fP-\fIMAX\fP
+messages with \(lqdate-received\(rq in a Date range
+.TP
+~R
+read messages
+.TP
+~s \fIEXPR\fP
+messages having \fIEXPR\fP in the \(lqSubject\(rq field.
+.TP
+~S
+superseded messages
+.TP
+~t \fIEXPR\fP
+messages addressed to \fIEXPR\fP
+.TP
+~T
+tagged messages
+.TP
+~u
+messages addressed to a subscribed mailing list (defined by \fBsubscribe\fP commands)
+.TP
+~U
+unread messages
+.TP
+~v
+message is part of a collapsed thread.
+.TP
+~V
+cryptographically verified messages
+.TP
+~x \fIEXPR\fP
+messages which contain \fIEXPR\fP in the \(lqReferences\(rq field
+.TP
+~X \fIMIN\fP-\fIMAX\fP
+messages with MIN - MAX attachments
+.TP
+~y \fIEXPR\fP
+messages which contain \fIEXPR\fP in the \(lqX-Label\(rq field
+.TP
+~z \fIMIN\fP-\fIMAX\fP
+messages with a size in the range \fIMIN\fP to \fIMAX\fP
+.TP
+~=
+duplicated messages (see $duplicate_threads)
+.TP
+~$
+unreferenced message (requries threaded view)
+.PD 1
+.DT
 .PP
 In the above, \fIEXPR\fP is a regular expression.
 .PP
