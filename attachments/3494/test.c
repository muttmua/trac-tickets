#include <curses.h>

int
main(void)
{
	int c;
	initscr(); cbreak(); noecho();
	nonl();
	intrflush(stdscr, FALSE);
	keypad(stdscr, TRUE);
	c = getch();
	endwin();
	printf("%d\n", c);
	return 0;
}
