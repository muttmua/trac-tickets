#!/usr/bin/perl

$seen_end_of_header = 0;
$seen_unformatted = 0;
$gnatsweb_sep = '--gnatsweb-attachment----';

L: while (<>) {
    s/\r\n/\n/;
    if (/^$/ && !$seen_end_of_header) {
	print "MIME-Version: 1.0\n";
	print 'Content-Type: multipart/mixed; boundary="',
	  $gnatsweb_sep, '"', "\n";
	print "\n";
	print "--", $gnatsweb_sep, "\n";
	print "\n";
	$seen_end_of_header = 1;
	next L;
    }

    if (/^\>Unformatted:/) {
	$seen_unformatted = 1;
    }
    
    s/^[ \t]*// if ($seen_unformatted);
      
    print $_;
}

print "--", $gnatsweb_sep, "--\n";
