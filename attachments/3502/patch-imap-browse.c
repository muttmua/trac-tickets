--- imap/browse.c.orig	2009-01-14 23:07:52.000000000 +0300
+++ imap/browse.c	2011-01-01 20:08:01.061619322 +0300
@@ -97,13 +97,18 @@
         rc = imap_cmd_step (idata);
         if (rc == IMAP_CMD_CONTINUE && list.name)
         {
+          char *name;
+
+          name = safe_strdup(list.name);
+          imap_utf7_encode(&name);
           if (!list.noinferiors && list.name[0] &&
-              !imap_mxcmp (list.name, mbox) &&
+              !imap_mxcmp (name, mbox) &&
             (n = strlen (mbox)) < sizeof (mbox) - 1)
           {
             mbox[n++] = list.delim;
             mbox[n] = '\0';
           }
+          FREE(&name);
         }
       }
       while (rc == IMAP_CMD_CONTINUE);
@@ -376,13 +381,18 @@
 
     if (rc == IMAP_CMD_CONTINUE && list.name)
     {
+      char *mbox;
+
       /* Let a parent folder never be selectable for navigation */
       if (isparent)
         list.noselect = 1;
+      mbox = safe_strdup(mx.mbox);
+      if (mbox) imap_utf7_decode(&mbox);
       /* prune current folder from output */
-      if (isparent || mutt_strncmp (list.name, mx.mbox, strlen (list.name)))
+      if (isparent || mutt_strncmp (list.name, mbox, strlen (list.name)))
         imap_add_folder (list.delim, list.name, list.noselect, list.noinferiors,
                          state, isparent);
+      FREE(&mbox);
     }
   }
   while (rc == IMAP_CMD_CONTINUE);
@@ -418,10 +428,18 @@
   if (isparent)
     strfcpy (relpath, "../", sizeof (relpath));
   /* strip current folder from target, to render a relative path */
-  else if (!mutt_strncmp (mx.mbox, folder, mutt_strlen (mx.mbox)))
-    strfcpy (relpath, folder + mutt_strlen (mx.mbox), sizeof (relpath));
   else
-    strfcpy (relpath, folder, sizeof (relpath));
+  {
+    char *mbox;
+
+    mbox = safe_strdup(mx.mbox);
+    if (mbox) imap_utf7_decode(&mbox);
+    if (!mutt_strncmp (mbox, folder, mutt_strlen (mbox)))
+      strfcpy (relpath, folder + mutt_strlen (mbox), sizeof (relpath));
+    else
+      strfcpy (relpath, folder, sizeof (relpath));
+    FREE(&mbox);
+  }
 
   /* apply filemask filter. This should really be done at menu setup rather
    * than at scan, since it's so expensive to scan. But that's big changes
