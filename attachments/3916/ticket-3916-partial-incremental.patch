exporting patch:
# HG changeset patch
# User Matthias Andree <matthias.andree@gmx.de>
# Date 1488443174 -3600
#      Thu Mar 02 09:26:14 2017 +0100
# Node ID 278da8354b4f5b1f9317a8a30b6d5437a2237aac
# Parent  0d9f56f087061e66f8a102a7ddc8a8dfd8afd933
Improve OPT_SSLVERIFYPARTIAL user experience.

Use OpenSSL's X509_V_FLAG_PARTIAL_CHAIN (requires 1.0.2b or newer),
and demote OPT_SSLVERIFYPARTIAL from a quadoption to a Boolean.

If the used OpenSSL version does not support this flag, do not compile
relevant code.

This waives the requirement to flip the option to ask-yes first and to
yes later, which is awkward and prone to failure.

diff --git a/init.h b/init.h
--- a/init.h
+++ b/init.h
@@ -79,6 +79,10 @@
 
 #define UL (unsigned long)
 
+#ifdef USE_SSL_OPENSSL
+/* need to check X509_V_FLAG_PARTIAL_CHAIN later */
+# include <openssl/x509_vfy.h>
+#endif
 #endif /* _MAKEDOC */
 
 #ifndef ISPELL
@@ -3378,25 +3382,21 @@
   ** the \fC$<account-hook>\fP function.
   */
 # ifdef USE_SSL_OPENSSL
-  { "ssl_verify_partial_chains", DT_QUAD, R_NONE, OPT_SSLVERIFYPARTIAL, MUTT_NO },
+#  ifdef X509_V_FLAG_PARTIAL_CHAIN
+  { "ssl_verify_partial_chains", DT_BOOL, R_NONE, OPT_SSLVERIFYPARTIAL, 0 },
   /*
   ** .pp
   ** This option should not be changed from the default unless you understand
   ** what you are doing.
   ** .pp
-  ** Setting this variable to \fIyes\fP will cause OpenSSL to automatically skip
-  ** unverified nodes in the certificate chain.  This could be desirable if you
-  ** do not want to rely on the system certificates, and only want to keep host
-  ** or intermediate certs in your $$certificate_file.
-  ** .pp
-  ** The \fIask-yes\fP and \fIask-no\fP settings are equivalent: they add
-  ** a (s)kip option to the interactive certificate prompt.  Mutt will not
-  ** remember skipped certificate nodes upon future connections.  However, this
-  ** setting could be useful to initially save desired certificates in the
-  ** chain to your $$certificate_file.
-  ** .pp
-  ** (OpenSSL only).
-  */
+  ** Setting this variable to \fIyes\fP will permit verifying partial
+  ** certification chains, i. e. when an intermediate signing CA, or the
+  ** host certificate, are marked as trusted (in $$certificate_file),
+  ** without marking the root signing CA as trusted.
+  ** .pp
+  ** (OpenSSL 1.0.2b and newer only).
+  */
+#  endif /* defined X509_V_FLAG_PARTIAL_CHAIN */
 # endif /* defined USE_SSL_OPENSSL */
   { "ssl_ciphers", DT_STR, R_NONE, UL &SslCiphers, UL 0 },
   /*
diff --git a/mutt.h b/mutt.h
--- a/mutt.h
+++ b/mutt.h
@@ -290,9 +290,6 @@
   OPT_RECALL,
 #if defined(USE_SSL)
   OPT_SSLSTARTTLS,
-# ifdef USE_SSL_OPENSSL
-  OPT_SSLVERIFYPARTIAL,
-# endif /* USE_SSL_OPENSSL */
 #endif
   OPT_SUBJECT,
   OPT_VERIFYSIG,      /* verify PGP signatures */
@@ -399,6 +396,9 @@
   OPTSSLFORCETLS,
   OPTSSLVERIFYDATES,
   OPTSSLVERIFYHOST,
+# ifdef USE_SSL_OPENSSL
+  OPT_SSLVERIFYPARTIAL,
+# endif /* USE_SSL_OPENSSL */
 #endif /* defined(USE_SSL) */
   OPTIMPLICITAUTOVIEW,
   OPTINCLUDEONLYFIRST,
diff --git a/mutt_ssl.c b/mutt_ssl.c
--- a/mutt_ssl.c
+++ b/mutt_ssl.c
@@ -23,6 +23,7 @@
 #include <openssl/ssl.h>
 #include <openssl/x509.h>
 #include <openssl/x509v3.h>
+#include <openssl/x509_vfy.h>
 #include <openssl/err.h>
 #include <openssl/rand.h>
 #include <openssl/evp.h>
@@ -459,6 +460,24 @@
     SSL_CTX_set_cipher_list (data->ctx, SslCiphers);
   }
 
+#ifdef X509_V_FLAG_PARTIAL_CHAIN
+  if (option (OPT_SSLVERIFYPARTIAL)) {
+    X509_VERIFY_PARAM *param;
+    param = X509_VERIFY_PARAM_new();
+    if (param) {
+      X509_VERIFY_PARAM_set_flags(param, X509_V_FLAG_PARTIAL_CHAIN);
+      if (0 == SSL_CTX_set1_param(data->ctx, param)) {
+	mutt_error (_("Warning: ssl_socket_open: couldn't set X509_V_FLAG_PARTIAL_CHAIN."));
+	mutt_sleep (2);
+      }
+      X509_VERIFY_PARAM_free(param);
+    } else {
+      mutt_error (_("Warning: ssl_socket_open: X509_VERIFY_PARAM_new() failed."));
+      mutt_sleep (2);
+    }
+  }
+#endif
+
   data->ssl = SSL_new (data->ctx);
   SSL_set_fd (data->ssl, conn->fd);
 
@@ -1112,8 +1131,7 @@
 
   /* The leaf/host certificate can't be skipped. */
   if ((idx != 0) &&
-      ((quadoption (OPT_SSLVERIFYPARTIAL) == MUTT_ASKNO) ||
-       (quadoption (OPT_SSLVERIFYPARTIAL) == MUTT_ASKYES)))
+      (option (OPT_SSLVERIFYPARTIAL)))
     allow_skip = 1;
 
   /* L10N:
