#!/bin/sh

xterm -geometry 80x60+20+20 -T "$0" -e "
sleep 4
base64 -d <<EOF
G1s/MTA0OWgbWz8xaBs9DQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0K
DQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoNCg0KDQoN
Cg0KDQoNCg0KDQoNCg0KDQoNChtbSBtbMkobW0hjdXJsKDEpICAgICAgICAgICAgICAgICAgICAg
ICAgICAgQ3VybCBNYW51YWwgICAgICAgICAgICAgICAgICAgICAgICAgIGN1cmwoMSkbW20NChtb
bQ0KG1sxbU5BTUUbW20bW20NCiAgICAgICBjdXJsIC0gdHJhbnNmZXIgYSBVUkwbW20NChtbbQ0K
G1sxbVNZTk9QU0lTG1ttG1ttDQogICAgICAgG1sxbWN1cmwbW20gG1sxbVtvcHRpb25zXRtbbSAb
WzRtW1VSTC4uLl0bWzI0bRtbbQ0KG1ttDQobWzFtREVTQ1JJUFRJT04bW20bW20NCiAgICAgICAb
WzFtY3VybBtbbSAgaXMgIGEgdG9vbCB0byB0cmFuc2ZlciBkYXRhIGZyb20gb3IgdG8gYSBzZXJ2
ZXIsIHVzaW5nIG9uZSBvZiB0aGUbW20NCiAgICAgICBzdXBwb3J0ZWQgcHJvdG9jb2xzIChESUNU
LCBGSUxFLCBGVFAsIEZUUFMsIEdPUEhFUiwgSFRUUCwgSFRUUFMsICBJTUFQLBtbbQ0KICAgICAg
IElNQVBTLCAgTERBUCwgIExEQVBTLCAgUE9QMywgUE9QM1MsIFJUTVAsIFJUU1AsIFNDUCwgU0ZU
UCwgU01UUCwgU01UUFMsG1ttDQogICAgICAgVEVMTkVUIGFuZCBURlRQKS4gIFRoZSBjb21tYW5k
IGlzIGRlc2lnbmVkIHRvIHdvcmsgd2l0aG91dCB1c2VyICBpbnRlci0bW20NCiAgICAgICBhY3Rp
b24uG1ttDQobW20NCiAgICAgICBjdXJsIG9mZmVycyBhIGJ1c2xvYWQgb2YgdXNlZnVsIHRyaWNr
cyBsaWtlIHByb3h5IHN1cHBvcnQsIHVzZXIgYXV0aGVuLRtbbQ0KICAgICAgIHRpY2F0aW9uLCBG
VFAgdXBsb2FkLCBIVFRQIHBvc3QsIFNTTCBjb25uZWN0aW9ucywgY29va2llcywgZmlsZSAgdHJh
bnMtG1ttDQogICAgICAgZmVyIHJlc3VtZSBhbmQgbW9yZS4gQXMgeW91IHdpbGwgc2VlIGJlbG93
LCB0aGUgbnVtYmVyIG9mIGZlYXR1cmVzIHdpbGwbW20NCiAgICAgICBtYWtlIHlvdXIgaGVhZCBz
cGluIRtbbQ0KG1ttDQogICAgICAgY3VybCBpcyBwb3dlcmVkIGJ5ICBsaWJjdXJsICBmb3IgIGFs
bCAgdHJhbnNmZXItcmVsYXRlZCAgZmVhdHVyZXMuICBTZWUbW20NCiAgICAgICAbWzFtbGliY3Vy
bBtbbSgzKSBmb3IgZGV0YWlscy4bW20NChtbbQ0KG1sxbVVSTBtbbRtbbQ0KICAgICAgIFRoZSAg
VVJMICBzeW50YXggaXMgcHJvdG9jb2wtZGVwZW5kZW50LiBZb3UnbGwgZmluZCBhIGRldGFpbGVk
IGRlc2NyaXAtG1ttDQogICAgICAgdGlvbiBpbiBSRkMgMzk4Ni4bW20NChtbbQ0KICAgICAgIFlv
dSBjYW4gc3BlY2lmeSBtdWx0aXBsZSBVUkxzIG9yIHBhcnRzIG9mIFVSTHMgIGJ5ICB3cml0aW5n
ICBwYXJ0ICBzZXRzG1ttDQogICAgICAgd2l0aGluIGJyYWNlcyBhcyBpbjobW20NChtbbQ0KICAg
ICAgICBodHRwOi8vc2l0ZS57b25lLHR3byx0aHJlZX0uY29tG1ttDQobW20NCiAgICAgICBvciB5
b3UgY2FuIGdldCBzZXF1ZW5jZXMgb2YgYWxwaGFudW1lcmljIHNlcmllcyBieSB1c2luZyBbXSBh
cyBpbjobW20NChtbbQ0KICAgICAgICBmdHA6Ly9mdHAubnVtZXJpY2Fscy5jb20vZmlsZVsxLTEw
MF0udHh0G1ttDQogICAgICAgIGZ0cDovL2Z0cC5udW1lcmljYWxzLmNvbS9maWxlWzAwMS0xMDBd
LnR4dCAgICAod2l0aCBsZWFkaW5nIHplcm9zKRtbbQ0KICAgICAgICBmdHA6Ly9mdHAubGV0dGVy
cy5jb20vZmlsZVthLXpdLnR4dBtbbQ0KG1ttDQogICAgICAgTmVzdGVkICBzZXF1ZW5jZXMgIGFy
ZSBub3Qgc3VwcG9ydGVkLCBidXQgeW91IGNhbiB1c2Ugc2V2ZXJhbCBvbmVzIG5leHQbW20NCiAg
ICAgICB0byBlYWNoIG90aGVyOhtbbQ0KG1ttDQogICAgICAgIGh0dHA6Ly9hbnkub3JnL2FyY2hp
dmVbMTk5Ni0xOTk5XS92b2xbMS00XS9wYXJ0e2EsYixjfS5odG1sG1ttDQobW20NCiAgICAgICBZ
b3UgY2FuIHNwZWNpZnkgYW55IGFtb3VudCBvZiBVUkxzIG9uIHRoZSBjb21tYW5kIGxpbmUuICBU
aGV5ICB3aWxsICBiZRtbbQ0KICAgICAgIGZldGNoZWQgaW4gYSBzZXF1ZW50aWFsIG1hbm5lciBp
biB0aGUgc3BlY2lmaWVkIG9yZGVyLhtbbQ0KG1ttDQogICAgICAgWW91ICBjYW4gIHNwZWNpZnkg
YSBzdGVwIGNvdW50ZXIgZm9yIHRoZSByYW5nZXMgdG8gZ2V0IGV2ZXJ5IE50aCBudW1iZXIbW20N
CiAgICAgICBvciBsZXR0ZXI6G1ttDQobW20NCiAgICAgICAgaHR0cDovL3d3dy5udW1lcmljYWxz
LmNvbS9maWxlWzEtMTAwOjEwXS50eHQbW20NCiAgICAgICAgaHR0cDovL3d3dy5sZXR0ZXJzLmNv
bS9maWxlW2EtejoyXS50eHQbW20NChtbbQ0KICAgICAgIElmIHlvdSBzcGVjaWZ5IFVSTCB3aXRo
b3V0IHByb3RvY29sOi8vIHByZWZpeCwgIGN1cmwgIHdpbGwgIGF0dGVtcHQgIHRvG1ttDQogICAg
ICAgZ3Vlc3MgIHdoYXQgIHByb3RvY29sICB5b3UgbWlnaHQgd2FudC4gSXQgd2lsbCB0aGVuIGRl
ZmF1bHQgdG8gSFRUUCBidXQbW20NCiAgICAgICB0cnkgb3RoZXIgcHJvdG9jb2xzIGJhc2VkIG9u
IG9mdGVuLXVzZWQgaG9zdCBuYW1lIHByZWZpeGVzLiAgRm9yICBleGFtLRtbbQ0KICAgICAgIHBs
ZSwgIGZvciAgaG9zdCBuYW1lcyBzdGFydGluZyB3aXRoICJmdHAuIiBjdXJsIHdpbGwgYXNzdW1l
IHlvdSB3YW50IHRvG1ttDQogICAgICAgc3BlYWsgRlRQLhtbbQ0KG1ttDQogICAgICAgY3VybCB3
aWxsIGRvIGl0cyBiZXN0IHRvIHVzZSB3aGF0IHlvdSBwYXNzIHRvIGl0IGFzIGEgVVJMLiAgSXQg
IGlzICBub3QbW20NChtbN20gTWFudWFsIHBhZ2UgY3VybCgxKSBsaW5lIDEgKHByZXNzIGggZm9y
IGhlbHAgb3IgcSB0byBxdWl0KRtbMjdtG1tLDRtbSy8bW0toCGgbW0t0CHQbW0t0CHQbW0twCHAb
W0tzCHMNG1tLG1sxOzFIY3VybCgxKSAgICAgICAgICAgICAgICAgICAgICAgICAgIEN1cmwgTWFu
dWFsICAgICAgICAgICAgICAgICAgICAgICAgICBjdXJsKDEpG1ttDQobWzI7MUgbW20NChtbMzsx
SBtbMW1OQU1FG1ttG1ttDQobWzQ7MUggICAgICAgY3VybCAtIHRyYW5zZmVyIGEgVVJMG1ttDQob
WzU7MUgbW20NChtbNjsxSBtbMW1TWU5PUFNJUxtbbRtbbQ0KG1s3OzFIICAgICAgIBtbMW1jdXJs
G1ttIBtbMW1bb3B0aW9uc10bW20gG1s0bVtVUkwuLi5dG1syNG0bW20NChtbODsxSBtbbQ0KG1s5
OzFIG1sxbURFU0NSSVBUSU9OG1ttG1ttDQobWzEwOzFIICAgICAgIBtbMW1jdXJsG1ttICBpcyAg
YSB0b29sIHRvIHRyYW5zZmVyIGRhdGEgZnJvbSBvciB0byBhIHNlcnZlciwgdXNpbmcgb25lIG9m
IHRoZRtbbQ0KG1sxMTsxSCAgICAgICBzdXBwb3J0ZWQgcHJvdG9jb2xzIChESUNULCBGSUxFLCBG
VFAsIEZUUFMsIEdPUEhFUiwgSFRUUCwgSFRUUFMsICBJTUFQLBtbbQ0KG1sxMjsxSCAgICAgICBJ
TUFQUywgIExEQVAsICBMREFQUywgIFBPUDMsIFBPUDNTLCBSVE1QLCBSVFNQLCBTQ1AsIFNGVFAs
IFNNVFAsIFNNVFBTLBtbbQ0KG1sxMzsxSCAgICAgICBURUxORVQgYW5kIFRGVFApLiAgVGhlIGNv
bW1hbmQgaXMgZGVzaWduZWQgdG8gd29yayB3aXRob3V0IHVzZXIgIGludGVyLRtbbQ0KG1sxNDsx
SCAgICAgICBhY3Rpb24uG1ttDQobWzE1OzFIG1ttDQobWzE2OzFIICAgICAgIGN1cmwgb2ZmZXJz
IGEgYnVzbG9hZCBvZiB1c2VmdWwgdHJpY2tzIGxpa2UgcHJveHkgc3VwcG9ydCwgdXNlciBhdXRo
ZW4tG1ttDQobWzE3OzFIICAgICAgIHRpY2F0aW9uLCBGVFAgdXBsb2FkLCBIVFRQIHBvc3QsIFNT
TCBjb25uZWN0aW9ucywgY29va2llcywgZmlsZSAgdHJhbnMtG1ttDQobWzE4OzFIICAgICAgIGZl
ciByZXN1bWUgYW5kIG1vcmUuIEFzIHlvdSB3aWxsIHNlZSBiZWxvdywgdGhlIG51bWJlciBvZiBm
ZWF0dXJlcyB3aWxsG1ttDQobWzE5OzFIICAgICAgIG1ha2UgeW91ciBoZWFkIHNwaW4hG1ttDQob
WzIwOzFIG1ttDQobWzIxOzFIICAgICAgIGN1cmwgaXMgcG93ZXJlZCBieSAgbGliY3VybCAgZm9y
ICBhbGwgIHRyYW5zZmVyLXJlbGF0ZWQgIGZlYXR1cmVzLiAgU2VlG1ttDQobWzIyOzFIICAgICAg
IBtbMW1saWJjdXJsG1ttKDMpIGZvciBkZXRhaWxzLhtbbQ0KG1syMzsxSBtbbQ0KG1syNDsxSBtb
MW1VUkwbW20bW20NChtbMjU7MUggICAgICAgVGhlICBVUkwgIHN5bnRheCBpcyBwcm90b2NvbC1k
ZXBlbmRlbnQuIFlvdSdsbCBmaW5kIGEgZGV0YWlsZWQgZGVzY3JpcC0bW20NChtbMjY7MUggICAg
ICAgdGlvbiBpbiBSRkMgMzk4Ni4bW20NChtbMjc7MUgbW20NChtbMjg7MUggICAgICAgWW91IGNh
biBzcGVjaWZ5IG11bHRpcGxlIFVSTHMgb3IgcGFydHMgb2YgVVJMcyAgYnkgIHdyaXRpbmcgIHBh
cnQgIHNldHMbW20NChtbMjk7MUggICAgICAgd2l0aGluIGJyYWNlcyBhcyBpbjobW20NChtbMzA7
MUgbW20NChtbMzE7MUggICAgICAgIGh0dHA6Ly9zaXRlLntvbmUsdHdvLHRocmVlfS5jb20bW20N
ChtbMzI7MUgbW20NChtbMzM7MUggICAgICAgb3IgeW91IGNhbiBnZXQgc2VxdWVuY2VzIG9mIGFs
cGhhbnVtZXJpYyBzZXJpZXMgYnkgdXNpbmcgW10gYXMgaW46G1ttDQobWzM0OzFIG1ttDQobWzM1
OzFIICAgICAgICBmdHA6Ly9mdHAubnVtZXJpY2Fscy5jb20vZmlsZVsxLTEwMF0udHh0G1ttDQob
WzM2OzFIICAgICAgICBmdHA6Ly9mdHAubnVtZXJpY2Fscy5jb20vZmlsZVswMDEtMTAwXS50eHQg
ICAgKHdpdGggbGVhZGluZyB6ZXJvcykbW20NChtbMzc7MUggICAgICAgIGZ0cDovL2Z0cC5sZXR0
ZXJzLmNvbS9maWxlW2Etel0udHh0G1ttDQobWzM4OzFIG1ttDQobWzM5OzFIICAgICAgIE5lc3Rl
ZCAgc2VxdWVuY2VzICBhcmUgbm90IHN1cHBvcnRlZCwgYnV0IHlvdSBjYW4gdXNlIHNldmVyYWwg
b25lcyBuZXh0G1ttDQobWzQwOzFIICAgICAgIHRvIGVhY2ggb3RoZXI6G1ttDQobWzQxOzFIG1tt
DQobWzQyOzFIICAgICAgICBodHRwOi8vYW55Lm9yZy9hcmNoaXZlWzE5OTYtMTk5OV0vdm9sWzEt
NF0vcGFydHthLGIsY30uaHRtbBtbbQ0KG1s0MzsxSBtbbQ0KG1s0NDsxSCAgICAgICBZb3UgY2Fu
IHNwZWNpZnkgYW55IGFtb3VudCBvZiBVUkxzIG9uIHRoZSBjb21tYW5kIGxpbmUuICBUaGV5ICB3
aWxsICBiZRtbbQ0KG1s0NTsxSCAgICAgICBmZXRjaGVkIGluIGEgc2VxdWVudGlhbCBtYW5uZXIg
aW4gdGhlIHNwZWNpZmllZCBvcmRlci4bW20NChtbNDY7MUgbW20NChtbNDc7MUggICAgICAgWW91
ICBjYW4gIHNwZWNpZnkgYSBzdGVwIGNvdW50ZXIgZm9yIHRoZSByYW5nZXMgdG8gZ2V0IGV2ZXJ5
IE50aCBudW1iZXIbW20NChtbNDg7MUggICAgICAgb3IgbGV0dGVyOhtbbQ0KG1s0OTsxSBtbbQ0K
G1s1MDsxSCAgICAgICAgaHR0cDovL3d3dy5udW1lcmljYWxzLmNvbS9maWxlWzEtMTAwOjEwXS50
eHQbW20NChtbNTE7MUggICAgICAgIGh0dHA6Ly93d3cubGV0dGVycy5jb20vZmlsZVthLXo6Ml0u
dHh0G1ttDQobWzUyOzFIG1ttDQobWzUzOzFIICAgICAgIElmIHlvdSBzcGVjaWZ5IFVSTCB3aXRo
b3V0IHByb3RvY29sOi8vIHByZWZpeCwgIGN1cmwgIHdpbGwgIGF0dGVtcHQgIHRvG1ttDQobWzU0
OzFIICAgICAgIGd1ZXNzICB3aGF0ICBwcm90b2NvbCAgeW91IG1pZ2h0IHdhbnQuIEl0IHdpbGwg
dGhlbiBkZWZhdWx0IHRvIEhUVFAgYnV0G1ttDQobWzU1OzFIICAgICAgIHRyeSBvdGhlciBwcm90
b2NvbHMgYmFzZWQgb24gb2Z0ZW4tdXNlZCBob3N0IG5hbWUgcHJlZml4ZXMuICBGb3IgIGV4YW0t
G1ttDQobWzU2OzFIICAgICAgIHBsZSwgIGZvciAgaG9zdCBuYW1lcyBzdGFydGluZyB3aXRoICJm
dHAuIiBjdXJsIHdpbGwgYXNzdW1lIHlvdSB3YW50IHRvG1ttDQobWzU3OzFIICAgICAgIHNwZWFr
IEZUUC4bW20NChtbNTg7MUgbW20NChtbNTk7MUggICAgICAgY3VybCB3aWxsIGRvIGl0cyBiZXN0
IHRvIHVzZSB3aGF0IHlvdSBwYXNzIHRvIGl0IGFzIGEgVVJMLiAgSXQgIGlzICBub3QbW20NChtb
NjA7MUgbWzE7MUhjdXJsKDEpICAgICAgICAgICAgICAgICAgICAgICAgICAgQ3VybCBNYW51YWwg
ICAgICAgICAgICAgICAgICAgICAgICAgIGN1cmwoMSkbW20NChtbMjsxSBtbbQ0KG1szOzFIG1sx
bU5BTUUbW20bW20NChtbNDsxSCAgICAgICBjdXJsIC0gdHJhbnNmZXIgYSBVUkwbW20NChtbNTsx
SBtbbQ0KG1s2OzFIG1sxbVNZTk9QU0lTG1ttG1ttDQobWzc7MUggICAgICAgG1sxbWN1cmwbW20g
G1sxbVtvcHRpb25zXRtbbSAbWzRtW1VSTC4uLl0bWzI0bRtbbQ0KG1s4OzFIG1ttDQobWzk7MUgb
WzFtREVTQ1JJUFRJT04bW20bW20NChtbMTA7MUggICAgICAgG1sxbWN1cmwbW20gIGlzICBhIHRv
b2wgdG8gdHJhbnNmZXIgZGF0YSBmcm9tIG9yIHRvIGEgc2VydmVyLCB1c2luZyBvbmUgb2YgdGhl
G1ttDQobWzExOzFIICAgICAgIHN1cHBvcnRlZCBwcm90b2NvbHMgKERJQ1QsIEZJTEUsIEZUUCwg
RlRQUywgR09QSEVSLCBIVFRQLCAbWzdtSFRUUFMbWzI3bSwgIElNQVAsG1ttDQobWzEyOzFIICAg
ICAgIElNQVBTLCAgTERBUCwgIExEQVBTLCAgUE9QMywgUE9QM1MsIFJUTVAsIFJUU1AsIFNDUCwg
U0ZUUCwgU01UUCwgU01UUFMsG1ttDQobWzEzOzFIICAgICAgIFRFTE5FVCBhbmQgVEZUUCkuICBU
aGUgY29tbWFuZCBpcyBkZXNpZ25lZCB0byB3b3JrIHdpdGhvdXQgdXNlciAgaW50ZXItG1ttDQob
WzE0OzFIICAgICAgIGFjdGlvbi4bW20NChtbMTU7MUgbW20NChtbMTY7MUggICAgICAgY3VybCBv
ZmZlcnMgYSBidXNsb2FkIG9mIHVzZWZ1bCB0cmlja3MgbGlrZSBwcm94eSBzdXBwb3J0LCB1c2Vy
IGF1dGhlbi0bW20NChtbMTc7MUggICAgICAgdGljYXRpb24sIEZUUCB1cGxvYWQsIEhUVFAgcG9z
dCwgU1NMIGNvbm5lY3Rpb25zLCBjb29raWVzLCBmaWxlICB0cmFucy0bW20NChtbMTg7MUggICAg
ICAgZmVyIHJlc3VtZSBhbmQgbW9yZS4gQXMgeW91IHdpbGwgc2VlIGJlbG93LCB0aGUgbnVtYmVy
IG9mIGZlYXR1cmVzIHdpbGwbW20NChtbMTk7MUggICAgICAgbWFrZSB5b3VyIGhlYWQgc3BpbiEb
W20NChtbMjA7MUgbW20NChtbMjE7MUggICAgICAgY3VybCBpcyBwb3dlcmVkIGJ5ICBsaWJjdXJs
ICBmb3IgIGFsbCAgdHJhbnNmZXItcmVsYXRlZCAgZmVhdHVyZXMuICBTZWUbW20NChtbMjI7MUgg
ICAgICAgG1sxbWxpYmN1cmwbW20oMykgZm9yIGRldGFpbHMuG1ttDQobWzIzOzFIG1ttDQobWzI0
OzFIG1sxbVVSTBtbbRtbbQ0KG1syNTsxSCAgICAgICBUaGUgIFVSTCAgc3ludGF4IGlzIHByb3Rv
Y29sLWRlcGVuZGVudC4gWW91J2xsIGZpbmQgYSBkZXRhaWxlZCBkZXNjcmlwLRtbbQ0KG1syNjsx
SCAgICAgICB0aW9uIGluIFJGQyAzOTg2LhtbbQ0KG1syNzsxSBtbbQ0KG1syODsxSCAgICAgICBZ
b3UgY2FuIHNwZWNpZnkgbXVsdGlwbGUgVVJMcyBvciBwYXJ0cyBvZiBVUkxzICBieSAgd3JpdGlu
ZyAgcGFydCAgc2V0cxtbbQ0KG1syOTsxSCAgICAgICB3aXRoaW4gYnJhY2VzIGFzIGluOhtbbQ0K
G1szMDsxSBtbbQ0KG1szMTsxSCAgICAgICAgaHR0cDovL3NpdGUue29uZSx0d28sdGhyZWV9LmNv
bRtbbQ0KG1szMjsxSBtbbQ0KG1szMzsxSCAgICAgICBvciB5b3UgY2FuIGdldCBzZXF1ZW5jZXMg
b2YgYWxwaGFudW1lcmljIHNlcmllcyBieSB1c2luZyBbXSBhcyBpbjobW20NChtbMzQ7MUgbW20N
ChtbMzU7MUggICAgICAgIGZ0cDovL2Z0cC5udW1lcmljYWxzLmNvbS9maWxlWzEtMTAwXS50eHQb
W20NChtbMzY7MUggICAgICAgIGZ0cDovL2Z0cC5udW1lcmljYWxzLmNvbS9maWxlWzAwMS0xMDBd
LnR4dCAgICAod2l0aCBsZWFkaW5nIHplcm9zKRtbbQ0KG1szNzsxSCAgICAgICAgZnRwOi8vZnRw
LmxldHRlcnMuY29tL2ZpbGVbYS16XS50eHQbW20NChtbMzg7MUgbW20NChtbMzk7MUggICAgICAg
TmVzdGVkICBzZXF1ZW5jZXMgIGFyZSBub3Qgc3VwcG9ydGVkLCBidXQgeW91IGNhbiB1c2Ugc2V2
ZXJhbCBvbmVzIG5leHQbW20NChtbNDA7MUggICAgICAgdG8gZWFjaCBvdGhlcjobW20NChtbNDE7
MUgbW20NChtbNDI7MUggICAgICAgIGh0dHA6Ly9hbnkub3JnL2FyY2hpdmVbMTk5Ni0xOTk5XS92
b2xbMS00XS9wYXJ0e2EsYixjfS5odG1sG1ttDQobWzQzOzFIG1ttDQobWzQ0OzFIICAgICAgIFlv
dSBjYW4gc3BlY2lmeSBhbnkgYW1vdW50IG9mIFVSTHMgb24gdGhlIGNvbW1hbmQgbGluZS4gIFRo
ZXkgIHdpbGwgIGJlG1ttDQobWzQ1OzFIICAgICAgIGZldGNoZWQgaW4gYSBzZXF1ZW50aWFsIG1h
bm5lciBpbiB0aGUgc3BlY2lmaWVkIG9yZGVyLhtbbQ0KG1s0NjsxSBtbbQ0KG1s0NzsxSCAgICAg
ICBZb3UgIGNhbiAgc3BlY2lmeSBhIHN0ZXAgY291bnRlciBmb3IgdGhlIHJhbmdlcyB0byBnZXQg
ZXZlcnkgTnRoIG51bWJlchtbbQ0KG1s0ODsxSCAgICAgICBvciBsZXR0ZXI6G1ttDQobWzQ5OzFI
G1ttDQobWzUwOzFIICAgICAgICBodHRwOi8vd3d3Lm51bWVyaWNhbHMuY29tL2ZpbGVbMS0xMDA6
MTBdLnR4dBtbbQ0KG1s1MTsxSCAgICAgICAgaHR0cDovL3d3dy5sZXR0ZXJzLmNvbS9maWxlW2Et
ejoyXS50eHQbW20NChtbNTI7MUgbW20NChtbNTM7MUggICAgICAgSWYgeW91IHNwZWNpZnkgVVJM
IHdpdGhvdXQgcHJvdG9jb2w6Ly8gcHJlZml4LCAgY3VybCAgd2lsbCAgYXR0ZW1wdCAgdG8bW20N
ChtbNTQ7MUggICAgICAgZ3Vlc3MgIHdoYXQgIHByb3RvY29sICB5b3UgbWlnaHQgd2FudC4gSXQg
d2lsbCB0aGVuIGRlZmF1bHQgdG8gSFRUUCBidXQbW20NChtbNTU7MUggICAgICAgdHJ5IG90aGVy
IHByb3RvY29scyBiYXNlZCBvbiBvZnRlbi11c2VkIGhvc3QgbmFtZSBwcmVmaXhlcy4gIEZvciAg
ZXhhbS0bW20NChtbNTY7MUggICAgICAgcGxlLCAgZm9yICBob3N0IG5hbWVzIHN0YXJ0aW5nIHdp
dGggImZ0cC4iIGN1cmwgd2lsbCBhc3N1bWUgeW91IHdhbnQgdG8bW20NChtbNTc7MUggICAgICAg
c3BlYWsgRlRQLhtbbQ0KG1s1ODsxSBtbbQ0KG1s1OTsxSCAgICAgICBjdXJsIHdpbGwgZG8gaXRz
IGJlc3QgdG8gdXNlIHdoYXQgeW91IHBhc3MgdG8gaXQgYXMgYSBVUkwuICBJdCAgaXMgIG5vdBtb
bQ0KG1s2MDsxSCAgICAgICB0cnlpbmcgIHRvICB2YWxpZGF0ZSBpdCBhcyBhIHN5bnRhY3RpY2Fs
bHkgY29ycmVjdCBVUkwgYnkgYW55IG1lYW5zIGJ1dBtbbQ0KICAgICAgIGlzIGluc3RlYWQgG1sx
bXZlcnkbW20gbGliZXJhbCB3aXRoIHdoYXQgaXQgYWNjZXB0cy4bW20NChtbbQ0KICAgICAgIEN1
cmwgd2lsbCBhdHRlbXB0IHRvIHJlLXVzZSBjb25uZWN0aW9ucyBmb3IgbXVsdGlwbGUgZmlsZSB0
cmFuc2ZlcnMsIHNvG1ttDQogICAgICAgdGhhdCAgZ2V0dGluZyBtYW55IGZpbGVzIGZyb20gdGhl
IHNhbWUgc2VydmVyIHdpbGwgbm90IGRvIG11bHRpcGxlIGNvbi0bW20NCiAgICAgICBuZWN0cyAv
IGhhbmRzaGFrZXMuIFRoaXMgaW1wcm92ZXMgc3BlZWQuIE9mIGNvdXJzZSB0aGlzIGlzIG9ubHkg
ZG9uZSBvbhtbbQ0KICAgICAgIGZpbGVzICBzcGVjaWZpZWQgIG9uICBhICBzaW5nbGUgY29tbWFu
ZCBsaW5lIGFuZCBjYW5ub3QgYmUgdXNlZCBiZXR3ZWVuG1ttDQogICAgICAgc2VwYXJhdGUgY3Vy
bCBpbnZva2VzLhtbbQ0KG1ttDQobWzFtUFJPR1JFU1MbW20gG1sxbU1FVEVSG1ttG1ttDQobWzdt
IE1hbnVhbCBwYWdlIGN1cmwoMSkgbGluZSAxMyAocHJlc3MgaCBmb3IgaGVscCBvciBxIHRvIHF1
aXQpG1syN20bW0s=
EOF
sleep 999
" &

sleep 2
xterm -geometry 80x40+200+200 -e "sleep 999"

# $Id: fdbug49786.sh 60980 2013-06-07 10:32:32Z vinc17/ypig $
