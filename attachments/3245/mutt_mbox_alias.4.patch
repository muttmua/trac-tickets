diff -r d213f3acdb68 doc/manual.xml.head
--- a/doc/manual.xml.head	Mon Jun 15 14:24:27 2009 -0700
+++ b/doc/manual.xml.head	Wed Jun 17 22:28:53 2009 -0400
@@ -4355,6 +4355,34 @@
 
 </sect1>
 
+<sect1 id="mbalias">
+<title>Mailbox Aliases</title>
+
+<para>Usage:</para>
+
+<cmdsynopsis>
+<command>mbalias</command>
+<arg choice="plain">
+<replaceable class="parameter">name</replaceable>
+</arg>
+<arg choice="plain">
+<replaceable class="parameter">path</replaceable>
+</arg>
+</cmdsynopsis>
+
+<para>
+This command will let you define aliases for your mailboxes. This is 
+typically used with IMAP or POP mailboxes with longer path/URLs but it
+can be used for any other path. Within Mutt you can use the '*' prefix
+to use and access your alias like any other path. Here is an example with
+<command>mailboxes</command>:
+<screen>
+mbalias work imaps://user@imap.bigcompany.com/projects/x/inbox
+mailboxes *work</screen>
+</para>
+
+</sect1>
+
 </chapter>
 
 <chapter id="advancedusage">
@@ -5361,6 +5389,12 @@
 @<emphasis>alias</emphasis> &mdash; refers to the <link linkend="save-hook">default save folder</link> as determined by the address of the alias
 </para>
 </listitem>
+<listitem>
+
+<para>
+*<emphasis>alias</emphasis> &mdash; refers to a <link linkend="mbalias">Mailbox alias</link>
+</para>
+</listitem>
 
 </itemizedlist>
 
diff -r d213f3acdb68 doc/muttrc.man.head
--- a/doc/muttrc.man.head	Mon Jun 15 14:24:27 2009 -0700
+++ b/doc/muttrc.man.head	Wed Jun 17 22:28:53 2009 -0400
@@ -272,8 +272,20 @@
 and subscribed mailing lists.  The \fBunsubscribe\fP command removes
 it from the list of subscribed mailing lists. The \fb-group\fP flag
 adds all of the subsequent regular expressions to the named group.
-.TP
+.PP
+.nf
+\fBmbalias\fP \fIname\fP \fIpath\fP
+.fi
+.IP
+This command lets you define aliases for your mailboxes. It is used
+mostly to shorten very long path and URLs such as IMAP or POP accounts but
+can also be used with any other prefix or path. You can use it within mutt
+with the '*' prefix.
+.PP
+.nf
 \fBmbox-hook\fP [\fB!\fP]\fIpattern\fP \fImailbox\fP
+.fi
+.IP
 When mutt changes to a mail folder which matches \fIpattern\fP,
 \fImailbox\fP will be used as the \(lqmbox\(rq folder, i.e., read
 messages will be moved to that folder when the mail folder is left.
diff -r d213f3acdb68 globals.h
--- a/globals.h	Mon Jun 15 14:24:27 2009 -0700
+++ b/globals.h	Wed Jun 17 22:28:53 2009 -0400
@@ -220,6 +220,7 @@
 
 WHERE ALIAS *Aliases INITVAL (0);
 WHERE LIST *UserHeader INITVAL (0);
+WHERE MBALIAS *MbAliases INITVAL (0);
 
 /*-- formerly in pgp.h --*/
 WHERE REGEXP PgpGoodSign;
diff -r d213f3acdb68 init.c
--- a/init.c	Mon Jun 15 14:24:27 2009 -0700
+++ b/init.c	Wed Jun 17 22:28:53 2009 -0400
@@ -1388,6 +1388,79 @@
   return -1;
 }
 
+static int parse_mbalias (BUFFER *buf, BUFFER *s, unsigned long data, BUFFER *err)
+{
+  MBALIAS *tmp = MbAliases;
+  MBALIAS *last = NULL;
+  BUFFER name, path;
+  char exp_path[_POSIX_PATH_MAX];
+
+  memset (&name, 0, sizeof (name));
+  memset (&path, 0, sizeof (path));
+
+  if (!MoreArgs (s))
+  {
+    strfcpy (err->data, _("too few arguments"), err->dsize);
+    goto error;
+  }
+
+  mutt_extract_token (&name, s, 0);
+
+  if (!buf->data)
+  {
+    strfcpy (err->data, _("too few arguments"), err->dsize);
+    goto error;
+  }
+
+  mutt_extract_token (&path, s, 0);
+
+  if (MoreArgs (s))
+  {
+    strfcpy (err->data, _("too many arguments"), err->dsize);
+    goto error;
+  }
+
+  /* keep the expanded path with the alias */
+  strfcpy (exp_path, path.data, sizeof (exp_path));
+  mutt_expand_path (exp_path, sizeof (exp_path));
+  FREE (&path.data);
+
+  /* check to see if an alias with this name already exists */
+  for (; tmp; tmp = tmp->next)
+  {
+    if (!mutt_strcasecmp (tmp->name, name.data))
+      break;
+    last = tmp;
+  }
+
+  if (!tmp)
+  {
+    /* create a new alias */
+    tmp = (MBALIAS *) safe_calloc (1, sizeof (MBALIAS));
+    tmp->name = safe_strdup (name.data);
+    FREE (&name.data);
+    tmp->path = safe_strdup (exp_path);
+  }
+  else
+  {
+    /* override the previous value */
+    mutt_str_replace(&tmp->path, exp_path);
+  }
+
+
+  if (last)
+    last->next = tmp;
+  else
+    MbAliases = tmp;
+
+  return 0;
+
+error:
+  FREE (&name.data);
+  FREE (&path.data);
+  return (-1);
+}
+
 static int
 parse_unmy_hdr (BUFFER *buf, BUFFER *s, unsigned long data, BUFFER *err)
 {
diff -r d213f3acdb68 init.h
--- a/init.h	Mon Jun 15 14:24:27 2009 -0700
+++ b/init.h	Wed Jun 17 22:28:53 2009 -0400
@@ -3414,6 +3414,7 @@
 static int parse_unlists (BUFFER *, BUFFER *, unsigned long, BUFFER *);
 static int parse_alias (BUFFER *, BUFFER *, unsigned long, BUFFER *);
 static int parse_unalias (BUFFER *, BUFFER *, unsigned long, BUFFER *);
+static int parse_mbalias (BUFFER *, BUFFER *, unsigned long, BUFFER *);
 static int parse_ignore (BUFFER *, BUFFER *, unsigned long, BUFFER *);
 static int parse_unignore (BUFFER *, BUFFER *, unsigned long, BUFFER *);
 static int parse_source (BUFFER *, BUFFER *, unsigned long, BUFFER *);
@@ -3472,6 +3473,7 @@
   { "macro",		mutt_parse_macro,	0 },
   { "mailboxes",	mutt_parse_mailboxes,	M_MAILBOXES },
   { "unmailboxes",	mutt_parse_mailboxes,	M_UNMAILBOXES },
+  { "mbalias",		parse_mbalias,		0 },
   { "message-hook",	mutt_parse_hook,	M_MESSAGEHOOK },
   { "mbox-hook",	mutt_parse_hook,	M_MBOXHOOK },
   { "mime_lookup",	parse_list,	UL &MimeLookupList },
diff -r d213f3acdb68 mutt.h
--- a/mutt.h	Mon Jun 15 14:24:27 2009 -0700
+++ b/mutt.h	Wed Jun 17 22:28:53 2009 -0400
@@ -568,6 +568,13 @@
   short num;
 } ALIAS;
 
+typedef struct mbalias
+{
+  char *name;
+  char *path;
+  struct mbalias *next;
+} MBALIAS;
+
 typedef struct envelope
 {
   ADDRESS *return_path;
diff -r d213f3acdb68 muttlib.c
--- a/muttlib.c	Mon Jun 15 14:24:27 2009 -0700
+++ b/muttlib.c	Wed Jun 17 22:28:53 2009 -0400
@@ -444,6 +444,22 @@
 	}
       }
       break;
+
+      case '*':
+      {
+	MBALIAS *alias = MbAliases;
+
+	tail = s + 1;
+	for (; alias; alias = alias->next)
+	{
+	  if (!mutt_strcasecmp (alias->name, s + 1)) {
+	    strfcpy (p, alias->path, sizeof (p));
+	    tail = "";
+	    break;
+	  }
+	}
+      }
+      break;
       
       case '>':
       {
