#include "config.h"
#include "mutt.h"
#include <time.h>
#include <stdio.h>

/* 
 * calculate_local_gmt_offset() - gives your current timezone's offset from GMT,
 * in seconds, as a time_t.
 * 
 * x is the struct tm for the time we want to check, or NULL for now.
 * 
 * Note that mktime() returns the same time_t whether DST is in effect or not,
 * and then localtime() takes your configured timezone's DST value into
 * consideration when it calculates the corresponding struct tm.  So, to get the
 * difference between your local TZ and GMT, we need to pretend DST isn't in
 * effect when it is. 
 */
   

int main(int argc, char **argv)
{
    time_t t;
    struct tm local;
    struct tm *tmp;

    t = time(NULL);
    printf("time (from time): %ld\n", t);
    printf("time is %s\n", ctime(&t));

    /* now */
    tmp = localtime(&t);
    local = *tmp;
    t = mutt_mktime(&local, 0);
    printf("asctime(not local): %s", ctime(&t));
    t = mutt_mktime(&local, 1);
    printf("asctime(local): %s", ctime(&t));

    /* this offset has no DST, should be -5h */
    local = (struct tm){ .tm_sec = 0, .tm_min = 0, .tm_mday = 25, .tm_mon = 11, 
              .tm_year = 70, .tm_wday = 0, .tm_yday = 0, .tm_isdst = -1 };
    printf("No DST, in Boston should show 5hr difference.\n");
    t = mutt_mktime(&local, 0);
    printf("asctime(not local): %s", ctime(&t));
    t = mutt_mktime(&local, 1);
    printf("asctime(local): %s", ctime(&t));

    /* This has DST, should be -4h */
    printf("DST, in Boston should show 4hr difference.\n");
    local = (struct tm){ .tm_sec = 0, .tm_min = 0, .tm_mday = 25, .tm_mon = 6, 
              .tm_year = 106, .tm_wday = 0, .tm_yday = 0, .tm_isdst = -1 };
    t = mutt_mktime(&local, 0);
    printf("asctime(not local): %s", ctime(&t));
    t = mutt_mktime(&local, 1);
    printf("asctime(local): %s", ctime(&t));
 
    /* Crosses DST */
    printf("Crosses DST, should gain an hour (3hr difference in Boston)\n");
    local = (struct tm){ .tm_sec = 0, .tm_min = 0, .tm_hour = 4, .tm_mday = 6, 
              .tm_mon = 10, .tm_year = 116, .tm_wday = 0, .tm_yday = 0, 
              .tm_isdst = -1 };
    t = mutt_mktime(&local, 0);
    printf("asctime(not local): %s", ctime(&t));
    t = mutt_mktime(&local, 1);
    printf("asctime(local): %s", ctime(&t));
 
    /* Crosses DST in other direction */
    printf("Crosses DST, should lose an hour (6hr difference in Boston)\n");
    local = (struct tm){ .tm_sec = 0, .tm_min = 0, .tm_hour = 4, .tm_mday = 13, 
              .tm_mon = 2, .tm_year = 116, .tm_wday = 0, .tm_yday = 0, 
              .tm_isdst = -1 };
    t = mutt_mktime(&local, 0);
    printf("asctime(not local): %s", ctime(&t));
    t = mutt_mktime(&local, 1);
    printf("asctime(local): %s", ctime(&t));
 
    return 0;
}


