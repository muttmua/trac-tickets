diff -r 626cd5190109 init.c
--- a/init.c	Wed May 01 23:24:11 2002 +0000
+++ b/init.c	Wed Jun 07 00:49:07 2006 +0100
@@ -2664,14 +2664,17 @@ int mutt_dump_variables (void)
   
   for (i = 0; MuttVars[i].option; i++)
   {
-    snprintf (command, sizeof (command), "set ?%s\n", MuttVars[i].option);
-    if (mutt_parse_rc_line (command, &token, &err) == -1)
-    {
-      fprintf (stderr, "%s\n", err.data);
-      FREE (&token.data);
-      return 1;
-    }
-    printf("%s\n", err.data);
+    if (MuttVars[i].type != DT_SYN)
+    {
+        snprintf (command, sizeof (command), "set ?%s\n", MuttVars[i].option);
+	if (mutt_parse_rc_line (command, &token, &err) == -1)
+	{
+		fprintf (stderr, "%s\n", err.data);
+		FREE (&token.data);
+		return 1;
+	}
+	printf("%s\n", err.data);
+    }
   }
   
   FREE (&token.data);
