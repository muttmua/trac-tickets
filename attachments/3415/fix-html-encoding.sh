#!/bin/mksh
# from the file given on command line argument $1, which is a html file,
# find the encoding meta header and use iconv to convert the file from
# terminal encoding to the specified encoding in situ.
# the encoding of the email is given as $2 if possible
# Author: 05/2010 Clemens Helfmeier

# find encoding in html source (not very good code but works most of the time)
to_encoding=`sed -r "s/^.*charset=([0-9a-zA-Z-]*).*$/\1/p; d;" $1 | sed -r "1 !d;"`
# in case, the html does not contain any charset encoding information use the mail encoding by default
[ -z "$to_encoding" ] && echo "Using mail charset ..." && to_encoding="$2"
# make the encoding uppercase and readable
to_encoding=`echo "$to_encoding" | sed -r "y/abcdefghijklmnopqrstuvwxyz_/ABCDEFGHIJKLMNOPQRSTUVWXYZ-/"`
# check if our iconv suports this encoding
if /mnt/disc0_1/bin/iconv -l | sed -r 'y/abcdefghijklmnopqrstuvwxyz_/ABCDEFGHIJKLMNOPQRSTUVWXYZ-/; s/\\/\\\\/g; s/\*/\\*/g; s/\./\\./g;' | grep -- "$to_encoding" 2>/dev/null 1>&2; then
  # for any encoding, add the iconv -t flag
  if [[ "$to_encoding" -ne "$2" ]]; then
    # mail and html encoding do not match, report but use the html encoding
    echo "Discrepancy of file and html encoding."
  fi;
  to_encoding="-t $to_encoding"
else
  # our iconv does not support this encoding
  echo "Unknown encoding, using mapping for $to_encoding."
  # try to map it somehow usable - needs to be customized, is only a fallback solution
  case "$to_encoding" in
    *1252) to_encoding="-t ISO-8859-1";;
       # for all unknown charsets, the default is ISO-8859-1
    *) to_encoding="-t ISO-8859-1";;
  esac;
fi;
# Report what encoding we use
echo "From $from_encoding to $to_encoding"
# and finally convert the file (in situ is not possible with iconv)!
cat $1 | /mnt/disc0_1/bin/iconv $from_encoding $to_encoding > $1.1
mv $1.1 $1

