Ticket:  1861
Status:  new
Summary: colors within $status_format not yet possible

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2004-04-19 11:04:38 UTC
Last Updated: 2007-04-10 05:42:50 UTC

Priority:  trivial
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.5.1-20040112+1
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#244428.
Please Cc all your replies to 244428@bugs.debian.org .]

From: Grant Bowman <grantbow@grantbow.com>
Subject: mutt: colors within $status_format not yet possible
Date: Sun, 18 Apr 2004 06:01:04 -0700

Hi Marco,

Mutt is very capable with colors for many tasks.  The capability to
color each item is very useful and I use it alot.  However there is one
highly visible UI string that could use embedded color codes:
$status_format.  I didn't see any patch, hint in the mutt source code or
description (googling) anywhere of how to get color (and perhaps bold
codes) embedded within the string.

Status lines are ideal places to use color to diferentiate and contrast
different data elements that are more or less important.  I'm on a roll
now, having spent quite a bit of time to synchronize the configurations
of irssi and vim to use colors (with similar semantic meanings) within
their status strings to great effect.

Keep up the good work,

-- 
-- Grant Bowman                                <grantbow@grantbow.com>


-- System Information:
Debian Release: testing/unstable
  APT prefers testing
  APT policy: (500, 'testing'), (50, 'unstable')
Architecture: i386 (i586)
Kernel: Linux 2.6.3-1-386
Locale: LANG=en_US.UTF-8, LC_CTYPE=en_US.UTF-8

Versions of packages mutt depends on:
ii  exim4-daemon-light [mail-tr 4.30-8       Lightweight version of the Exim (v
ii  libc6                       2.3.2.ds1-11 GNU C Library: Shared libraries an
ii  libidn11                    0.4.1-1      GNU libidn library, implementation
ii  libncursesw5                5.4-3        Shared libraries for terminal hand
ii  libsasl2                    2.1.15-6     Authentication abstraction library



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-27 11:45:09 UTC rado
* Added comment:
{{{
change-request

Didn't Glanzmann have some status-bar patch for this?
See http://WIKI.mutt.org/?PatchList for solutions.
}}}

--------------------------------------------------------------------------------
2007-04-10 05:42:50 UTC brendan
* component changed to display
* Updated description:
{{{
Package: mutt
Version: 1.5.5.1-20040112+1
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#244428.
Please Cc all your replies to 244428@bugs.debian.org .]

From: Grant Bowman <grantbow@grantbow.com>
Subject: mutt: colors within $status_format not yet possible
Date: Sun, 18 Apr 2004 06:01:04 -0700

Hi Marco,

Mutt is very capable with colors for many tasks.  The capability to
color each item is very useful and I use it alot.  However there is one
highly visible UI string that could use embedded color codes:
$status_format.  I didn't see any patch, hint in the mutt source code or
description (googling) anywhere of how to get color (and perhaps bold
codes) embedded within the string.

Status lines are ideal places to use color to diferentiate and contrast
different data elements that are more or less important.  I'm on a roll
now, having spent quite a bit of time to synchronize the configurations
of irssi and vim to use colors (with similar semantic meanings) within
their status strings to great effect.

Keep up the good work,

-- 
-- Grant Bowman                                <grantbow@grantbow.com>


-- System Information:
Debian Release: testing/unstable
  APT prefers testing
  APT policy: (500, 'testing'), (50, 'unstable')
Architecture: i386 (i586)
Kernel: Linux 2.6.3-1-386
Locale: LANG=en_US.UTF-8, LC_CTYPE=en_US.UTF-8

Versions of packages mutt depends on:
ii  exim4-daemon-light [mail-tr 4.30-8       Lightweight version of the Exim (v
ii  libc6                       2.3.2.ds1-11 GNU C Library: Shared libraries an
ii  libidn11                    0.4.1-1      GNU libidn library, implementation
ii  libncursesw5                5.4-3        Shared libraries for terminal hand
ii  libsasl2                    2.1.15-6     Authentication abstraction library



>How-To-Repeat:
>Fix:
}}}
