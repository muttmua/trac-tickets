Ticket:  3740
Status:  new
Summary: multi-byte characters not handled in query window

Reporter: mathstuf
Owner:    mutt-dev

Opened:       2015-02-25 06:14:16 UTC
Last Updated: 2015-02-25 06:14:16 UTC

Priority:  minor
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
When users in an address book used via query_command have multibyte characters or extra wide characters, the columns are not aligned. wcswidth should be used for alignment rather than strlen.
