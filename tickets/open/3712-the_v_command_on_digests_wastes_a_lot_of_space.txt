Ticket:  3712
Status:  new
Summary: the "v" command on digests wastes a lot of space

Reporter: jidanni
Owner:    mutt-dev

Opened:       2014-11-05 17:31:50 UTC
Last Updated: 2014-11-05 23:13:49 UTC

Priority:  trivial
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
I notice the "v" command on digests wastes a lot of space, cutting off
the subjects and then having a lot of blanks.
{{{
  I     1 "Shark Tooth" CIA Lock Picking Trick.                                   [message/rfc822, 7bit, 0.3K]
  I     2 <jidanni@jidanni.org> You could make the                                [message/rfc822, 7bit, 0.4K]
  I     3 RE: Credit-Update Changes November 5, 20                                [message/rfc822, 7bit, 0.4K]
  I     4 Pepto Bismol Linked to Cancer                                           [message/rfc822, 7bit, 0.3K]
  I     5 HARP Program May Drop Your Rate to All-T                                [message/rfc822, 7bit, 0.3K]
}}}
I note my .muttrc has
{{{
spam "X-Spam-Status: .*score=([-0-9.]+)" %1
set index_format="%H %4C %Z %{%b %d} %-15.15L (%?l?%4l&%4c?) %s"
set sort=spam
set realname=
set askcc
auto_view text/html
}}}

--------------------------------------------------------------------------------
2014-11-05 17:46:55 UTC dgc@bikeshed.us
* Added comment:
{{{
It looks like you're using the default $attach_format, which is designed
for displays narrower than yours seems to be.  Try this:

set attach_format="%u%D%I %t%4n %T%d%*   [%.7m/%.10M, %.6e%<C?, %C>, %s] "

The default, for contrast:
set attach_format="%u%D%I %t%4n %T%.40d%> [%.7m/%.10M, %.6e%<C?, %C>, %s] "


* On 05 Nov 2014, Mutt wrote: 
}}}

--------------------------------------------------------------------------------
2014-11-05 18:25:42 UTC jidanni
* Added comment:
{{{
OK that's much better. Gee can't the width be sensed via $COLUMNS or
some curses thing?
}}}

--------------------------------------------------------------------------------
2014-11-05 19:46:24 UTC dgc
* Added comment:
That's basically what "%* " and "%> " do.  The reason this wasn't full-width before is the "%.40d", which says "copy up to 40 characters from the message subject into a 40-column span of text". We can't rewrite length specs in user-set format strings according to the actual minus the expected screen width. Instead we let the fill expansions do that for us. %>c fills to the right with character c until the right end of the text abuts the right edge of the screen, but lets that text fall off the edge if it's too long.  %*c fills to the left, stomping over anything it must to make the text to the right fit. So the reason why this format works is that it doesn't impose limits on anything's width, but instead lets the padding operation scratch out whatever there's not room for.

The RFE here would be that the default format change to (something like) what I posted.  I'd be +0 on that: not a big deal to me but it makes sense to do.

--------------------------------------------------------------------------------
2014-11-05 23:13:49 UTC jidanni
* Added comment:
{{{
OK glad I found the bug!
}}}
