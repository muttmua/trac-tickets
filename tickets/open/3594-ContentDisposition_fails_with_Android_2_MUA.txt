Ticket:  3594
Status:  accepted
Summary: Content-Disposition fails with Android 2.* MUA

Reporter: Ambrevar
Owner:    me

Opened:       2012-09-26 18:44:37 UTC
Last Updated: 2012-12-01 23:12:26 UTC

Priority:  minor
Component: mutt
Keywords:  android header content-disposition

--------------------------------------------------------------------------------
Description:
When trying to read mails composed with Mutt, users of Android 2.* default MUA will see nothing but an "Unknown.txt" attachment, which is indeed the body of the mail. The content is still readable, but it is confusing for Android users (and not very convenient).

The issue comes from Android misbehaving with the Content-Disposition line in the header of the mail, whatever the value is. Mutt is one of the few MUA that automatically adds this line to every mail header.

I quickly patched mutt_write_mime_header@sendmail.c to remove the line and it works. As far as I know, there is no way to toggle off this line directly from muttrc. Would be nice to add this feature.

--------------------------------------------------------------------------------
2012-12-01 23:12:26 UTC me
* owner changed to me
* priority changed to minor
* status changed to accepted
* type changed to enhancement
