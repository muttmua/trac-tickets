Ticket:  3019
Status:  new
Summary: Wish: auto-delete certain incoming attachments

Reporter: kscott
Owner:    mutt-dev

Opened:       2008-01-24 03:39:40 UTC
Last Updated: 2008-01-24 11:02:42 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I get a lot of emails that routinely include an image attachment as a signature (e.g. corporate logo that wastes 43K in every message).  The filenames of the images may vary (e.g. pic15146.jpg, every time it's a different number in the filename), but these files' contents always match something in a small set of images that I don't want to keep seeing/storing.

I'd like, either when I retrieve messages via POP, or when I view the message, for Mutt to automatically compare the attachments' checksums against a list of known values, and if an attachment happens to match something in my list, automatically delete that attachment, without any direct action needed on my part.

On a related note, as various people reply to a message that had attachments, the same attached files will often reappear in each reply.  I'd like for Mutt to recognize that some of the attached files are copes of other attachments that I've recently received in other messages, or that I've sent out.  In this case, I'd want Mutt to ask me before deleting the attachment from the newly-received message.

--------------------------------------------------------------------------------
2008-01-24 11:02:42 UTC Paul Walker
* Added comment:
{{{
On Thu, Jan 24, 2008 at 03:39:40AM -0000, Mutt wrote:

>  I'd like, either when I retrieve messages via POP, or when I view the
>  message, for Mutt to automatically compare the attachments' checksums

I really do think you're asking for mutt to do something that doesn't belong
in there. This is much more a job for procmail than mutt.
}}}
