Ticket:  3486
Status:  new
Summary: "Attach:" pseudo-header cannot handle filenames with newline

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2011-01-01 19:34:49 UTC
Last Updated: 2011-01-01 19:34:49 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/568832

{{{
The "Attach:" pseudo-header can now attach filenames with spaces in
them, through \-escaping. That's good, and works for spaces and tabs
(^I, \t). But it does not work for newlines (^J, \n).
}}}

I'm filling it as a defect because maybe it should also handle newlines as well, if you feel that this is an enhancement please feel free to reclassify!

Cheers
Antonio
