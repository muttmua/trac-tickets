Ticket:  3310
Status:  new
Summary: Overhaul new mail reporting

Reporter: brendan
Owner:    mutt-dev

Opened:       2009-07-30 04:46:04 UTC
Last Updated: 2015-11-23 21:14:30 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Support notification for both new and old mail (configurable) for all mailbox types. See NewMailHandling.

There are two separate issues: one is distinguishing between new mail and old mail, and the other is being able to tell whether a mailbox has changed since it was last opened, without regard to whether the mail in it is new or old.

For problem 2 (has the mailbox changed?) I think we'll need a function for each mailbox type that returns a type-specific cookie (e.g., for mbox  timestamp+size, for IMAP UIDVALIDITY+UIDNEXT, for maildir perhaps the number of files under new and the name of the most recent under cur). None of these should be more expensive to calculate than what the buffy check already does.

--------------------------------------------------------------------------------
2009-08-09 20:34:31 UTC brendan
* Updated description:
Support notification for both new and old mail (configurable) for all mailbox types. See NewMailHandling.

There are two separate issues: one is distinguishing between new mail and old mail, and the other is being able to tell whether a mailbox has changed since it was last opened, without regard to whether the mail in it is new or old.

For problem 2 (has the mailbox changed?) I think we'll need a function for each mailbox type that returns a type-specific cookie (e.g., for mbox  timestamp+size, for IMAP UIDVALIDITY+UIDNEXT, for maildir perhaps the number of files under new and the name of the most recent under cur). None of these should be more expensive to calculate than what the buffy check already does.

--------------------------------------------------------------------------------
2009-08-09 22:04:31 UTC pdmef
* Added comment:
Old mail detection is going to be really really expensive as that implies a full parse of the mailbox for mbox/mmdf and fully walking cur/new for maildir folders. So because of maildir I guess we do never ever want to make old mail detection defaulting to yes.

For new mail detection, I don't see why need to scan cur/, that'll be painfully slow. Whether a maildir/mh folder has changed should be determined by looking at the mtime of cur/new or the dir itself. Until somebody reports this doesn't reliably work, looking at actual files takes way too long. The current buffy check for maildir is very fast as it doesn't walk the dir but usually can finish at the first message already because it's likely neither old nor trashed nor read. In the average case I think this even gives constant time (+ fs time) no matter how many new messages there are. I think we should keep it that way.

* Updated description:
Support notification for both new and old mail (configurable) for all mailbox types. See NewMailHandling.

There are two separate issues: one is distinguishing between new mail and old mail, and the other is being able to tell whether a mailbox has changed since it was last opened, without regard to whether the mail in it is new or old.

For problem 2 (has the mailbox changed?) I think we'll need a function for each mailbox type that returns a type-specific cookie (e.g., for mbox  timestamp+size, for IMAP UIDVALIDITY+UIDNEXT, for maildir perhaps the number of files under new and the name of the most recent under cur). None of these should be more expensive to calculate than what the buffy check already does.

--------------------------------------------------------------------------------
2009-08-09 22:15:21 UTC brendan
* Added comment:
Old mail counting would need a caching layer, certainly. I'm starting with a simpler cookie which can simply distinguish whether new mail has arrived since the last cookie check. The maildir scheme I described above is probably overkill as you say, and will be pared down as necessary to keep performance on par with what it is now.

* Updated description:
Support notification for both new and old mail (configurable) for all mailbox types. See NewMailHandling.

There are two separate issues: one is distinguishing between new mail and old mail, and the other is being able to tell whether a mailbox has changed since it was last opened, without regard to whether the mail in it is new or old.

For problem 2 (has the mailbox changed?) I think we'll need a function for each mailbox type that returns a type-specific cookie (e.g., for mbox  timestamp+size, for IMAP UIDVALIDITY+UIDNEXT, for maildir perhaps the number of files under new and the name of the most recent under cur). None of these should be more expensive to calculate than what the buffy check already does.

--------------------------------------------------------------------------------
2010-06-24 12:54:45 UTC mehturt
* cc changed to mehturt@gmail.com

--------------------------------------------------------------------------------
2010-09-13 02:54:21 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [a51df78218e8]) add $mail_check_recent boolean to control whether Mutt will notify about all new mail, or just new mail received since the last visit to a mailbox

closes #3271

partly addresses #3310

--------------------------------------------------------------------------------
2014-02-03 22:44:10 UTC mnahkola
* Added comment:
Well, $mail_check_recent is broken anyway with Maildir folders and Mutt 1.5.21 (Debian/Ubuntu packaged). Especially when there are multiple applications that touch the Maildir setup. 

IMHO, at least with both $mail_check_recent=no and $mark_old=no, one message in "cur" without the S flag should be sufficient to count that folder having unread messages. This is not the case now. 


What this means in practice, is that mails scanned-in by Notmuch (packaged version, oldish) in a Maildir mailbox no longer count towards having unread mail there. This is clearly incorrect.

Also happens if you use both mutt directly and, say, something else via IMAP (Dovecot in my case) on the same base mailbox structure, just having an IMAP client access the account (not displaying any message or marking anything as read) is sufficient to make the mail to be shown with "O" and no longer count for having "unread" mail in that mailbox, for Mutt.


Mails in the Maildir "new" subdirectory are seen as new just fine, though, and of course it'll be slower to trawl through all of "cur" but can't be helped for correctness of operation.



Checking only the "new" subdirectory could possibly be appropriate with $mail_check_recent=yes and $mark_old=yes, although even so there could be some room for confusion.

--------------------------------------------------------------------------------
2014-02-03 23:06:22 UTC mnahkola
* Added comment:
Oh, seems to be broken the same way in 1.5.22 too.

--------------------------------------------------------------------------------
2015-11-23 21:14:30 UTC kevin8t8
* milestone changed to 2.0
