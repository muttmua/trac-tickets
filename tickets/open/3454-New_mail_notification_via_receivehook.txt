Ticket:  3454
Status:  new
Summary: New mail notification via receive-hook

Reporter: mehturt
Owner:    mutt-dev

Opened:       2010-09-22 08:39:23 UTC
Last Updated: 2010-11-22 15:21:28 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I wrote a patch that enables one to execute arbitrary system command upon new mail reception.  The command to execute is configurable.  I have tested it against 1.5.20 and 1.5.21.

Example of configuration in .muttrc:
receive-hook . "system notify-send 'New mail'"

--------------------------------------------------------------------------------
2010-09-22 08:39:46 UTC mehturt
* Added attachment mutt-1.5.21.patch
* Added comment:
Patch against 1.5.21

--------------------------------------------------------------------------------
2010-11-22 15:21:28 UTC william.crosmun@emc.com
* Added comment:
{{{
FYI - I installed this patch as written. It emulates the message hooks. Unfortunately, it causes a segmentation error if the pattern is anything other than . 

 

Eg:

 

Receive-hook ~p "system /usr/bin/xmessage 'New Mail'"

 

Will generate a segmentation error on New Mail. The call to mutt_message_hook in curs_main.c passes 0 as the second argument rather than a HEADER pointer. At the point in curs_main.c where mutt_message_hook is called, we don't have a HEADER pointer, we have a CONTEXT. 

 

I modified his patch to emulate a folder-hook rather than a message hook. Instead of calling mutt_message_hook in curs_main.c, I called mutt_folder_hook (Context -> path). This works for me as I have 6 copies of mutt open at the same time, each on a different folder. I only want to be notified on two of the folders, so my muttrc file contains

 

Receive-hook INBOX "system /usr/bin/xmessage 'New Mail in INBOX'"

Receive-hook Support "system /usr/bin/xmessage 'New Mail in Support"

 

I'm not sure what a general solution would look like, so I'm not including my code changes (though I can if there is any interest).

 

___________________________________________ 
William Crosmun 
BIRT

EMC²            
Where information lives 
62 T.W Alexander Drive 
Research Triangle Park, NC 27709 

Phone:  (919) 248-6302 
Tieline:       8-262-6302 
Cell:    (919) 201-5224
Fax:    (919) 248-6108  
Email:  crosmun_william@emc.com
}}}
