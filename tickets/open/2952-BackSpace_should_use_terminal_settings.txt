Ticket:  2952
Status:  new
Summary: <BackSpace> should use terminal settings

Reporter: memoryhole
Owner:    mutt-dev

Opened:       2007-09-07 17:24:40 UTC
Last Updated: 2007-09-13 17:49:46 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When <BackSpace> is used in a macro or key binding, the expected result is that pressing the backspace key will trigger that macro or key binding. However, since <BackSpace> is considered an alias for {{{^H}}} and not all terminals emit {{{^H}}} when the backspace key is pressed, this can lead to unexpected behavior. Mutt *should* attempt to detect what the BackSpace key is by fetching the "erase" key from current environment (the same way stty does).

--------------------------------------------------------------------------------
2007-09-10 07:45:39 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-09-07 17:24:40 -0000, Mutt wrote:
> #2952: <BackSpace> should use terminal settings
> 
>  When <BackSpace> is used in a macro or key binding, the expected result is
>  that pressing the backspace key will trigger that macro or key binding.
>  However, since <BackSpace> is considered an alias for {{{^H}}} and not all
>  terminals emit {{{^H}}} when the backspace key is pressed, this can lead
>  to unexpected behavior. Mutt *should* attempt to detect what the BackSpace
>  key is by fetching the "erase" key from current environment (the same way
>  stty does).

I can't reproduce this problem here. For instance, my terminal
generates ^? when I type the BackSpace key, and if I add

  bind editor <backspace> backward-char

to my .muttrc, then BackSpace does a backward-char as required
by the line above.
}}}

--------------------------------------------------------------------------------
2007-09-10 08:19:36 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-09-10 09:45:36 +0200, Vincent Lefevre wrote:
> I can't reproduce this problem here. For instance, my terminal
> generates ^? when I type the BackSpace key, and if I add
> 
>   bind editor <backspace> backward-char
> 
> to my .muttrc, then BackSpace does a backward-char as required
> by the line above.

I forgot to say: AFAIK, Mutt gets the string corresponding to the
BackSpace from the terminfo (via the curses). You should make sure
that these terminfo settings fit your terminal (unfortunately some
default configurations are buggy).
}}}

--------------------------------------------------------------------------------
2007-09-10 14:25:03 UTC Kyle Wheeler
* Added comment:
{{{
On Monday, September 10 at 08:19 AM, quoth Mutt:
> I forgot to say: AFAIK, Mutt gets the string corresponding to the 
> BackSpace from the terminfo (via the curses). You should make sure 
> that these terminfo settings fit your terminal (unfortunately some 
> default configurations are buggy). }}}

Ahh, I understand. So, in other words, mutt ignores what the terminal 
is *actually* emitting (which it could find out) and assumes that it 
has not been configured in a non-default way.

I mean, xterm (and many other fancier terminals) can have the key that 
they emit configured dynamically.

This still seems like a bug to me: mutt should be doing something more 
useful than asking terminfo what the default backspace character for 
the $TERM is. Besides the fact that it can be configured to be 
non-default, as you say, some of these terminal definitions are a 
little buggy. There is a more reliable way to do it, so mutt should 
use that (imho).

~Kyle
}}}

--------------------------------------------------------------------------------
2007-09-10 14:33:41 UTC Thomas Dickey
* Added comment:
{{{
On Mon, 10 Sep 2007, Kyle Wheeler wrote:

> This still seems like a bug to me: mutt should be doing something more useful 
> than asking terminfo what the default backspace character for the $TERM is. 
> Besides the fact that it can be configured to be non-default, as you say, 
> some of these terminal definitions are a little buggy. There is a more 
> reliable way to do it, so mutt should use that (imho).

But you don't really know if backspace is the same as erase...
}}}

--------------------------------------------------------------------------------
2007-09-10 14:54:14 UTC Kyle Wheeler
* Added attachment part0001.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
On Monday, September 10 at 02:33 PM, quoth Mutt:
>> This still seems like a bug to me: mutt should be doing something 
>> more useful than asking terminfo what the default backspace 
>> character for the $TERM is. Besides the fact that it can be 
>> configured to be non-default, as you say, some of these terminal 
>> definitions are a little buggy. There is a more reliable way to do 
>> it, so mutt should use that (imho).
>
> But you don't really know if backspace is the same as erase... 

Of course you don't know for certain---if there was a way to know for 
certain, then every application would use it. But consider for a 
moment what most (knowledgeable) folks would do if their terminal is 
emitting something as backspace that doesn't match the current erase 
character: either reconfigure their terminal to emit the expected 
erase character OR use stty to set what the erase character is.

Besides, the default erase character is pulled from terminfo just the 
same as mutt does. The problem is that with stty you can correct that 
for most applications if the default is wrong, and yet mutt will 
ignore whatever corrections the user makes.

But let's phrase this another way: what do you expect a user to do 
whose TERM entry is wrong? Edit the terminfo files? Figure out how to 
configure their terminal to match the broken terminfo files? Use stty 
to change the terminal settings to match the terminal? All of these 
are valid options, and *should* all work (and DO all work with most 
other terminal applications, such as vim, emacs, libreadline, etc.), 
but only the first two work with mutt. That seems broken to me.

~Kyle
}}}

--------------------------------------------------------------------------------
2007-09-11 07:37:38 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-09-10 09:24:34 -0500, Kyle Wheeler wrote:
> Ahh, I understand. So, in other words, mutt ignores what the
> terminal is *actually* emitting (which it could find out) and
> assumes that it has not been configured in a non-default way.

Conversely, you can't be sure that what the user (or something else)
has configured for erase is really the backspace key.

> I mean, xterm (and many other fancier terminals) can have the key
> that they emit configured dynamically.

xterm can change other keys dynamically. The only way to get all these
right is to change the value of $TERM.

> This still seems like a bug to me: mutt should be doing something
> more useful than asking terminfo what the default backspace
> character for the $TERM is. Besides the fact that it can be
> configured to be non-default, as you say, some of these terminal
> definitions are a little buggy. There is a more reliable way to do
> it, so mutt should use that (imho).

I don't think this is really a bug (for the above reasons). But if
this is possible, Mutt should probably add an option allowing the
user to choose the erase character for backspace.
}}}

--------------------------------------------------------------------------------
2007-09-11 07:48:12 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-09-10 09:53:13 -0500, Kyle Wheeler wrote:
> But let's phrase this another way: what do you expect a user to do whose 
> TERM entry is wrong? Edit the terminfo files? Figure out how to configure 
> their terminal to match the broken terminfo files? Use stty to change the 
> terminal settings to match the terminal? All of these are valid options, 
> and *should* all work (and DO all work with most other terminal 
> applications, such as vim, emacs, libreadline, etc.), but only the first 
> two work with mutt. That seems broken to me.

But remember that in Mutt, the backspace key is not used in the editor
only, but also in the index, the pager and so on. And what if some user
prefers to use the delete key as erase?
}}}

--------------------------------------------------------------------------------
2007-09-11 15:18:09 UTC Kyle Wheeler
* Added comment:
{{{
On Tuesday, September 11 at 07:37 AM, quoth Mutt:
> Conversely, you can't be sure that what the user (or something else) 
> has configured for erase is really the backspace key.

Fair enough, but I don't expect mutt to handle pathological users.

I'm suggesting we go with "least surprise". The "erase" character, 
whatever it is and no matter what key emits it, is the character that 
triggers "backspace-like" behavior. So when I say "mutt, when I hit 
backspace, do X", I expect mutt to understand backspace the same way 
that every other program does: whatever key I hit that happens to 
produce backspace behavior is the one I'm referring to. If I happen to 
have configured my terminal such that the 'r' character is the "erase" 
character (such that if I type "bar" I end up with just "b"), then 
every program I run in that terminal is essentially treating 'r' as my  
backspace key. Thus when I tell mutt to bind "backspace" to something, 
I expect mutt to bind 'r' to that something. It's simply consistent.

And if I've done everything in my power short of writing my own 
terminfo entry to convince my terminal that ^? is the backspace 
character, then why must mutt be the only program still stubbornly 
insisting that ^H is backspace? Vim doesn't use $TERM to override the 
stty setting. Nor does emacs, nor does libreadline, nor does telnet, 
xchat, bitchx, or any other terminal-based application. *WHY* is this 
considered "good" behavior for mutt?

When I say "mutt, backspace means foo", what I'm really saying is 
"Mutt, when I press the key that behaves as a backspace...". Of course 
mutt can't make sure that they key I press is really *labelled* 
"Backspace". On many Apple keyboards, it's actually labelled "Delete" 
even though for all other intents and purposes it's the backspace key. 
That's because the <BackSpace> key is a reference to "the key that has  
the backspace effect". Of *course* mutt can't be prescient and somehow 
find out whether or not my keyboard even *has* a BackSpace key, nor 
can it find out for certain what that key actually emits. But it CAN 
trust that when the user says "backspace" they don't think mutt is 
omniscient. Simply using whatever happens to have the backspace effect 
(namely, the "erase" character) makes perfect sense, and is consistent 
with virtually every other terminal-based program. More to the point, 
I really don't see how using anything else makes any sense at all.

Currently mutt is extracting this bit of information from the terminfo 
entry. My point is that this is insufficient because the terminfo may 
be wrong (buggy) or outdated (superseded). If this was our only 
recourse, I'd say that's fine, but it's not the only recourse. There's 
a better way to do it! Mutt can't find out if the terminfo entry is 
wrong or buggy, but it CAN find out if the terminfo entry has been 
superseded, and I think it *should*.

~Kyle
}}}

--------------------------------------------------------------------------------
2007-09-11 15:18:10 UTC Kyle Wheeler
* Added attachment part0001.2.pgp
* Added comment:
Added by email2trac

--------------------------------------------------------------------------------
2007-09-11 15:20:47 UTC Kyle Wheeler
* Added attachment part0001.3.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
On Tuesday, September 11 at 07:48 AM, quoth Mutt:
> But remember that in Mutt, the backspace key is not used in the 
> editor only, but also in the index, the pager and so on.

So what? Should mutt not be consistent? If I have:

     bind index <BackSpace> delete-message
     bind editor <BackSpace> backspace

...should I not expect that in both cases I am referring to the same 
key?

> And what if some user prefers to use the delete key as erase?

Then good for them! They should use stty to set the erase sequence to 
be ^[[3~. Once the user has done so, however, when the user later 
presses the key labeled "backspace", why should mutt be expected to 
know that the key the user pressed has ink on it that says 
"backspace"? It *shouldn't* be expected to, because of course it 
can't. Mutt should say to itself "what key has the backspace effect? 
this must be what the user is referring to when the user says 
<BackSpace>, because I have no way of knowing which key on the 
keyboard has 'Backspace' printed on it, or even if there is one." When  
the user sets some other key as the "erase" key, that key, by 
definition, becomes the backspace key. Whatever was previously 
considered the backspace key is now just a generic key. No matter 
what's printed on that key, no matter what position it has in the 
keyboard, that's all irrelevant because the computer can't be expected 
to know!

Think of it like binding keys in a video game. Your joystick may have 
buttons labeled "Fire" and "Shield", but if you tell the videogame 
that the "Fire" button should trigger the shield, then the button with 
"Shield" printed on it is now just an arbitrary button that emits a 
certain character sequence. The ink on its surface gives it no 
particular claim to the title "the shield button". The button labeled 
"Fire" is now the shield button. In the same way, when I set some 
character sequence (such as ^[[3~) to be the "erase" sequence, I am 
defining whatever key emits that sequence to be the "backspace" key, 
no matter what happens to be printed on the actual buttons, because 
that's the key that performs a backspace.

<BackSpace> must be defined by behavior, not by the ink printed on the 
key, because of course no program can know what's printed on the key.

~Kyle
}}}

--------------------------------------------------------------------------------
2007-09-11 16:42:30 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-09-11 09:57:56 -0500, Kyle Wheeler wrote:
> On Tuesday, September 11 at 07:37 AM, quoth Mutt:
>> Conversely, you can't be sure that what the user (or something else) has 
>> configured for erase is really the backspace key.
>
> Fair enough, but I don't expect mutt to handle pathological users.

There may be good reasons to do that.

> I'm suggesting we go with "least surprise". The "erase" character, whatever 
> it is and no matter what key emits it, is the character that triggers 
> "backspace-like" behavior. So when I say "mutt, when I hit backspace, do 
> X", I expect mutt to understand backspace the same way that every other 
> program does: whatever key I hit that happens to produce backspace behavior 
> is the one I'm referring to. If I happen to have configured my terminal 
> such that the 'r' character is the "erase" character (such that if I type 
> "bar" I end up with just "b"), then every program I run in that terminal is 
> essentially treating 'r' as my  backspace key. Thus when I tell mutt to 
> bind "backspace" to something, I expect mutt to bind 'r' to that something. 
> It's simply consistent.

However readline (as you cited it in one of your messages) doesn't
work the way you want. For instance, if I create a .inputrc file
with:

Rubout: "foo"

to bind backspace to the string "foo" and run "stty erase r", then
start bash, I get "foo" when typing backspace, and the 'r' key still
does the erase function.
}}}

--------------------------------------------------------------------------------
2007-09-11 16:56:13 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-09-11 10:20:16 -0500, Kyle Wheeler wrote:
> On Tuesday, September 11 at 07:48 AM, quoth Mutt:
>> But remember that in Mutt, the backspace key is not used in the editor 
>> only, but also in the index, the pager and so on.
>
> So what? Should mutt not be consistent? If I have:
>
>     bind index <BackSpace> delete-message
>     bind editor <BackSpace> backspace
>
> ...should I not expect that in both cases I am referring to the same key?

Yes. If you notice that this refers to different keys, then there's
a bug.

>> And what if some user prefers to use the delete key as erase?
>
> Then good for them! They should use stty to set the erase sequence to be 
> ^[[3~.

First, the sequence sent by the delete key is not necessarily ^[[3~.
Secondly, this is not sufficient: one also needs to bind <Delete> to
backspace. And everything works OK.

> <BackSpace> must be defined by behavior, not by the ink printed on
> the key, because of course no program can know what's printed on the
> key.

No, that's not how the terminfo works. <BackSpace> means the backspace
key. No more. Just like <Left> means the left key and so on. If you
want some kind of key aliases based on tty settings, e.g. <tty:erase>,
<tty:kill> and so on, this could be a new feature in Mutt. But that
should be more probably an addition to the curses library.
}}}

--------------------------------------------------------------------------------
2007-09-11 17:23:01 UTC Kyle Wheeler
* Added comment:
{{{
On Tuesday, September 11 at 04:42 PM, quoth Mutt:
>> essentially treating 'r' as my  backspace key. Thus when I tell 
>> mutt to bind "backspace" to something, I expect mutt to bind 'r' to 
>> that something. It's simply consistent.
>
> However readline (as you cited it in one of your messages) doesn't 
> work the way you want. For instance, if I create a .inputrc file 
> with:
>
> Rubout: "foo"
>
> to bind backspace to the string "foo" and run "stty erase r", then 
> start bash, I get "foo" when typing backspace, and the 'r' key still 
> does the erase function. 

Good point; it's even worse than that. Rubout in readline (in my 
xterm) maps to ^? (which, interestingly, is what I've configured my 
backspace key to emit), while <BackSpace> in mutt (in my xterm) maps 
to ^H.

You're correct that readline is defining the configuration keyword 
RUBOUT statically rather than detecting it dynamically, which I view 
as wrong, but bizarrely, readline is somehow detecting it to be 
different than how mutt is detecting it. So, readline would appear to 
be a bad example.

Upon further examination, I find readline interestingly inconsistent. 
Your readline still performs a backward-delete-char if you've used 
stty to set r as the erase character. In my terminal, readline seems 
choosy about whether it's going to respect the erase character or not. 
For example, if I set ^R to be the erase character, readline refuses 
to obey: ^R still triggers reverse-search-history. And if I set r to 
be the erase character, typing r still inserts an r. But using stty to 
switch between the usual suspects (^H and ^?), readline will happily 
trigger backward-delete-char for whichever one I've set as the erase 
character, with no change to my ~/.inputrc.

~Kyle
}}}

--------------------------------------------------------------------------------
2007-09-11 17:23:04 UTC Kyle Wheeler
* Added attachment part0001.4.pgp
* Added comment:
Added by email2trac

--------------------------------------------------------------------------------
2007-09-11 17:36:45 UTC Kyle Wheeler
* Added comment:
{{{
On Tuesday, September 11 at 04:56 PM, quoth Mutt:
>> <BackSpace> must be defined by behavior, not by the ink printed on 
>> the key, because of course no program can know what's printed on the 
>> key.
>
> No, that's not how the terminfo works. <BackSpace> means the backspace 
> key. No more. Just like <Left> means the left key and so on.

How is the terminfo supposed to know what the user has painted on 
their keyboard? It can't know whether the backspace key emits a ^H or 
a ^? (or any other value), so why pretend that it's an authoritative 
resource on the question of what the backspace key emits? I recognize 
that it would be awfully nice if we could somehow always know what key 
sequence the backspace key emits, but we simply can't.

After all, what is it about the backspace key that makes it the 
backspace key? There's its position on the keyboard, and the fact that 
it says "backspace" on it, and what we expect the computer to do when 
we press it. Its position on the keyboard can change, and what is 
written on it can change as well (Apple keyboards don't have Backspace 
and Delete keys, they have Delete and Del keys; older Sun keyboards 
don't have Backspace keys either, they have keys with a left-arrow on 
them (these keys are the same size as the <Left> key, have the same 
picture on them, but are just in the top-right corner of the qwerty 
area of the keyboard)). What it all really boils down to as far as 
what we mean when we say "the backspace key" is "that key that we 
press that we think will delete the character to the left of the 
cursor" and no more. But truly, since we can change which key performs  
that operation, why are we still working with an obviously broken 
definition of "the backspace key" as "that key in the upper-right 
corner of the qwerty section of the keyboard that has either 
Backspace, Delete, a left-arrow, or something else similarly 
suggestive on it"?

~Kyle
}}}

--------------------------------------------------------------------------------
2007-09-11 17:36:46 UTC Kyle Wheeler
* Added attachment part0001.5.pgp
* Added comment:
Added by email2trac

--------------------------------------------------------------------------------
2007-09-13 11:03:51 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-09-11 12:36:36 -0500, Kyle Wheeler wrote:
> On Tuesday, September 11 at 04:56 PM, quoth Mutt:
>>> <BackSpace> must be defined by behavior, not by the ink printed on the 
>>> key, because of course no program can know what's printed on the key.
>>
>> No, that's not how the terminfo works. <BackSpace> means the backspace 
>> key. No more. Just like <Left> means the left key and so on.
>
> How is the terminfo supposed to know what the user has painted on their 
> keyboard?

It doesn't need to.

> It can't know whether the backspace key emits a ^H or a ^? (or any
> other value), so why pretend that it's an authoritative resource on
> the question of what the backspace key emits?

This is not specific to backspace. Same problems can occur with the
arrow keys, page up, page down, home, end and probably other ones,
because there isn't a single standard, and different terminals
generate different sequences. The only solution is to make sure
that the terminfo data match what the terminal sends.

> What it all really boils down to as far as what we mean when we say
> "the backspace key" is "that key that we press that we think will
> delete the character to the left of the cursor" and no more.

No, the backspace key is just the key named "backspace". No more.

The fact that you can do "stty erase <some_key>" won't change the
backspace key. You just change the erase character, or if you want
the binding of the erase function to some key. If some user does
"stty erase r", the key 'R' will still be the key 'R'. You can't
say that it is the backspace key.
}}}

--------------------------------------------------------------------------------
2007-09-13 16:02:37 UTC Kyle Wheeler
* Added attachment part0001.6.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
On Thursday, September 13 at 05:16 PM, quoth Vincent Lefevre:
>On 2007-09-13 08:53:59 -0500, Kyle Wheeler wrote:
>> And what of keyboards without a key named "backspace"?
>
>The key can't be used, so that its configuration doesn't matter.
>But in general, one finds a key on the keyboard and use it as
>backspace.
>
>> My computer (an Apple) only has a "delete" key.
>
>So, I assume that you use the "delete" key for the erase function.

My point is that in such situations, there *is* no "backspace" key, 
and yet mutt presumes that there is one. More to the point, so does 
every other piece of software. For all intents and purposes, that key 
in that general location is considered the backspace key. What happens 
to be painted on the key (i.e. what the key is named) is irrelevant, 
and *should* be irrelevant.

~Kyle
}}}

--------------------------------------------------------------------------------
2007-09-13 16:04:41 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-09-13 08:53:59 -0500, Kyle Wheeler wrote:
> And what of keyboards without a key named "backspace"?

The key can't be used, so that its configuration doesn't matter.
But in general, one finds a key on the keyboard and use it as
backspace.

> My computer (an Apple) only has a "delete" key.

So, I assume that you use the "delete" key for the erase function.

> The Sun keyboards I've used only had a second left-arrow key in an
> unusual location.

The location doesn't matter. "xev" for instance gives you the names
of the keys (at the X level). You should make sure that they are the
same in the terminfo entry.
}}}

--------------------------------------------------------------------------------
2007-09-13 16:20:04 UTC Kyle Wheeler
* Added comment:
{{{
On Thursday, September 13 at 01:02 PM, quoth Vincent Lefevre:
>> What it all really boils down to as far as what we mean when we say 
>> "the backspace key" is "that key that we press that we think will 
>> delete the character to the left of the cursor" and no more.
>
> No, the backspace key is just the key named "backspace". No more.

And what of keyboards without a key named "backspace"? My computer (an 
Apple) only has a "delete" key. The Sun keyboards I've used only had a 
second left-arrow key in an unusual location. They are not *named* 
"backspace", they are *named* "delete" and "left-arrow", respectively. 
Shall we assume that mutt must know this, somehow?

~Kyle
}}}

--------------------------------------------------------------------------------
2007-09-13 16:20:05 UTC Kyle Wheeler
* Added attachment part0001.7.pgp
* Added comment:
Added by email2trac

--------------------------------------------------------------------------------
2007-09-13 16:31:57 UTC Kyle Wheeler
* Added attachment part0001.8.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
On Thursday, September 13 at 04:04 PM, quoth Mutt:
> On 2007-09-13 08:53:59 -0500, Kyle Wheeler wrote:
> > And what of keyboards without a key named "backspace"?
>
> The key can't be used, so that its configuration doesn't matter.

The configuration of the BackSpace key *does* matter because it is not 
simply ignored for keyboards that do not have keys named "backspace". 
On the contrary, mutt will still insist that a <BackSpace> key exists, 
and is signified by ^H. Thus, when another key is pressed that happens 
to emit ^H, mutt presumes that the BackSpace key was pressed. In other 
words, mutt recognizes the backspace key by what it *does* rather than 
by how it is *named*. Any key that emits the appropriate byte sequence 
is assumed to be the backspace key, regardless of what that key 
*actually* is.

> But in general, one finds a key on the keyboard and use it as
> backspace.

Precisely. This key then becomes "the backspace key" by virtue of its 
function rather than any other detail about the key.

~Kyle
}}}

--------------------------------------------------------------------------------
2007-09-13 17:49:46 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-09-13 11:31:26 -0500, Kyle Wheeler wrote:
> The configuration of the BackSpace key *does* matter because it is not 
> simply ignored for keyboards that do not have keys named "backspace". On 
> the contrary, mutt will still insist that a <BackSpace> key exists, and is 
> signified by ^H.

Not necessarily ^H. The sequence is defined in the terminfo data.
This is true for any special key.

> Thus, when another key is pressed that happens to emit ^H, 
> mutt presumes that the BackSpace key was pressed.

Yes, Mutt (in fact, the curses library) cannot make the difference
(well, AFAIK, some libraries could support a maximum delay between
the characters, but this can lead to problems on slow SSH connections,
even ADSL).

> In other words, mutt recognizes the backspace key by what it *does*
> rather than by how it is *named*.

No, it recognizes the keys by what sequence of characters is sent by
the terminal. In the terminfo data, I have: "kbs=\177". This means
that the backspace key (kbs) is recognized if ^? is generated by the
terminal.

> Any key that emits the appropriate byte sequence is assumed to be 
> the backspace key, regardless of what that key *actually* is.

Yes, the byte sequence given in the terminfo data. But this isn't
necessarily the erase character.
}}}
