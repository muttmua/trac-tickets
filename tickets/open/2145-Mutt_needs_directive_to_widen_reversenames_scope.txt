Ticket:  2145
Status:  new
Summary: Mutt needs directive to widen reverse-name's scope

Reporter: pack-muttbugs@rattus.net
Owner:    mutt-dev

Opened:       2005-12-06 16:30:53 UTC
Last Updated: 1970-01-01 00:00:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
reverse-name only looks at a small number, possibly 1 field. It would be much more useful if a 'reverse-fields' directive could be added so that the user could specify an ordered list of fields that should be checked for addresses than match an alternate and could be used for bulding the from that reverse-name enables.

Presumably the current list is "To, Cc", whereas I would very much like "Envelope-to:" as the sole list (I use tokenised email with an MTA that adds the envelope-to header)
>How-To-Repeat:
>Fix:
See description.
}}}
