Ticket:  3433
Status:  new
Summary: expired never updated after initial parse

Reporter: dylex
Owner:    mutt-dev

Opened:       2010-07-27 02:46:49 UTC
Last Updated: 2010-07-27 02:46:49 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
If you have a long-running mutt or use hcache, the expired flags (~E) are never updated as Expires dates pass unless the message is reparsed for some reason.

(I have a fix for this in the context of an improved version of Omen Wild's patch which explicitly stores expires headers in the envelope, but short of that or time_t expires in HEADER, I don't have any better suggestions.)
