Ticket:  2448
Status:  new
Summary: S/MIME decrypting keyid selection

Reporter: Alain Bench <veronatif@free.fr>
Owner:    mutt-dev

Opened:       2006-08-25 12:31:02 UTC
Last Updated: 2007-04-02 02:38:15 UTC

Priority:  major
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{

Hello,

    There are some problems with S/MIME decrypting key selection, when
one has multiple keys, and has unset $smime_decrypt_use_default_key:

 -1) Before each display of a crypted message, Mutt iteratively prompts
for the decrypting key to use:

| Use ID 12345678.1 for email@example.com ? ([no]/yes):
| Use ID 12345678.2 for email@example.com ? ([no]/yes):
| Use ID 12345678.x for email@example.com ? ([no]/yes):
| Use ID 12345678.0 for email@example.com ? ([no]/yes):
| Enter keyID for email@example.com:

    ...and finally shows the smime menu listing all available private
keys, even for other email addreses:

| S/MIME certificates matching "".


    That's not very practical, given that:

 - User may not know which encrypting key was used.
 - User may not remember which 12345678.x selects the wanted key
(display the certficate's label would help).
 - The firsts y/n prompts are disordered: The last prompted ".0" was the
first in ~/.smime/keys/.index
 - The labels are displayed only at the 6th step, the key listing.
 - Each selection of a different keyid wipes the passphrase.
 - There is no error on misselected keyid, just an empty body.
 - There is no error on mistyped passphrase, just an empty body.
 - The whole sequence restarts each time user does <view-attachments>,
<exit><display-message>, <display-toggle-weed>, or goes to view another
crypted message.


    Possible solutions:

   -a) Is there really no full automatic way to guess which key
encrypted the message?

   -b) Could we store as many passphrases as keyids? Or optionally a
single passphrase for all keyids? The current halfway scheme doesn't
seem to make much sense.

   -c) In such long iterative selection sequences, shouldn't there be a
shortcut to go directly to the listing? Maybe something similar to "?"
at <change-folder> prompt going to browser, or yet another $option.


    Further problems:

 -2) <decrypt-copy> and <decrypt-save> dont't prompt for a key, nor
passphrase, and won't decrypt. Result is original crypted mail.


 -3) <decode-copy> and <decode-save> do prompt for a passphrase, but not
for a keyid. If crypted message, last selected keyid, and passphrase are
matching: Result is the expected decoded and decrypted message.
Otherwise result is a message with silently lost empty body. Without any
error nor warning.


 -4) Doing <view-attachments> on an S/MIME opaque signed message (that
is *not* crypted) uselessly starts the keyid selection sequence (but
doesn't prompt for a passphrase).


Bye!    Alain.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2007-04-02 02:38:15 UTC brendan
* component changed to crypto
* Updated description:
{{{

Hello,

    There are some problems with S/MIME decrypting key selection, when
one has multiple keys, and has unset $smime_decrypt_use_default_key:

 -1) Before each display of a crypted message, Mutt iteratively prompts
for the decrypting key to use:

| Use ID 12345678.1 for email@example.com ? ([no]/yes):
| Use ID 12345678.2 for email@example.com ? ([no]/yes):
| Use ID 12345678.x for email@example.com ? ([no]/yes):
| Use ID 12345678.0 for email@example.com ? ([no]/yes):
| Enter keyID for email@example.com:

    ...and finally shows the smime menu listing all available private
keys, even for other email addreses:

| S/MIME certificates matching "".


    That's not very practical, given that:

 - User may not know which encrypting key was used.
 - User may not remember which 12345678.x selects the wanted key
(display the certficate's label would help).
 - The firsts y/n prompts are disordered: The last prompted ".0" was the
first in ~/.smime/keys/.index
 - The labels are displayed only at the 6th step, the key listing.
 - Each selection of a different keyid wipes the passphrase.
 - There is no error on misselected keyid, just an empty body.
 - There is no error on mistyped passphrase, just an empty body.
 - The whole sequence restarts each time user does <view-attachments>,
<exit><display-message>, <display-toggle-weed>, or goes to view another
crypted message.


    Possible solutions:

   -a) Is there really no full automatic way to guess which key
encrypted the message?

   -b) Could we store as many passphrases as keyids? Or optionally a
single passphrase for all keyids? The current halfway scheme doesn't
seem to make much sense.

   -c) In such long iterative selection sequences, shouldn't there be a
shortcut to go directly to the listing? Maybe something similar to "?"
at <change-folder> prompt going to browser, or yet another $option.


    Further problems:

 -2) <decrypt-copy> and <decrypt-save> dont't prompt for a key, nor
passphrase, and won't decrypt. Result is original crypted mail.


 -3) <decode-copy> and <decode-save> do prompt for a passphrase, but not
for a keyid. If crypted message, last selected keyid, and passphrase are
matching: Result is the expected decoded and decrypted message.
Otherwise result is a message with silently lost empty body. Without any
error nor warning.


 -4) Doing <view-attachments> on an S/MIME opaque signed message (that
is *not* crypted) uselessly starts the keyid selection sequence (but
doesn't prompt for a passphrase).


Bye!    Alain.
>How-To-Repeat:
>Fix:
}}}
