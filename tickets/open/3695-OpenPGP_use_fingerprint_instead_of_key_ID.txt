Ticket:  3695
Status:  new
Summary: OpenPGP: use fingerprint instead of key ID

Reporter: ilf
Owner:    mutt-dev

Opened:       2014-06-22 13:08:30 UTC
Last Updated: 2015-04-16 02:01:00 UTC

Priority:  major
Component: crypto
Keywords:  OpenPGP, PGP, GnuPG, fingerprint, keyid

--------------------------------------------------------------------------------
Description:
From [https://help.riseup.net/en/security/message-security/openpgp/gpg-best-practices OpenPGP Best Practices]:

  [https://help.riseup.net/en/security/message-security/openpgp/gpg-best-practices#dont-rely-on-the-keyid Don’t rely on the keyid.]

  Short OpenPGP keyids, for example 0×2861A790, are 32 bits long. They have been [http://www.asheesh.org/note/debian/short-key-ids-are-bad-news shown] to be easily spoofed by another key with the same keyid. Long OpenPGP keyids (for example 0xA1E6148633874A3D) are 64 bits long. They are [http://thread.gmane.org/gmane.ietf.openpgp/7413 trivially collidable], which is [https://www.debian-administration.org/users/dkg/weblog/105 also a potentially serious problem].

  If you want to deal with a cryptographically-strong identifier for a key, you should use the full fingerprint. You should never rely on the short, or even long, keyID.

I have not dug deep into mutt code, but it seems that everywhere the user is presented with key identifiers, the GnuPG default keyid-format 0xshort is used, f.e. in the "PGP keys matching" menu.

I propose that mutt transitions away from keyid to the full fingerprint everywhere for adressing keys: internal calls, interaction with external pgp_* programs, and presentation to the user.

Thanks, and keep up the good work!

--------------------------------------------------------------------------------
2014-06-22 16:17:04 UTC ilf
* Added comment:
I just learned that ever since [http://notmuchmail.org/news/release-0.6/ Notmuch received OpenPGP support in 0.6 on 2011-07-01], they have been doing it this way.

--------------------------------------------------------------------------------
2015-01-18 16:28:47 UTC erAck
* Added comment:
Note that you can set pgp_long_ids=yes for 64 bit key IDs.

--------------------------------------------------------------------------------
2015-01-18 16:49:31 UTC ilf
* Added comment:
Replying to [comment:2 erAck]:
> Note that you can set pgp_long_ids=yes for 64 bit key IDs.

Nice, but why is it "Default: no"?

Also, that doesn't change the original feature request :)

--------------------------------------------------------------------------------
2015-02-18 23:03:11 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [47b4e57b2f1c68f40cf607e625b85a97aa03299e]:
{{{
#!CommitTicketReference repository="" revision="47b4e57b2f1c68f40cf607e625b85a97aa03299e"
Convert pgp_key_t fingerprint to a char* (see #3695)

Currently only pgppubring.c is using the fingerprint field, and is using
it to store a binary version of the fingerprint.

Convert the field to store a null-terminated string.
Modify pgppubring.c to use to use the new field.
}}}

--------------------------------------------------------------------------------
2015-02-18 23:03:12 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [af5951f5d81c13b54f8896b2b372fb1774547c06]:
{{{
#!CommitTicketReference repository="" revision="af5951f5d81c13b54f8896b2b372fb1774547c06"
Add fingerprint record parsing for pgp list keys. (see #3695)

Modify parse_pub_line to parse fpr records and add the fingerprint to
the pgp_key_t's fingerprint field.

Add "--with-fingerprint --with-fingerprint" to the
pgp_list_pubring_command and pgp_list_secring_command commands in
contrib/gpg.rc.  The second invocation generates fpr records for subkeys
too.
}}}

--------------------------------------------------------------------------------
2015-02-18 23:03:13 UTC Eike Rathke <erack@erack.de>
* Added comment:
In [f5b1b75c595809ad658a11898ddaf8aca850c484]:
{{{
#!CommitTicketReference repository="" revision="f5b1b75c595809ad658a11898ddaf8aca850c484"
Allow fingerprint user input for key selection. (see #3695)

Accept and check input of a fingerprint and find the matching key.

Note that for both to work, match against and display of fingerprint, the
pgp_list_pubring_command and pgp_list_secring_command need to contain the
--with-fingerprint option, or have with-fingerprint in ~/.gnupg/gpg.conf.
}}}

--------------------------------------------------------------------------------
2015-04-16 02:01:00 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [d12cb775b779fd40afc7cfd68be6976b833f29c8]:
{{{
#!CommitTicketReference repository="" revision="d12cb775b779fd40afc7cfd68be6976b833f29c8"
Use fingerprints instead of keyIDs internally. (see #3695)

Add a helper function, pgp_fpr_or_lkeyid(), that returns the fingerprint
if available, otherwise falls back to the long key id.  Convert Mutt to
use that value for pgp command invocation.

Change gpgme to use an equivalent crypt_fpr_or_lkeyid() function in a
couple places too (for keylist generation and sign-as key selection).

Update documentation to mention fingerprints and the --with-fingerprint
option for gpg invocation.

Change pgp_long_ids to default: yes, but add a note mentioning
it's only used for the display of key IDs in a few places.
}}}
