Ticket:  3485
Status:  new
Summary: An option could be added to control IMAP message fetching

Reporter: duyang
Owner:    brendan

Opened:       2010-12-31 08:36:07 UTC
Last Updated: 2011-06-26 05:36:44 UTC

Priority:  minor
Component: IMAP
Keywords:  IMAP message fetching

--------------------------------------------------------------------------------
Description:
Hi,

I found that all the message in the current window view(IMAP mailbox) would be fetched at once everything I switch to a new window page. 
this is a good for when most message's body have been cached.

but this also has low performance, when I have a lot of messages not seen(have been cached yet) in one inbox and I want to check a mail which I have to switch several pages down the window, then I have to wait for all mails above the mail (which I want to check) being downloaded.

if I have several large mails above it, maybe I have to wait for many hours. it is even more worse if I have a low network bandwidth. so it looks a little unacceptable.

so I think an option could be added to control the way about message fetching for IMAP. it controls fetching message once for a page or once for a message just it was viewed.

if allows message could be fetched only it was checked, the mutt's IMAP performance could be much more preferable.

thanks a lot!
du yang

--------------------------------------------------------------------------------
2011-06-26 05:36:44 UTC brendan
* milestone changed to 2.0
