Ticket:  1616
Status:  new
Summary: mutt grows in memory to >40MB

Reporter: turgon@debian.org
Owner:    mutt-dev

Opened:       2003-08-27 05:41:29 UTC
Last Updated: 1970-01-01 00:00:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4+20030817
Severity: normal

-- Please type your report below this line

	I think that mutt could have some serious memory leak. I usually read
some big messages (logs, maybe greater than 1MB each mail), and I use syntax
hilighting in some of them to ease the reading. When I read some of these
messages, mutt starts growing. This is a "snapshot" of mutt after ~2h30m of
use:

  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  Command
 8296 robe      15  10 49860  48m 1432 S  0.0  7.7   2:31.51 mutt

	This is a problem, because I usually start mutt on the morning and
only exit of it when I finish working, on the afternoon. I too feel that mutt
acts slowly when I try to advance in these messages. This only happens in
these big, hilighted messages, so I suppose mutt has some problem with them.
These are generally messages generated with fwlogwatch or logcheck. These are
the lines that I use to hilight these messages:

--- cut here ---

color body	brightblue default "Domain not found"
color body	brightblue default "Relay access denied"
color body	brightblue default "need fully-qualified address"
color body	brightblue default "Connection refused"
color body	brightblue default "Cannot resolve PTR record"
color body	brightyellow  red   "Broken pipe"
color body	brightblue default "DPT=[0-9]+"
color body	brightblue default "SPT=[0-9]+"
color body	brightblue default "DST=[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"
color body	brightblue default "SRC=[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"

# fwlogwatch:
color body	brightred default "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+ port [0-9]+"

--- cut here ---

	I can provide of one of these big messages, though I'd have to
offuscate it a little to preserve the data about the customers of the firm I
work for. I have not included my personal muttrc because it has somewhat
private data that I don't want to get mirrored on the Net :-) But I'd do it
(after removing that data) too, if it's needed.  Ask for anything that you
need.

-- System Information
Debian Release: testing/unstable
Kernel Version: Linux zeus 2.4.20-grsec #1 Tue Jun 10 11:48:02 CEST 2003 i686 unknown unknown GNU/Linux

Versions of the packages mutt depends on:
ii  libc6          2.3.1-17       GNU C Library: Shared libraries and Timezone
ii  libidn9        0.1.11-3       GNU libidn library, implementation of IETF I
ii  libncurses5    5.3.20030719-1 Shared libraries for terminal handling
ii  libsasl2       2.1.15-5       Authentication abstraction library
exim	Not installed or no info
ii  postfix        2.0.13-4       A high-performance mail transport agent
	^^^ (Provides virtual package mail-transport-agent)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Leyendo especificaciones de /usr/lib/gcc-lib/i386-linux/3.2.3/specs
Configurado con: ../src/configure -v --enable-languages=c,c++,java,f77,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.2 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-objc-gc i386-linux
Modelo de hilos: posix
gcc versión 3.2.3

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20-grsec (i686) [using ncurses 5.3] [using libidn 0.1.11 (compiled with 0.1.14)]
Opciones especificadas al compilar:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
Para contactar a los desarrolladores mande un mensaje a <mutt-dev@mutt.org>.
Para reportar un fallo use la utilería flea(1) por favor.

patch-1.5.3.rr.compressed.1
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.5.1.cd.edit_threads.9.2
patch-1.3.27.bse.xtitles.1
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.56d

--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
ignore date delivered-to precedence errors-to in-reply-to user-agent
ignore x-loop x-sender x-mailer x-msmail-priority x-mimeole x-priority
ignore x-accept-language x-authentication-warning thread- priority
bind editor    "\e<delete>"    kill-word
bind editor    "\e<backspace>" kill-word
bind editor     <delete>  delete-char
unset use_domain
unset use_from
set sort=threads
unset write_bcc
unset bounce_delivered
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
set pipe_decode
macro generic <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro index   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro pager   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
open-hook	\\.gz$ "gzip -cd %f > %t"
close-hook	\\.gz$ "gzip -c %t > %f"
append-hook	\\.gz$ "gzip -c %t >> %f"
open-hook	\\.bz2$ "bzip2 -cd %f > %t"
close-hook	\\.bz2$ "bzip2 -c %t > %f"
append-hook	\\.bz2$ "bzip2 -c %t >> %f"
color normal	white black
color attachment brightyellow black
color hdrdefault cyan black
color indicator black cyan
color markers	brightred black
color quoted	green black
color signature cyan black
color status	brightgreen blue
color tilde	blue black
color tree	red black
set pgp_decode_command="gpg  --charset utf-8   --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet  --batch  --output - %f"
set pgp_verify_command="gpg   --status-fd=2 --no-verbose --quiet  --batch  --output - --verify %s %f"
set pgp_decrypt_command="gpg   --status-fd=2 --passphrase-fd 0 --no-verbose --quiet  --batch  --output - %f"
set pgp_sign_command="gpg    --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="gpg   --charset utf-8 --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap gpg  --charset utf-8    --batch  --quiet  --no-verbose --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap gpg  --charset utf-8 --passphrase-fd 0  --batch --quiet  --no-verbose  --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="gpg  --no-verbose --import %f"
set pgp_export_command="gpg   --no-verbose --export --armor %r"
set pgp_verify_key_command="gpg   --verbose --batch  --fingerprint --check-sigs %r"
set pgp_list_pubring_command="gpg   --no-verbose --batch --quiet   --with-colons --list-keys %r" 
set pgp_list_secring_command="gpg   --no-verbose --batch --quiet   --with-colons --list-secret-keys %r" 
set pgp_good_sign="^\\[GNUPG:\\] GOODSIG"
set smime_ca_location="~/.smime/ca-bundle.crt"
set smime_certificates="~/.smime/certificates"
set smime_keys="~/.smime/keys"
set smime_pk7out_command="openssl smime -verify -in %f -noverify -pk7out"
set smime_get_cert_command="openssl pkcs7 -print_certs -in %f"
set smime_get_signer_cert_command="openssl smime -verify -in %f -noverify -signer %c -out /dev/null"
set smime_get_cert_email_command="openssl x509 -in %f -noout -email"
set smime_import_cert_command="/usr/lib/mutt/smime_keys add_cert %f"
set smime_encrypt_command="openssl smime -encrypt -%a -outform DER -in %f %c"
set smime_sign_command="openssl smime -sign -signer %c -inkey %k -passin stdin -in %f -certfile %i -outform DER"
set smime_decrypt_command="openssl smime -decrypt -passin stdin -inform DER -in %f -inkey %k -recip %c"
set smime_verify_command="openssl smime -verify -inform DER -in %s %C -content %f"
set smime_verify_opaque_command="openssl smime -verify -inform DER -in %s %C"
--- End /etc/Muttrc



>How-To-Repeat:
	
>Fix:
}}}

--------------------------------------------------------------------------------
2003-08-27 05:41:29 UTC turgon@debian.org
* Added comment:
{{{
Package: mutt
Version: 1.5.4+20030817
Severity: normal

-- Please type your report below this line

	I think that mutt could have some serious memory leak. I usually read
some big messages (logs, maybe greater than 1MB each mail), and I use syntax
hilighting in some of them to ease the reading. When I read some of these
messages, mutt starts growing. This is a "snapshot" of mutt after ~2h30m of
use:

 PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  Command
8296 robe      15  10 49860  48m 1432 S  0.0  7.7   2:31.51 mutt

	This is a problem, because I usually start mutt on the morning and
only exit of it when I finish working, on the afternoon. I too feel that mutt
acts slowly when I try to advance in these messages. This only happens in
these big, hilighted messages, so I suppose mutt has some problem with them.
These are generally messages generated with fwlogwatch or logcheck. These are
the lines that I use to hilight these messages:

--- cut here ---

color body	brightblue default "Domain not found"
color body	brightblue default "Relay access denied"
color body	brightblue default "need fully-qualified address"
color body	brightblue default "Connection refused"
color body	brightblue default "Cannot resolve PTR record"
color body	brightyellow  red   "Broken pipe"
color body	brightblue default "DPT=[0-9]+"
color body	brightblue default "SPT=[0-9]+"
color body	brightblue default "DST=[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"
color body	brightblue default "SRC=[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"

# fwlogwatch:
color body	brightred default "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+ port [0-9]+"

--- cut here ---

	I can provide of one of these big messages, though I'd have to
offuscate it a little to preserve the data about the customers of the firm I
work for. I have not included my personal muttrc because it has somewhat
private data that I don't want to get mirrored on the Net :-) But I'd do it
(after removing that data) too, if it's needed.  Ask for anything that you
need.

-- System Information
Debian Release: testing/unstable
Kernel Version: Linux zeus 2.4.20-grsec #1 Tue Jun 10 11:48:02 CEST 2003 i686 unknown unknown GNU/Linux

Versions of the packages mutt depends on:
ii  libc6          2.3.1-17       GNU C Library: Shared libraries and Timezone
ii  libidn9        0.1.11-3       GNU libidn library, implementation of IETF I
ii  libncurses5    5.3.20030719-1 Shared libraries for terminal handling
ii  libsasl2       2.1.15-5       Authentication abstraction library
exim	Not installed or no info
ii  postfix        2.0.13-4       A high-performance mail transport agent
	^^^ (Provides virtual package mail-transport-agent)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Leyendo especificaciones de /usr/lib/gcc-lib/i386-linux/3.2.3/specs
Configurado con: ../src/configure -v --enable-languages=c,c++,java,f77,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.2 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-objc-gc i386-linux
Modelo de hilos: posix
gcc versión 3.2.3

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20-grsec (i686) [using ncurses 5.3] [using libidn 0.1.11 (compiled with 0.1.14)]
Opciones especificadas al compilar:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
Para contactar a los desarrolladores mande un mensaje a <mutt-dev@mutt.org>.
Para reportar un fallo use la utilería flea(1) por favor.

patch-1.5.3.rr.compressed.1
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.5.1.cd.edit_threads.9.2
patch-1.3.27.bse.xtitles.1
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.56d

--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
ignore date delivered-to precedence errors-to in-reply-to user-agent
ignore x-loop x-sender x-mailer x-msmail-priority x-mimeole x-priority
ignore x-accept-language x-authentication-warning thread- priority
bind editor    "\e<delete>"    kill-word
bind editor    "\e<backspace>" kill-word
bind editor     <delete>  delete-char
unset use_domain
unset use_from
set sort=threads
unset write_bcc
unset bounce_delivered
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
set pipe_decode
macro generic <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro index   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro pager   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
open-hook	\\.gz$ "gzip -cd %f > %t"
close-hook	\\.gz$ "gzip -c %t > %f"
append-hook	\\.gz$ "gzip -c %t >> %f"
open-hook	\\.bz2$ "bzip2 -cd %f > %t"
close-hook	\\.bz2$ "bzip2 -c %t > %f"
append-hook	\\.bz2$ "bzip2 -c %t >> %f"
color normal	white black
color attachment brightyellow black
color hdrdefault cyan black
color indicator black cyan
color markers	brightred black
color quoted	green black
color signature cyan black
color status	brightgreen blue
color tilde	blue black
color tree	red black
set pgp_decode_command="gpg  --charset utf-8   --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet  --batch  --output - %f"
set pgp_verify_command="gpg   --status-fd=2 --no-verbose --quiet  --batch  --output - --verify %s %f"
set pgp_decrypt_command="gpg   --status-fd=2 --passphrase-fd 0 --no-verbose --quiet  --batch  --output - %f"
set pgp_sign_command="gpg    --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="gpg   --charset utf-8 --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap gpg  --charset utf-8    --batch  --quiet  --no-verbose --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap gpg  --charset utf-8 --passphrase-fd 0  --batch --quiet  --no-verbose  --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="gpg  --no-verbose --import %f"
set pgp_export_command="gpg   --no-verbose --export --armor %r"
set pgp_verify_key_command="gpg   --verbose --batch  --fingerprint --check-sigs %r"
set pgp_list_pubring_command="gpg   --no-verbose --batch --quiet   --with-colons --list-keys %r" 
set pgp_list_secring_command="gpg   --no-verbose --batch --quiet   --with-colons --list-secret-keys %r" 
set pgp_good_sign="^\\[GNUPG:\\] GOODSIG"
set smime_ca_location="~/.smime/ca-bundle.crt"
set smime_certificates="~/.smime/certificates"
set smime_keys="~/.smime/keys"
set smime_pk7out_command="openssl smime -verify -in %f -noverify -pk7out"
set smime_get_cert_command="openssl pkcs7 -print_certs -in %f"
set smime_get_signer_cert_command="openssl smime -verify -in %f -noverify -signer %c -out /dev/null"
set smime_get_cert_email_command="openssl x509 -in %f -noout -email"
set smime_import_cert_command="/usr/lib/mutt/smime_keys add_cert %f"
set smime_encrypt_command="openssl smime -encrypt -%a -outform DER -in %f %c"
set smime_sign_command="openssl smime -sign -signer %c -inkey %k -passin stdin -in %f -certfile %i -outform DER"
set smime_decrypt_command="openssl smime -decrypt -passin stdin -inform DER -in %f -inkey %k -recip %c"
set smime_verify_command="openssl smime -verify -inform DER -in %s %C -content %f"
set smime_verify_opaque_command="openssl smime -verify -inform DER -in %s %C"
--- End /etc/Muttrc
}}}
