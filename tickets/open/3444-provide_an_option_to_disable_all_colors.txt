Ticket:  3444
Status:  new
Summary: provide an option to disable all colors

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2010-08-11 12:30:46 UTC
Last Updated: 2010-08-11 12:30:46 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi,
I would like to have an option to disable all colors from mutt and use the standard one, I can work around this by setting the terminal to vt100 or something else that does not support colors or by writing a very long conf file to disable all colors on all parts of mutt.

It would be great to have an option to disable all colors with a single line change on .muttrc

Cheers
Antonio
