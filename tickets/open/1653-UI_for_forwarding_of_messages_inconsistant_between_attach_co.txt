Ticket:  1653
Status:  new
Summary: UI for forwarding of messages inconsistant between attach context and index/pager contexts

Reporter: jtoth@acm.org
Owner:    mutt-dev

Opened:       2003-09-28 21:56:04 UTC
Last Updated: 1970-01-01 00:00:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4i
Severity: normal

-- Please type your report below this line

mutt version is actually CVS from 2003-09-14 (2 weeks old), I believe.
Just cvs update'd and installed, though.

Context: I'm trying to simplify my handling of spam messages.  I have it
working so that I can just type ",S" in index, pager, and attach to save
the message to a "spam" folder, and "S" in index and pager to both save
the message and forward it to a spam reporting address.

But it looks like I'm going to have to redefine "editor" to "touch
<file>;sleep 1" or some such in order to get "S" working in attach
context, since it isn't recognizing "forward_edit".  Also, I had long
ago noticed that the order of the asking of questions in the attach
context is different (but had never sent in a flea--bad me!).

I was going to mention a similar inconsistancy about its complaining
about unmodified messages, but judging by the Changelog and my test just
now, looks like that's been fixed.

--- begin snippet from .muttrc ---

macro index ",S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Save spam to spam folder"
macro pager ",S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Save spam to spam folder"
macro attach ",S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Save spam to spam folder"

macro index "S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><forward-message>spamcop<enter>y<save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Process spam"
macro pager "S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><forward-message>spamcop<enter>y<save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Process spam"

--- end snippet from .muttrc ---
--- begin .mutt/pre-spam ---

# vim:ft=muttrc:shiftwidth=2:expandtab
set noconfirmappend
set forward_edit=no
set mime_forward=yes

--- end .mutt/pre-spam ---
--- begin .mutt/post-spam ---

# vim:ft=muttrc:sw=2:expandtab
set confirmappend
set forward_edit=yes
set mime_forward=ask-yes

--- end .mutt/post-spam ---

-- System Information
System Version: Linux baka.acw.vcu.edu 2.4.20-20.7smp #1 SMP Mon May 12 12:31:27 EDT 2003 i686 unknown
RedHat Release: Red Hat Linux release 7.3 (Valhalla)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/2.96/specs
gcc version 2.96 20000731 (Red Hat Linux 7.3 2.96-113)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20-20.7smp (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

-- 
Jim Toth
jtoth@acm.org
Folowups to the bug report only, please--I'm subscribed to mutt-dev.


>How-To-Repeat:
	
>Fix:
}}}

--------------------------------------------------------------------------------
2003-09-28 21:56:04 UTC jtoth@acm.org
* Added comment:
{{{
Package: mutt
Version: 1.5.4i
Severity: normal

-- Please type your report below this line

mutt version is actually CVS from 2003-09-14 (2 weeks old), I believe.
Just cvs update'd and installed, though.

Context: I'm trying to simplify my handling of spam messages.  I have it
working so that I can just type ",S" in index, pager, and attach to save
the message to a "spam" folder, and "S" in index and pager to both save
the message and forward it to a spam reporting address.

But it looks like I'm going to have to redefine "editor" to "touch
<file>;sleep 1" or some such in order to get "S" working in attach
context, since it isn't recognizing "forward_edit".  Also, I had long
ago noticed that the order of the asking of questions in the attach
context is different (but had never sent in a flea--bad me!).

I was going to mention a similar inconsistancy about its complaining
about unmodified messages, but judging by the Changelog and my test just
now, looks like that's been fixed.

--- begin snippet from .muttrc ---

macro index ",S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Save spam to spam folder"
macro pager ",S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Save spam to spam folder"
macro attach ",S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Save spam to spam folder"

macro index "S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><forward-message>spamcop<enter>y<save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Process spam"
macro pager "S" "<enter-command>source /home/jtoth/.mutt/pre-spam<enter><forward-message>spamcop<enter>y<save-message>=spam<enter><enter-command>source /home/jtoth/.mutt/post-spam<enter>" "Process spam"

--- end snippet from .muttrc ---
--- begin .mutt/pre-spam ---

# vim:ft=muttrc:shiftwidth=2:expandtab
set noconfirmappend
set forward_edit=no
set mime_forward=yes

--- end .mutt/pre-spam ---
--- begin .mutt/post-spam ---

# vim:ft=muttrc:sw=2:expandtab
set confirmappend
set forward_edit=yes
set mime_forward=ask-yes

--- end .mutt/post-spam ---

-- System Information
System Version: Linux baka.acw.vcu.edu 2.4.20-20.7smp #1 SMP Mon May 12 12:31:27 EDT 2003 i686 unknown
RedHat Release: Red Hat Linux release 7.3 (Valhalla)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/2.96/specs
gcc version 2.96 20000731 (Red Hat Linux 7.3 2.96-113)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20-20.7smp (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

-- 
Jim Toth
jtoth@acm.org
Folowups to the bug report only, please--I'm subscribed to mutt-dev.
}}}
