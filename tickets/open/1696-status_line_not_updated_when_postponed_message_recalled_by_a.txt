Ticket:  1696
Status:  reopened
Summary: status line not updated when postponed message recalled by another mutt

Reporter: vincent@vinc17.org
Owner:    mutt-dev

Opened:       2003-11-11 15:58:57 UTC
Last Updated: 2005-10-06 22:45:02 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4-1
Severity: normal

-- Please type your report below this line

I have a mutt with the following status line:

---Mutt: arc [Msg:15041 Post:1 48M]---(threads)-------------------

The postponed message has been recalled by another Mutt, so that
Mail/postponed is now an empty file, but I can't make the Post:1
disappear (note: NFS is not involved here, all data are local).

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/powerpc-linux/3.3.2/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc --disable-multilib powerpc-linux
Thread model: posix
gcc version 3.3.2 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.5.1i (2003-11-05)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-newpmac (ppc) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.14)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/home/lefevre/share/mutt"
SYSCONFDIR="/home/lefevre/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.3.24.ats.parent_match.1
patch-1.5.1.vl.savehist.1


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-26 11:10:10 UTC rado
* Added comment:
{{{
I wonder whether buffy.c

   if (force)
      mutt_update_num_postponed ();

has any influence on this and therefore how "force" is produced.
}}}

--------------------------------------------------------------------------------
2005-10-05 12:57:00 UTC rado
* Added comment:
{{{
need more input from mutt-dev
}}}

* status changed to new

--------------------------------------------------------------------------------
2005-10-07 16:29:31 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2005-10-04 19:57:00 +0200, Rado Smiljanic wrote:
> Synopsis: status line not updated when postponed message recalled by another mutt
> 
> State-Changed-From-To: open->chatting
> State-Changed-By: rado
> State-Changed-When: Tue, 04 Oct 2005 19:57:00 +0200
> State-Changed-Why:
> need more input from mutt-dev

More precisely, the *displayed* number of postponed messages can
change when a message is postponed, but not when a message is
recalled by another Mutt instance. But this doesn't seem to be
always reproducible.

Assuming the number of postponed messages is displayed in the
status line (at least when it is > 0):

1. Start a Mutt instance (A).
2. Start a second Mutt instance (B).
3. In Mutt instance (A), compose a message and postpone it.
   You'll get "Post:1" in this instance.
4. Do <refresh> (here, ^L) in Mutt instance (B).
   You'll get "Post:1" too.
5. In Mutt instance (A), compose another message and postpone it.
   You'll get "Post:2" in this instance.
6. Do <refresh> in Mutt instance (B). You'll get "Post:2" too.
7. Recall a postponed message from Mutt instance (A) and cancel it.
   You'll get "Post:1" in this instance.

Now, here's the bug:

8. Do <refresh> in Mutt instance (B). -> Nothing happens: "Post:2"
   is still displayed (instead of "Post:1").

You can go on:

9. Recall the remaining postponed message from Mutt instance (A) and
   cancel it. The status line shows no postponed messages.
10. Do <refresh> in Mutt instance (B). -> Nothing happens: "Post:2"
    is still displayed.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2005-10-07 16:43:59 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
More information, with only 1 postponed message...

1. Start a Mutt instance (A).
2. Start a second Mutt instance (B).

Mail/postponed:
atime   1128634688
mtime   1128634654
ctime   1128634658

3. In Mutt instance (A), compose a message and postpone it.
   You'll get "Post:1" in this instance.

Mail/postponed:
atime   1128634728
mtime   1128634728
ctime   1128634728

4. Do <refresh> (here, ^L) in Mutt instance (B).
   You'll get "Post:1" too.

Mail/postponed:
atime   1128634745
mtime   1128634728
ctime   1128634728

5. Recall the postponed message from Mutt instance (A) and cancel it.
   The status line shows no postponed messages.

Mail/postponed:
atime   1128634763
mtime   1128634728
ctime   1128634763

6. Do <refresh> in Mutt instance (B). -> Nothing happens: "Post:1"
   is still displayed.

Mail/postponed:
atime   1128634763
mtime   1128634728
ctime   1128634763

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}
