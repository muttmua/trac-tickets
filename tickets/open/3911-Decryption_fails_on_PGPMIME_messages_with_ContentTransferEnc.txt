Ticket:  3911
Status:  new
Summary: Decryption fails on PGP/MIME messages with Content-Transfer-Encoding: base64

Reporter: warai_otoko
Owner:    mutt-dev

Opened:       2017-01-24 19:55:54 UTC
Last Updated: 2017-01-24 19:55:54 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
Mutt is unable to decrypt PGP/MIME messages from a Microsoft Exchange user I've been talking to. Attempting to read the messages just fails with "Could not decrypt PGP message".

I inspected the messages to see whether they were malformed somehow, and noticed that the "PGP/MIME version identification" and "OpenPGP encrypted message" parts were both base64 encoded. I tried editing the messages to remove the "Content-Transfer-Encoding: base64" lines and decode the message parts, and mutt was able to decrypt and read the modified messages.

I've tried this with both the classic and GPGME backends, and the behavior is the same in both cases.

Procedure to reproduce: Attempt to open a multipart/encrypted message whose parts are base64 encoded.
