Ticket:  2754
Status:  new
Summary: [wish] add capability to jump back to last read with 1 keystroke

Reporter: blacktrash@gmx.net
Owner:    mutt-dev

Opened:       2007-02-12 08:55:33 UTC
Last Updated: 1970-01-01 00:00:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Add feature to jump back to any last read message with one keystroke.
Some newsreaders have this (eg. Slrn, bound to "l" by default).
This would be an improvement to a mail _reader_.
ATM also can't be worked around with macros.
>How-To-Repeat:
>Fix:
Unknown
}}}
