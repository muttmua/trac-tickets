Ticket:  3036
Status:  new
Summary: reply to tagged does not untag

Reporter: ossi
Owner:    mutt-dev

Opened:       2008-02-25 21:10:10 UTC
Last Updated: 2008-02-25 21:50:06 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
mark multiple messages in the index, hit "reply". so far, everything is fine. however, after returning to the index, the messages are still tagged. this highly unexpected, if not even dangerous (deleting messages afterwards isn't exactly exceptional).


--------------------------------------------------------------------------------
2008-02-25 21:50:06 UTC Moritz Barsnick
* Added comment:
{{{
Happens with "bounce" as well.

I've gotten very used to this behavior and have gotten careful. (Press
"^T ." afterwards to untag all.) Somewhat annoying though. (Might be
avoided by redefining keys.)
}}}
