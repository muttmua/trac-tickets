Ticket:  3177
Status:  new
Summary: mutt wish: send_charset default "us-ascii:utf-8"

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-01-28 21:26:50 UTC
Last Updated: 2009-06-30 14:38:09 UTC

Priority:  minor
Component: charset
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarded from http://bugs.debian.org/441950

---

Currently the send_charset default compiled into mutt
is "us-ascii:iso-8859-1:utf-8".

In the third millenium, with virtually all software supporting
UTF-8 and most users using UTF-8 locales, it would be nice if
this could be changed to "us-ascii:utf-8".

This would e.g. make the sent-mail folder containing only UTF-8 and
not a mixture of two incompatible charsets.



--------------------------------------------------------------------------------
2009-01-29 03:29:17 UTC vinc17
* cc changed to vincent@vinc17.org
* Added comment:
Well, when it can be used, ISO-8859-1 is more compact than UTF-8. Though this may not be significant in most cases, I would prefer to keep the current default.

--------------------------------------------------------------------------------
2009-02-03 20:19:08 UTC myon
* Added comment:
As much as I like utf-8, I think the current default makes sense.

--------------------------------------------------------------------------------
2009-02-10 12:11:10 UTC pdmef
* Added comment:
+1 for keeping the current value.

--------------------------------------------------------------------------------
2009-02-10 12:14:15 UTC antonio@dyne.org
* Added comment:
Hi,
please resolve this if you think that this won't be implemented, so we can resolve the debian bug on our side :-)

Cheers
Antonio

--------------------------------------------------------------------------------
2009-02-10 16:57:55 UTC bunk
* cc changed to vincent@vinc17.org, bunk
* Added comment:
A short version of the discussion in the Debian bug some people here might not have read:

It only makes a size difference if you use non-ASCII characters AND no characters outside iso-8859-1 (like the € sign) in an email. And the size advantage in these cases would typically be something around 1%, so not really noticable.

Having more charsets in the mix can cause problems when sending patches in the body of an email (e.g. when submitting patches to linux-kernel, where patches in the mail body are the only accepted form of submitting patches to the mailinglist).

And one single character in the email making the charset flip from iso-8859-1 to the incompatible utf-8 makes such problems semi-random.


--------------------------------------------------------------------------------
2009-02-10 20:18:15 UTC blacktrash
* Added comment:
Replying to [comment:5 bunk]:
> A short version of the discussion in the Debian bug some people here might not have read:
> 
> It only makes a size difference if you use non-ASCII characters AND no characters outside iso-8859-1 (like the € sign) in an email.

But € is part of iso-8859-15, so

set send_charset="us-ascii:iso-8859-1:iso-8859-15:windows-1252:utf-8"

would choose iso-8859-15.

--------------------------------------------------------------------------------
2009-02-10 20:27:19 UTC bunk
* Added comment:
Replying to [comment:6 blacktrash]:
> Replying to [comment:5 bunk]:
> > A short version of the discussion in the Debian bug some people here might not have read:
> > 
> > It only makes a size difference if you use non-ASCII characters AND no characters outside iso-8859-1 (like the € sign) in an email.
> 
> But € is part of iso-8859-15, so
> 
> set send_charset="us-ascii:iso-8859-1:iso-8859-15:windows-1252:utf-8"
> 
> would choose iso-8859-15.

There are no limits in how complicated you could make this if you desperately want to avoid utf-8.

But why?

Having iso-8859-1 preferred over UTF-8 was a good choice back in 2000 when $send_charset was set this way in init.h, since back then UTF-8 support in MUAs was not always good.

Now in 2009 that's no longer a problem.

Globally, the move from a gazillion different charsets to UTF-8 is a huge improvement.

People send MBs of attachments, and many emails are anyway Spam, so what's the real gain of having everything more complicated (and with semi-random problems since one character can change the encoding completely) just for saving a few bytes in some cases?

--------------------------------------------------------------------------------
2009-02-11 02:44:03 UTC vinc17
* Added comment:
Hmm... I forgot spam filtering. Charset information can be taken into account in antispam rules (without having to analyze the body). For instance, with iso-8859-1, one is certain that this isn't Russian spam.

Concerning other uses, different users have different needs. Some users may prefer UTF-8, but other users may prefer ISO-8859-1 (e.g. still used very much in LaTeX files, at least here in France).

Let's also mention that emacs 21 is still used by many people (e.g. under Debian/stable, and though the next stable version will be released soon, not all users will upgrade immediately, for various reasons) and its UTF-8 support is broken. So, no, ISO-8859-1 is not dead (yet).

Now, with good tools (such as Mutt), the internal encoding of e-mail messages should not matter concerning the behavior. The size of (encoded) messages can still matter, even though this isn't really significant in most cases. Note that the 1% mentioned above may be something like 10% in some cases.

--------------------------------------------------------------------------------
2009-06-30 14:38:09 UTC pdmef
* component changed to charset
