Ticket:  3390
Status:  new
Summary: chdir dialog is nearly useless (and crashes mutt)

Reporter: qwqwqw
Owner:    mutt-dev

Opened:       2010-03-08 10:52:11 UTC
Last Updated: 2010-03-18 03:47:58 UTC

Priority:  major
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi there,

I have severe problems when trying to work with
deep directory structures. Main problem is when
saving attachments. I can hardly navigate to a
specfic directory from within mutt:

1.) Whats the point of the chdir dialog?

v (view attachment)
s (save to file)
starting to enter directory name
use tab twice to open chdir dialog
i can navigate directorys
y goes back to the point where i pressed tab twice

when trying to make a bugreport for this
i found something even worse:

2.) I can crash mutt with chdir dialog

v (view attachment)
s (save to file)
starting to enter directory name
use tab twice to open chdir dialog
navigate to a directory
c (shows current directory)
enter (dialog line disappears)
enter (screen goes blank)
enter (segfault)

debian lenny i386 mutt-patched 1.5.18-6

--------------------------------------------------------------------------------
2010-03-08 10:54:11 UTC qwqwqw
* Added comment:
(repost with better formatting)

I have severe problems when trying to work with
deep directory structures. Main problem is when
saving attachments. I can hardly navigate to a
specfic directory from within mutt:

1.) Whats the point of the chdir dialog?

*v (view attachment)
*s (save to file)
*starting to enter directory name
*use tab twice to open chdir dialog
*i can navigate directorys
*y goes back to the point where i pressed tab twice (wtf?)

when trying to make a bugreport for this
i found something even worse:

2.) I can crash mutt with chdir dialog

*v (view attachment)
*s (save to file)
*starting to enter directory name
*use tab twice to open chdir dialog
*navigate to a directory
*c (shows current directory)
*enter (dialog line disappears)
*enter (screen goes blank)
*enter (segfault)

debian lenny i386 mutt-patched 1.5.18-6

--------------------------------------------------------------------------------
2010-03-09 00:24:58 UTC brendan
* Added comment:
I agree that's a pretty strange interface. But I can't reproduce 2. Can you, with the current version of mutt?

* status changed to infoneeded_new

--------------------------------------------------------------------------------
2010-03-09 12:03:35 UTC qwqwqw
* Added comment:
sorry, i cannot upgrade to debian/sid (would break too many other things)

compiling myself 1.5.20 + (sidebar 1.5.20) did work but that installation behaves different:
* sidebar wont show up (although the sidebar patch is installed)
* color scheme is not being honoured (have_color and everything is there)
(i assume differences of the config for that, but the debian config does not work with the custom compile - something with compressed folders?)
* and finally the bug does not occur anymore (however the chdir dialog is still useless :-)

Here my config:
~> mutt -v
Mutt 1.5.20 (2009-06-14)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.32.8 (i686)
ncurses: ncurses 5.7.20090124 (compiled with 5.7)
libidn: 1.10 (compiled with 1.10)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
-USE_POP  -USE_IMAP  +USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/home/qwqwqw/temp/mutt/share/mutt"
SYSCONFDIR="/home/qwqwqw/temp/mutt/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

patch-1.5.20.sidebar.20090619.txt

* status changed to new

--------------------------------------------------------------------------------
2010-03-18 03:47:58 UTC shallpion
* Added comment:
I recently noticed the usage of the "chdir" command. I manage several IMAP accounts and was very annoyed by the strange phenomenon that if I change to an IMAP folder, the "current directory" does not change as well, which means if I press <change-folder> and press <tab> I am still in default directory. This was frustrating to me because I sometimes want to view some other folders instead inbox from the IMAP account. So I use folder-hook alont with push to chdir to realize this function. But to be honest I don't quite understand why mutt doesn't change directory along with the changing of folders. :( But anyway chdir is currently very useful for me. 

But I cannot reproduce the crash problem. I am using mutt-1.5.20-r13 in gentoo.
