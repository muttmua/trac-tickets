Ticket:  3479
Status:  new
Summary: Incorrect MIME header decode

Reporter: gul
Owner:    mutt-dev

Opened:       2010-12-16 19:47:43 UTC
Last Updated: 2010-12-16 19:47:43 UTC

Priority:  major
Component: MIME
Keywords:  

--------------------------------------------------------------------------------
Description:
Header line
{{{
From: =?KOI8-R?B?7MXbwT1BLkRlc2hlcmU=?= http://sos.karelia.ru/desher/ <deshere@rambler.ru>
}}}
displays as
{{{
From: =?KOI8-R?B?7MXbwT1BLkRlc2hlcmU=?= http:
        "//sos.karelia.ru/desher/" <deshere@rambler.ru>
}}}
but not as 
{{{
From: Леша=A.Deshere http://sos.karelia.ru/desher/ <deshere@rambler.ru>
}}}
which is correct AFAIU.
I can get additional info (full header, mutt config) if it's needed for reproducing.
