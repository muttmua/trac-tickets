Ticket:  1605
Status:  new
Summary: MIME header copied into mail reply with 1.5.4i

Reporter: David Shaw <dshaw@jabberwocky.com>
Owner:    mutt-dev

Opened:       2003-08-02 10:51:09 UTC
Last Updated: 2009-06-30 14:32:48 UTC

Priority:  minor
Component: MIME
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4i
Severity: normal

-- Please type your report below this line

I noticed that when replying to some PGP-signed messages from Kmail, I
get a "Content-Description: signed data" at the top of the reply.

Attached is a mail that can duplicate this.  Just load the message and
try to reply to it.  I'm not sure if this is a mutt issue or a Kmail
issue.

-- System Information
System Version: Linux claude.jabberwocky.com 2.4.18-3 #1 Thu Apr 18 07:37:53 EDT 2002 i686 unknown
RedHat Release: Red Hat Linux release 7.3 (Valhalla)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/2.96/specs
gcc version 2.96 20000731 (Red Hat Linux 7.3 2.96-112)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-3 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.
}}}

--------------------------------------------------------------------------------
2003-08-02 10:51:09 UTC David Shaw <dshaw@jabberwocky.com>
* Added attachment experiment
* Added comment:
experiment

--------------------------------------------------------------------------------
2003-08-03 06:46:20 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
Well, that's supposed to be a feature -- Kmail adds the
content-description, and mutt keeps the content description for any
attachments it quotes.

(I'm indifferent about this, and could quite well live with making
the behavior either optional, adding a quote tag in front of the
description, and whatever else.)

Suggestions and opinions welcome.
-- 
Thomas Roessler			      <roessler@does-not-exist.org>




On 2003-08-01 23:51:09 -0400, David Shaw wrote:
> From: David Shaw <dshaw@jabberwocky.com>
> To: submit@bugs.guug.de
> Date: Fri, 1 Aug 2003 23:51:09 -0400
> Subject: bug#1605: mutt-1.5.4i: MIME header copied into mail reply with 1.5.4i
> Reply-To: David Shaw <dshaw@jabberwocky.com>, 1605@bugs.guug.de
> X-Spam-Level: 
> 
> Package: mutt
> Version: 1.5.4i
> Severity: normal
> 
> -- Please type your report below this line
> 
> I noticed that when replying to some PGP-signed messages from Kmail, I
> get a "Content-Description: signed data" at the top of the reply.
> 
> Attached is a mail that can duplicate this.  Just load the message and
> try to reply to it.  I'm not sure if this is a mutt issue or a Kmail
> issue.
> 
> -- System Information
> System Version: Linux claude.jabberwocky.com 2.4.18-3 #1 Thu Apr 18 07:37:53 EDT 2002 i686 unknown
> RedHat Release: Red Hat Linux release 7.3 (Valhalla)
> 
> -- Build environment information
> 
> (Note: This is the build environment installed on the system
> muttbug is run on.  Information may or may not match the environment
> used to build mutt.)
> 
> - gcc version information
> gcc
> Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/2.96/specs
> gcc version 2.96 20000731 (Red Hat Linux 7.3 2.96-112)
> 
> - CFLAGS
> -Wall -pedantic -g -O2
> 
> -- Mutt Version Information
> 
> Mutt 1.5.4i (2003-03-19)
> Copyright (C) 1996-2002 Michael R. Elkins and others.
> Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
> Mutt is free software, and you are welcome to redistribute it
> under certain conditions; type `mutt -vv' for details.
> 
> System: Linux 2.4.18-3 (i686) [using ncurses 5.2]
> Compile options:
> -DOMAIN
> +DEBUG
> -HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
> +USE_FCNTL  -USE_FLOCK
> +USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
> +HAVE_REGCOMP  -USE_GNU_REGEX  
> +HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
> +HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
> +CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
> +ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
> +HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
> ISPELL="/usr/bin/ispell"
> SENDMAIL="/usr/sbin/sendmail"
> MAILPATH="/var/mail"
> PKGDATADIR="/usr/local/share/mutt"
> SYSCONFDIR="/usr/local/etc"
> EXECSHELL="/bin/sh"
> -MIXMASTER
> To contact the developers, please mail to <mutt-dev@mutt.org>.
> To report a bug, please use the flea(1) utility.

> >From foo@bar Thu Jul 17 10:12:34 2003
> From: someone@example.com
> To: someoneelse@example.com
> Subject: Weird header problem
> Date: Thu, 17 Jul 2003 16:11:47 +0200
> User-Agent: KMail/1.5.1
> MIME-Version: 1.0
> Content-Type: multipart/signed;
>   protocol="application/pgp-signature";
>   micalg=pgp-sha1;
>   boundary="Boundary-02=_s6qF/4lAYZvABJO";
>   charset="iso-8859-1"
> Content-Transfer-Encoding: 7bit
> Content-Length: 1397
> 
> 
> --Boundary-02=_s6qF/4lAYZvABJO
> Content-Type: text/plain;
>   charset="iso-8859-1"
> Content-Transfer-Encoding: quoted-printable
> Content-Description: signed data
> Content-Disposition: inline
> 
> This is the main part of the email.
> 
> --Boundary-02=_s6qF/4lAYZvABJO
> Content-Type: application/pgp-signature
> Content-Description: signature
> 
> This is the signature.
> 
> --Boundary-02=_s6qF/4lAYZvABJO--
}}}

--------------------------------------------------------------------------------
2003-08-03 11:02:42 UTC David Shaw <dshaw@jabberwocky.com>
* Added comment:
{{{
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

On Sat, Aug 02, 2003 at 01:46:20PM +0200, Thomas Roessler wrote:
> Well, that's supposed to be a feature -- Kmail adds the
> content-description, and mutt keeps the content description for any
> attachments it quotes.
> 
> (I'm indifferent about this, and could quite well live with making
> the behavior either optional, adding a quote tag in front of the
> description, and whatever else.)
> 
> Suggestions and opinions welcome.

I think the ideal would be to have the behavior optional, and also put
a quote tag in front of the description (to me, the
content-description line is part of the attachment, and should be
quoted along with it).

How to best add the ability to turn this on and off?  A new switch, or
perhaps use the 'ignore' command (content-description is sort of a
header) ?

David
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.3rc2 (GNU/Linux)
Comment: Key available at http://www.jabberwocky.com/david/keys.asc

iEYEARECAAYFAj8siWIACgkQ4mZch0nhy8ngjQCdHBFSW+LE54xY68zzc+WSnDDG
JpoAoL1YzfBF/nDKVNNcyzhcjI/kvVC1
=hoLG
-----END PGP SIGNATURE-----
}}}

--------------------------------------------------------------------------------
2005-09-05 13:12:06 UTC brendan
* Added comment:
{{{
Refile as change-request.
}}}

--------------------------------------------------------------------------------
2005-09-17 07:48:52 UTC ab
* Added comment:
{{{
Upload patch-1.5.10.ab.quote_description.2, tag patch,
deduppe unform.
}}}

--------------------------------------------------------------------------------
2005-09-17 08:59:36 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello David, thanks for the report,

 On Sunday, August 3, 2003 at 12:02:42 AM -0400, David M. Shaw wrote:

> On Sat, Aug 02, 2003 at 01:46:20PM +0200, Thomas Roessler wrote:
>> feature -- Kmail adds the content-description, and mutt keeps the
>> content description for any attachments it quotes.
> I think the ideal would be to have the behavior optional, and also put
> a quote tag in front of the description (to me, the
> content-description line is part of the attachment, and should be
> quoted along with it). How to best add the ability to turn this on and
> off? A new switch, or perhaps use the 'ignore' command
> (content-description is sort of a header) ?

    I agree the quoted CDesc (and Content-Disposition name parameter)
should get $indent_string prepended.

    If it makes sense to quote CDesc in replies to multipart parts, it
probably make sense to quote CDesc in replies to monoparts also. So I
moved the code from end of multipart_handler() to beginning of
mutt_body_handler() which is called for both main body and each part.

    Finally I'd like to not make the feature disablable. I consider it
to probably not justify Yet Another Variable. I would not like to
overload "ignore" meaning (one may want to quote Cdesc but not see it in
pager). And after all the replier is already expected to trim quoted
text, so one line more to trim seems to me hopefully bearable.

    Uploaded patch-1.5.10.ab.quote_description.2 doing all this.


Bye!	Alain.
-- 
set honor_followup_to=yes in muttrc is the default value, and makes your
list replies go where the original author wanted them to go: Only to the
list, or with a private copy.
}}}

--------------------------------------------------------------------------------
2005-09-17 13:17:35 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2005-09-16 20:05:01 +0200, Alain Bench wrote:

>      I agree the quoted CDesc (and Content-Disposition name parameter)
>  should get $indent_string prepended.

I don't think so -- it's actually metainformation about the
attachment, much like the tag line above.  I really think it makes
sense not to quote it.

Regards,
--=20
Thomas Roessler =B7 Personal soap box at <http://log.does-not-exist.org/>.
}}}

--------------------------------------------------------------------------------
2005-09-18 16:04:34 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hi Thomas!

 On Friday, September 16, 2005 at 8:25:01 PM +0200, Thomas Roessler wrote:

Content-Description: a followup to a bug report
CDisp-name:
> On 2005-09-16 20:05:01 +0200, Alain Bench wrote:
>> I agree the quoted CDesc (and Content-Disposition name parameter)
>> should get $indent_string prepended.
> I don't think so -- it's actually metainformation about the
> attachment, much like the tag line above. I really think it makes
> sense not to quote it.

    Tag line == attribution line? Yes, description and name are
metainformations. So no ">". Uploaded ab.quote_description.3 doing so.
But then description looks really as a header leaking to the reply body,
triggering this bug report. We are not accustomed to see unannounced
metainfos in body, out of attribution or forward separator... Hum...
Modeled after forward, what about:

----- Content-Description: a followup to a bug report -----

    ...or shorter and less MIMEish:

----- Description: a followup to a bug report -----


    There is also quoted the Content-Disposition name parameter. This is
very rare. Why is this one quoted, and not Content-Type name parameter?


Bye!	Alain.
-- 
Give your computer's unused idle processor cycles to a scientific goal:
The Folding@home project at <URL:http://folding.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2005-09-18 17:23:47 UTC ab
* Added comment:
{{{
Hello David, thanks for the report,
 On 2005-09-16 20:05:01 +0200, Alain Bench wrote:
 upload patch-1.5.11.ab.quote_description.3
}}}

--------------------------------------------------------------------------------
2009-06-30 14:32:48 UTC pdmef
* component changed to MIME
* Updated description:
{{{
Package: mutt
Version: 1.5.4i
Severity: normal

-- Please type your report below this line

I noticed that when replying to some PGP-signed messages from Kmail, I
get a "Content-Description: signed data" at the top of the reply.

Attached is a mail that can duplicate this.  Just load the message and
try to reply to it.  I'm not sure if this is a mutt issue or a Kmail
issue.

-- System Information
System Version: Linux claude.jabberwocky.com 2.4.18-3 #1 Thu Apr 18 07:37:53 EDT 2002 i686 unknown
RedHat Release: Red Hat Linux release 7.3 (Valhalla)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/2.96/specs
gcc version 2.96 20000731 (Red Hat Linux 7.3 2.96-112)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-3 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.
}}}
