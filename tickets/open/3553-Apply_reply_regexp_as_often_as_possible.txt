Ticket:  3553
Status:  new
Summary: Apply reply_regexp as often as possible

Reporter: EdSchouten
Owner:    mutt-dev

Opened:       2011-12-01 09:45:36 UTC
Last Updated: 2011-12-01 09:45:36 UTC

Priority:  minor
Component: mutt
Keywords:  reply_regexp

--------------------------------------------------------------------------------
Description:
By default, we set reply_regexp as follows:

{{{
"^(re([\[0-9\]+])*|aw):[ \t]*"
}}}

Now if you receive an email that has a subject that starts with
something like "Re: Re: Re:", added by broken mail clients, Mutt will
only trim the first Re: tag when replying or showing it in the index.

There are a lot of people that work around this by setting reply_regexp
to something like this:

{{{
"^((re([\[0-9\]+])*|aw):[ \t]*)+"
}}}

In my opinion this behaviour should be the default. Alternatively, we
could solve this internally by changing the code to apply ReplyRegexp as often as possible.

Attached is a patch that implements this.

--------------------------------------------------------------------------------
2011-12-01 09:46:19 UTC EdSchouten
* Added attachment reply-regexp-repeat.txt
* Added comment:
Patch that changes mutt to reply the ReplyRegexp multiple times
