Ticket:  3491
Status:  new
Summary: hangs when IP address changes

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2011-01-01 20:01:51 UTC
Last Updated: 2012-04-29 19:10:11 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.mutt.org/599136

{{{
I use mutt with an IMAP server (imap.crustytoothpaste.net, running
Dovecot).  My IMAP connection is always encrypted.  I'm running mutt on
my laptop, so often I forget to quit mutt before I put the lid down.
When I resume my laptop, I am often at another location with a different
IP address.  When I hit . to refresh the buffer list or pretty much
anything else, mutt becomes unresponsive and I cannot do anything, even
quit, for some amount of time (probably until the network times out).
Even Ctrl-C doesn't work.  I'm forced to wait or use Ctrl-\.

I'd really appreciate it if mutt could be more responsive in this case.
I would prefer if it would detect that the network address had changed
or even that the connection it once had does not exist anymore.  I would
even be satisfied if I could just quit and restart mutt (even if I had
to interrupt it with Ctrl-C to make it responsive again).
}}}


--------------------------------------------------------------------------------
2012-04-27 10:44:40 UTC antonio@dyne.org
* Added comment:
Hi guys,
this is still an issue and it is causing problems to everyone who needs to switch their connection without closing and reopening mutt in the process.

Any change that you could have a look at this?

--------------------------------------------------------------------------------
2012-04-29 19:10:11 UTC petr_p
* Added comment:
I don't think mutt (or any generic application) should watch local IP addresses. This is job for kernel to close stream connections after loosing local address.

Though mutt should be able to interrupt network operation on user request. If server becomes unresponsive, or connection is to slow, it's useful to be able to abort e.g. fetching e-mail body.

I think simple SIGINT handler could set a flag and EINTRed syscall loop could break on the flag.

Also there could be possibility to adjust network time-out (SO_RCVTIMEO, SO_SNDTIMEO) if default system-wide value is not suitable (like connect_timeout exists for establishing a connection).
