Ticket:  3972
Status:  reopened
Summary: "set reverse_name" should scan "To:"->"CC:"->"From:" in order

Reporter: aoki.osamu
Owner:    mutt-dev

Opened:       2017-09-24 09:07:11 UTC
Last Updated: 2017-10-11 14:48:22 UTC

Priority:  major
Component: mutt
Keywords:  reverse_name

--------------------------------------------------------------------------------
Description:
The alternates regex match seems to be done over the header as they appear in it.  But considering it is supposed to meant to match address to be used for "From", it should place "From" as the last resort choice. (Excluding From for scanning is not a good idea since you may encounter this when you post to ML post and follow up etc.)

This is usually not a major problem but we can do better. When you try to test mail system between your accounts, your receiving end doesn't get to set as From address.

To solve this problem, we need a fine touch to order this scanning order to be:

  "To:"->"CC:"->"From:" in order

This problem also causes a secondary problem of setting "To:" field not to the old-From but to old-To address since the old-From is already used as the new-From.  Not so nice.  


--------------------------------------------------------------------------------
2017-09-24 13:11:29 UTC kevin8t8
* Added comment:
I think I am misunderstanding the problem.  The current code **already** scans "To" first, then "Cc", and finally the "From" header, in that order.

--------------------------------------------------------------------------------
2017-10-06 01:59:04 UTC kevin8t8
* Added comment:
Since I haven't heard back, I'm going to close this ticket.  If you believe there is an actual problem, feel free to reopen with more information.

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2017-10-11 14:48:22 UTC aoki.osamu
* Added comment:
{{{
Hi,

On Sun, Sep 24, 2017 at 01:11:30PM -0000, Mutt wrote:

If that is true, I don't understand the reason why I had problem.
It looked "From" first.

Anyway, I will report this again if I get this in a clearly presentable
case.

Thanks.

Osamu
}}}

* resolution changed to 
* status changed to reopened
