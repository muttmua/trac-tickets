Ticket:  3171
Status:  new
Summary: mutt: Incorrect attribution line when replying to mail with multiple From addresses

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-01-26 23:11:14 UTC
Last Updated: 2009-01-26 23:11:14 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/472397

---

When replying to a mail with multiple From addresses (such as this
mail), mutt generates an attribution line which only mentions the first
name listed in From.  It should attribute all the authors of the quoted
mail.

Ideally, it would join two author names with "and", and join three or
more author names with commas and add an "and" before the last.
However, the exact form of the attribution doesn't matter as much; it
just needs to attribute all authors of the quoted mail.

