Ticket:  949
Status:  new
Summary: mutt -Z should not clear the screen if there is no new mail

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2002-01-07 14:48:31 UTC
Last Updated: 2007-04-12 16:58:47 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
[NOTE: this bug report has been submitted to the debian BTS as Bug!#127544.
Please Cc all your replies to 127544@bugs.debian.org.]
{{{
From: Gonzalo Paniagua Javier <gpanjav@jazzfree.com>
Subject: mutt: Behavior of the -Z option
Date: Thu, 03 Jan 2002 00:06:58 +0100
}}}
It would be nice if the -Z option simply shows the message saying that there are no mailboxes with new mail without clearing the screen first.

--------------------------------------------------------------------------------
2005-08-02 12:33:54 UTC brendan
* Added comment:
{{{
switched to change-request.
}}}

--------------------------------------------------------------------------------
2007-04-12 16:58:47 UTC brendan
* Updated description:
[NOTE: this bug report has been submitted to the debian BTS as Bug!#127544.
Please Cc all your replies to 127544@bugs.debian.org.]
{{{
From: Gonzalo Paniagua Javier <gpanjav@jazzfree.com>
Subject: mutt: Behavior of the -Z option
Date: Thu, 03 Jan 2002 00:06:58 +0100
}}}
It would be nice if the -Z option simply shows the message saying that there are no mailboxes with new mail without clearing the screen first.
* summary changed to mutt -Z should not clear the screen if there is no new mail
