Ticket:  655
Status:  new
Summary: color-only markers for line wrap indicators

Reporter: Jamie Heilman <jamie@audible.transient.net>
Owner:    mutt-dev

Opened:       2001-06-11 07:32:55 UTC
Last Updated: 2009-06-30 14:52:00 UTC

Priority:  trivial
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.2.5-4
Severity: wishlist

It would be really nice if when combining 'unset markers' and
'set smart_wrap' the color for markers was applied to the first character
in the wrapped line still.  This would make it easy to see when lines are
being wrapped (on color terminals anyway) but not interfere with the
ability to easily cut and paste chunks of wrapped text without extraneous
characters.  The old behavoir could ofcourse be simulated by setting the
marker color to the color of normal text, should someone find this new
behavior undesireable.

-- 
Jamie Heilman                   http://audible.transient.net/~jamie/
"Most people wouldn't know music if it came up and bit them on the ass."
                                                        -Frank Zappa


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-06 17:16:06 UTC brendan
* Added comment:
{{{
Refiled as change-request.
}}}

--------------------------------------------------------------------------------
2009-06-30 14:52:00 UTC pdmef
* component changed to display
* Updated description:
{{{
Package: mutt
Version: 1.2.5-4
Severity: wishlist

It would be really nice if when combining 'unset markers' and
'set smart_wrap' the color for markers was applied to the first character
in the wrapped line still.  This would make it easy to see when lines are
being wrapped (on color terminals anyway) but not interfere with the
ability to easily cut and paste chunks of wrapped text without extraneous
characters.  The old behavoir could ofcourse be simulated by setting the
marker color to the color of normal text, should someone find this new
behavior undesireable.

-- 
Jamie Heilman                   http://audible.transient.net/~jamie/
"Most people wouldn't know music if it came up and bit them on the ass."
                                                        -Frank Zappa


>How-To-Repeat:
>Fix:
}}}
