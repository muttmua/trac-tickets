Ticket:  3656
Status:  new
Summary: "TLS connection established" message causes an annoying delay

Reporter: grawity
Owner:    brendan

Opened:       2013-10-18 17:05:02 UTC
Last Updated: 2013-10-18 17:05:02 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
Whenever Mutt makes a SSL/TLS connection, the ssl_negotiate() function displays a message along the lines of "TLS connection established using ''FOO'' (''BAR'')".

This message causes a '''really annoying''' one-second delay. By which I mean that it makes the startup '''four to five times slower''', because the TLS handshake itself only takes 0.30 seconds in my case.

For now, I recompile Mutt myself, with the calls to mutt_message(…)/mutt_sleep(0) commented out, but it would be really nice if it was somehow fixed upstream – e.g. by getting rid of the message completely, or by removing the delay...

(Applies to all recent versions of Mutt – currently using 1.5.22 on Arch and 1.5.21 on Debian.)
