Ticket:  3694
Status:  new
Summary: The manual leaves out a place where "mailcap" can be automatically found

Reporter: balderdash
Owner:    mutt-dev

Opened:       2014-06-16 23:22:38 UTC
Last Updated: 2014-06-16 23:25:31 UTC

Priority:  minor
Component: doc
Keywords:  

--------------------------------------------------------------------------------
Description:
In the manual (dev version), there is a list of places where mutt will look for a mailcap file.  I understand the manual as saying that these are the only places mutt will automatically check for a mailcap.  If that's what the manual is supposed to mean, then it's wrong; I have found experimentally that ~/.mutt/mailcap is automatically picked up.

(On Mac OSX, all versions).

--------------------------------------------------------------------------------
2014-06-16 23:24:36 UTC balderdash
* Added comment:
I forgot to add that the result of running "mutt -nF /dev/null -Q mailcap_path", which is recommended by the manual, does not have ~/.mutt/mailcap in there.  Nevertheless, I can tweak the file ~/.mutt/mailcap and the tweaks take effect in mutt.  (No, I don't have a symlink to that file sitting at ~/.mailcap).

--------------------------------------------------------------------------------
2014-06-16 23:25:31 UTC balderdash
* Added comment:
Arghhh, forgot also to add that: I have not in any way set the variable $mailcap_path.
