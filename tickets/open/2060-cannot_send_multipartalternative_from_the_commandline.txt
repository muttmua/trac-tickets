Ticket:  2060
Status:  new
Summary: cannot send multipart/alternative from the command-line

Reporter: kyle@commsecure.com.au
Owner:    mutt-dev

Opened:       2005-09-08 01:12:56 UTC
Last Updated: 2010-08-08 22:58:04 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
$ mutt -e "set content_type=multipart/alternative" -s test kyle@commsecure.com.au < /tmp/kkk
No boundary parameter found! [report this error]
No boundary parameter found! [report this error]
Could not send the message.
$ cat /tmp/kkk 
MIME-Version: 1.0
Content-Type: multipart/alternative; boundary="===============1195369947=="

--===============1195369947==
MIME-Version: 1.0
Content-Type: text/plain

text message
--===============1195369947==
MIME-Version: 1.0
Content-Type: text/html

<html><body>html message</body></html>
--===============1195369947==--
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2010-08-08 22:58:04 UTC me
* Updated description:
{{{
$ mutt -e "set content_type=multipart/alternative" -s test kyle@commsecure.com.au < /tmp/kkk
No boundary parameter found! [report this error]
No boundary parameter found! [report this error]
Could not send the message.
$ cat /tmp/kkk 
MIME-Version: 1.0
Content-Type: multipart/alternative; boundary="===============1195369947=="

--===============1195369947==
MIME-Version: 1.0
Content-Type: text/plain

text message
--===============1195369947==
MIME-Version: 1.0
Content-Type: text/html

<html><body>html message</body></html>
--===============1195369947==--
>How-To-Repeat:
>Fix:
Unknown
}}}
* milestone changed to 
* type changed to enhancement
