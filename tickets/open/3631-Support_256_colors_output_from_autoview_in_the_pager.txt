Ticket:  3631
Status:  new
Summary: Support 256 colors output from autoview in the pager

Reporter: ptaff
Owner:    mutt-dev

Opened:       2013-01-30 12:11:01 UTC
Last Updated: 2013-01-30 12:11:01 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
The pager currently supports the display of traditional 16-color ANSI output from autoview but does not support xterm-256color codes, neither with ncurses nor slang.

This was previously shortly discussed on mutt-users:
http://marc.info/?l=mutt-users&m=135869598705446&w=2
