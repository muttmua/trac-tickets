Ticket:  1090
Status:  new
Summary: bcc:s given away through encryption

Reporter: <bernhard@intevation.de>
Owner:    mutt-dev

Opened:       2002-03-07 14:33:26 UTC
Last Updated: 2008-07-09 09:32:06 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
When encrypting (with gpg) and having several bccs,
it is encrypted to them all. So the to: and cc: recipients
can see the bcc:ed people.

The Ägypten-Project (adding encryption to kmail) has a similiar problem.
Check their comments on how to solve this:

	http://intevation.de/rt/webrt?serial_num=948&display=History

--------------------------------------------------------------------------------
2007-03-27 20:50:04 UTC brendan
* Added comment:
{{{
dedupe
}}}

--------------------------------------------------------------------------------
2007-04-02 01:58:12 UTC brendan
* Updated description:
When encrypting (with gpg) and having several bccs,
it is encrypted to them all. So the to: and cc: recipients
can see the bcc:ed people.

The Ägypten-Project (adding encryption to kmail) has a similiar problem.
Check their comments on how to solve this:

	http://intevation.de/rt/webrt?serial_num=948&display=History

--------------------------------------------------------------------------------
2007-04-02 02:29:13 UTC brendan
* component changed to crypto

--------------------------------------------------------------------------------
2008-07-09 09:32:06 UTC pdmef
* Added comment:
Just in case the mail traffic isn't accessible any longer, the proposed solution is to send one message encrypted for To/Cc to these and one per Bcc encrypted to To/Cc/Bcc to the Bcc address, i.e. N+1 messages for N Bcc: recipients.
