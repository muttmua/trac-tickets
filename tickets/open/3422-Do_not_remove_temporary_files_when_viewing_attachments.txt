Ticket:  3422
Status:  new
Summary: Do not remove temporary files when viewing attachments

Reporter: madduck
Owner:    mutt-dev

Opened:       2010-06-16 11:33:50 UTC
Last Updated: 2010-06-17 23:55:34 UTC

Priority:  major
Component: mutt
Keywords:  temporary files attachments data loss

--------------------------------------------------------------------------------
Description:
When you press enter on an attachment, mutt saves it to a temporary
file and spawns the viewer, passing it the filename as $1. When the
viewer exits, mutt removes the file.

I can think of three reasons not to do this:

- Some viewers, like ooffice or iceweasel, return control to their
  caller before the application has actually started. In such cases,
  it can happen that the file is gone by the time the viewer tries
  to open it.

- I might inadverently edit an image or document I received as an
  attachment and risk losing my changes if I don't save to a new
  name.

To guard against the first, mutt could delay deleting by a few
seconds.

To guard against the second, mutt could check a timestamp, size, or
hashsum, or combination thereof, and only delete if those have not
changed.

However, I think mutt should just not delete the temporary file, if
it's an attachment. /tmp is set to autoclean itself on reboot
anyway, and tmpreaper is used wherever this doesn't suffice. Plus,
disk space is getting bigger (and so is /tmp), so I don't see the
need to clean up


--------------------------------------------------------------------------------
2010-06-16 17:11:19 UTC Gary Johnson
* Added comment:
{{{
On 2010-06-16, Mutt wrote:

This is unreliable.  I've had some viewers take almost 20 seconds to
finish reading an attachment after starting.  I could not find a
delay time that was long enough to be safe while keeping mutt
responsive.  I now use a different technique that gives each viewer
its own copy of the attachment and lets the viewer delete it.

It might be nice if there was some way to indicate to mutt in the
mailcap rules whether to delete the file itself or let the viewer
delete the file, though.  That might save me the trouble of copying
the temporary file, but I haven't thought it through.


Mutt uses a timestamp now to determine whether the user has edited a
message.  The same technique could work here.


   1.  /tmp is not always set to autoclean itself on reboot.
   2.  Some Unix systems are not rebooted for years.
   3.  Not all installations use tmpreaper or the equivalent.
   4.  Not all mutt users leave 'tmpdir' set to /tmp.
   5.  Newer systems may have huge disks; older systems may not.
   6.  Temporary files have to be cleaned up sometime and I would
       rather that tools delete them when they're through with them
       than require me to manage yet another cron job.

My $0.02.

Regards,
Gary
}}}

--------------------------------------------------------------------------------
2010-06-17 23:55:34 UTC Vincent Lefevre
* Added comment:
{{{
On 2010-06-16 11:33:51 -0000, Mutt wrote:

As a workaround, I use a wrapper to copy the attachment and run the
viewer on the copy. Note: a copy is necessary, a hard link doesn't
work because Mutt clears the file before removing it. Copies are
automatically removed after some time (a few days).


This is not sufficient: some viewers require the file to be still
there until the document is closed.
}}}
