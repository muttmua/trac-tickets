Ticket:  3581
Status:  new
Summary: Folder-history ignores -f folder from CLI

Reporter: Wastl
Owner:    mutt-dev

Opened:       2012-05-18 12:07:29 UTC
Last Updated: 2012-05-18 17:40:29 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When you start mutt giving -f folder on the command line, that folder is not included among the suggestions (already seen folders) which appear when (later in the session) you press 'c' to change folder and use <up-arrow> at the prompt.

For a fix, it may be enough to add something of the sense
    if (explicit_folder)
      mutt_history_add (HC_MBOX, folder, 1);
in main.c e.g. somewhere around line 994, only in this form it is not possible because of variable scope. A hack with a wrapper in history.c worked for me.

--------------------------------------------------------------------------------
2012-05-18 17:40:29 UTC vinc17
* Added comment:
IMHO this should be controlled by an option. I often use -f with temporary folders I'll never use again. So, I don't want them to be in the history.
