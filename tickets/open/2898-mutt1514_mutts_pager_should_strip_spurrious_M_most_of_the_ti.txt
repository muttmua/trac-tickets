Ticket:  2898
Status:  new
Summary: mutt-1.5.14: mutt's pager should strip spurrious ^M, most of the time

Reporter: John Hawkinson <jhawk@MIT.EDU>
Owner:    

Opened:       2007-05-29 03:34:07 UTC
Last Updated: 2011-04-13 11:23:00 UTC

Priority:  minor
Component: display
Keywords:  pager patch

--------------------------------------------------------------------------------
Description:
Some domains seem to manage to send e-mail where every linebreak is a
`\r\n`. I'm not sure why they do this. The only one I've encountered
with great regularity is DOLBY.COM, though there was one other I got
last week that I seem to've misplaced, but it caused me to put this
on my queue. They only seem to arrive in quoted-printable...

Anyhow, mutt's pager displays these messages with a `^M` at the end of
each line, which really looks awkward and hard to read, e.g.:
{{{
Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do^M
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad^M
minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip^M
ex ea commodo consequat. Duis aute irure dolor in reprehenderit in^M
voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur^M
sint occaecat cupidatat non proident, sunt in culpa qui officia^M
}}}
(Okay, it's worse when you're actually reading the language across
the line break.)

Attached is a patch such that mutt throws away `^M`s unless OPTWEED is
set, so that if you turn on full headers ('h'), these `^M`s are
displayed. If you'd prefer to handle it a different way, let me know
and I will re-spin the patch.

Previously, mutt unconditionally escaped all `\r` chars as "`^M`".

I realize this is the sender's fault, but I don't think I should have
to put up with it.  (I could fix this with a display_filter,
but then it would be harder to toggle, I'd pay a fork penalty,
and other mutt users wouldn't get the benefit).

(I realize the patch uses a goto -- I do think it's clearer to add an
elsif stanza for the `\r` case and use a goto to the control-character
label, rather either duplicating the control-char printing code in `\r`
stanza, or adding
{{{
  || wc=='\r' && !option(OPTWEED)
}}}
to the control characer case (line 1184, `wc < 0x20`...).
If you'd prefer a goto-less version, I can do that too...)
{{{
--- /afs/sipb.mit.edu/project/sipb/src/mutt/mutt-1.5.15/pager.c	Sun Apr  1 17:58:56 2007
+++ mutt-1.5.15/pager.c	Mon May 28 22:59:28 2007
@@ -1175,8 +1175,15 @@
       else
 	col = t;
     }
+    else if (wc == '\r')
+    {
+      if (!option(OPTWEED)) {
+	goto controlchar;
+      }
+    }
     else if (wc < 0x20 || wc == 0x7f)
     {
+    controlchar:
       if (col + 2 > wrap_cols)
 	break;
       col += 2;

2007-05-28 22:59 -0400  John Hawkinson  <jhawk@mit.edu>

	* pager.c: Hide ^Ms in messages when OPTWEED ('h': suppress full
	headers) is set. For those rare people who get email with linebreaks
	ending in \r\n.
}}}
Thanks.

--jhawk@mit.edu
  John Hawkinson

--------------------------------------------------------------------------------
2007-05-29 03:46:03 UTC jhawk
* component changed to display
* keywords changed to pager
* priority changed to minor
* type changed to enhancement

--------------------------------------------------------------------------------
2008-08-29 02:56:47 UTC jhawk
* keywords changed to pager patch
* version changed to 1.5.14

--------------------------------------------------------------------------------
2009-05-14 08:30:56 UTC madduck
* cc changed to dev.mutt.org@pobox.madduck.net
* Added comment:
Actually, the spurious ^M don't seem to be the sender's fault, unless they are MIME-encoded in transit. CR and LF, whether part of CRLF or used themselves, may not be encoded, but Internet mail could be CR or LF or CRLF and mutt needs to be able to deal with that.

* type changed to defect

--------------------------------------------------------------------------------
2009-05-14 09:27:23 UTC pdmef
* Added comment:
That's the case for me: MIME-encoded CRLF remain and others are stripped. Since the OP mentions that these broken mails only arrive in quoted-printable, I guess the line endings are MIME-encoded. I.e. no mutt bug, I'd say.

* Updated description:

{{{
Package: mutt
Version: 1.5.15
Severity: wishlist

Some domains seem to manage to send e-mail where every linebreak is a
\r\n. I'm not sure why they do this. The only one I've encountered
with great regularity is DOLBY.COM, though there was one other I got
last week that I seem to've misplaced, but it caused me to put this
on my queue. They only seem to arrive in quoted-printable...

Anyhow, mutt's pager displays these messages with a ^M at the end of
each line, which really looks awkward and hard to read, e.g.:

Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do^M
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad^M
minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip^M
ex ea commodo consequat. Duis aute irure dolor in reprehenderit in^M
voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur^M
sint occaecat cupidatat non proident, sunt in culpa qui officia^M

(Okay, it's worse when you're actually reading the language across
the line break.)

Attached is a patch such that mutt throws away ^Ms unless OPTWEED is
set, so that if you turn on full headers ('h'), these ^Ms are
displayed. If you'd prefer to handle it a different way, let me know
and I will re-spin the patch.

Previously, mutt unconditionally escaped all \r chars as "^M".

I realize this is the sender's fault, but I don't think I should have
to put up with it.  (I could fix this with a display_filter,
but then it would be harder to toggle, I'd pay a fork penalty,
and other mutt users wouldn't get the benefit).

(I realize the patch uses a goto -- I do think it's clearer to add an
elsif stanza for the \r case and use a goto to the control-character
label, rather either duplicating the control-char printing code in \r
stanza, or adding
  || wc=='\r' && !option(OPTWEED)
to the control characer case (line 1184, wc < 0x20...).
If you'd prefer a goto-less version, I can do that too...)

--- /afs/sipb.mit.edu/project/sipb/src/mutt/mutt-1.5.15/pager.c	Sun Apr  1 17:58:56 2007
+++ mutt-1.5.15/pager.c	Mon May 28 22:59:28 2007
@@ -1175,8 +1175,15 @@
       else
 	col = t;
     }
+    else if (wc == '\r')
+    {
+      if (!option(OPTWEED)) {
+	goto controlchar;
+      }
+    }
     else if (wc < 0x20 || wc == 0x7f)
     {
+    controlchar:
       if (col + 2 > wrap_cols)
 	break;
       col += 2;

2007-05-28 22:59 -0400  John Hawkinson  <jhawk@mit.edu>

	* pager.c: Hide ^Ms in messages when OPTWEED ('h': suppress full
	headers) is set. For those rare people who get email with linebreaks
	ending in \r\n.

Thanks.

--jhawk@mit.edu
  John Hawkinson
}}}

--------------------------------------------------------------------------------
2009-05-14 16:55:36 UTC jhawk
* Added comment:
Yes, they're MIME-encoded. A specific example:

{{{X-MimeOLE: Produced By Microsoft Exchange V6.5
Content-class: urn:content-classes:message
MIME-Version: 1.0
Content-Type: multipart/mixed;
        boundary="----_=_NextPart_001_01C87E22.19285640"
Content-Length: 2115

------_=_NextPart_001_01C87E22.19285640
Content-Type: text/plain;
        charset="US-ASCII"
Content-Transfer-Encoding: quoted-printable
}}}

Will you take the patch anyhow, please?

--------------------------------------------------------------------------------
2009-05-14 18:14:03 UTC madduck
* Added comment:
I meant that the line-endings are mime-encoded, which they should not be. See RFC2045 6.7(1). I am actually not sure those \^M characters only show up if that happens. They also show up if mutt decode traditional PGP messages, which have \r embedded, for instance.

I vote for inclusion of the patch, but ideally with a separate option. I don't see the need to toggle that option, which could always be done with a macro if needed.

--------------------------------------------------------------------------------
2009-05-15 09:12:59 UTC pdmef
* Added comment:
Can you please include one or more content line? Only the headers is not enough. I suspect lines ending in "=0D". Also, you can use:

{{{
set display_filter="sed 's/^M$//'"
}}}

in .muttrc (a real Ctrl+M for {{{^M}}}, not {{{^}}} followed by {{{M}}}) to specify a filter stripping these CRLF sequences for display.

* Updated description:
{{{
Package: mutt
Version: 1.5.15
Severity: wishlist

Some domains seem to manage to send e-mail where every linebreak is a
\r\n. I'm not sure why they do this. The only one I've encountered
with great regularity is DOLBY.COM, though there was one other I got
last week that I seem to've misplaced, but it caused me to put this
on my queue. They only seem to arrive in quoted-printable...

Anyhow, mutt's pager displays these messages with a ^M at the end of
each line, which really looks awkward and hard to read, e.g.:

Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do^M
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad^M
minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip^M
ex ea commodo consequat. Duis aute irure dolor in reprehenderit in^M
voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur^M
sint occaecat cupidatat non proident, sunt in culpa qui officia^M

(Okay, it's worse when you're actually reading the language across
the line break.)

Attached is a patch such that mutt throws away ^Ms unless OPTWEED is
set, so that if you turn on full headers ('h'), these ^Ms are
displayed. If you'd prefer to handle it a different way, let me know
and I will re-spin the patch.

Previously, mutt unconditionally escaped all \r chars as "^M".

I realize this is the sender's fault, but I don't think I should have
to put up with it.  (I could fix this with a display_filter,
but then it would be harder to toggle, I'd pay a fork penalty,
and other mutt users wouldn't get the benefit).

(I realize the patch uses a goto -- I do think it's clearer to add an
elsif stanza for the \r case and use a goto to the control-character
label, rather either duplicating the control-char printing code in \r
stanza, or adding
  || wc=='\r' && !option(OPTWEED)
to the control characer case (line 1184, wc < 0x20...).
If you'd prefer a goto-less version, I can do that too...)

--- /afs/sipb.mit.edu/project/sipb/src/mutt/mutt-1.5.15/pager.c	Sun Apr  1 17:58:56 2007
+++ mutt-1.5.15/pager.c	Mon May 28 22:59:28 2007
@@ -1175,8 +1175,15 @@
       else
 	col = t;
     }
+    else if (wc == '\r')
+    {
+      if (!option(OPTWEED)) {
+	goto controlchar;
+      }
+    }
     else if (wc < 0x20 || wc == 0x7f)
     {
+    controlchar:
       if (col + 2 > wrap_cols)
 	break;
       col += 2;

2007-05-28 22:59 -0400  John Hawkinson  <jhawk@mit.edu>

	* pager.c: Hide ^Ms in messages when OPTWEED ('h': suppress full
	headers) is set. For those rare people who get email with linebreaks
	ending in \r\n.

Thanks.

--jhawk@mit.edu
  John Hawkinson
}}}

--------------------------------------------------------------------------------
2009-05-15 09:22:33 UTC madduck
* Added comment:
Ouch! You can even make mutt create wrong messages itself:

{{{
--Dxnq1zWXvFF0Q93v
Content-Type: text/plain; charset=iso-8859-1
Content-Disposition: inline
Content-Transfer-Encoding: quoted-printable

this is a test with different line endings=0D
=0D
-- =0D
martin | http://madduck.net/ | http://two.sentenc.es/=0D
}}}

I created a message with mutt, told vim to set the fileformat to DOS, sent it GPG-MIME-signed, and found the above in the sent folder and in the message when it arrived. Sure enough, {{{^M}}} showed up in the pager.

http://scratch.madduck.net/.tmp__mutt-piper-1000-28859-17 is the raw message (available for the next 30 days only)

--------------------------------------------------------------------------------
2009-05-15 15:49:47 UTC pdmef
* Added attachment fix-2898.diff
* Added comment:
turn \r\n into \n for qp encoding

--------------------------------------------------------------------------------
2009-05-15 15:50:41 UTC pdmef
* Added comment:
I think I won't commit the patch (since it doesn't work for me anyway). Since encoding these CR is forbidden for qp-encoding, I think the only sane thing we can do is to strip them only from qp-encoding, see the attached patch.

I don't think it makes sense to always turn CRLF into LF only.

--------------------------------------------------------------------------------
2009-05-16 04:53:39 UTC John Hawkinson
* Added comment:
{{{

Sorry. Here we go:

------_=_NextPart_001_01C87E22.19285640
Content-Type: text/plain;
        charset="US-ASCII"
Content-Transfer-Encoding: quoted-printable

Hi John,=0D=0A=0D=0AHere you go=2E=0D=0A=0D=0ACinema Support=0D=0ADolby Lab=



Indeed, this was noted in my original bug report, and rejected
for several reasons: 

(1) Harder to toggle off (2) pays a fork penalty (3) other mutt users
wouldn't get the benefit


Interesting [how does it fail for you?]


That sounds fine in prinipal. Great!

It seems like being able to see the defect with 'h' is probably
not important to anybody.

--jhawk@mit.edu
  John Hawkinson
}}}

--------------------------------------------------------------------------------
2009-05-16 16:15:34 UTC pdmef
* Added comment:
Replying to [comment:10 John Hawkinson]:
{{{
> Sorry. Here we go:
> 
> ------_=_NextPart_001_01C87E22.19285640
> Content-Type: text/plain;
>         charset="US-ASCII"
> Content-Transfer-Encoding: quoted-printable
> 
> Hi John,=0D=0A=0D=0AHere you go=2E=0D=0A=0D=0ACinema Support=0D=0ADolby Lab=
}}}

Hmm, my patch won't fix these. It'll only fix it when CRLF is found at the end of a line. I'll re-read what the standard says here (I cannot really imagine that CRLF is totally forbidden even with a silly encoding as in your example).

> Interesting [how does it fail for you?]

It doesn't do anything.
 
> It seems like being able to see the defect with 'h' is probably
> not important to anybody.

That would be easy to do. Given your example we first need to agree on what the problem actually is... :-)

--------------------------------------------------------------------------------
2009-05-31 11:01:34 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [fdaed73dd66a]) Turn trailing \r\n to \n for qp-encoded messages.

RFC2045 (sect. 6.7, (1) general 8bit representation)
states that neither CR nor LF of the trailing CRLF may
be qp-encoded. So we ignore trailing qp-encoded CRs.

See #2898 though this is a partial fix only.

--------------------------------------------------------------------------------
2009-05-31 11:05:55 UTC pdmef
* Added comment:
I'm turning this into an enhancement. The fix to conform to RFC2045 is pushed, so only turning CRLF into LF remains. With the example given I think it's perfectly fine that mutt displays {{{^M}}} because it's there and it's correct to display it.

I don't close this as invalid to get opinions whether we really should strip turn CRLF into LF in general and not only where we have to. I'd say: no.

* owner changed to pdmef
* status changed to assigned
* type changed to enhancement

--------------------------------------------------------------------------------
2009-06-01 05:19:02 UTC brendan
* Updated description:
Some domains seem to manage to send e-mail where every linebreak is a
`\r\n`. I'm not sure why they do this. The only one I've encountered
with great regularity is DOLBY.COM, though there was one other I got
last week that I seem to've misplaced, but it caused me to put this
on my queue. They only seem to arrive in quoted-printable...

Anyhow, mutt's pager displays these messages with a `^M` at the end of
each line, which really looks awkward and hard to read, e.g.:
{{{
Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do^M
eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad^M
minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip^M
ex ea commodo consequat. Duis aute irure dolor in reprehenderit in^M
voluptate velit esse cillum dolore eu fugiat nulla pariatur.  Excepteur^M
sint occaecat cupidatat non proident, sunt in culpa qui officia^M
}}}
(Okay, it's worse when you're actually reading the language across
the line break.)

Attached is a patch such that mutt throws away `^M`s unless OPTWEED is
set, so that if you turn on full headers ('h'), these `^M`s are
displayed. If you'd prefer to handle it a different way, let me know
and I will re-spin the patch.

Previously, mutt unconditionally escaped all `\r` chars as "`^M`".

I realize this is the sender's fault, but I don't think I should have
to put up with it.  (I could fix this with a display_filter,
but then it would be harder to toggle, I'd pay a fork penalty,
and other mutt users wouldn't get the benefit).

(I realize the patch uses a goto -- I do think it's clearer to add an
elsif stanza for the `\r` case and use a goto to the control-character
label, rather either duplicating the control-char printing code in `\r`
stanza, or adding
{{{
  || wc=='\r' && !option(OPTWEED)
}}}
to the control characer case (line 1184, `wc < 0x20`...).
If you'd prefer a goto-less version, I can do that too...)
{{{
--- /afs/sipb.mit.edu/project/sipb/src/mutt/mutt-1.5.15/pager.c	Sun Apr  1 17:58:56 2007
+++ mutt-1.5.15/pager.c	Mon May 28 22:59:28 2007
@@ -1175,8 +1175,15 @@
       else
 	col = t;
     }
+    else if (wc == '\r')
+    {
+      if (!option(OPTWEED)) {
+	goto controlchar;
+      }
+    }
     else if (wc < 0x20 || wc == 0x7f)
     {
+    controlchar:
       if (col + 2 > wrap_cols)
 	break;
       col += 2;

2007-05-28 22:59 -0400  John Hawkinson  <jhawk@mit.edu>

	* pager.c: Hide ^Ms in messages when OPTWEED ('h': suppress full
	headers) is set. For those rare people who get email with linebreaks
	ending in \r\n.
}}}
Thanks.

--jhawk@mit.edu
  John Hawkinson
* version changed to 1.5.15

--------------------------------------------------------------------------------
2010-08-24 17:54:30 UTC rsc
* cc changed to dev.mutt.org@pobox.madduck.net, mutt.org@robert-scheck.de

--------------------------------------------------------------------------------
2011-04-13 11:23:00 UTC m-a
* owner changed to 
* status changed to new
