Ticket:  2171
Status:  new
Summary: previous-thread doesn't jumps to first message if parent not available

Reporter: Mike.Dornberger@gmx.de
Owner:    mutt-dev

Opened:       2006-01-29 22:18:52 UTC
Last Updated: 2010-08-06 04:37:15 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
This is from Debian Bug#344766; in a nutshell, if a thread is missing the root message, doing next-thread from the previous thread goes to the first message, but doing previous-thread from the next one does not. Here is the original report:

-----------------------------------
Hi,

when in threaded mode, mutt doesn't jump to the first available message when
executing previous-thread (default binding: ^P) when the parent of two
replies is not available, which is at least unexpected. In my case it goes
to the second subthread. I wanted to read all what I have of this thread and
expected ^P to jump to the first message, so I can read all the messages by
pressing <right>.

You can test this with the file prev_thread-test.mbox that is compressed in
the attached file mutt_test_prev_thread.tar.bz2 [1]. Maybe an automated test can
be done with the muttrc I wrote. After executing

$ mutt -f prev_thread-test.1.mbox -F muttrc

the created file "content" should be the same as "content.expected".
Actually it gives the same as in "content.not_expected".

Greeting,
 Mike Dornberger
-----------------------------------

[1] http://people.debian.org/~adeodato/tmp/2006-01-29/mutt_test_prev_thread.tar.bz2
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-01-29 22:33:14 UTC owner@bugs.debian.org (Debian Bug Tracking System)
* Added comment:
{{{
Thank you for the additional information you have supplied regarding
this problem report.  It has been forwarded to the package maintainer(s)
and to other interested parties to accompany the original report.

Your message has been sent to the package maintainer(s):
 Adeodato Simó <dato@net.com.org.es>

If you wish to continue to submit further information on your problem,
please send it to 344766@bugs.debian.org, as before.

Please do not reply to the address at the top of this message,
unless you wish to report a problem with the Bug-tracking system.

Debian bug tracking system administrator
(administrator, Debian Bugs database)
}}}

--------------------------------------------------------------------------------
2009-02-07 13:17:23 UTC antonio@dyne.org
* Added comment:
still seen in 1.5.19

* version changed to 1.5.19

--------------------------------------------------------------------------------
2010-08-06 04:37:15 UTC me
* Added comment:
The cause here is at thread.c:988 in find_virtual():

  while (reverse && cur->next)
    cur = cur->next;

This occurs when moving to the previous (sub)thread and either it is missing (only known via references: or in-reply-to:) or the current view is limited; in either case the message is "invisible."

I can only speculate that the intent here was when you had a deeply nested thread tree that it might be confusing if the cursor jumped past the immediately preceding subthread, since the messages are not indented as far as they normally would be if the parent was actually visible.

* Updated description:
{{{
This is from Debian Bug#344766; in a nutshell, if a thread is missing the root message, doing next-thread from the previous thread goes to the first message, but doing previous-thread from the next one does not. Here is the original report:

-----------------------------------
Hi,

when in threaded mode, mutt doesn't jump to the first available message when
executing previous-thread (default binding: ^P) when the parent of two
replies is not available, which is at least unexpected. In my case it goes
to the second subthread. I wanted to read all what I have of this thread and
expected ^P to jump to the first message, so I can read all the messages by
pressing <right>.

You can test this with the file prev_thread-test.mbox that is compressed in
the attached file mutt_test_prev_thread.tar.bz2 [1]. Maybe an automated test can
be done with the muttrc I wrote. After executing

$ mutt -f prev_thread-test.1.mbox -F muttrc

the created file "content" should be the same as "content.expected".
Actually it gives the same as in "content.not_expected".

Greeting,
 Mike Dornberger
-----------------------------------

[1] http://people.debian.org/~adeodato/tmp/2006-01-29/mutt_test_prev_thread.tar.bz2
>How-To-Repeat:
>Fix:
Unknown
}}}
* milestone changed to 
