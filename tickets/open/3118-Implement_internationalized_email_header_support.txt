Ticket:  3118
Status:  new
Summary: Implement internationalized email header support

Reporter: pdmef
Owner:    mutt-dev

Opened:       2008-09-10 13:12:53 UTC
Last Updated: 2008-09-10 13:12:53 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
The specs are: http://www.ietf.org/rfc/rfc5335.txt, http://www.ietf.org/rfc/rfc5336.txt and http://www.ietf.org/rfc/rfc5337.txt. Or use some library to handle it (if available).
