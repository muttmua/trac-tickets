Ticket:  1158
Status:  new
Summary: persistent locale setting

Reporter: Bernard Massot <bmassot@free.fr>
Owner:    mutt-dev

Opened:       2002-04-19 12:32:13 UTC
Last Updated: 2007-04-07 14:25:29 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.28-2
Severity: normal

-- Please type your report below this line
I've set a send-hook which changes the $locale variable to have the month
string and day string changed in the date, when quoting a mail received from 
the person concerned with this send-hook. And when I return to the menu 
after sending the mail, the date is written with this locale instead of my 
default locale.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011002 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.28i (2002-03-13)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.17 (i586) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.tlr.mx_open_append.2
patch-1.3.28.cvs.indexsegfault
patch-1.3.27.bse.xtitles.1
patch-1.3.26.appoct.3
patch-1.3.15.sw.pgp-outlook.1
patch-1.3.27.admcd.gnutls.19
Md.use_editor
Md.paths_mutt.man
Md.muttbug_no_list
Md.use_etc_mailname
Md.muttbug_warning
Md.gpg_status_fd
patch-1.3.24.rr.compressed.1
patch-1.3.25.cd.edit_threads.9.1
patch-1.3.23.1.ametzler.pgp_good_sign


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-04-19 10:48:31 UTC Lars Hecking <lhecking@nmrc.ie>
* Added comment:
{{{
Bernard Massot writes:
> Package: mutt
> Version: 1.3.28-2
> Severity: normal
> 
> -- Please type your report below this line
> I've set a send-hook which changes the $locale variable to have the month
> string and day string changed in the date, when quoting a mail received from 
> the person concerned with this send-hook. And when I return to the menu 
> after sending the mail, the date is written with this locale instead of my 
> default locale.

Care to post the relevant parts of your config?

In any case, the fine manual has this to say in the section named
"Using Hooks" (4.4 in my copy of the manual):

 Note: if a hook changes configuration settings, these changes remain
 effective until the end of the current mutt session. As this is
 generally not desired, a default hook needs to be added before all
 other hooks to restore configuration defaults. Here is an example with
 send-hook and the my_hdr directive:

      send-hook . 'unmy_hdr From:'
      send-hook ~Cb@b.b my_hdr from: c@c.c
}}}

--------------------------------------------------------------------------------
2002-04-19 13:20:13 UTC Lars Hecking <lhecking@nmrc.ie>
* Added comment:
{{{
> >  Care to post the relevant parts of your config?
> send-hook . "set locale = C"
> send-hook . "set date_format = '!%a, %b %d, %Y at %I:%M:%S%p %Z'"
> send-hook . "set attribution = 'On %d, %n wrote:'"
> 
> send-hook user@foo.com "set locale = br_FR"
> send-hook user@foo.com "set date_format = '%A %d a viz %B %Y, da %H:%M:%S'"
> send-hook user@foo.com "set attribution = \"D'ar %d, %n en deus skrivet:\""

Works as intended for me, but that's with mutt 1.5.0 from cvs ...
}}}

--------------------------------------------------------------------------------
2002-08-07 22:31:48 UTC Michael Elkins <me@sigpipe.org>
* Added comment:
{{{
Lars Hecking wrote:
>   
> > >  Care to post the relevant parts of your config?
> > send-hook . "set locale = C"
> > send-hook . "set date_format = '!%a, %b %d, %Y at %I:%M:%S%p %Z'"
> > send-hook . "set attribution = 'On %d, %n wrote:'"
> > 
> > send-hook user@foo.com "set locale = br_FR"
> > send-hook user@foo.com "set date_format = '%A %d a viz %B %Y, da %H:%M:%S'"
> > send-hook user@foo.com "set attribution = \"D'ar %d, %n en deus skrivet:\""
> 
>  Works as intended for me, but that's with mutt 1.5.0 from cvs ...

I think the problem here is that the date in the index will be printed
using the $locale from the last message he sent, not that the *next*
message won't have the correct $locale setting.  There isn't a
post-send-hook which the user could reset the settings for use elsewhere
in Mutt.
}}}

--------------------------------------------------------------------------------
2003-09-14 18:40:46 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Bonjour Bernard, et merci pour le rapport!

On Thursday, April 18, 2002 at 8:03:59 PM +0200, Bernard Massot wrote:

>> Bernard Massot writes:
>>> I've set a send-hook which changes the $locale variable to have the
>>> month string and day string changed in the date, when quoting a mail
>>> received from the person concerned with this send-hook. And when I
>>> return to the menu after sending the mail, the date is written with
>>> this locale instead of my default locale.
> send-hook . "set locale = C"
> send-hook . "set date_format = '!%a, %b %d, %Y at %I:%M:%S%p %Z'"
> send-hook . "set attribution = 'On %d, %n wrote:'"
> send-hook user@foo.com "set locale = br_FR"
> send-hook user@foo.com "set date_format = '%A %d a viz %B %Y, da %H:%M:%S'"
> send-hook user@foo.com "set attribution = \"D'ar %d, %n en deus skrivet:\""

   OK, there is an easy solution for you, because your default $locale
seems to be C: Simply use a "!" in front of the date in $index_format.
Example set index_format="%4C %Z %{!%b %d} %-15.15L (%4l) %s". This will
print date not using $locale, so always in English.


   OTOH, this will not help in case one's default $locale is not C. As
exposed in thread, there is no way to reset a setting after a send-hook.
That's by design, and that won't change.

   One possible change would be to split $locale into 2 variables
$locale_index and $locale_attribution (or something like that, scope is
discussable). Or change date descriptors to insert locale selectors. Or
simpler add another "!"-like modifier to use system LC_TIME instead of
$locale. Any of these suggestions permitting wanted fixed language index
and wanted variable language attributions.


   Do you agree that persistent settings are not a bug? Do you agree we
change here severity to wishlist? Is there a programmer out there?


Bye!	Alain.
}}}

--------------------------------------------------------------------------------
2003-10-04 04:23:50 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# no infos, no followups
close 1035
# pipe-message doesn't output From_ when there is no From_: not a bug
close 1153
# not a bug
close 1432
# not a bug
close 1476
# no enough infos, no followups
close 1568
# solved by Thomas
close 1646
unmerge 844
severity 935 wishlist
severity 1158 wishlist
severity 1453 wishlist
}}}

--------------------------------------------------------------------------------
2003-10-17 18:13:07 UTC Alain Bench <veronatif@free.fr>
* Added attachment patch-1.4.ab.system_locale_time.1
* Added comment:
patch-1.4.ab.system_locale_time.1

* Added comment:
{{{
Hello Bernard and all,

On Sunday, September 14, 2003 at 1:40:46 AM +0200, Alain Bench wrote:

> add another "!"-like modifier to use system LC_TIME instead of $locale
> [...] permitting wanted fixed language index and wanted variable
> language attributions.

   Please try attached patch. It adds a new colon ":" modifier in date
formats to use environment LC_TIME locale. Usage exactly as "!",
everywhere a date format can be set: $index_format, $date_format,
$pgp_entry_format, $attribution, etc... But not $folder_format (?).

   When date format prepended by:

- nothing ==> $locale  variable hookable language
- !       ==> C	fixed English
- :       ==> LC_TIME	fixed language as system

   Exemple:

| set index_format="%4C %Z %{:%b %d} %-15.15L (%4l) %s"
                            ^

Bye!	Alain.
-- 
Give your computer's unused idle processor cycles to a scientific goal:
The Genome@home project at <URL:http://genomeathome.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2003-10-26 11:49:27 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# probably fixed, submitter unmailable
close 541
# locale misconfiguration, no further followups
close 790
# unreproducible, submitter unmailable
close 986
# closed by spam accident
reopen 1026
# submitter accepted closing
close 1183
# fixed by Steven Barker and Thomas Roessler in 1.5.4
close 1444
# not a bug
close 1466
reopen 1466 David Yitzchak Cohen <dave@bigfatdave.com>
close 1466
merge 1223 1614
tags 1158 patch
-- someone with default color knoweledge should review #845 #946 #984
}}}

--------------------------------------------------------------------------------
2003-12-20 15:23:34 UTC Alain Bench <veronatif@free.fr>
* Added attachment patch-1.5.4.ab.system_locale_time_doc.1
* Added comment:
patch-1.5.4.ab.system_locale_time_doc.1

* Added comment:
{{{
On Friday, October 17, 2003 at 1:13:07 AM +0200, Alain Bench wrote:

> a new colon ":" modifier in date formats to use environment LC_TIME
> locale. Usage exactly as "!"

   And here comes attached the documentation corresponding to
patch-1.4.ab.system_locale_time.1, it applies cleanly to Mutt 1.4 or
1.5.5.1.


Bye!	Alain.
-- 
Give your computer's unused idle processor cycles to a scientific goal:
The Genome@home project at <URL:http://genomeathome.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2005-09-30 12:25:20 UTC ab
* Added comment:
{{{
upload patch-1.5.11.ab.system_locale_time.2 Adds format
modifier using fixed LC_TIME locale. See discussion in MD
followups to mutt/1296. v2 rewrote doc (doc building
untested), and updated to 1.5.11. Does not change any
default behaviour, very low probability of breakage of
existing muttrc.
While at it, dedupped unform.
}}}

--------------------------------------------------------------------------------
2005-09-30 12:28:11 UTC ab
* Added comment:
{{{
upload patch-1.5.11.ab.index_time_locale.1 Changes default
value of $index_format to use LC_TIME localization by
default. Depends on ab.system_locale_time.2.
Does not touch browser: To be done in complement, as
resolution of mutt/1734.
}}}
