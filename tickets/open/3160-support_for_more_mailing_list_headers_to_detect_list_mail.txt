Ticket:  3160
Status:  new
Summary: support for more mailing list headers to detect list mail

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-01-25 22:51:26 UTC
Last Updated: 2009-01-27 14:58:58 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/463637

---
I have the following two lines in my ~/.muttrc:

  subscribe www-tag@w3.org
  color index yellow default ~l

This should be enought to highlight any mail addressed to the
www-tag@w3.org mailing list as yellow but it fails to recognise
certain headers of which a non-exhaustive list follows:

  X-Mailing-List
  X-Loop
  X-Original-To
  List-Id
  Resent-From

Any one of these can be used to determine the mailing list when the
actual To address is obscured because of a BCC.

I have attached a sample mbox with a message which is missed by the
config sample I have included above.



--------------------------------------------------------------------------------
2009-01-25 22:53:06 UTC antonio@dyne.org
* Added attachment sample.mbox

--------------------------------------------------------------------------------
2009-01-27 14:58:58 UTC pdmef
* Added comment:
There're two issues here: the message prodived isn't addressed to some www-tag@ address, so it's not a mutt bug that it doesn't recognize the "subscribe" command.

Second, mutt supports the List-Post header to recognize list mail, but that would be equivalent only to "lists" command, not "subscribe".

I'll change the summary as only issue 2 is a valid request.

* summary changed to support for more mailing list headers to detect list mail
