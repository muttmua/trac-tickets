Ticket:  3532
Status:  new
Summary: Use $SHELL when running mailcap-related commands

Reporter: vinc17
Owner:    mutt-dev

Opened:       2011-07-18 10:00:02 UTC
Last Updated: 2011-07-20 10:19:09 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Mutt should run mailcap-related commands (and possibly all commands, in which case the change could be done in system.c) with the user's shell (provided by $SHELL, else his login shell?) instead of /bin/sh. The reason is that /bin/sh is very limited and doesn't allow the user to provide a file to source in order to dynamically update the environment (for instance, zsh will source the user's .zshenv file). This is needed when running Mutt in GNU Screen across X11 sessions: updating the XAUTHORITY value is necessary before running an X application.

Notes:

* Some other programs already use $SHELL (etc.) instead of /bin/sh for running commands (e.g. ssh, gdb).

* This can be regarded as incompatible with the mailcap description in the mailcap(5) man page (at least under Debian), which says that system() is used, but anyway Mutt is already somewhat incompatible as it doesn't use system(), and I've reported a bug to change that: http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=634254

--------------------------------------------------------------------------------
2011-07-18 10:56:27 UTC m-a
* Added comment:
{{{
Am 18.07.2011 12:00, schrieb Mutt:


I contend that mutt SHOULD USE /bin/sh unconditionally so that mailcap
entries have predictable behaviour.  This is standard behaviour on
UNIX-like systems, and mutt SHOULD NOT USE the user's interactive shell.

While use of $SHELL may aid scripting, users can trivially write a
wrapper such as:

#!/bin/sh
exec /path/to/zsh /my/zsh/script "$@"

Using the login/interactive shell for scripting will fail in awkward
ways and generate tons of bug reports.
}}}

--------------------------------------------------------------------------------
2011-07-18 12:22:59 UTC vinc17
* Added comment:
Replying to [comment:1 m-a]:
> I contend that mutt SHOULD USE /bin/sh unconditionally so that mailcap
> entries have predictable behaviour.  This is standard behaviour on
> UNIX-like systems, and mutt SHOULD NOT USE the user's interactive shell.

The fact that the /bin/sh depends on the system (and may even change on a system, e.g. on Debian it is a symlink to either bash or dash) makes the behavior already not completely predictable. At least allowing the user to choose the shell allows him to choose the same shell on various machines.

The choice could be done via a variable.

> While use of $SHELL may aid scripting, users can trivially write a
> wrapper such as:
> 
> #!/bin/sh
> exec /path/to/zsh /my/zsh/script "$@"

If Mutt doesn't allow the user to execute the wrapper, this is not possible.

> Using the login/interactive shell for scripting will fail in awkward
> ways and generate tons of bug reports.

Well, I more or less agree. I suggested it only for consistency with other software. I don't think there would be many problems in practice, though (most users having some sh-compatible shell as their login shell).

--------------------------------------------------------------------------------
2011-07-18 12:43:52 UTC m-a
* Added comment:
{{{
Am 18.07.2011 14:22, schrieb Mutt:


That's no excuse to introduce deliberate incompatibilities by inheriting
a possibly non-POSIX-compliant interactive shell for scripting.
Anything that Debian links to /bin/sh would be compliant.


No - it's not necessary, as shown below.


Why would it _not_ do that?  chmod +x wrapper.sh  is all you need.

Best regards
Matthias Andree
}}}

--------------------------------------------------------------------------------
2011-07-18 18:46:21 UTC vinc17
* Added comment:
Replying to [comment:3 m-a]:
> Why would it _not_ do that?  chmod +x wrapper.sh  is all you need.

How can I tell Mutt to run the wrapper instead of /bin/sh?

--------------------------------------------------------------------------------
2011-07-18 19:12:07 UTC m-a
* Added comment:
{{{
Am 18.07.2011 20:46, schrieb Mutt:

Come on, this is Unix, so you just plug & match.
You specify the wrapper as program to execute in .mailcap or wherever.

Any instructions beyond this are outside the scope of this bug tracker
and subject to Linux/Unix shell programming introductory text books or
Internet tutorials.
}}}

--------------------------------------------------------------------------------
2011-07-18 21:58:34 UTC vinc17
* Added comment:
Replying to [comment:5 m-a]:
> Come on, this is Unix, so you just plug & match.
> You specify the wrapper as program to execute in .mailcap or wherever.

The full settings are provided by /etc/mailcap. So, how can I modify the .mailcap to tell Mutt to use the wrapper for /etc/mailcap?

Of course, it is out of the question to copy the /etc/mailcap contents to the user's .mailcap and add the wrapper there because this is the best way to end up with an inconsistent system (think about that: the /etc/mailcap is updated by the system from time to time, the /etc/mailcap depends on the machine while the .mailcap is sometimes on an NFS directory...).

> Any instructions beyond this are outside the scope of this bug tracker
> and subject to Linux/Unix shell programming introductory text books or
> Internet tutorials.

What you propose does not seem to work at all. So, don't speak about tutorials or whatever.

--------------------------------------------------------------------------------
2011-07-19 15:54:52 UTC m-a
* Added comment:
{{{
Am 18.07.2011 23:58, schrieb Mutt:

We're running in circles, and I'll drop out of the discussion now.
First you request that mutt introduces inconsistencies, and now you
propose that users avoid overriding configuration because that
introduces inconsistencies. See the contradiction in your own reasoning?
I do.
}}}

--------------------------------------------------------------------------------
2011-07-20 08:58:46 UTC muttbug111
* Added comment:
To anyone proposing to run every spawned program through $SHELL, regardless of whether that has anything to do with interactive shell use (which it probably doesn't):

Please test how this works on *your* system with SHELL=/bin/tcsh and discuss how this affects running mutt. If it doesn't work, consider making the proposal to /dev/null... ;-)

--------------------------------------------------------------------------------
2011-07-20 09:09:13 UTC m-a
* Added comment:
{{{
Also note that quick and rough testing is insufficient. As long as you
pass only trivial commands to tcsh, it will pretend/appear to work, but
start falling apart when using control constructs, variables (different
shells have different defaults, f. i. FreeBSD just fixed a port where
OSTYPE was set differently from "uname -s" depending on SHELL)...
}}}

--------------------------------------------------------------------------------
2011-07-20 10:19:09 UTC vinc17
* Added comment:
There is other software that uses $SHELL for commands (possibly including in system-wide configuration). If you set $SHELL to a shell sufficiently incompatible with POSIX, you may already have problems. Also note that Debian's /etc/mailcap uses underspecified and obsolescent POSIX features (it probably works with both bash and dash, but the system could have another shell for /bin/sh, which is just a symlink...).

Now, an alternative could be that the shell be specified as a Mutt variable (/bin/sh by default). That could be a replacement for the compile-time EXECSHELL. Indeed /bin/sh may change between Mutt builds, so that it can be better to have it set at run time.

Or any other alternative that would allow to have /bin/sh run in a corrected environment.
