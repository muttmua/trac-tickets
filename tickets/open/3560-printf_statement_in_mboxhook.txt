Ticket:  3560
Status:  new
Summary: printf statement in mbox-hook

Reporter: kierun
Owner:    mutt-dev

Opened:       2012-01-17 09:56:38 UTC
Last Updated: 2012-01-17 09:56:38 UTC

Priority:  major
Component: mutt
Keywords:  mbox-hook printf date

--------------------------------------------------------------------------------
Description:
A while ago, there was a patch that allowed mbox-hook to have printf-liek statements.  This was really useful for archiving emails in different directories with a date in the directory name. 

Is this patch still around or has it been ported to the development version?
