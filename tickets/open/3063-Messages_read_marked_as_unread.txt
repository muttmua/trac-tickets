Ticket:  3063
Status:  infoneeded_new
Summary: Messages read marked as unread

Reporter: mehturt
Owner:    mutt-dev

Opened:       2008-05-29 07:31:56 UTC
Last Updated: 2009-06-29 02:31:22 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
Sometimes messages that I read are marked as unread..
I read a message, then I sync-mailbox ($), then I open ! mailbox and message is unread again..
This happens only ocasionally..
I am using mutt with IMAPs on Ubuntu Hardy.

--------------------------------------------------------------------------------
2009-06-29 02:31:22 UTC brendan
* Added comment:
I think we'd need a .muttdebug trace to figure this one out.

* component changed to IMAP
* priority changed to minor
* status changed to infoneeded_new
