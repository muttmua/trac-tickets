Ticket:  3074
Status:  accepted
Summary: Mutt hangs indefinitely

Reporter: jaymzh
Owner:    brendan

Opened:       2008-06-10 00:01:17 UTC
Last Updated: 2009-06-07 06:04:03 UTC

Priority:  major
Component: IMAP
Keywords:  duplicate 2938

--------------------------------------------------------------------------------
Description:
On many occasions, I find that an unattended mutt (1.5.17+20080114-1+b1 from Debian) will hang indefinitely. Never having time to debug it, I usually kill it and restart it, but today I let it run in strace until it happend. Here's what the end of the strace looks like - it seems to get an ERESTARTSYS while reading from the imaps socket! ERESTARTSYS is reserved as a signal between drivers and the kernel signal handling code and is, theoretically, never supposed to make it to userspace. Thus, this is quite odd.

I'm happy to add ltraces or whatever else may be useful....
{{{
write(1, "\r\33[17B\33[37m\33[40mFetching message"..., 76) = 76
write(1, "\33[79;29H\33[37m\33[40m1834/1838 (99%"..., 54) = 54
gettimeofday({1212696193, 854853}, NULL) = 0
getrusage(RUSAGE_SELF, {ru_utime={0, 812050}, ru_stime={0, 236014}, ...}) = 0
time(NULL)                              = 1212696193
times({tms_utime=81, tms_stime=23, tms_cutime=1, tms_cstime=2}) = 1734919932
gettimeofday({1212696193, 855156}, NULL) = 0
getrusage(RUSAGE_SELF, {ru_utime={0, 812050}, ru_stime={0, 236014}, ...}) = 0
time(NULL)                              = 1212696193
times({tms_utime=81, tms_stime=23, tms_cutime=1, tms_cstime=2}) = 1734919932
send(4, "\27\3\1\0\373*\341\374\221\t.p\233T*Y\354\0\223\303><\205"..., 256, 0) = 256
_llseek(5, 0, [0], SEEK_SET)            = 0
recv(4, "\27\3\1\v\33", 5, 0)           = 5
recv(4, "x\204\315|I\203Ph`\317\205-\356\16L\315S\3551\263\302N"..., 2842, 0) = 1383
recv(4, "\246\212O\2675gm\364wR!\16\31\256\361\374\373\21\352u#"..., 1459, 0) = 1388
recv(4, 0x81d31d0, 71, 0)               = ? ERESTARTSYS (To be restarted)
--- SIGWINCH (Window changed) @ 0 (0) ---
sigreturn()                             = ? (mask now [])
recv(4,  <unfinished ...>
}}}

- Phil

--------------------------------------------------------------------------------
2008-06-10 00:04:56 UTC jaymzh
* Added attachment mutt.strace
* Added comment:
Strace

--------------------------------------------------------------------------------
2008-06-10 00:05:29 UTC jaymzh
* Added comment:
Since the copy and paste of the strace didn't work very well, I attached another copy.

--------------------------------------------------------------------------------
2008-07-01 20:39:18 UTC jaymzh
* Added comment:
Ping. Anyone have a chance to look at this? Is there any extra data I can provide?

--------------------------------------------------------------------------------
2008-07-01 20:51:47 UTC Brendan Cully
* Added comment:
{{{
On Tuesday, 01 July 2008 at 20:39, Mutt wrote:
> #3074: Mutt hangs indefinitely
> 
> Comment (by jaymzh):
> 
>  Ping. Anyone have a chance to look at this? Is there any extra data I can
>  provide?

My guess about ERESTARTSYS is that it's the same value as some other
error condition -- you could probably read errno.h or one of the files
it includes and find another name for the value.

As for the error itself, I am guessing that the problem is a NAT
router between mutt and the server that is forgetting about the
connection (but not resetting it). You might be able to work around it
by lowering your values for timeout and/or to force more frequent
polling messages across the line, which should act as keepalive
traffic at the NAT router.
}}}

--------------------------------------------------------------------------------
2008-07-01 21:16:47 UTC jaymzh
* Added comment:
I find that unlikely for the following reasons:

 1. It always happens while active (half way through fetching message headers 99% of the time)
 2. It often happens during interactive use (hit '$', it fetches messages, gets, say 5 into 20 new ones, and then freezes).

I'm also currently running mutt on a machine directly on the 'net with no NAT to a server directly on the net with no NAT.

--------------------------------------------------------------------------------
2008-07-01 21:19:50 UTC brendan
* Updated description:
On many occasions, I find that an unattended mutt (1.5.17+20080114-1+b1 from Debian) will hang indefinitely. Never having time to debug it, I usually kill it and restart it, but today I let it run in strace until it happend. Here's what the end of the strace looks like - it seems to get an ERESTARTSYS while reading from the imaps socket! ERESTARTSYS is reserved as a signal between drivers and the kernel signal handling code and is, theoretically, never supposed to make it to userspace. Thus, this is quite odd.

I'm happy to add ltraces or whatever else may be useful....
{{{
write(1, "\r\33[17B\33[37m\33[40mFetching message"..., 76) = 76
write(1, "\33[79;29H\33[37m\33[40m1834/1838 (99%"..., 54) = 54
gettimeofday({1212696193, 854853}, NULL) = 0
getrusage(RUSAGE_SELF, {ru_utime={0, 812050}, ru_stime={0, 236014}, ...}) = 0
time(NULL)                              = 1212696193
times({tms_utime=81, tms_stime=23, tms_cutime=1, tms_cstime=2}) = 1734919932
gettimeofday({1212696193, 855156}, NULL) = 0
getrusage(RUSAGE_SELF, {ru_utime={0, 812050}, ru_stime={0, 236014}, ...}) = 0
time(NULL)                              = 1212696193
times({tms_utime=81, tms_stime=23, tms_cutime=1, tms_cstime=2}) = 1734919932
send(4, "\27\3\1\0\373*\341\374\221\t.p\233T*Y\354\0\223\303><\205"..., 256, 0) = 256
_llseek(5, 0, [0], SEEK_SET)            = 0
recv(4, "\27\3\1\v\33", 5, 0)           = 5
recv(4, "x\204\315|I\203Ph`\317\205-\356\16L\315S\3551\263\302N"..., 2842, 0) = 1383
recv(4, "\246\212O\2675gm\364wR!\16\31\256\361\374\373\21\352u#"..., 1459, 0) = 1388
recv(4, 0x81d31d0, 71, 0)               = ? ERESTARTSYS (To be restarted)
--- SIGWINCH (Window changed) @ 0 (0) ---
sigreturn()                             = ? (mask now [])
recv(4,  <unfinished ...>
}}}

- Phil

--------------------------------------------------------------------------------
2008-07-01 21:21:12 UTC brendan
* Added comment:
Yes, that doesn't sound like a NAT problem. I think it would be helpful to attach GDB to the mutt process when it becomes wedged and get a backtrace.

--------------------------------------------------------------------------------
2008-07-01 21:31:19 UTC jaymzh
* Added comment:
Will do. I can usually get it to happen within a day (it's fairly unpredictable, hence I usually get it unattended)

- Phil

--------------------------------------------------------------------------------
2008-07-02 09:42:35 UTC jaymzh
* Added comment:
Not a terribly useful bt I'm afraid:

(gdb) bt
#0  0xffffe410 in __kernel_vsyscall ()
#1  0xb7dcc461 in recv () from /lib/i686/cmov/libc.so.6
#2  0xb7eaf91c in ?? () from /usr/lib/libgnutls.so.26
#3  0x00000004 in ?? ()
#4  0x081f092c in ?? ()
#5  0x00000376 in ?? ()
#6  0x00000000 in ?? ()
(gdb) 

It does imply it's waiting for network traffic - but there's no NAT between these two machines. There are, in this case, stateful firewalls (both are iptables using the conntrack module).


--------------------------------------------------------------------------------
2008-07-03 02:23:05 UTC Brendan Cully
* Added comment:
{{{
>  Not a terribly useful bt I'm afraid:
> 
>  (gdb) bt
>  #0  0xffffe410 in __kernel_vsyscall ()
>  #1  0xb7dcc461 in recv () from /lib/i686/cmov/libc.so.6
>  #2  0xb7eaf91c in ?? () from /usr/lib/libgnutls.so.26
>  #3  0x00000004 in ?? ()
>  #4  0x081f092c in ?? ()
>  #5  0x00000376 in ?? ()
>  #6  0x00000000 in ?? ()
>  (gdb)
> 
>  It does imply it's waiting for network traffic - but there's no NAT
>  between these two machines. There are, in this case, stateful firewalls
>  (both are iptables using the conntrack module).

Can you build mutt with debugging symbols? Perhaps with --enable-debug
and CFLAGS=-ggdb.

It also might be worth trying to use openssl instead of gnutls. I
think I remember reports of hangs with gnutls in the past.
}}}

--------------------------------------------------------------------------------
2008-07-03 14:35:49 UTC jaymzh
* Added comment:
So, the funny thing is I actually had the debug symbols loaded in gdb. Here's from just after mutt launch:

{{{
#0  0xffffe410 in __kernel_vsyscall ()
#1  0xb7de7aeb in poll () from /lib/i686/cmov/libc.so.6
#2  0xb7f8b76a in _nc_timed_wait () from /lib/libncursesw.so.5
#3  0xb7f6c6ed in _nc_wgetch () from /lib/libncursesw.so.5
#4  0xb7f6d2a1 in wgetch () from /lib/libncursesw.so.5
#5  0x08063795 in mutt_getch () at ../curs_lib.c:99
#6  0x0807f923 in km_dokey (menu=6) at ../keymap.c:391
#7  0x080648c4 in mutt_index_menu () at ../curs_main.c:631
#8  0x08081d28 in main (argc=1, argv=0xbff5a2f4) at ../main.c:1005
}}}

But at freeze time:
{{{
#0  0xffffe410 in __kernel_vsyscall ()
#1  0xb7df3461 in recv () from /lib/i686/cmov/libc.so.6
#2  0xb7ed691c in ?? () from /usr/lib/libgnutls.so.26
#3  0x00000004 in ?? ()
#4  0x08750738 in ?? ()
#5  0x00000554 in ?? ()
#6  0x00000000 in ?? ()
}}}

Which is very strange... I'm still debugging, but I can't see any good reason for this.

--------------------------------------------------------------------------------
2008-07-03 16:13:33 UTC Brendan Cully
* Added comment:
{{{
On Thursday, 03 July 2008 at 14:35, Mutt wrote:
> #3074: Mutt hangs indefinitely
> 
> Comment (by jaymzh):
> 
>  So, the funny thing is I actually had the debug symbols loaded in gdb.
>  Here's from just after mutt launch:
> 
>  {{{
>  #0  0xffffe410 in __kernel_vsyscall ()
>  #1  0xb7de7aeb in poll () from /lib/i686/cmov/libc.so.6
>  #2  0xb7f8b76a in _nc_timed_wait () from /lib/libncursesw.so.5
>  #3  0xb7f6c6ed in _nc_wgetch () from /lib/libncursesw.so.5
>  #4  0xb7f6d2a1 in wgetch () from /lib/libncursesw.so.5
>  #5  0x08063795 in mutt_getch () at ../curs_lib.c:99
>  #6  0x0807f923 in km_dokey (menu=6) at ../keymap.c:391
>  #7  0x080648c4 in mutt_index_menu () at ../curs_main.c:631
>  #8  0x08081d28 in main (argc=1, argv=0xbff5a2f4) at ../main.c:1005
>  }}}
> 
>  But at freeze time:
>  {{{
>  #0  0xffffe410 in __kernel_vsyscall ()
>  #1  0xb7df3461 in recv () from /lib/i686/cmov/libc.so.6
>  #2  0xb7ed691c in ?? () from /usr/lib/libgnutls.so.26
>  #3  0x00000004 in ?? ()
>  #4  0x08750738 in ?? ()
>  #5  0x00000554 in ?? ()
>  #6  0x00000000 in ?? ()
>  }}}
> 
>  Which is very strange... I'm still debugging, but I can't see any good
>  reason for this.

Looks like stack corruption somewhere after entering gnutls. Does it
help to build against openssl? Or perhaps to use a newer version of
gnutls?
}}}

--------------------------------------------------------------------------------
2008-07-18 14:36:13 UTC jaymzh
* Added comment:
OK, compiled with openssl and still get the same behavior. Sorry for the delay.

Stacktrace:
{{{
(gdb) bt
#0  0xffffe410 in __kernel_vsyscall ()
#1  0xb7cf1ec3 in read () from /lib/i686/cmov/libc.so.6
#2  0xb7e3dd47 in ?? () from /usr/lib/i686/cmov/libcrypto.so.0.9.8
#3  0x00000003 in ?? ()
#4  0x081231e0 in ?? ()
#5  0x00000541 in ?? ()
#6  0xb7ed0124 in ?? () from /usr/lib/i686/cmov/libcrypto.so.0.9.8
#7  0x0811ca80 in ?? ()
#8  0x00000000 in ?? ()
}}}

ldd:
{{{
        linux-gate.so.1 =>  (0xffffe000)
        libncurses.so.5 => /lib/libncurses.so.5 (0xb7ed0000)
        libssl.so.0.9.8 => /usr/lib/i686/cmov/libssl.so.0.9.8 (0xb7e8b000)
        libcrypto.so.0.9.8 => /usr/lib/i686/cmov/libcrypto.so.0.9.8 (0xb7d41000)
        libz.so.1 => /usr/lib/libz.so.1 (0xb7d2c000)
        libgdbm.so.3 => /usr/lib/libgdbm.so.3 (0xb7d26000)
        libc.so.6 => /lib/i686/cmov/libc.so.6 (0xb7bcb000)
        libdl.so.2 => /lib/i686/cmov/libdl.so.2 (0xb7bc6000)
        /lib/ld-linux.so.2 (0xb7f19000)
}}}

--------------------------------------------------------------------------------
2008-07-18 20:39:57 UTC brendan
* Added comment:
Still stack corruption after frame 2. I can't tell what's going on, but it may be a mutt bug.

* milestone changed to 1.6

--------------------------------------------------------------------------------
2008-07-24 00:11:04 UTC jaymzh
* Added comment:
Note that when I recompiled with openssl, I also moved to 1.5.18 (I just grabbed the latest sources), so this also affects that version too.

Is there any other debugging I can do or information I can provide?

* version changed to 1.5.18

--------------------------------------------------------------------------------
2008-07-24 19:26:45 UTC agriffis
* Added comment:
Why is mutt in crypto code while fetching message headers anyway?  SSL?  Also wondering what the SIGWINCH is doing there...  terminal resized?  That could account for the syscall being interrupted.  Maybe the corruption happens while handling that signal.

--------------------------------------------------------------------------------
2008-07-24 19:45:47 UTC agriffis
* Added comment:
In fact, if the problem could be the SIGWINCH then maybe jaymzh should switch to non-SSL for testing, to see if the bug can be repro'd without SSL in the picture.

--------------------------------------------------------------------------------
2008-07-24 20:08:08 UTC jaymzh
* Added comment:
This most often happens on an unattended mutt (i.e. I'm in another virtual desktop for 10 minutes, come back, and it's frozen), so there's no resizing of the terminal. So a SIGWINCH is pretty odd.

As for testing without SSL, not with my current setup - I'm not sending my passwords in plain text over the internet. I may be able to setup a test scenario, but I don't know when.

--------------------------------------------------------------------------------
2008-07-24 20:16:56 UTC brendan
* Added comment:
It would be good to run mutt with -d2 and paste the last 10 or so lines of ~/.muttdebug0 (you may want to anonymize it a bit). There might be some clues there.

--------------------------------------------------------------------------------
2008-07-28 20:37:31 UTC jaymzh
* Added comment:
I've now gone two days where I ran mutt with -d2 and it didn't hang. But if I start it up without it I can get a hang very quickly. Something about the debug code path seems to prevent it from hanging.

I can try it with -d1 tomorrow, I guess.

- Phil

--------------------------------------------------------------------------------
2008-07-29 13:48:12 UTC Phil Dibowitz
* Added comment:
{{{
On Mon, Jul 28, 2008 at 08:37:31PM -0000, Mutt wrote:
> #3074: Mutt hangs indefinitely
> 
> Comment (by jaymzh):
> 
>  I've now gone two days where I ran mutt with -d2 and it didn't hang. But
>  if I start it up without it I can get a hang very quickly. Something about
>  the debug code path seems to prevent it from hanging.
> 
>  I can try it with -d1 tomorrow, I guess.

I seem to have lost TICKET_APPEND privs to this ticket - can someone fix that?
Anyway, waht I was trying to add through the website was:

Hmm. It froze again, but this time on fetching a message, not on headers.
Debug was on level 1. Here's the last handful of lines:

{{{
Updating progress: 25600
Updating progress: 26624
Updating progress: 27648
Updating progress: 28672
Updating progress: 29696
Updating progress: 30720
Updating progress: 31744
Updating progress: 32768
../commands.c:112: mutt_mktemp returns "/tmp/mutt-rc3-1000-18353-101".
WEED is Set
WEED is Set
../commands.c:112: mutt_mktemp returns "/tmp/mutt-rc3-1000-18353-102".
WEED is Set
WEED is Set
mutt_free_body: Not unlinking qsoiaragoyuk.jpeg.
mutt_free_body: Not unlinking qigakeql.jpeg.
../../imap/message.c:407: mutt_mktemp returns "/tmp/mutt-rc3-1000-18353-103".
Updating progress: 0
Updating progress: 0
Updating progress: 1024
Updating progress: 2048
Updating progress: 3072
Updating progress: 4096
Updating progress: 5120
Updating progress: 6144
Updating progress: 7168
Updating progress: 8192
Updating progress: 9216
Updating progress: 10240
Updating progress: 11264
Updating progress: 12288
Updating progress: 13312
Updating progress: 14336
Updating progress: 15360
}}}
}}}

--------------------------------------------------------------------------------
2008-07-29 15:19:42 UTC Phil Dibowitz
* Added comment:
{{{
Another one. Dubug 1 was on - this time it hung during purging messages.
Stacktrace:

#0  0xffffe410 in __kernel_vsyscall ()
#1  0xb7d09461 in recv () from /lib/i686/cmov/libc.so.6
#2  0xb7ded91c in ?? () from /usr/lib/libgnutls.so.26
#3  0x00000005 in ?? ()
#4  0x081fc150 in ?? ()
#5  0x00000005 in ?? ()
#6  0x00000000 in ?? ()


debug log:
Updating progress: 40960
Updating progress: 41984
Updating progress: 43008
Updating progress: 44032
Updating progress: 45056
Updating progress: 46080
Updating progress: 47104
Updating progress: 48128
Updating progress: 49152
Updating progress: 50176
Updating progress: 51200
Updating progress: 52224
Updating progress: 53248
Updating progress: 54272
Updating progress: 55296
Updating progress: 56320
Updating progress: 57344
Updating progress: 58368
Updating progress: 59392
Updating progress: 60416
Updating progress: 61440
Updating progress: 62464
Updating progress: 63488
Updating progress: 64512
Updating progress: 65536
Updating progress: 66560
../commands.c:112: mutt_mktemp returns "/tmp/mutt-rc3-1000-20384-37".
WEED is Set

}}}

--------------------------------------------------------------------------------
2008-08-08 12:45:26 UTC jaymzh
* Added comment:
OK, I managed to get it to do this with debug set to 2. Before it was on my laptop, this time it was on a significantly faster desktop. Seems perhaps something race-y?

{{{
(gdb) bt
#0  0xb7d8fcdc in recv () from /lib/libc.so.6
#1  0xb7e6c91c in ?? () from /usr/lib/libgnutls.so.26
#2  0x00000005 in ?? ()
#3  0x0a01ba70 in ?? ()
#4  0x00000a84 in ?? ()
#5  0x00000000 in ?? ()
}}}

debug log:
{{{
parse_parameters: `boundary="----------=_489C39A3.954AC0DE"'
parse_parameter: `boundary' = `----------=_489C39A3.954AC0DE'
5< a0064 OK Completed (0.000 sec)
Overriding UIDNEXT: 369705 -> 369712
5> a0065 NOOP
5< * 48 EXISTS
Handling EXISTS
cmd_handle_untagged: New mail in INBOX - 48 messages total.
5< * 48 RECENT
5< a0065 OK Completed
imap_cmd_finish: Fetching new mail
../../imap/message.c:104: mutt_mktemp returns "/tmp/mutt-rider-1000-31911-35".
Updating progress: 0
Updating progress: 39
5> a0066 FETCH 39:48 (UID FLAGS INTERNALDATE RFC822.SIZE BODY.PEEK[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LINES LIST-POST X-LABEL)])
}}}

--------------------------------------------------------------------------------
2008-08-08 12:51:16 UTC jaymzh
* Added comment:
Oh, I should mention that for the above log/backtrace, it's using:

  mutt   1.5.18
  gnutls 2.4.1

Since I can reproduce the bug with openssl and gnutls, I've switched to the distro default compile that uses gnutls instead of my hand-compiled openssl one.

Please let me know if there is anything else I can provide.

--------------------------------------------------------------------------------
2008-08-11 19:45:00 UTC jaymzh
* Added comment:
Any word on this? Anything else you need me to test or provide?

--------------------------------------------------------------------------------
2008-08-14 12:18:22 UTC jaymzh
* Added comment:
Another one. BT:

{{{
#0  0xb7c9ecdc in recv () from /lib/libc.so.6
#1  0xb7d7b91c in ?? () from /usr/lib/libgnutls.so.26
#2  0x00000005 in ?? ()
#3  0x087ce640 in ?? ()
#4  0x0000007e in ?? ()
#5  0x00000000 in ?? ()
}}}

debug:
{{{
5< * 50 FETCH (FLAGS (\Recent) UID 375838 INTERNALDATE "14-Aug-2008 07:22:10 -0400" RFC822.SIZE 110982 BODY[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LINES LIST-POST X-LABEL)] {376}
Handling FETCH
FETCH response ignored for this message
imap_read_literal: reading 376 bytes
5< )
parse_parameters: `boundary="----=_Part_9086_10854502.1218712866185"'
parse_parameter: `boundary' = `----=_Part_9086_10854502.1218712866185'
5< a0051 OK Completed (0.000 sec)
IMAP queue drained
Overriding UIDNEXT: 375833 -> 375839
5> a0052 NOOP
5< * 55 EXISTS
Handling EXISTS
cmd_handle_untagged: New mail in INBOX - 55 messages total.
5< * 55 RECENT
5< a0052 OK Completed
IMAP queue drained
imap_cmd_finish: Fetching new mail
../../imap/message.c:104: mutt_mktemp returns "/tmp/mutt-rider-1000-10418-27".
Updating progress: 0
Updating progress: 51
5> a0053 FETCH 51:55 (UID FLAGS INTERNALDATE RFC822.SIZE BODY.PEEK[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LINES LIST-POST X-LABEL)])
}}}

--------------------------------------------------------------------------------
2008-08-14 14:37:27 UTC jaymzh
* Added comment:
Another.

Debug log:
{{{
5< a0060 OK Completed
imap_cmd_finish: Expunging mailbox
Expunging message UID 375979.
Expunging message UID 375980.
Expunging message UID 375981.
Expunging message UID 375982.
Expunging message UID 375983.
You are on the last message.
5> a0061 NOOP
5< * 43 EXISTS
Handling EXISTS
cmd_handle_untagged: New mail in INBOX - 43 messages total.
5< * 23 RECENT
5< a0061 OK Completed
imap_cmd_finish: Fetching new mail
../../imap/message.c:104: mutt_mktemp returns "/tmp/mutt-rider-1000-11200-43".
Updating progress: 0
Updating progress: 36
5> a0062 FETCH 36:43 (UID FLAGS INTERNALDATE RFC822.SIZE BODY.PEEK[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LINES LIST-POST X-LABEL)])
}}}

BT:
{{{
#0  0xb7c90cdc in recv () from /lib/libc.so.6
#1  0xb7d6d91c in ?? () from /usr/lib/libgnutls.so.26
#2  0x00000005 in ?? ()
#3  0x0a127a20 in ?? ()
#4  0x000004dc in ?? ()
#5  0x00000000 in ?? ()
}}}

--------------------------------------------------------------------------------
2008-08-14 15:55:31 UTC jaymzh
* Added comment:
Again:

debug logs:
{{{
Handling EXISTS
5< * 15 RECENT
5< a0060 OK Completed
imap_cmd_finish: Expunging mailbox
Expunging message UID 375979.
Expunging message UID 375980.
Expunging message UID 375981.
Expunging message UID 375982.
Expunging message UID 375983.
You are on the last message.
5> a0061 NOOP
5< * 43 EXISTS
Handling EXISTS
cmd_handle_untagged: New mail in INBOX - 43 messages total.
5< * 23 RECENT
5< a0061 OK Completed
imap_cmd_finish: Fetching new mail
../../imap/message.c:104: mutt_mktemp returns "/tmp/mutt-rider-1000-11200-43".
Updating progress: 0
Updating progress: 36
5> a0062 FETCH 36:43 (UID FLAGS INTERNALDATE RFC822.SIZE BODY.PEEK[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LINES LIST-POST X-LABEL)])
}}}

bt:
{{{
#0  0xb7dcacdc in recv () from /lib/libc.so.6
#1  0xb7ea791c in ?? () from /usr/lib/libgnutls.so.26
#2  0x00000004 in ?? ()
#3  0x0947aa58 in ?? ()
#4  0x00000a84 in ?? ()
#5  0x00000000 in ?? ()
}}}

--------------------------------------------------------------------------------
2008-08-14 19:52:55 UTC jaymzh
* Added comment:
Happened again.

debug log:
{{{
5< * 35 EXISTS
Handling EXISTS
5< * 15 RECENT
5< a0060 OK Completed
imap_cmd_finish: Expunging mailbox
Expunging message UID 375979.
Expunging message UID 375980.
Expunging message UID 375981.
Expunging message UID 375982.
Expunging message UID 375983.
You are on the last message.
5> a0061 NOOP
5< * 43 EXISTS
Handling EXISTS
cmd_handle_untagged: New mail in INBOX - 43 messages total.
5< * 23 RECENT
5< a0061 OK Completed
imap_cmd_finish: Fetching new mail
../../imap/message.c:104: mutt_mktemp returns "/tmp/mutt-rider-1000-11200-43".
Updating progress: 0
Updating progress: 36
5> a0062 FETCH 36:43 (UID FLAGS INTERNALDATE RFC822.SIZE BODY.PEEK[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LINES LIST-POST X-LABEL)])
}}}

bt
{{{
#0  0xb7dcdcdc in recv () from /lib/libc.so.6
#1  0xb7eaa91c in ?? () from /usr/lib/libgnutls.so.26
#2  0x00000004 in ?? ()
#3  0x08f14698 in ?? ()
#4  0x000007b9 in ?? ()
#5  0x00000000 in ?? ()
}}}

--------------------------------------------------------------------------------
2008-08-14 22:00:38 UTC brendan
* Added comment:
Reading through these traces (the last few of which appear to be identical), I don't see anything amiss. I can't imagine what the race could be, since mutt is single-threaded. In each of these, the last line of the trace indicates that mutt has sent out a command to the server and is now waiting for a response.

At this point I'm not sure what would help except for tracing the server side to see if it actually receives the last command sent by mutt, and whether if it has, it's sent a response that mutt isn't receiving.

--------------------------------------------------------------------------------
2008-08-15 10:23:47 UTC jaymzh
* Added comment:
Well, in theory that seems fine except for the stack corruption, right?

I'm running it now with a tcpdump on both ends. It's SSL encrypted, but at least we can see the general flow of traffic.

--------------------------------------------------------------------------------
2008-08-15 14:07:59 UTC jaymzh
* Added comment:
So, my tcpdumps seem to support your theory that mutt sends a request...
however it seems like it's not finishing sending the request (perhaps due to
stack corruption?). Here's the tcpdumps:

From my desktop:
{{{
14:40:17.045181 IP rider.40162 > mail.ipom.com.imaps: . ack 150264 win 1002 <nop,nop,timestamp 147177642 1920054529>
14:40:17.045920 IP rider.40162 > mail.ipom.com.imaps: P 11235:11487(252) ack 150264 win 1002 <nop,nop,timestamp 147177643 1920054529>
14:40:17.173980 IP mail.ipom.com.imaps > rider.40162: . ack 11487 win 8416 <nop,nop,timestamp 1920054658 147177643>
14:40:17.176517 IP mail.ipom.com.imaps > rider.40162: . 150264:151712(1448) ack 11487 win 8416 <nop,nop,timestamp 1920054659 147177643>
14:40:17.177599 IP mail.ipom.com.imaps > rider.40162: . 151712:153160(1448) ack 11487 win 8416 <nop,nop,timestamp 1920054659 147177643>
14:40:17.177615 IP rider.40162 > mail.ipom.com.imaps: . ack 153160 win 1002 <nop,nop,timestamp 147177676 1920054659>
}}}

From the server
{{{
08:40:16.306438 IP mail.ipom.com.imaps > 84-74-89-238.dclient.hispeed.ch.40162: P 104982:105073(91) ack 10019 win 8416 <nop,nop,timestamp 1920054529 147176538>
08:40:16.434817 IP 84-74-89-238.dclient.hispeed.ch.40162 > mail.ipom.com.imaps: . ack 105073 win 1002 <nop,nop,timestamp 147177642 1920054529>
08:40:16.434830 IP 84-74-89-238.dclient.hispeed.ch.40162 > mail.ipom.com.imaps: P 10019:10271(252) ack 105073 win 1002 <nop,nop,timestamp 147177643 1920054529>
08:40:16.434867 IP mail.ipom.com.imaps > 84-74-89-238.dclient.hispeed.ch.40162: . ack 10271 win 8416 <nop,nop,timestamp 1920054658 147177643>
08:40:16.436319 IP mail.ipom.com.imaps > 84-74-89-238.dclient.hispeed.ch.40162: . 105073:106521(1448) ack 10271 win 8416 <nop,nop,timestamp 1920054659 147177643>
08:40:16.436346 IP mail.ipom.com.imaps > 84-74-89-238.dclient.hispeed.ch.40162: . 106521:107969(1448) ack 10271 win 8416 <nop,nop,timestamp 1920054659 147177643>
08:40:16.576475 IP 84-74-89-238.dclient.hispeed.ch.40162 > mail.ipom.com.imaps: . ack 107969 win 1002 <nop,nop,timestamp 147177676 1920054659>
}}}


The logs on the imap server show:

{{{
Aug 15 08:57:13 virt cyrus/imaps[17989]: Connection timed out, closing connection
}}}

and the last thing before that is expunging messages at 08:30:10 - so it never
seems a full request. Note that once again, the stack is corrupted:

{{{
#0  0xb7d70cdc in recv () from /lib/libc.so.6
#1  0xb7e4d91c in ?? () from /usr/lib/libgnutls.so.26
#2  0x00000004 in ?? ()
#3  0x093944c8 in ?? ()
#4  0x000004dc in ?? ()
#5  0x00000000 in ?? ()
}}}

But note that it's not missing symbols, as I can get a nice stacktrace with a
happy mutt:

{{{
#0  0xb7d5f586 in poll () from /lib/libc.so.6
#1  0xb7fe19ca in _nc_timed_wait () from /lib/libncursesw.so.5
#2  0xb7fc26fd in _nc_wgetch () from /lib/libncursesw.so.5
#3  0xb7fc32b1 in wgetch () from /lib/libncursesw.so.5
#4  0x08063a15 in mutt_getch () at ../curs_lib.c:99
#5  0x0807fc43 in km_dokey (menu=6) at ../keymap.c:391
#6  0x08064b44 in mutt_index_menu () at ../curs_main.c:631
#7  0x08082048 in main (argc=2, argv=0xbf81a6e4) at ../main.c:1005
}}}

--------------------------------------------------------------------------------
2008-08-19 09:26:20 UTC jaymzh
* Added comment:
Any thoughts on this? Did my packet traces show anything? Can I provide more details? This is happening several times a day and it's quite frustrating.

Thanks,
- Phil

--------------------------------------------------------------------------------
2008-08-25 08:06:36 UTC brendan
* Added comment:
Actually the packet trace seems to indicate that the last thing going out from the mutt end is just an ACK, not actual data. I wonder if we're missing some kind of TLS-level flush call on the socket? I'm not sure whether we have a real stack corruption here or some weird gnutls effect. I don't really have a good idea what could be happening.

* component changed to IMAP
* owner changed to brendan

--------------------------------------------------------------------------------
2008-08-25 23:34:53 UTC jaymzh
* Added comment:
Well, I get the same results with openssl, so it's not a gnutls issue. That doesn't mean it's not TLS level though.

What further information can I provide?

--------------------------------------------------------------------------------
2008-08-31 03:21:56 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [c2af57b1e7ee]) Make gnutls read function more robust against interruptions.
Signals should be masked off anyway, but see #3074.

--------------------------------------------------------------------------------
2008-08-31 03:23:34 UTC brendan
* Added comment:
Care to give the patch referenced above a try? It's a bit of a stab in the dark, but may help.

--------------------------------------------------------------------------------
2008-09-09 11:54:12 UTC jaymzh
* Added comment:
Sorry for the delay, I've been traveling and swamped at work. Patch is applied and source is compiling. I'll let you know shortly.

--------------------------------------------------------------------------------
2008-09-09 19:41:49 UTC jaymzh
* Added comment:
Nope, same freeze. Same stack trace.

--------------------------------------------------------------------------------
2008-12-15 22:38:03 UTC jaymzh
* Added comment:
Ping?
Any further data I can provide?

--------------------------------------------------------------------------------
2008-12-31 19:13:24 UTC ry4an
* cc changed to ry4an-mutttrac@ry4an.org
* Added comment:
I'm seeing this same behavior.  I've got the no-symbols stackdump and all.

--------------------------------------------------------------------------------
2009-01-04 00:44:23 UTC brendan
* keywords changed to duplicate 2938

--------------------------------------------------------------------------------
2009-01-05 00:21:06 UTC brendan
* status changed to accepted

--------------------------------------------------------------------------------
2009-06-07 06:04:03 UTC brendan
* Added comment:
Downgrading from 1.6-blocker.

* milestone changed to 2.0
