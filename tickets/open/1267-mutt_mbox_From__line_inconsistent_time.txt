Ticket:  1267
Status:  reopened
Summary: mutt: mbox: From_ line inconsistent time

Reporter: Eduardo Pérez Ureta <eperez@it.uc3m.es>
Owner:    mutt-dev

Opened:       2002-07-10 02:07:04 UTC
Last Updated: 2009-04-16 16:36:05 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: wishlist

There is a problem in the From line of mbox messages.
You are using ctime() and that function uses localtime without saving
the timezone. So you must use asctime (gmtime (&t)) for these dates to
have any meaning.

It moves from localtime to gmtime. Because localtimes must
be accompanied with a timezone and rollback one hour once a year.

There are two ways to close this bug:
- Applying the patch.
- Adding the timezone in the form +0100, but this would break the format
}}}

--------------------------------------------------------------------------------
2002-07-10 02:07:04 UTC Eduardo Pérez Ureta <eperez@it.uc3m.es>
* Added attachment mutt_correct_mbox_from_line.diff
* Added comment:
mutt_correct_mbox_from_line.diff

--------------------------------------------------------------------------------
2005-08-24 12:04:37 UTC rado
* Added comment:
{{{
Does this really apply?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-24 12:04:38 UTC rado
* Added comment:
{{{
Isn't the "From " line supposed to indicate the "received" time,
which actually only matters to the recipient, therefore no other than localtime needed?
I'm using that "From " time personally and I'd either get confused by
not knowing what time it locally means or get crazy because of miscalculations. :)
}}}

--------------------------------------------------------------------------------
2005-09-22 09:38:45 UTC rado
* Added comment:
{{{
no response for 4 weeks.
}}}

* status changed to closed

--------------------------------------------------------------------------------
2005-11-11 09:47:20 UTC Eduardo Pérez Ureta <eduardo.perez@uc3m.es>
* Added comment:
{{{
On 2005-08-23 19:04:37 +0200, Rado Smiljanic wrote:
> Synopsis: mutt: mbox: From line inconsistent time
> 
> State-Changed-From-To: open->feedback
> State-Changed-By: rado
> State-Changed-When: Tue, 23 Aug 2005 19:04:37 +0200
> State-Changed-Why:
> Does this really apply?
> 
> 
> 
> **** Comment added by rado on Tue, 23 Aug 2005 19:04:37 +0200 ****
>  Isn't the "From " line supposed to indicate the "received" time,

Yes.

> which actually only matters to the recipient, therefore no other than localtime needed?
> I'm using that "From " time personally and I'd either get confused by
> not knowing what time it locally means or get crazy because of miscalculations. :)

You can't know what localtime is when you move around the world.
If you use UTC there's no problem at all.
If you save the "localtime" all sort of problems begin:
- You have to remember where the computer that was running the MUA was
  when it received the email (There's no timezone attached there)
- As there are no timezone there are an hour when the DST changes that
  you don't know when the email was received.

I think it's the correct idea TM to store the From time in UTC.
If you want you can convert that time to the localtime to visualize it
in mutt.

I solve many timezone related problems each day and storing UTC times is
always the best idea, then it's easy to show the time in whatever
timezone/timeformat the user wants.
}}}

--------------------------------------------------------------------------------
2005-11-12 08:06:27 UTC rado
* Added comment:
{{{
On 2005-08-23 19:04:37 +0200, Rado Smiljanic wrote:
There is still life, and a valid reasoning
}}}

* status changed to new

--------------------------------------------------------------------------------
2005-11-12 08:06:28 UTC rado
* Added comment:
{{{
I didn't think of roaming users, you've got a point there.
BTW, your mail made it through, simply replying to bugs-any
is enough.
(added "_" to From in subject)
}}}

--------------------------------------------------------------------------------
2009-04-16 16:36:05 UTC pdmef
* Added comment:
Since there's no official mbox spec, I don't know really what to do. The change is trivial, qmail's mbox(5) manpage as well as other sources say UTC is to be used. On the other hand, popular tools like procmail use localtime.

If we made this change, all mails written by procmail to mbox would be broken plus those written by mutt.

* Updated description:
{{{
Package: mutt
Version: 1.4i
Severity: wishlist

There is a problem in the From line of mbox messages.
You are using ctime() and that function uses localtime without saving
the timezone. So you must use asctime (gmtime (&t)) for these dates to
have any meaning.

It moves from localtime to gmtime. Because localtimes must
be accompanied with a timezone and rollback one hour once a year.

There are two ways to close this bug:
- Applying the patch.
- Adding the timezone in the form +0100, but this would break the format
}}}
