Ticket:  64
Status:  new
Summary: mutt: set reply-to on commandline

Reporter: Joost van Baal <joostvb@xs4all.nl>
Owner:    mutt-dev

Opened:       2000-03-04 12:19:30 UTC
Last Updated: 2005-08-06 20:19:29 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.0.1-9
Severity: wishlist

Hi,

It would be nice if it were able to set the Reply-To header on the commandline
in mutt, and have this address be subject to alias expansion. The REPLYTO 
environment variable is not subject to alias expansion, nor is setting a 
"my_hdr Reply-To: foo" as a -e flag to mutt. I added a -r commandline switch. 
Here's my patch to the mutt 1.1.7i source:


diff -durN mutt-1.1.7/doc/manual.sgml mutt-1.1.7-hacked/doc/manual.sgml
--- mutt-1.1.7/doc/manual.sgml	Wed Mar  1 01:19:52 2000
+++ mutt-1.1.7-hacked/doc/manual.sgml	Fri Mar  3 18:26:17 2000
@@ -2598,6 +2598,7 @@
 -n      do not read the system Muttrc
 -m      specify a default mailbox type
 -p      recall a postponed message
+-r      specify a reply-to (Reply-To) address
 -R      open mailbox in read-only mode
 -s      specify a subject (enclose in quotes if it contains spaces)
 -v      show version number and compile-time definitions
diff -durN mutt-1.1.7/doc/mutt.man mutt-1.1.7-hacked/doc/mutt.man
--- mutt-1.1.7/doc/mutt.man	Tue Feb  1 19:44:04 2000
+++ mutt-1.1.7-hacked/doc/mutt.man	Fri Mar  3 18:17:02 2000
@@ -34,6 +34,7 @@
 [-H \fIdraftfile\fP]
 [-i \fIinclude\fP]
 [-m \fItype\fP]
+[-r \fIaddress\fP]
 [-s \fIsubject\fP]
 .SH DESCRIPTION
 .PP
@@ -68,6 +69,8 @@
 Causes Mutt to bypass the system configuration file.
 .IP "-p"
 Resume a postponed message.
+.IP "-r"
+Specify a reply-to (Reply-To) address.
 .IP "-R"
 Open a mailbox in \fIread-only\fP mode.
 .IP "-s \fIsubject\fP"
@@ -102,7 +105,7 @@
 .IP "TMPDIR"
 Directory in which temporary files are created.
 .IP "REPLYTO"
-Default Reply-To address.
+Default Reply-To address. Overrules -r flag.
 .IP "VISUAL"
 Editor to invoke when the ~v command is given in the builtin editor.
 .SH FILES
diff -durN mutt-1.1.7/main.c mutt-1.1.7-hacked/main.c
--- mutt-1.1.7/main.c	Thu Feb 24 22:24:36 2000
+++ mutt-1.1.7-hacked/main.c	Fri Mar  3 18:19:29 2000
@@ -117,6 +117,7 @@
   -m <type>\tspecify a default mailbox type\n\
   -n\t\tcauses Mutt not to read the system Muttrc\n\
   -p\t\trecall a postponed message\n\
+  -r\t\tspecify a reply-to (Reply-To) address\n\
   -R\t\topen mailbox in read-only mode\n\
   -s <subj>\tspecify a subject (must be in quotes if it has spaces)\n\
   -v\t\tshow version and compile-time definitions\n\
@@ -360,7 +361,7 @@
   memset (Options, 0, sizeof (Options));
   memset (QuadOptions, 0, sizeof (QuadOptions));
   
-  while ((i = getopt (argc, argv, "a:b:F:f:c:d:e:H:s:i:hm:npRvxyzZ")) != EOF)
+  while ((i = getopt (argc, argv, "a:b:F:f:c:d:e:H:s:i:hm:npr:RvxyzZ")) != EOF)
     switch (i)
     {
       case 'a':
@@ -378,12 +379,15 @@
 
       case 'b':
       case 'c':
+      case 'r':
 	if (!msg)
 	  msg = mutt_new_header ();
 	if (!msg->env)
 	  msg->env = mutt_new_envelope ();
 	if (i == 'b')
 	  msg->env->bcc = rfc822_parse_adrlist (msg->env->bcc, optarg);
+	else if (i == 'r')
+	  msg->env->reply_to = rfc822_parse_adrlist (msg->env->reply_to, optarg);
 	else
 	  msg->env->cc = rfc822_parse_adrlist (msg->env->cc, optarg);
 	break;
diff -durN mutt-1.1.7/send.c mutt-1.1.7-hacked/send.c
--- mutt-1.1.7/send.c	Thu Feb 17 19:20:16 2000
+++ mutt-1.1.7-hacked/send.c	Fri Mar  3 18:20:13 2000
@@ -1043,6 +1043,7 @@
     msg->env->to = mutt_expand_aliases (msg->env->to);
     msg->env->cc = mutt_expand_aliases (msg->env->cc);
     msg->env->bcc = mutt_expand_aliases (msg->env->bcc);
+    msg->env->reply_to =  mutt_expand_aliases (msg->env->reply_to);
   }
   else
   {


Patching as patch -p1 < patchfile in the mutt source gives the -r flag.

BTW, on Tue, 28 Dec 1999 17:36:40 +0100, I posted an earlier version of 
this patch on mutt-dev@mutt.org. 

I need this feature to get my work done. Hope this is of any use to 
someone else also :)

Bye,

Joost van Baal


-- System Information
Debian Release: 2.2
Kernel Version: Linux nagy 2.2.12 #1 Wed Sep 1 11:43:20 CEST 1999 i686 unknown


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-07 14:19:29 UTC brendan
* Added comment:
{{{
change-request
}}}
