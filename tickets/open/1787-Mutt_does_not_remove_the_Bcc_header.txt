Ticket:  1787
Status:  new
Summary: Mutt does not remove the Bcc header

Reporter: Vincent Lefèvre <vincent@vinc17.org>
Owner:    mutt-dev

Opened:       2004-01-31 08:26:17 UTC
Last Updated: 2005-10-18 08:36:44 UTC

Priority:  minor
Component: mutt
Keywords:  #2113

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.5.1-200401
Severity: important

-- Please type your report below this line

Mutt does not remove the Bcc header when sending a mail. I know that
there is a write_bcc option, but the default feature is dangerous as

  * the user may have write_bcc set (this is the default) and doesn't
    notice the problem;

  * it is possible that the user didn't need to unset it in the past,
    but when the MTA is changed, things are different and the user
    doesn't necessarily remember this option...

IMHO, the write_bcc option must be unset by default, and I even think
that it shouldn't exist: Mutt should always strip the Bcc header when
sending a mail by SMTP.

According to the RFCs 2821 and 2822, removing the Bcc line (or a similar
alternative method, as described in RFC 2822) is the responsability of
the MUA, except in the case where the envelope is to be built from the
headers by the MTA.

(I'm using exim, but according to the RFCs, it does nothing wrong.)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/3.2.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,f77,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.2 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-objc-gc i386-linux
Thread model: posix
gcc version 3.2.3 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.5.1i (2003-11-05)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.24 (i686) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.14)]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="mailbox"
PKGDATADIR="/home/lefevre/share/mutt"
SYSCONFDIR="/home/lefevre/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.3.24.ats.parent_match.1
patch-1.5.1.vl.savehist.1


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-02-07 14:45:06 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Salut Vincent,

On Friday, January 30, 2004 at 3:26:17 PM +0100, Vincent Lefèvre wrote:

> IMHO, the write_bcc option must be unset by default, and I even think
> that it shouldn't exist

   Allegedly some people need $write_bcc=yes, because their $sendmail
uses headers not arguments. And some need "no", because their $sendmail
doesn't strip "Bcc:". So it seems clear the option must exist.

   Now for a sensible default:

1) Current "yes" could lead to disclosure of "Bcc:" content to other
recipients, in case MTA didn't strip it (most do).

2) "no" could lead to mails silently not delivered to "Bcc:"
recipients, in case $sendmail parses header (finds "To:" and "Cc:", not
"Bcc:").

   It seems to me danger #2 is worse than #1 (Well, a matter of
opinion, I guess). But I don't know comparatively how many people are in
both cases. I only believe the vast majority can have either value
without visible change, because they have an MTA doing both "Bcc:"
stripping, and enveloppe building from command arguments.


   At this point my opinion is: Retain $write_bcc option, don't change
the default of $write_bcc, and close the bug #1787.


   Note: It seems Debian uses Exim by default, and has changed default
$write_bcc to "no" (thru /etc/Muttrc). This seems sensible for a
distribution.

   Note 2: $write_bcc=no doesn't write "Bcc:" field to $record folder,
which is bad. This is another bug in itself, #897 and Debian #122173,
and should not be taken into account to here decide a default
$write_bcc. People in case #1 (no strip) may want to look at Bug #122173
for a nice Exim friendly workaround.


Bye!	Alain.
-- 
A: Because it messes up the order in which people normally read text.
Q: Why is top-posting such a bad thing?
}}}

--------------------------------------------------------------------------------
2004-02-07 18:06:29 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
Salut Alain,

On 2004-02-06 21:45:06 +0100, Alain Bench wrote:
>  On Friday, January 30, 2004 at 3:26:17 PM +0100, Vincent Lefèvre wrote:
> > IMHO, the write_bcc option must be unset by default, and I even think
> > that it shouldn't exist
> 
>     Allegedly some people need $write_bcc=yes, because their $sendmail
> uses headers not arguments. And some need "no", because their $sendmail
> doesn't strip "Bcc:". So it seems clear the option must exist.

Well, perhaps this could be a workaround for broken MTAs (or if the
user modifies the value of $sendmail). But the manual is clear:

 6.3.228.  sendmail

 Type: path
 Default: "/usr/sbin/sendmail -oem -oi"

 Specifies the program and arguments used to deliver mail sent by Mutt.
 Mutt expects that the specified program interprets additional
 arguments as recipient addresses.

So, $sendmail should use arguments, not headers (in particular with the
default value of $sendmail).

>     Now for a sensible default:
> 
>  1) Current "yes" could lead to disclosure of "Bcc:" content to other
> recipients, in case MTA didn't strip it (most do).

Well, exim does strip it if it is called with options specifying that
headers should be used for the recipients. But Mutt doesn't do that
by default.

>  2) "no" could lead to mails silently not delivered to "Bcc:"
> recipients, in case $sendmail parses header (finds "To:" and "Cc:",
> not "Bcc:").

If they aren't, then this is either a bug in the MTA (which breaks the
compatibility with sendmail -- if Mutt developers want to accept that,
then $sendmail shouldn't have a default value, of course), or this is
because the user modified the $sendmail value, in which case he should
also have to set the $write_bcc value accordingly.

So, (2) is not an acceptable default for consistency with the default
$sendmail value.

>     It seems to me danger #2 is worse than #1 (Well, a matter of
> opinion, I guess).

I disagree because:
 * with (1), the user doesn't necessarily know that "Bcc:" hasn't been
   stripped, and this problem may last a long time (in my case, I Bcc
   to myself on a private address, and I don't want to receive spam or
   viruses there);
 * with (2), the user will quickly be warned when he sees that he
   doesn't receive replies or if the Bcc recipients complain about
   not receiving the mail they were waiting for or whatever reason...

Anyway, the most important point is consistency as explained above.

>     Note: It seems Debian uses Exim by default, and has changed default
> $write_bcc to "no" (thru /etc/Muttrc). This seems sensible for a
> distribution.

But I don't use the Debian package for Mutt (since I need to compile it
with my own patches).

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2004-02-10 14:07:51 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Saturday, February 7, 2004 at 1:06:29 AM +0100, Vincent Lefèvre wrote:

> $sendmail should use arguments, not headers

   Well args seems the natural and logical method, I agree. But that's
not the debate: Some users just do it the other way. I don't think we
are going to disallow it?


> the user modified the $sendmail value, in which case he should also
> have to set the $write_bcc value accordingly.

   Some $sendmail may use headers even without explicit -t option, by
default. Like be a script rewriting header then passing the mail to the
real MTA. Or users may add -t but be unaware of the interaction with
$write_bcc. Sensible defaults are less important if users know all.


>> danger #2 is worse than #1
> I disagree because: [snip 2 points about probs visibility]

   That really seems a matter of opinion, or circumstances. I mean:
Reading your 2 points I concluded the opposite of you. I think that
myself in those cases would have immediately noticed the unstripped
"Bcc:", at the first mail, but would perhaps take longer to notice
that part of recipients stays unreached.


> the most important point is consistency

   Point taken.

   OTOS changing the default will break config for users that do no set
explicitly $write_bcc because the default "yes" suits their needs.


Bye!	Alain.
-- 
set honor_followup_to=yes in muttrc is the default value, and makes your
list replies go where the original author wanted them to go: Only to the
list, or with a private copy.
}}}

--------------------------------------------------------------------------------
2004-02-11 22:06:15 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2004-02-09 21:07:51 +0100, Alain Bench wrote:
>  On Saturday, February 7, 2004 at 1:06:29 AM +0100, Vincent Lefèvre wrote:
> > $sendmail should use arguments, not headers
> 
>     Well args seems the natural and logical method, I agree. But that's
> not the debate: Some users just do it the other way. I don't think we
> are going to disallow it?

The manual says that arguments are used. So, if some users choose
to use headers, their configuration is broken and have to accept
the consequences.

> > the user modified the $sendmail value, in which case he should also
> > have to set the $write_bcc value accordingly.
> 
>     Some $sendmail may use headers even without explicit -t option, by
> default. Like be a script rewriting header then passing the mail to the
> real MTA. Or users may add -t but be unaware of the interaction with
> $write_bcc. Sensible defaults are less important if users know all.

All of these problems come from either the MTA's or the user's fault.

> > the most important point is consistency
> 
>     Point taken.
> 
>     OTOS changing the default will break config for users that do no set
> explicitly $write_bcc because the default "yes" suits their needs.

Users should know that defaults may change, in particular when they
have been inconsistent. And this change could still be documented in
the upgrade notes.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2004-02-11 23:40:36 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
On Mon, 09 Feb 2004, Alain Bench wrote:

> > the most important point is consistency
> 
>     Point taken.
> 
>     OTOS changing the default will break config for users that do no set
> explicitly $write_bcc because the default "yes" suits their needs.

It's time to change it.
1.5.6 is the last release from development branch.
1.5.6 changed the syntax of alternates.

I don't like potentially insecure default setting.

-- 
tamo
}}}

--------------------------------------------------------------------------------
2004-02-12 12:06:33 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Wed, Feb 11, 2004 at 05:06:15AM +0100, Vincent Lefevre wrote:

> All of these problems come from either the MTA's or the user's fault.

I still don't think that means we should gratuitously break a working
configuration. Particularly since, although the MTA may be broken, we have
an easy workaround.

-- 
Paul

I don't need to be made to look evil. I can do that on my own. 
--  Christopher Walken
}}}

--------------------------------------------------------------------------------
2004-02-12 17:20:54 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2004-02-11 20:06:33 +0000, Paul Walker wrote:
> On Wed, Feb 11, 2004 at 05:06:15AM +0100, Vincent Lefevre wrote:
> > All of these problems come from either the MTA's or the user's fault.
> 
> I still don't think that means we should gratuitously break a working
> configuration.

No, it does not work as expected by default with exim (which does
exactly what the RFC says).

> Particularly since, although the MTA may be broken, we have an easy
> workaround.

At least there should be a big warning if the default is not changed.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2005-10-19 02:36:44 UTC ab
* Added comment:
{{{
crossref duplicate #2113
deduppe unform
}}}
