Ticket:  2256
Status:  new
Summary: Feature Request: folder_format add count old messages

Reporter: philipp@kolmann.at
Owner:    mutt-dev

Opened:       2006-05-31 09:38:49 UTC
Last Updated: 2008-10-31 19:30:34 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
I have maildir running here with mark_old=yes.

It would be nice to have in the Mailbox Browser also a Possibility to show how many old-marked mails are in the folders (like the new count).

Thanks
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2008-10-31 19:30:34 UTC jukka
* cc changed to j+mutt@salmi.ch
