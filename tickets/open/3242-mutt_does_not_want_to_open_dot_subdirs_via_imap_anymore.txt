Ticket:  3242
Status:  accepted
Summary: mutt: does not want to open dot subdirs via imap anymore

Reporter: antonio@dyne.org
Owner:    lucki2791

Opened:       2009-06-03 07:37:06 UTC
Last Updated: 2016-08-30 04:52:25 UTC

Priority:  major
Component: browser
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/530671

This bug seems to happen only with dovecot, which is using a dot (".") as folder separator, particularly it happens *only if* the subdirs starts with a dot (INBOX.subdir is not considered), if you have a look at the .muttdebug0 attached it is clear that mutt is trying to open "/boot" and not ".boot".

How to reproduce:
{{{
1) open mutt, insert imap password, got the INBOX list
2) c?
3) select a subdir, press enter
4) error message: Invalid mailbox name
}}}

This happens only with 1.5.19 and we also tried to release a new version with the following two commits integrated:

{{{
  Prioritize the entered IMAP folder delimiter.
  http://dev.mutt.org/hg/mutt/rev/d6ee34f3ccaf

  Fix last commit
  http://dev.mutt.org/hg/mutt/rev/f76046ce4686
}}}

Unfortuantely this did not change the behavior.
I'm setting the priority as 'normal' and not 'minor' as I do usually because this is an 'important' bug on the debian side.



--------------------------------------------------------------------------------
2009-06-03 07:37:18 UTC antonio@dyne.org
* Added attachment muttdebug-luk

--------------------------------------------------------------------------------
2009-06-09 02:58:49 UTC brendan
* Added comment:
setting `$imap_delim_chars=/` will work around this until it's fixed.

* component changed to IMAP
* milestone changed to 1.6
* owner changed to brendan
* status changed to accepted

--------------------------------------------------------------------------------
2009-06-10 04:07:35 UTC brendan
* Added comment:
Note to myself: see #2897 for a set of cases not to break while fixing this one. This code is weirdly hard to get right -- I think the underlying task mutt has set itself may be oxymoronic.

--------------------------------------------------------------------------------
2009-06-10 04:18:17 UTC brendan
* Added comment:
This is going to need a set of test cases. I'll tackle this after 1.5.20.

--------------------------------------------------------------------------------
2009-09-18 16:44:33 UTC gars
* cc changed to 530671@bugs.debian.org, gars@sandine.net
* Added comment:
Replying to [ticket:3242 antonio@…]:
> This bug seems to happen only with dovecot, which is using
> a dot (".") as folder separator, particularly it happens
> *only if* the subdirs starts with a dot (INBOX.subdir is
> not considered)

I am also seeing this behavior for mutt 1.5.19 and 1.5.20.
INBOX.subdir might work, but mbox.subdir does not.  In fact,
I actually have a folder named "inbox.rt", and when I try
to change to it, mutt changes it to "INBOX/rt" and I get
a "no folder" failure.  In fact, if I press "c" and then
in[Tab], it used to complete to inbox.rt, but now the
"in" gets deleted when I press [Tab].  If I press "c" and
then [Tab][Tab], I see "inbox.rt" in my list of folders and
can browse to it but if I press [Enter] to select it, mutt
tries to change to "INBOX/rt", which does not exist.

The imap server in this case is dovecot.

Regards,
Gary S.

--------------------------------------------------------------------------------
2009-10-27 17:37:52 UTC ramon
* Added comment:
I can confirm the same behaviour against a kolab/cyrus based imap server with shared mailboxes in the form: shared.mysharedmailbox

Suggested work-around fixes the behaviour, after searching for this for quite some time, I updated the debian bug with your workaround.  

--------------------------------------------------------------------------------
2012-05-14 00:43:59 UTC lucki2791
* cc changed to 530671@bugs.debian.org, gars@sandine.net, lucki2791@gmail.com
* component changed to browser
* milestone changed to 2.0
* owner changed to lucki2791
* type changed to task
* version changed to 1.5.21

--------------------------------------------------------------------------------
2015-08-03 08:15:19 UTC grawity
* cc changed to 530671@bugs.debian.org, gars@sandine.net, lucki2791@gmail.com, grawity@gmail.com

--------------------------------------------------------------------------------
2016-08-30 04:52:25 UTC antonio@dyne.org
* Added comment:
Are we going to fix this? It would be great if we do!
