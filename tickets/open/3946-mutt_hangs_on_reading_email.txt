Ticket:  3946
Status:  new
Summary: mutt hangs on reading email

Reporter: gnu.support
Owner:    mutt-dev

Opened:       2017-05-29 09:06:59 UTC
Last Updated: 2017-05-29 19:53:10 UTC

Priority:  major
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
I am using mutt version 1.8.2. with Maildir

Here is the message, which you need to unzip and place in Maildir/cur and try to read to replicate the situation.

Once I try to access this message, mutt hangs, and message does not even open. The CPU is raising high.

Message is here:
https://rcdrun.com/files/tmp/1496047554.M365271P16777Q2.protected.rcdrun.com:2,.gz

--------------------------------------------------------------------------------
2017-05-29 09:09:57 UTC gnu.support
* Added comment:
I wish to add, that user simply clicked on mailto: link on a website, and that his Gmail or Yahoo, inserted some kind of javascript in the message. Yet, mutt shall not hang on that one.

--------------------------------------------------------------------------------
2017-05-29 19:53:10 UTC kevin8t8
* Added comment:
It looks like mutt is not hung: the message does come up and scroll, but it takes a long time.

This is probably due to the long lines.  There are a couple lines of length 462407 and 313962, and a few others of around of 5000-66954.  

Mutt is not great at dealing with huge lines.  I'll try to see if there are any improvements that can be made, but I'm marking this as an enhancement.

* component changed to display
* priority changed to major
* type changed to enhancement
