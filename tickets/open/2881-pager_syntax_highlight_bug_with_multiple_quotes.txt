Ticket:  2881
Status:  new
Summary: pager: syntax highlight bug with multiple quotes

Reporter: egmont
Owner:    mutt-dev

Opened:       2007-04-23 15:43:04 UTC
Last Updated: 2007-04-23 15:43:04 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
Mutt-1.5.15 without any patches, linked against ncursesw. Default global config, plus these three lines in my own .muttrc:
{{{
color quoted  green black
color quoted1 yellow black
bind pager <space> next-line
}}}

Create a message where (after some unquoted lines) a more deeply quoted text appears, and later appear texts with lower nested level. Example:
{{{
foo
foo
foo
> > > q3
> > q2
> q1
bar
}}}

Switch from the previous mail to this one in a window that has a small height so that only some unqoted "foo"s appear. Start scrolling to the bottom line-by-line by pressing Space, or enlarging the window.

When "> > > q3" appears, it is fully green. When "> > q2" appears, it gets fully green, and the triple-quoted line is repainted so that "> > " remains in green but "> q3" turns to yellow. When "> q1" is scrolled in, both previous lines change their color again, "q3" goes back to green and "q2" gets yellow.

Even when only a small part of a message is shown, mutt should scan the whole message to see possible quotation symbols so that it can already highlight the syntax correctly. It's quite irritating when it starts to repaint already displayed texts with different color.

