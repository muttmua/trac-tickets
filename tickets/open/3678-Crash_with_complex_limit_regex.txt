Ticket:  3678
Status:  new
Summary: Crash with complex limit regex

Reporter: ralf
Owner:    mutt-dev

Opened:       2014-02-10 15:49:13 UTC
Last Updated: 2014-02-10 15:49:13 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I'm occasionally using the `l' limit function with a rather large regular expression.  Lately I started experiencing mutt crashing and it appears this is due to the regex' size having reached about 512 characters.  The regex itself is relativly simple of the kind (foo|bar|joe@blah.org|blabla.com|...) - nothing fancy.

Observed with the Fedora 19 package mutt-1.5.21-20.fc19.x86_64.
