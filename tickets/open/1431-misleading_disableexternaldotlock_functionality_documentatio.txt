Ticket:  1431
Status:  reopened
Summary: misleading --disable-external-dotlock functionality/ documentation vs. naming/ intuition

Reporter: siegert@sfu.ca
Owner:    mutt-dev

Opened:       2002-12-24 00:16:32 UTC
Last Updated: 2009-06-30 14:20:51 UTC

Priority:  minor
Component: build
Keywords:  

--------------------------------------------------------------------------------
Description:
I configured mutt (same problem in 1.4 and 1.5.3) with the following
command:

`./configure --enable-pop --enable-imap --disable-external-dotlock --with-ssl`

but despite the --disable-external-dotlock configure checks /var/mail
($mutt_cv_mailpath), finds that it is writable by group mail, and then
configures to install mutt_dotlock setgid mail. However, when using mutt
via imap the permissions on $mutt_cv_mailpath are totally irrelevant, thus
if --disable-external-dotlock is specified no setgid mail mutt_dotlock
program should be installed no matter what the permissions on
$mutt_cv_mailpath are. The following patch to the configure script
implements such a behaviour.
{{{
--- mutt-1.5.3/configure.nosetgid	Tue Dec 17 02:35:56 2002
+++ mutt-1.5.3/configure	Mon Dec 23 15:44:44 2002
@@ -4570,6 +4570,12 @@
 #define MAILPATH "$mutt_cv_mailpath"
 EOF
 
+# Check whether --enable-external_dotlock or --disable-external_dotlock was given.
+if test "${enable_external_dotlock+set}" = set; then
+  enableval="$enable_external_dotlock"
+  mutt_cv_external_dotlock="$enableval"
+fi
+
 
         echo $ac_n "checking if $mutt_cv_mailpath is world writable""... $ac_c" 1>&6
 echo "configure:4576: checking if $mutt_cv_mailpath is world writable" >&5
@@ -4618,6 +4624,7 @@
 
         else
 
+            if test $mutt_cv_external_dotlock = yes; then
                 echo $ac_n "checking if $mutt_cv_mailpath is group writable""... $ac_c" 1>&6
 echo "configure:4623: checking if $mutt_cv_mailpath is group writable" >&5
 if eval "test \"`echo '$''{'mutt_cv_groupwrite'+set}'`\" = set"; then
@@ -4668,13 +4675,8 @@
 
                         mutt_cv_setgid=yes
                 fi
+             fi
         fi
-fi
-
-# Check whether --enable-external_dotlock or --disable-external_dotlock was given.
-if test "${enable_external_dotlock+set}" = set; then
-  enableval="$enable_external_dotlock"
-  mutt_cv_external_dotlock="$enableval"
 fi
 
 
}}}

--------------------------------------------------------------------------------
2002-12-24 01:01:24 UTC Martin Siegert <siegert@sfu.ca>
* Added comment:
{{{
On Tue, Dec 24, 2002 at 01:43:58AM +0100, Vincent Lefevre wrote:
> On Mon, Dec 23, 2002 at 16:16:32 -0800, siegert@sfu.ca wrote:
> > I configured mutt (same problem in 1.4 and 1.5.3) with the following
> > command:
> > 
> > ./configure --enable-pop --enable-imap --disable-external-dotlock --with-ssl
> > 
> > but despite the --disable-external-dotlock configure checks /var/mail
> > ($mutt_cv_mailpath), finds that it is writable by group mail, and then
> > configures to install mutt_dotlock setgid mail. However, when using mutt
> > via imap the permissions on $mutt_cv_mailpath are totally irrelevant, thus
> > if --disable-external-dotlock is specified no setgid mail mutt_dotlock
> > program should be installed no matter what the permissions on
> > $mutt_cv_mailpath are. The following patch to the configure script
> > implements such a behaviour.
> 
> I disagree. Configuring with --enable-imap doesn't mean that you'll
> use IMAP only. It means that you add IMAP support. Think about the
> users who have several mail accounts.

I agree with that statement, but it is really beside the point:
when I configure with --disable-external-dotlock I mean it. In that case
I do not want a setgid mutt_dotlock program no matter what.
If I want to use imap and local mail spool I would not specify
--disable-external-dotlock.

Martin
}}}

--------------------------------------------------------------------------------
2002-12-24 01:54:11 UTC Martin Siegert <siegert@sfu.ca>
* Added comment:
{{{
On Tue, Dec 24, 2002 at 02:10:55AM +0100, Vincent Lefevre wrote:
> On Mon, Dec 23, 2002 at 17:01:24 -0800, Martin Siegert wrote:
> > I agree with that statement, but it is really beside the point:
> > when I configure with --disable-external-dotlock I mean it. In that case
> > I do not want a setgid mutt_dotlock program no matter what.
> > If I want to use imap and local mail spool I would not specify
> > --disable-external-dotlock.
> 
> OK, but the problem is completely unrelated with IMAP.

Correct. I only used imap as an example to explain why
--disable-external-dotlock should take precedence over other settings.
The patch that I submitted indeed has nothing to do with imap, it only
checks whether --enable-external-dotlock was set, and if yes checks
whether sgid mail is needed as before.

Somebody with more knowledge of configure scripts should check whether

if test $mutt_cv_external_dotlock = yes

is really the same as

if ! test $mutt_cv_external_dotlock = no

I do not know whether $mutt_cv_external_dotlock can take values other than
yes and no. If that is the case, one probably should use the latter if
block in the patch.

Martin
}}}

--------------------------------------------------------------------------------
2002-12-24 18:43:58 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Mon, Dec 23, 2002 at 16:16:32 -0800, siegert@sfu.ca wrote:
> I configured mutt (same problem in 1.4 and 1.5.3) with the following
> command:
> 
> ./configure --enable-pop --enable-imap --disable-external-dotlock --with-ssl
> 
> but despite the --disable-external-dotlock configure checks /var/mail
> ($mutt_cv_mailpath), finds that it is writable by group mail, and then
> configures to install mutt_dotlock setgid mail. However, when using mutt
> via imap the permissions on $mutt_cv_mailpath are totally irrelevant, thus
> if --disable-external-dotlock is specified no setgid mail mutt_dotlock
> program should be installed no matter what the permissions on
> $mutt_cv_mailpath are. The following patch to the configure script
> implements such a behaviour.

I disagree. Configuring with --enable-imap doesn't mean that you'll
use IMAP only. It means that you add IMAP support. Think about the
users who have several mail accounts. Here, I add the following
configure option:

 --with-mailpath=${MAIL:+$MAIL:h}

which means that on the accounts where $MAIL is empty, an empty
mailpath is used.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2002-12-24 19:10:55 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Mon, Dec 23, 2002 at 17:01:24 -0800, Martin Siegert wrote:
> I agree with that statement, but it is really beside the point:
> when I configure with --disable-external-dotlock I mean it. In that case
> I do not want a setgid mutt_dotlock program no matter what.
> If I want to use imap and local mail spool I would not specify
> --disable-external-dotlock.

OK, but the problem is completely unrelated with IMAP.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2002-12-24 19:21:31 UTC Martin Siegert <siegert@sfu.ca>
* Added comment:
{{{
On Tue, Dec 24, 2002 at 12:50:58PM +0100, Vincent Lefevre wrote:
> On Mon, Dec 23, 2002 at 17:54:11 -0800, Martin Siegert wrote:
> > Correct. I only used imap as an example to explain why
> > --disable-external-dotlock should take precedence over other settings.
> > The patch that I submitted indeed has nothing to do with imap, it only
> > checks whether --enable-external-dotlock was set, and if yes checks
> > whether sgid mail is needed as before.
> 
> But I don't think that --disable-external-dotlock means "disable
> dotlocking".

No, it means disable the external dotlock program (mutt_dotlock).

> I think that it is just the opposite of
> --enable-external-dotlock, which means: "Force use of an external
> dotlock program". It is "force", not "allow". So, when choosing
> --disable-external-dotlock, Mutt will still use mutt_dotlock if
> it finds it necessary.

... and that is a bug.

> IMHO, you're using the wrong option.

Once more: I do not want that the configure/compile/installation
procedure installs a setgid mail mutt_dotlock program when I
specify --disable-external-dotlock. 
If that means that mutt uses mutt_dotlock.o as a module - fine (as long
as this does not mean that mutt itself will be installed sgid mail, which
as far as I can tell never happens).

I just want that --disable-external-dotlock does what the name of the
option implies namely the same as --enable-external-dotlock=no.
How can that be the wrong option? If an option does not do what the
name of the option implies then that is a bug.

I can imagine the following scenario:

1) if --enable-external-dotlock is specified, install the external
  mutt_dotlock program.
2) if --disable-external-dotlock or --enable-external-dotlock=no is
  specified, do not install mutt_dotlock (no matter what).
3) if neither --enable-external-dotlock nor --disable-external-dotlock
  is specified, install mutt_dotlock, if necessary.

There must be a way to configure mutt such that the external dotlock
program is disabled. If the option is not --disable-external-dotlock,
what should it be?

Martin
}}}

--------------------------------------------------------------------------------
2002-12-24 21:20:56 UTC Martin Siegert <siegert@sfu.ca>
* Added comment:
{{{
On Tue, Dec 24, 2002 at 09:19:13PM +0100, Vincent Lefevre wrote:
> On Tue, Dec 24, 2002 at 11:21:31 -0800, Martin Siegert wrote:
> > On Tue, Dec 24, 2002 at 12:50:58PM +0100, Vincent Lefevre wrote:
> > > But I don't think that --disable-external-dotlock means "disable
> > > dotlocking".
> > 
> > No, it means disable the external dotlock program (mutt_dotlock).
> 
> Where do you find that in the documentation?

This is exactly the point: why should I need documentation to find
out what --disable-external-dotlock means? If it does not mean
disable external dotlock, then this is a bug.

> --disable-external-dotlock isn't even listed in "./configure --help"
> (because this is the default, IMHO).
> 
> > > I think that it is just the opposite of
> > > --enable-external-dotlock, which means: "Force use of an external
> > > dotlock program". It is "force", not "allow". So, when choosing
> > > --disable-external-dotlock, Mutt will still use mutt_dotlock if
> > > it finds it necessary.
> > 
> > ... and that is a bug.
> 
> No, "./configure --help" says:
> 
>   --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
>   --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]
> 
> So, --disable-FEATURE is the opposite of --enable-FEATURE.
> 
> > > IMHO, you're using the wrong option.
> > 
> > Once more: I do not want that the configure/compile/installation
> > procedure installs a setgid mail mutt_dotlock program when I
> > specify --disable-external-dotlock. 
> 
> Where have you found that in the documentation?

It's plain english. There is no way out here: the attitude "yes we say this,
but we really mean that" might be ok for M$ products, but it should not
be ok for mutt.

> > I just want that --disable-external-dotlock does what the name of the
> > option implies namely the same as --enable-external-dotlock=no.
> 
> But --enable-external-dotlock=no doesn't mean not to use an external
> dotlock. It is just the default, i.e. Mutt doesn't use an external
> dotlock if possible.
> 
> > How can that be the wrong option? If an option does not do what the
> > name of the option implies then that is a bug.
> 
> OK, so the name of the option could be changed. But the behaviour
> for this option is:
> 
>   Force use of an external dotlock program

Then call it --force-external-dotlock and implement
--disable-external-dotlock.

> > I can imagine the following scenario:
> > 
> > 1) if --enable-external-dotlock is specified, install the external
> >    mutt_dotlock program.
> > 2) if --disable-external-dotlock or --enable-external-dotlock=no is
> >    specified, do not install mutt_dotlock (no matter what).
> > 3) if neither --enable-external-dotlock nor --disable-external-dotlock
> >    is specified, install mutt_dotlock, if necessary.
> 
> No, AFAIK, --disable-FEATURE/--enable-FEATURE options are binary
> options. This is necessary to be able to override configuration
> related to FEATURE under some conditions.

If that is the case (I am not an expert for configure scripts), then
the option should be renamed to something like --use-external-dotlock
in order to allow yes, no and not set as values for the corresponding
variable.
I actually would prefer to leave the name of the option as is, but 
implement --enable-external-dotlock as what it means: allow external
dotlock program (i.e., --disable-external-dotlock then would mean: do not
allow external dotlock program). I am not sure whether there is a need
for an option that forces the installation of an external dotlock program.

If all this is unacceptable, then at least the option should be renamed
and there should be an option --disable-sgid.

> > There must be a way to configure mutt such that the external dotlock
> > program is disabled. If the option is not --disable-external-dotlock,
> > what should it be?
> 
> As I've already said, --with-homespool and/or --with-mailpath.

That is for setting the mailpath or homespool. Using it for disabling
mutt_dotlock is at best a workaround. It it not intuitive. It is not
documented (i.e., that this should be used to disable the external
dotlock program instead of --disable-external-dotlock; try to write
that documentation: the outcome must sound silly).

Martin
}}}

--------------------------------------------------------------------------------
2002-12-25 05:50:58 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Mon, Dec 23, 2002 at 17:54:11 -0800, Martin Siegert wrote:
> Correct. I only used imap as an example to explain why
> --disable-external-dotlock should take precedence over other settings.
> The patch that I submitted indeed has nothing to do with imap, it only
> checks whether --enable-external-dotlock was set, and if yes checks
> whether sgid mail is needed as before.

But I don't think that --disable-external-dotlock means "disable
dotlocking". I think that it is just the opposite of
--enable-external-dotlock, which means: "Force use of an external
dotlock program". It is "force", not "allow". So, when choosing
--disable-external-dotlock, Mutt will still use mutt_dotlock if
it finds it necessary. IMHO, you're using the wrong option.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2002-12-25 14:19:13 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Tue, Dec 24, 2002 at 11:21:31 -0800, Martin Siegert wrote:
> On Tue, Dec 24, 2002 at 12:50:58PM +0100, Vincent Lefevre wrote:
> > But I don't think that --disable-external-dotlock means "disable
> > dotlocking".
> 
> No, it means disable the external dotlock program (mutt_dotlock).

Where do you find that in the documentation?

--disable-external-dotlock isn't even listed in "./configure --help"
(because this is the default, IMHO).

> > I think that it is just the opposite of
> > --enable-external-dotlock, which means: "Force use of an external
> > dotlock program". It is "force", not "allow". So, when choosing
> > --disable-external-dotlock, Mutt will still use mutt_dotlock if
> > it finds it necessary.
> 
> ... and that is a bug.

No, "./configure --help" says:

 --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
 --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]

So, --disable-FEATURE is the opposite of --enable-FEATURE.

> > IMHO, you're using the wrong option.
> 
> Once more: I do not want that the configure/compile/installation
> procedure installs a setgid mail mutt_dotlock program when I
> specify --disable-external-dotlock. 

Where have you found that in the documentation?

> I just want that --disable-external-dotlock does what the name of the
> option implies namely the same as --enable-external-dotlock=no.

But --enable-external-dotlock=no doesn't mean not to use an external
dotlock. It is just the default, i.e. Mutt doesn't use an external
dotlock if possible.

> How can that be the wrong option? If an option does not do what the
> name of the option implies then that is a bug.

OK, so the name of the option could be changed. But the behaviour
for this option is:

 Force use of an external dotlock program

> I can imagine the following scenario:
> 
> 1) if --enable-external-dotlock is specified, install the external
>    mutt_dotlock program.
> 2) if --disable-external-dotlock or --enable-external-dotlock=no is
>    specified, do not install mutt_dotlock (no matter what).
> 3) if neither --enable-external-dotlock nor --disable-external-dotlock
>    is specified, install mutt_dotlock, if necessary.

No, AFAIK, --disable-FEATURE/--enable-FEATURE options are binary
options. This is necessary to be able to override configuration
related to FEATURE under some conditions.

> There must be a way to configure mutt such that the external dotlock
> program is disabled. If the option is not --disable-external-dotlock,
> what should it be?

As I've already said, --with-homespool and/or --with-mailpath.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2002-12-25 15:59:02 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Tue, Dec 24, 2002 at 13:20:56 -0800, Martin Siegert wrote:
> On Tue, Dec 24, 2002 at 09:19:13PM +0100, Vincent Lefevre wrote:
> > On Tue, Dec 24, 2002 at 11:21:31 -0800, Martin Siegert wrote:
> > > On Tue, Dec 24, 2002 at 12:50:58PM +0100, Vincent Lefevre wrote:
> > > > But I don't think that --disable-external-dotlock means "disable
> > > > dotlocking".
> > > 
> > > No, it means disable the external dotlock program (mutt_dotlock).
> > 
> > Where do you find that in the documentation?
> 
> This is exactly the point: why should I need documentation to find
> out what --disable-external-dotlock means? If it does not mean
> disable external dotlock, then this is a bug.

Well, configure options are for administrators, not for the end user
(of course, an end user may also be an administrator). They should
look a little further than the option name... "configure --help" gives
some help, so does the INSTALL file (though it is incomplete). But
well, I agree that an option name that exactly means what it does
would be better.

> Then call it --force-external-dotlock

IMHO, the option should start with the standard --enable/--disable
(because it is standard). Perhaps --enable-force-ext-dotlock (not
too long?).

> and implement --disable-external-dotlock.

Already done with other options (see below).

> > As I've already said, --with-homespool and/or --with-mailpath.
> 
> That is for setting the mailpath or homespool. Using it for disabling
> mutt_dotlock is at best a workaround. It it not intuitive.

It is intuitive. If you have MAIL set to /var/mail/something, it
is normal that the configure script behaves as if you would receive
mail here.

But perhaps the bug is that the configure script assumes by default
that users receive mail there (I don't know if this is the case).

There is another problem: if the mail reception method changes,
you may need to reinstall Mutt. IMHO, this is not acceptable.
Things should be more dynamical.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2002-12-25 17:05:59 UTC "Paul Walker" <paul@black-sun.demon.co.uk>
* Added comment:
{{{
> I disagree. Configuring with --enable-imap doesn't mean that you'll
> use IMAP only. It means that you add IMAP support. Think about the

While this is true, it doesn't change the fact that disable-dotlock
shouldn't install a dotlock helper, so it is still broken. :-)

(Before anyone comments on the X-Mailer - yes, I know, this laptop only has
WinXP available for the moment. I'm not keen on downloading CDs over a 56k
link...)
}}}

--------------------------------------------------------------------------------
2002-12-26 05:24:27 UTC Thomas Roessler <roessler-mobile@does-not-exist.net>
* Added comment:
{{{
On 2002-12-23 17:01:24 -0800, Martin Siegert wrote:

> I agree with that statement, but it is really beside the point:
> when I configure with --disable-external-dotlock I mean it. In
> that case I do not want a setgid mutt_dotlock program no matter
> what. If I want to use imap and local mail spool I would not
> specify --disable-external-dotlock.

There's a reason for the current documentation of the
--enable-external-dotlock switch: "Force use of an external dotlock
program." The option you're complaining about isn't even documented.

-- 
Thomas Roessler	(mobile)	<roessler-mobile@does-not-exist.net>
}}}

--------------------------------------------------------------------------------
2002-12-30 10:06:55 UTC Dave <dave@dave.tj>
* Added comment:
{{{
On Tue, Dec 24, 2002 at 10:59:02PM +0100, Vincent Lefevre wrote:

> On Tue, Dec 24, 2002 at 13:20:56 -0800, Martin Siegert wrote:
> > On Tue, Dec 24, 2002 at 09:19:13PM +0100, Vincent Lefevre wrote:
> > > On Tue, Dec 24, 2002 at 11:21:31 -0800, Martin Siegert wrote:
> > > > On Tue, Dec 24, 2002 at 12:50:58PM +0100, Vincent Lefevre wrote:
> > > > > But I don't think that --disable-external-dotlock means "disable
> > > > > dotlocking".
> > > > No, it means disable the external dotlock program (mutt_dotlock).
> > > Where do you find that in the documentation?
> > This is exactly the point: why should I need documentation to find
> > out what --disable-external-dotlock means? If it does not mean
> > disable external dotlock, then this is a bug.
[...]
> > Then call it --force-external-dotlock
> IMHO, the option should start with the standard --enable/--disable
> (because it is standard). Perhaps --enable-force-ext-dotlock (not
> too long?).
How about changing the semantics a tad:
--enable-external-dotlock={yes,no,auto} (with a default of auto)
with
--disable-external-dotlock being a synonym for ...=no
and
--force-external-dotlock being a synonym for ...=yes

I hope this idea helps a bit. . .
- Dave


-- 
When you say "I wrote a program that crashed Windows", people just stare
at you blankly and say "Hey, I got those with the system, *for free*".
       -- Linus Torvalds

Linux: because a PC is a terrible thing to waste
       -- ksh@cis.ufl.edu put this on Tshirts in '93
}}}

--------------------------------------------------------------------------------
2002-12-31 00:34:08 UTC Dave <dave@dave.tj>
* Added comment:
{{{
On Mon, Dec 30, 2002 at 01:05:38PM +0100, Vincent Lefevre wrote:

> On Sun, Dec 29, 2002 at 23:06:55 -0500, Dave wrote:
> > How about changing the semantics a tad:
> > --enable-external-dotlock={yes,no,auto} (with a default of auto)
> > with
> > --disable-external-dotlock being a synonym for ...=no
> > and
> > --force-external-dotlock being a synonym for ...=yes
> 
> This wouldn't be standard. Remember that the configure script
> is generated from the configure.in file using autoconf macros,
I haven't written a configure.in (shouldn't it be renamed
configure.ac by now, anyway?) in a while, but there are many
programs (including FSF-owned ones) that accept an argument to
--enable-whatever options.  Most common in my off-hand memory is
--enable-threads={auto,linuxthreads,mit,posix,none} which was rapidly
becoming standard when POSIX wasn't well-supported by most OSes.  (POSIX
threads are still non-native on most OSes, BTW, so this option continues
to be very valuable for performance aficionados even in the present day.)

Anyway, the auto* system isn't about arbitrary restrictions.  I've been
able to do some rather funky stuff with it, and the tools made it easy
to both implement and maintain my weirdness.  This should be a piece of
cake, in contrast.

- Dave


-- 
When you say "I wrote a program that crashed Windows", people just stare
at you blankly and say "Hey, I got those with the system, *for free*".
       -- Linus Torvalds

Uncle Cosmo, why do they call this a word processor?
It's simple, Skyler.  You've seen what food processors do to food, right?
}}}

--------------------------------------------------------------------------------
2002-12-31 06:05:38 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Sun, Dec 29, 2002 at 23:06:55 -0500, Dave wrote:
> How about changing the semantics a tad:
> --enable-external-dotlock={yes,no,auto} (with a default of auto)
> with
> --disable-external-dotlock being a synonym for ...=no
> and
> --force-external-dotlock being a synonym for ...=yes

This wouldn't be standard. Remember that the configure script
is generated from the configure.in file using autoconf macros,
AC_ARG_ENABLE in this case:

# AC_ARG_ENABLE(FEATURE, HELP-STRING, [ACTION-IF-TRUE], [ACTION-IF-FALSE])
# ------------------------------------------------------------------------
AC_DEFUN([AC_ARG_ENABLE],
[m4_divert_once([HELP_ENABLE], [[
Optional Features:
 --disable-FEATURE       do not include FEATURE (same as --enable-FEATURE=no)
 --enable-FEATURE[=ARG]  include FEATURE [ARG=yes]]])dnl
m4_divert_once([HELP_ENABLE], [$2])dnl
# Check whether --enable-$1 or --disable-$1 was given.
if test "[${enable_]m4_bpatsubst([$1], -, _)+set}" = set; then
 enableval="[$enable_]m4_bpatsubst([$1], -, _)"
 $3
m4_ifvaln([$4], [else
 $4])dnl
fi; dnl
])# AC_ARG_ENABLE

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2003-01-06 17:54:11 UTC Martin Siegert <siegert@sfu.ca>
* Added comment:
{{{
On Mon, Jan 06, 2003 at 11:35:54AM +0000, Lars Hecking wrote:
> Thomas Roessler writes:
> > On 2002-12-23 17:01:24 -0800, Martin Siegert wrote:
> > 
> > > I agree with that statement, but it is really beside the point:
> > > when I configure with --disable-external-dotlock I mean it. In
> > > that case I do not want a setgid mutt_dotlock program no matter
> > > what. If I want to use imap and local mail spool I would not
> > > specify --disable-external-dotlock.
> > 
> > There's a reason for the current documentation of the
> > --enable-external-dotlock switch: "Force use of an external dotlock
> > program." The option you're complaining about isn't even documented.
> 
>  For every --enable-* exists a corresponding --disable option - that's
>  how autoconf works. --enable-foo=no is the same as --disable-foo.
> 
>  I agree that it should be possible to switch off the external dotlock program.
>  The OP's patch, however, is not correct. I'll take a look at the issue when
>  I find some time (just back from holidays).

There are really two problems here:

- the --disable-external-dotlock (and therefore the --enable-external-dotlock)
 have a misleading name since they do not do what the name implies.

- a real "--disable-external-dotlock" option is missing.

in principle the two have nothing to do with each other although because
of the misleading names they become related.

My suggestion would be to rewrite configure such that
--disable-external-dotlock really does what the name implies. Therefore
--enable-external-dotlock would have to be redefined to "enable external
dotlock program if needed". In order to get the functionality of the
old --enable-external-dotlock option I suggest to introduce a new option
--force-external-dotlock so that --enable-external-dotlock plus
--force-external-dotlock together have the effect of the old
--enable-external-dotlock option. Alternatively, one could define
--force-external-dotlock such that it alone will cause the installation
of mutt_dotlock.

But the main complaint from my bug report is really that it should be
possible to switch off the external dotlock program (and using the
--with-mailspool option or similar to do that is not acceptable because
that option may be needed for its original purpose).

Martin
}}}

--------------------------------------------------------------------------------
2003-01-06 18:34:14 UTC Martin Siegert <siegert@sfu.ca>
* Added comment:
{{{
On Mon, Jan 06, 2003 at 07:25:28PM +0100, Vincent Lefevre wrote:
> On Mon, Jan 06, 2003 at 09:54:11 -0800, Martin Siegert wrote:
> > - a real "--disable-external-dotlock" option is missing.
> 
> --disable-dotlock-install would be much better, as the user may want
> to use an existing (already installed) external dotlock.
> 
> > But the main complaint from my bug report is really that it should be
> > possible to switch off the external dotlock program (and using the
> > --with-mailspool option or similar to do that is not acceptable because
> > that option may be needed for its original purpose).
> 
> I think you meant --mailpath. If it is needed for its original purpose
> and you don't have the necessary permissions, then you need an
> external dotlock, unless you want to disable dotlocking. If you have
> the necessary permissions, then there is no problem.

Incorrect. Typical case: mutt is installed on an application server
where (if mutt would be run on that machine) a mutt_dotlock would be
needed. The binary is NFS exported to a timeshare host where the
dotlock program is not needed.

Martin
}}}

--------------------------------------------------------------------------------
2003-01-07 03:35:54 UTC Lars Hecking <lhecking@nmrc.ie>
* Added comment:
{{{
Thomas Roessler writes:
> On 2002-12-23 17:01:24 -0800, Martin Siegert wrote:
> 
> > I agree with that statement, but it is really beside the point:
> > when I configure with --disable-external-dotlock I mean it. In
> > that case I do not want a setgid mutt_dotlock program no matter
> > what. If I want to use imap and local mail spool I would not
> > specify --disable-external-dotlock.
> 
> There's a reason for the current documentation of the
> --enable-external-dotlock switch: "Force use of an external dotlock
> program." The option you're complaining about isn't even documented.

For every --enable-* exists a corresponding --disable option - that's
how autoconf works. --enable-foo=no is the same as --disable-foo.

I agree that it should be possible to switch off the external dotlock program.
The OP's patch, however, is not correct. I'll take a look at the issue when
I find some time (just back from holidays).
}}}

--------------------------------------------------------------------------------
2003-01-07 12:25:28 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Mon, Jan 06, 2003 at 09:54:11 -0800, Martin Siegert wrote:
> - a real "--disable-external-dotlock" option is missing.

--disable-dotlock-install would be much better, as the user may want
to use an existing (already installed) external dotlock.

> But the main complaint from my bug report is really that it should be
> possible to switch off the external dotlock program (and using the
> --with-mailspool option or similar to do that is not acceptable because
> that option may be needed for its original purpose).

I think you meant --mailpath. If it is needed for its original purpose
and you don't have the necessary permissions, then you need an
external dotlock, unless you want to disable dotlocking. If you have
the necessary permissions, then there is no problem.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2003-01-07 12:40:42 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Mon, Jan 06, 2003 at 10:34:14 -0800, Martin Siegert wrote:
> Incorrect. Typical case: mutt is installed on an application server
> where (if mutt would be run on that machine) a mutt_dotlock would be
> needed. The binary is NFS exported to a timeshare host where the
> dotlock program is not needed.

OK, so something dynamical (no longer a compile-time choice). This is
also what I suggested in a previous message.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2007-02-08 14:23:58 UTC rado
* Added comment:
{{{
Lars Hecking said he'd look at it again after holidays.
Are they over yet? It's been a long time.
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2007-02-08 14:23:59 UTC rado
* Added comment:
{{{
Changed misleading synopsis from:
"configure insists on installing mutt_dotlock although not needed with imap"

Lars Hecking, are your holidays over?
You said you'd look at it again then.
You have LOOONG holidays, man. :)
}}}

--------------------------------------------------------------------------------
2007-02-16 08:40:35 UTC rado
* Added comment:
{{{
Assumed it was forgotten, wanted to refresh,
but mismemorized the "just back from holidays" as "when back..."
I'm very sorry for the noise, back to sleep/ "open".
}}}

* status changed to new

--------------------------------------------------------------------------------
2007-02-16 08:40:36 UTC rado
* Added comment:
{{{
Misread "just back" as "when back", I'm very sorry and beg for pardon for bothering.
}}}

--------------------------------------------------------------------------------
2007-04-02 07:27:05 UTC brendan
* Updated description:
I configured mutt (same problem in 1.4 and 1.5.3) with the following
command:

`./configure --enable-pop --enable-imap --disable-external-dotlock --with-ssl`

but despite the --disable-external-dotlock configure checks /var/mail
($mutt_cv_mailpath), finds that it is writable by group mail, and then
configures to install mutt_dotlock setgid mail. However, when using mutt
via imap the permissions on $mutt_cv_mailpath are totally irrelevant, thus
if --disable-external-dotlock is specified no setgid mail mutt_dotlock
program should be installed no matter what the permissions on
$mutt_cv_mailpath are. The following patch to the configure script
implements such a behaviour.
{{{
--- mutt-1.5.3/configure.nosetgid	Tue Dec 17 02:35:56 2002
+++ mutt-1.5.3/configure	Mon Dec 23 15:44:44 2002
@@ -4570,6 +4570,12 @@
 #define MAILPATH "$mutt_cv_mailpath"
 EOF
 
+# Check whether --enable-external_dotlock or --disable-external_dotlock was given.
+if test "${enable_external_dotlock+set}" = set; then
+  enableval="$enable_external_dotlock"
+  mutt_cv_external_dotlock="$enableval"
+fi
+
 
         echo $ac_n "checking if $mutt_cv_mailpath is world writable""... $ac_c" 1>&6
 echo "configure:4576: checking if $mutt_cv_mailpath is world writable" >&5
@@ -4618,6 +4624,7 @@
 
         else
 
+            if test $mutt_cv_external_dotlock = yes; then
                 echo $ac_n "checking if $mutt_cv_mailpath is group writable""... $ac_c" 1>&6
 echo "configure:4623: checking if $mutt_cv_mailpath is group writable" >&5
 if eval "test \"`echo '$''{'mutt_cv_groupwrite'+set}'`\" = set"; then
@@ -4668,13 +4675,8 @@
 
                         mutt_cv_setgid=yes
                 fi
+             fi
         fi
-fi
-
-# Check whether --enable-external_dotlock or --disable-external_dotlock was given.
-if test "${enable_external_dotlock+set}" = set; then
-  enableval="$enable_external_dotlock"
-  mutt_cv_external_dotlock="$enableval"
 fi
 
 
}}}
* milestone changed to 1.6

--------------------------------------------------------------------------------
2008-06-26 06:06:11 UTC brendan
* Added comment:
I don't think this is work blocking 1.6 over. Patch still welcome though :)

* milestone changed to 2.0

--------------------------------------------------------------------------------
2009-06-30 14:20:51 UTC pdmef
* component changed to build
