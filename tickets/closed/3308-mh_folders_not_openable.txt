Ticket:  3308
Status:  closed
Summary: mh folders not openable

Reporter: kees
Owner:    mutt-dev

Opened:       2009-07-27 18:23:18 UTC
Last Updated: 2009-07-28 04:14:43 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
mh folders were not able to be opened.  This patch appears to fix the issue by correctly propagating errors (or lack there of).


--------------------------------------------------------------------------------
2009-07-27 18:23:54 UTC kees
* Added attachment mh-folder-opening.patch

--------------------------------------------------------------------------------
2009-07-28 04:14:43 UTC Kees Cook <kees@outflux.net>
* Added comment:
(In [2fc9348684fe]) Properly propagate mh_read_sequences result. Closes #3308.

* resolution changed to fixed
* status changed to closed
