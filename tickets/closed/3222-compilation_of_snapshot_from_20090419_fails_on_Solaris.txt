Ticket:  3222
Status:  closed
Summary: compilation of snapshot from 20090419 fails on Solaris

Reporter: grobian
Owner:    mutt-dev

Opened:       2009-04-23 12:47:31 UTC
Last Updated: 2009-04-25 08:51:35 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
make[2]: Entering directory `/Library/Gentoo/var/tmp/portage/mail-client/mutt-1.5.19_p20090419/work/mutt-1.5.19hg'
i386-pc-solaris2.10-gcc  -Wall -pedantic -Wno-long-long -march=core2 -O3 -pipe   -o mutt addrbook.o alias.o attach.o base64.o browser.o buffy.o color.o compress.o crypt.o cryptglue.o commands.o complete.o compose.o copy.o curs_lib.o curs_main.o date.o edit.o enter.o flags.o init.o filter.o from.o getdomain.o group.o handler.o hash.o hdrline.o headers.o help.o hook.o keymap.o main.o mbox.o menu.o mh.o mx.o pager.o parse.o pattern.o postpone.o query.o recvattach.o recvcmd.o rfc822.o rfc1524.o rfc2047.o rfc2231.o rfc3676.o score.o send.o sendlib.o signal.o sort.o status.o system.o thread.o charset.o history.o lib.o muttlib.o editmsg.o mbyte.o url.o ascii.o crypt-mod.o patchlist.o smime.o crypt-mod-smime-classic.o resize.o smtp.o bcache.o nntp.o newsrc.o account.o mutt_socket.o mutt_tunnel.o mutt_ssl.o hcache.o md5.o  strsep.o wcscasecmp.o regex.o -Limap -limap -lncursesw -lssl -lcrypto -lz -lintl -liconv -liconv   -lsocket -lnsl  -L/Library/Gentoo/usr/lib// -ldb
pattern.o: In function `patmatch':
pattern.c:(.text+0x3ff): undefined reference to `strcasestr'
pattern.o: In function `msg_search':
pattern.c:(.text+0x13e9): undefined reference to `strcasestr'
pattern.o: In function `match_adrlist':
pattern.c:(.text+0x18de): undefined reference to `strcasestr'
pattern.c:(.text+0x198e): undefined reference to `strcasestr'
pattern.c:(.text+0x1a3e): undefined reference to `strcasestr'
pattern.o:pattern.c:(.text+0x2ad3): more undefined references to `strcasestr' follow
collect2: ld returned 1 exit status
make[2]: *** [mutt] Error 1


(Open)Solaris lacks strcasestr, which I think was introduced here:
http://dev.mutt.org/hg/mutt/diff/269e3da22cc2/pattern.c

--------------------------------------------------------------------------------
2009-04-25 08:51:35 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [c6de4f51b8c7]) Add strcasestr() from uclibc to unbreak (Open)Solaris build. Closes #3222.

* resolution changed to fixed
* status changed to closed
