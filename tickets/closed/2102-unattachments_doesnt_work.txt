Ticket:  2102
Status:  closed
Summary: "unattachments" doesn't work

Reporter: ttakah@lapis.plala.or.jp
Owner:    mutt-dev

Opened:       2005-10-05 14:15:02 UTC
Last Updated: 2005-10-07 10:15:01 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
"attachments" works well.
But "unattachments" does not remove "attachments" entries.

>How-To-Repeat:
:attachments +A */.*
:attachments +A image/jpeg
:unattachments +A */.*

-> Still all "Content-Disposition: attachment" files are counted.



:attachments +A */.*
:attachments -A plain/text
:unattachments -A plain/text

-> Still "plain/text" attachments are ignored.




You can use my comval.8 patch (PR: 2077) to see that
"unattachments" does not work at all.

>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-10-05 23:52:34 UTC David Champion <dgc@uchicago.edu>
* Added comment:
{{{
* On 2005.10.05, in <E1EN96s-0005rJ-N7@trithemius.gnupg.org>,
*	"New Mutt PR" <cb@df7cb.de> wrote:
> 
> >Description:
> "attachments" works well.
> But "unattachments" does not remove "attachments" entries.

This was a simple bug that must have been introduced late... I know
this worked at some point.  It was the difference between strcmp and
!strcmp. :P

But diagnosing it, I found some true horror elsewhere in the
unattachments handling, and rewrote a few things.  This will certainly
avoid segfaults.

I also added "attachments ?" which will list your current attachments
settings in muttrc format.

Thanks, Tamo.

-- 
 -D.    dgc@uchicago.edu        NSIT    University of Chicago

--9Iq5ULCa7nGtWwZS
Content-Type: text/plain; charset=us-ascii
Content-Disposition: attachment; filename="unattachments.diff"


CVSROOT = 
Using: /opt/bin/cvs diff
Index: init.c
===================================================================
RCS file: /home/roessler/cvs/mutt/init.c,v
retrieving revision 3.45
diff -u -r3.45 init.c
--- init.c	4 Oct 2005 06:05:39 -0000	3.45
+++ init.c	5 Oct 2005 18:45:37 -0000
@@ -889,7 +889,7 @@
 static int parse_unattach_list (BUFFER *buf, BUFFER *s, LIST **ldata, BUFFER *err)
 {
   ATTACH_MATCH *a;
-  LIST *lp, *lastp;
+  LIST *lp, *lastp, *newlp;
   char *tmp;
   int major;
   char *minor;
@@ -916,8 +916,10 @@
     }
     major = mutt_check_mime_type(tmp);
 
+    /* We must do our own walk here because remove_from_list() will only
+     * remove the LIST->data, not anything pointed to by the LIST->data. */
     lastp = NULL;
-    for(lp = *ldata; lp; lp = lastp->next)
+    for(lp = *ldata; lp; )
     {
       a = (ATTACH_MATCH *)lp->data;
       dprint(5, (debugfile, "parse_unattach_list: check %s/%s [%d] : %s/%s [%d]\n",
@@ -927,32 +929,49 @@
 	dprint(5, (debugfile, "parse_unattach_list: removed %s/%s [%d]\n",
 		    a->major, a->minor, a->major_int));
 	regfree(&a->minor_rx);
-        FREE(&a->major);
-        if (lastp)
-	{
-          lastp->next = lp->next;
-	}
-	lastp = lp;
-        FREE (&lp->data);	/* same as a */
-        FREE (&lp);
+	FREE(&a->major);
+
+	/* Relink backward */
+	if (lastp)
+	  lastp->next = lp->next;
+	else
+	  *ldata = lp->next;
+
+        newlp = lp->next;
+        FREE(&lp->data);	/* same as a */
+        FREE(&lp);
+        lp = newlp;
+        continue;
       }
 
       lastp = lp;
       lp = lp->next;
     }
 
-    remove_from_list (ldata, buf->data);
   }
   while (MoreArgs (s));
    
+  FREE(&tmp);
   _attachments_clean();
   return 0;
 }
 
+static int print_attach_list (LIST *lp, char op, char *name)
+{
+  while (lp) {
+    printf("attachments %c%s %s/%s\n", op, name,
+           ((ATTACH_MATCH *)lp->data)->major,
+           ((ATTACH_MATCH *)lp->data)->minor);
+    lp = lp->next;
+  }
+
+  return 0;
+}
+
 
 static int parse_attachments (BUFFER *buf, BUFFER *s, unsigned long data, BUFFER *err)
 {
-  char op, *p;
+  char op, *category;
   LIST **listp;
 
   mutt_extract_token(buf, s, 0);
@@ -961,19 +980,34 @@
     return -1;
   }
 
-  p = buf->data;
-  op = *p++;
+  category = buf->data;
+  op = *category++;
+
+  if (op == '?') {
+    mutt_endwin (NULL);
+    fflush (stdout);
+    printf("\nCurrent attachments settings:\n\n");
+    print_attach_list(AttachAllow,   '+', "A");
+    print_attach_list(AttachExclude, '-', "A");
+    print_attach_list(InlineAllow,   '+', "I");
+    print_attach_list(InlineExclude, '-', "I");
+    set_option (OPTFORCEREDRAWINDEX);
+    set_option (OPTFORCEREDRAWPAGER);
+    mutt_any_key_to_continue (NULL);
+    return 0;
+  }
+
   if (op != '+' && op != '-') {
     op = '+';
-    p--;
+    category--;
   }
-  if (!mutt_strncasecmp(p, "attachment", strlen(p))) {
+  if (!mutt_strncasecmp(category, "attachment", strlen(category))) {
     if (op == '+')
       listp = &AttachAllow;
     else
       listp = &AttachExclude;
   }
-  else if (!mutt_strncasecmp(p, "inline", strlen(p))) {
+  else if (!mutt_strncasecmp(category, "inline", strlen(category))) {
     if (op == '+')
       listp = &InlineAllow;
     else
@@ -1004,13 +1038,13 @@
     op = '+';
     p--;
   }
-  if (mutt_strncasecmp(p, "attachment", strlen(p))) {
+  if (!mutt_strncasecmp(p, "attachment", strlen(p))) {
     if (op == '+')
       listp = &AttachAllow;
     else
       listp = &AttachExclude;
   }
-  else if (mutt_strncasecmp(p, "inline", strlen(p))) {
+  else if (!mutt_strncasecmp(p, "inline", strlen(p))) {
     if (op == '+')
       listp = &InlineAllow;
     else
Index: doc/manual.xml.head
===================================================================
RCS file: /home/roessler/cvs/mutt/doc/manual.xml.head,v
retrieving revision 3.12
diff -u -r3.12 manual.xml.head
--- doc/manual.xml.head	4 Oct 2005 06:05:39 -0000	3.12
+++ doc/manual.xml.head	5 Oct 2005 18:51:09 -0000
@@ -5165,6 +5165,7 @@
 <screen>
 attachments   {+|-}disposition mime-type
 unattachments {+|-}disposition mime-type
+attachments   ?
 </screen>
 
 <para>
@@ -5253,6 +5254,11 @@
 attachments   -I message/external-body
 </screen>
 
+<para>
+"attachments ?" will list your current settings in Muttrc format, so
+that it can be pasted elsewhere.
+</para>
+
 </sect2>
 
 <sect2 id="mime-lookup">

--9Iq5ULCa7nGtWwZS--
}}}

--------------------------------------------------------------------------------
2005-10-06 14:25:35 UTC brendan
* Added comment:
{{{
Patch applied, thanks.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2005-10-08 18:12:58 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
* Wed Oct  5 2005 David Champion <dgc@uchicago.edu>

>  I also added "attachments ?" which will list your current attachments
>  settings in muttrc format.

If you need such an feature, please support my comval patch,
which adds a command to show the status of all the hooks
and almost all the commands, e.g. alternates, my_hdr,
subscribe, mailboxes, spam/nospam and so on.


>  Thanks, Tamo.

Welcome, thank you for the patch!

And thanks, Brendan.


>  +<para>
>  +"attachments ?" will list your current settings in Muttrc format, so
>  +that it can be pasted elsewhere.
>  +</para>

-- 
tamo
}}}
