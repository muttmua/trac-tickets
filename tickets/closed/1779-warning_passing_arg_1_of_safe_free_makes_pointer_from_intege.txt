Ticket:  1779
Status:  closed
Summary: warning: passing arg 1 of `safe_free' makes pointer from integer without a cast.

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2004-01-26 03:45:07 UTC
Last Updated: 2006-03-24 23:22:50 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: ?
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#229453.
Please Cc all your replies to 229453@bugs.debian.org .]

From: Goswin Brederlow <brederlo@informatik.uni-tuebingen.de>
Subject: warning: passing arg 1 of `safe_free' makes pointer from integer without a cast.
Date: Sat, 24 Jan 2004 23:06:00 +0100

Hi,

you are trying to misuse a long to store pointers into memory and to
free them later (e.g. init.c:922, init.c:951). The problem is the
following struct:

struct option_t
{
  char *option;
  short type;
  short flags;
  unsigned long data;
  unsigned long init; /* initial value */
};

You should no rely on unsigned long being large enough to hold a
pointer. The C99 standard defines intptr_t and uintptr_t if you realy
need an integer type there, which you should cast to pointer before
using it as one. Otherwise I suggest using an

union {
  unsigned long data;
  void* data_ptr;
}

which would be the only clean way to do this in traditional C.


Seeing a _safe_ function getting called with such an insecure cast
warning is a bit of an oximoron.

MfG
	Goswin

-- System Information:
Debian Release: testing/unstable
Architecture: i386
Kernel: Linux dual 2.4.23dual #1 SMP Sun Dec 14 13:57:16 CET 2003 i686
Locale: LANG=C, LC_CTYPE=de_DE



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-01-26 03:45:07 UTC Marco d'Itri <md@linux.it>
* Added comment:
{{{
Package: mutt
Version: ?
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#229453.
Please Cc all your replies to 229453@bugs.debian.org .]

From: Goswin Brederlow <brederlo@informatik.uni-tuebingen.de>
Subject: warning: passing arg 1 of `safe_free' makes pointer from integer without a cast.
Date: Sat, 24 Jan 2004 23:06:00 +0100

Hi,

you are trying to misuse a long to store pointers into memory and to
free them later (e.g. init.c:922, init.c:951). The problem is the
following struct:

struct option_t
{
 char *option;
 short type;
 short flags;
 unsigned long data;
 unsigned long init; /* initial value */
};

You should no rely on unsigned long being large enough to hold a
pointer. The C99 standard defines intptr_t and uintptr_t if you realy
need an integer type there, which you should cast to pointer before
using it as one. Otherwise I suggest using an

union {
 unsigned long data;
 void* data_ptr;
}

which would be the only clean way to do this in traditional C.


Seeing a _safe_ function getting called with such an insecure cast
warning is a bit of an oximoron.

MfG
	Goswin

-- System Information:
Debian Release: testing/unstable
Architecture: i386
Kernel: Linux dual 2.4.23dual #1 SMP Sun Dec 14 13:57:16 CET 2003 i686
Locale: LANG=C, LC_CTYPE=de_DE
}}}

--------------------------------------------------------------------------------
2006-03-25 17:22:50 UTC paul
* Added comment:
{{{
Patch was applied (according to Debian bug log). gcc now
emits no significant warnings when building mutt, even with
-pedantic. Tested building with --std=c99 just to be sure,
same thing. Visual check can't see the lines the submitter
was complaining about.

(Please re-open if I've missed an instance.)
}}}

* resolution changed to fixed
* status changed to closed
