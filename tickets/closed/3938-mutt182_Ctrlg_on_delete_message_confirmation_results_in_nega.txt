Ticket:  3938
Status:  closed
Summary: mutt-1.8.2: Ctrl+g on delete message confirmation results in negative sidebar counts

Reporter: jcdenton
Owner:    Kevin McCarthy <kevin@8t8.us>

Opened:       2017-05-04 18:14:12 UTC
Last Updated: 2017-05-05 01:13:50 UTC

Priority:  major
Component: 
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.8.2-1
Severity: normal

-- Please type your report below this line

If mutt is set to prompt confirming message deletion before changing mailboxes, pressing Ctrl+g to abort this results in a sidebar update of "-1" messages.  The value goes away on the next mail check.

-- Mutt Version Information

Mutt 1.8.2 (2017-04-18)
Copyright (C) 1996-2016 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 3.16.0-4-amd64 (x86_64)
ncurses: ncurses 5.9.20140913 (compiled with 5.9)
libidn: 1.29 (compiled with 1.29)
hcache backend: tokyocabinet 1.4.48

Compiler:
Using built-in specs.
COLLECT_GCC=gcc
COLLECT_LTO_WRAPPER=/usr/lib/gcc/x86_64-linux-gnu/4.9/lto-wrapper
Target: x86_64-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Debian 4.9.2-10' --with-bugurl=file:///usr/share/doc/gcc-4.9/README.Bugs --enable-languages=c,c++,java,go,d,fortran,objc,obj-c++ --prefix=/usr --program-suffix=-4.9 --enable-shared --enable-linker-build-id --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --with-gxx-include-dir=/usr/include/c++/4.9 --libdir=/usr/lib --enable-nls --with-sysroot=/ --enable-clocale=gnu --enable-libstdcxx-debug --enable-libstdcxx-time=yes --enable-gnu-unique-object --disable-vtable-verify --enable-plugin --with-system-zlib --disable-browser-plugin --enable-java-awt=gtk --enable-gtk-cairo --with-java-home=/usr/lib/jvm/java-1.5.0-gcj-4.9-amd64/jre --enable-java-home --with-jvm-root-dir=/usr/lib/jvm/java-1.5.0-gcj-4.9-amd64 --with-jvm-jar-dir=/usr/lib/jvm-exports/java-1.5.0-gcj-4.9-amd64 --with-arch-directory=amd64 --with-ecj-jar=/usr/share/java/eclipse-ecj.jar --enable-objc-gc --enable-multiarch --with-arch-32=i586 --with-abi=m64
  --with-multilib-list=m32,m64,mx32 --enable-multilib --with-tune=generic --enable-checking=release --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu
Thread model: posix
gcc version 4.9.2 (Debian 4.9.2-10) 

Configure options: '--prefix=/usr/local' '--enable-gpgme' '--enable-hcache' '--enable-sidebar'

Compilation CFLAGS: -Wall -pedantic -Wno-long-long -g -O2

Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
-USE_POP  -USE_IMAP  -USE_SMTP  
-USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  +CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  +USE_SIDEBAR  -USE_COMPRESSED  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.
}}}


--------------------------------------------------------------------------------
2017-05-05 01:10:56 UTC kevin8t8
* Added comment:
I could reproduce something like this for the unread and flagged message counts.  The function was updating those counts too early, and repeated cancellations would decrement and decrement those values.  I'm guessing that this is what you are referring too, but it wouldn't have affected the total message count - just those two.

I'm going to push a fix and close the ticket.  If it turns out there is another bug you are experiencing, please feel free to reopen the ticket.

--------------------------------------------------------------------------------
2017-05-05 01:13:50 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"c08c72a0e24c36e2eeb768ff341bf9f88da95780" 7033:c08c72a0e24c]:
{{{
#!CommitTicketReference repository="" revision="c08c72a0e24c36e2eeb768ff341bf9f88da95780"
Fix sidebar count updates when closing mailbox. (closes #3938)

The context unread and flagged counts were being updated too early in
mx_close_mailbox().  Cancelling at any of the following prompts would
leave them in an incorrect state.  Additionally, $move could increase
the delete count (for flagged messages), and $delete, if answered no,
could turn off message deletion.

Move all the sidebar buffy stat updating to the bottom of the
function, after all the prompts and processing.
}}}

* owner changed to Kevin McCarthy <kevin@8t8.us>
* resolution changed to fixed
* status changed to closed
