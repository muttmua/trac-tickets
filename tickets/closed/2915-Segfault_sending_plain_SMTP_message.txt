Ticket:  2915
Status:  closed
Summary: Segfault sending plain SMTP message.

Reporter: georg
Owner:    brendan

Opened:       2007-06-18 18:31:03 UTC
Last Updated: 2008-06-26 06:00:09 UTC

Priority:  major
Component: SMTP
Keywords:  segfault

--------------------------------------------------------------------------------
Description:
Compiled mutt with --enable-debug, --enable-smtp on a standard debian system. When trying to send an eMail via SMTP, only smtp_url is set to a relay-server, no user, no pass given, mutt dies with a segmentation fault. Attached is the debug-information:

{{{
$ cat .muttdebug2
Mutt 1.5.16 started at Mon Jun 18 19:16:11 2007
.
Debugging at level 9.

Reading configuration file '/usr/local/etc/Muttrc'.
parse_attach_list: ldata = 080d96e4, *ldata = 00000000
parse_attach_list: added */.* [9]
parse_attach_list: ldata = 080d96e8, *ldata = 00000000
parse_attach_list: added text/x-vcard [7]
parse_attach_list: added application/pgp.* [2]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: added application/x-pkcs7-.* [2]
parse_attach_list: ldata = 080d96ec, *ldata = 00000000
parse_attach_list: added text/plain [7]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: added message/external-body [4]
parse_attach_list: ldata = 080d96f0, *ldata = 00000000
parse_attach_list: added message/external-body [4]
Reading configuration file '/etc/Muttrc'.
parse_attach_list: ldata = 080d96e4, *ldata = 080ee1b8
parse_attach_list: skipping */.*
parse_attach_list: added */.* [9]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: skipping message/external-body
parse_attach_list: added text/x-vcard [7]
parse_attach_list: added application/pgp.* [2]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: skipping message/external-body
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: added application/x-pkcs7-.* [2]
parse_attach_list: ldata = 080d96ec, *ldata = 080f0b20
parse_attach_list: skipping text/plain
parse_attach_list: added text/plain [7]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: skipping message/external-body
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: added message/external-body [4]
parse_attach_list: ldata = 080d96f0, *ldata = 080f3d58
parse_attach_list: skipping message/external-body
parse_attach_list: added message/external-body [4]
send.c:1176: mutt_mktemp returns "/tmp/mutt-server-1000-28930-0".
sendlib.c:2541: mutt_mktemp returns "/tmp/mutt-server-1000-28930-1".
send.c:969: mutt_mktemp returns "/tmp/mutt-server-1000-28930-2".
Using default SMTP port 25
Connected to relay.rwth-aachen.de:25 on fd=4
4< 220 relay.rwth-aachen.de ESMTP Sendmail 8.13.7/8.13.3/1; Mon, 18 Jun 2007 19:16:49 +0200 (MEST)
4> HELO server.lan
4< 250 relay.rwth-aachen.de Hello dynamic-unidsl-85-197-22-176.westend.de [85.197.22.176], pleased to meet you
}}}

--------------------------------------------------------------------------------
2007-07-05 00:40:43 UTC brendan
* Updated description:
Compiled mutt with --enable-debug, --enable-smtp on a standard debian system. When trying to send an eMail via SMTP, only smtp_url is set to a relay-server, no user, no pass given, mutt dies with a segmentation fault. Attached is the debug-information:

{{{
$ cat .muttdebug2
Mutt 1.5.16 started at Mon Jun 18 19:16:11 2007
.
Debugging at level 9.

Reading configuration file '/usr/local/etc/Muttrc'.
parse_attach_list: ldata = 080d96e4, *ldata = 00000000
parse_attach_list: added */.* [9]
parse_attach_list: ldata = 080d96e8, *ldata = 00000000
parse_attach_list: added text/x-vcard [7]
parse_attach_list: added application/pgp.* [2]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: added application/x-pkcs7-.* [2]
parse_attach_list: ldata = 080d96ec, *ldata = 00000000
parse_attach_list: added text/plain [7]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: added message/external-body [4]
parse_attach_list: ldata = 080d96f0, *ldata = 00000000
parse_attach_list: added message/external-body [4]
Reading configuration file '/etc/Muttrc'.
parse_attach_list: ldata = 080d96e4, *ldata = 080ee1b8
parse_attach_list: skipping */.*
parse_attach_list: added */.* [9]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: skipping message/external-body
parse_attach_list: added text/x-vcard [7]
parse_attach_list: added application/pgp.* [2]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: skipping message/external-body
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: added application/x-pkcs7-.* [2]
parse_attach_list: ldata = 080d96ec, *ldata = 080f0b20
parse_attach_list: skipping text/plain
parse_attach_list: added text/plain [7]
parse_attach_list: ldata = 080d96e8, *ldata = 080ef238
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: skipping message/external-body
parse_attach_list: skipping text/x-vcard
parse_attach_list: skipping application/pgp.*
parse_attach_list: skipping application/x-pkcs7-.*
parse_attach_list: added message/external-body [4]
parse_attach_list: ldata = 080d96f0, *ldata = 080f3d58
parse_attach_list: skipping message/external-body
parse_attach_list: added message/external-body [4]
send.c:1176: mutt_mktemp returns "/tmp/mutt-server-1000-28930-0".
sendlib.c:2541: mutt_mktemp returns "/tmp/mutt-server-1000-28930-1".
send.c:969: mutt_mktemp returns "/tmp/mutt-server-1000-28930-2".
Using default SMTP port 25
Connected to relay.rwth-aachen.de:25 on fd=4
4< 220 relay.rwth-aachen.de ESMTP Sendmail 8.13.7/8.13.3/1; Mon, 18 Jun 2007 19:16:49 +0200 (MEST)
4> HELO server.lan
4< 250 relay.rwth-aachen.de Hello dynamic-unidsl-85-197-22-176.westend.de [85.197.22.176], pleased to meet you
}}}
* milestone changed to 1.6
* owner changed to brendan
* status changed to assigned

--------------------------------------------------------------------------------
2008-05-18 02:35:22 UTC brendan
* component changed to SMTP
* summary changed to Segfault sending plain SMTP message.
* version changed to 1.5.16

--------------------------------------------------------------------------------
2008-05-18 16:32:09 UTC paul
* Added comment:
I've just tested (both with tree tip and with the mutt version in Debian), and I can't reproduce this. SMTP seems to work fine, both with TLS and all cleartext. Are you still able to reproduce this? If so, could you attach a (suitably sanitised) copy of your .muttrc, or the output from "mutt -D"?

--------------------------------------------------------------------------------
2008-06-26 06:00:09 UTC brendan
* Added comment:
No reply. I suspect this is a duplicate of #3079 (fixed).

* resolution changed to fixed
* status changed to closed
