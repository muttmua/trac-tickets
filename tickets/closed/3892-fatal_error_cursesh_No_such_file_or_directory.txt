Ticket:  3892
Status:  closed
Summary: fatal error: curses.h: No such file or directory

Reporter: vinc17
Owner:    mutt-dev

Opened:       2016-11-03 08:28:32 UTC
Last Updated: 2016-11-03 15:39:25 UTC

Priority:  blocker
Component: build
Keywords:  

--------------------------------------------------------------------------------
Description:
I get the following error with r6834 under Ubuntu 14.04.5 LTS:
{{{
gcc -std=gnu99 -DHAVE_CONFIG_H -I. -I..  -I.. -I../intl -I../intl  -Wall -pedantic -Wno-long-long -g -O3 -march=native -MT auth.o -MD -MP -MF .deps/auth.Tpo -c -o auth.o auth.c
In file included from imap_private.h:24:0,
                 from auth.c:28:
../mutt_curses.h:52:21: fatal error: curses.h: No such file or directory
 # include <curses.h>
                     ^
compilation terminated.
}}}
The cause may be the changeset r6833. The machine has:
{{{
/usr/include/ncursesw/curses.h
/usr/include/ncursesw/ncurses.h
}}}
and {{{config.log}}} contains:
{{{
#define HAVE_NCURSESW_NCURSES_H 1
}}}
but this variable isn't defined in {{{config.h}}}.

--------------------------------------------------------------------------------
2016-11-03 09:02:41 UTC vinc17
* Added comment:
The problem is related to variable expansion. In
{{{
AC_CHECK_HEADERS($cf_ncurses/ncurses.h,[cf_cv_ncurses_header="$cf_ncurses/ncurses.h"],
}}}
the {{{cf_cv_ncurses_header="$cf_ncurses/ncurses.h"}}} part is OK since this will be evaluated as a part of the shell script (the variable is expanded by the shell), but the {{{$cf_ncurses/ncurses.h}}} in the first argument is not OK because the shell is not involved everywhere in the generated code.

--------------------------------------------------------------------------------
2016-11-03 09:17:59 UTC Vincent Lefevre <vincent@vinc17.net>
* Added comment:
In [changeset:"ce07aa1182141ed0de8ddb8503137e7d592aec55" 6835:ce07aa118214]:
{{{
#!CommitTicketReference repository="" revision="ce07aa1182141ed0de8ddb8503137e7d592aec55"
Fixed issue from changeset 4da647a80c55. (closes #3892)

Shell variables cannot be used in the first argument of AC_CHECK_HEADERS.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2016-11-03 15:39:25 UTC kevin8t8
* Added comment:
Sorry about that.  It actually compiled fine for me, but I should have been more careful with the change.  Thank you for the quick fix.
