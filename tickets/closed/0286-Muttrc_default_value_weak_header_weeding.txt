Ticket:  286
Status:  closed
Summary: Muttrc default value (weak header weeding)

Reporter: Osamu Aoki <debian@aokiconsulting.com>
Owner:    mutt-dev

Opened:       2000-09-25 06:11:39 UTC
Last Updated: 2008-05-29 05:22:28 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.2.5-2
Severity: normal

-- Please type your report below this line
I call this an usability bug: Header weeding default is too week.

Software should be attractive with its default settings.  In this sense, 
current MUTT's default /etc/Muttrc setting may not be an ideal one.

I wish its default is consistent with what suggested in the manual.

Your consideration to change /etc/Muttrc to use ignore * and unignore 
combination will be most appreciated.  Also hdr_order may be good
to include to give readability.  (See below for an example.)

After all these are preference thing, this is not critical but just an 
wish list.  

Anyway, thanks for great MUA.

osamu@aokiconsulting.com (personal correspondence)
debian@aokiconsulting.com (ML correspondence)

-- Mutt Version Information

Mutt 1.2.5i (2000-07-28)
Copyright (C) 1996-2000 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.2.17 [using ncurses 5.0]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +USE_FCNTL  -USE_FLOCK
+USE_IMAP  +USE_GSS  +USE_SSL  +USE_POP  +HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  +ENABLE_NLS
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
SHAREDIR="/usr/share/mutt"
SYSCONFDIR="/etc"
ISPELL="/usr/bin/ispell"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the muttbug utility.


--- Begin /etc/Muttrc
ignore *
unignore from date subject to cc
unignore organization organisation x-mailer: x-newsreader: x-mailing-list:
unignore posted-to: user-agent
hdr_order date from to cc subject
bind editor    "\e<delete>"    kill-word
bind editor    "\e<backspace>" kill-word
bind editor     <delete>  delete-char
unset use_domain
unset use_from
set sort=threads
unset write_bcc
unset bounce_delivered
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!zless /usr/share/doc/mutt/manual.txt.gz\n" "Show Mutt documentation"
macro index   <f1> "!zless /usr/share/doc/mutt/manual.txt.gz\n" "Show Mutt documentation"
macro pager   <f1> "!zless /usr/share/doc/mutt/manual.txt.gz\n" "Show Mutt documentation"
color hdrdefault cyan default
color quoted   green default
color signature        cyan default
color attachment brightyellow default
color indicator black cyan
color status   brightgreen blue
color tree     red default
color markers  brightred default
color tilde    blue default
color header   brightgreen default ^From:
color header   brightcyan default ^To:
color header   brightcyan default ^Reply-To:
color header   brightcyan default ^Cc:
color header   brightblue default ^Subject:
color body     brightred default [\-\.+_a-zA-Z0-9]+@[\-\.a-zA-Z0-9]+
color body     brightblue default (http|ftp)://[\-\.\,/%~_:?\#a-zA-Z0-9]+
charset-hook US-ASCII     ISO-8859-1
charset-hook x-unknown    ISO-8859-1
charset-hook windows-1250 CP1250
charset-hook windows-1251 CP1251
charset-hook windows-1252 CP1252
charset-hook windows-1253 CP1253
charset-hook windows-1254 CP1254
charset-hook windows-1255 CP1255
charset-hook windows-1256 CP1256
charset-hook windows-1257 CP1257
charset-hook windows-1258 CP1258
set pgp_sign_micalg=pgp-sha1 # default for DSS keys
set pgp_decode_command="gpg %?p?--passphrase-fd 0? --no-verbose --batch --output - %f"
set pgp_verify_command="gpg --no-verbose --batch --output - --verify %s %f"
set pgp_decrypt_command="gpg --passphrase-fd 0 --no-verbose --batch --output - %f"
set pgp_sign_command="gpg --no-verbose --batch --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="gpg --no-verbose --batch --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap gpg -v --batch --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap gpg --passphrase-fd 0 -v --batch --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="gpg --no-verbose --import -v %f"
set pgp_export_command="gpg --no-verbose --export --armor %r"
set pgp_verify_key_command="gpg --no-verbose --batch --fingerprint --check-sigs %r"
set pgp_list_pubring_command="gpg --no-verbose --batch --with-colons --list-keys %r" 
set pgp_list_secring_command="gpg --no-verbose --batch --with-colons --list-secret-keys %r" 
set pgp_getkeys_command=""
--- End /etc/Muttrc


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2000-09-26 06:30:43 UTC Christopher Ruehl <ruehlc@europe.com>
* Added comment:
{{{
> set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap gpg  ... 

your configuration is just inspire,
but if you build with ./configure --prefix={whatever} 
the pgpewrap will be install in {whatever}/bin ;)

regards
cr



-- 
lächle, denn es könnte schlimmer kommen .. und es kommt schlimmer 
 murfy's law
}}}

--------------------------------------------------------------------------------
2001-06-05 01:34:05 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
severity 286 wishlist
thanks
}}}

--------------------------------------------------------------------------------
2005-08-24 12:38:11 UTC rado
* Added comment:
{{{
better defaults support
}}}

--------------------------------------------------------------------------------
2007-11-20 12:16:55 UTC pdmef
* Added comment:
Along with #2984 it may be a good idea to set such defaults for 1.6.

* component changed to display
* milestone changed to 1.6
* priority changed to minor

--------------------------------------------------------------------------------
2007-11-22 14:51:42 UTC Osamu Aoki
* Added comment:
{{{
On Tue, Nov 20, 2007 at 12:17:08PM -0000, Mutt wrote:
> #286: Muttrc default value (weak header weeding)
> 
> Changes (by pdmef):
> 
>   * priority:  trivial => minor
>   * component:  mutt => display
>   * milestone:  => 1.6
> 
> Comment:
> 
>  Along with #2984 it may be a good idea to set such defaults for 1.6.

Thanks for consideration.

As for the comment on my post as:

> > set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap gpg  ... 
> 
> your configuration is just inspire,
> but if you build with ./configure --prefix={whatever} 
> the pgpewrap will be install in {whatever}/bin ;)
> 
> regards
> cr

The Debian system (which I use) installs command not executed directly
from the shell prompt under /usr/lib/<package>/ as per its policy or
FHS.

If I were to build and install mutt on this system, I am supposed to do
./configure --prefix=/usr/local or =/opt to avoid conflicts with
distribution files :-)

Osamu
}}}

--------------------------------------------------------------------------------
2008-05-21 22:04:44 UTC Paul Walker
* Added attachment mutt.muttrc.1
* Added comment:
Added by email2trac

* Added comment:
{{{
Hi,

It seems a reasonable thing; the existing sample muttrc does seem to take a
rather odd approach to filtering header lines.

I've never attached something to a bug via email before, let's see how this
goes...
}}}

--------------------------------------------------------------------------------
2008-05-29 05:22:28 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
(In [f4259a92dab6]) Make default muttrc header weeding tidier. Closes #286.

* resolution changed to fixed
* status changed to closed
