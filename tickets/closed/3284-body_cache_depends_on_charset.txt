Ticket:  3284
Status:  closed
Summary: body cache depends on $charset

Reporter: pdmef
Owner:    pdmef

Opened:       2009-06-29 20:35:29 UTC
Last Updated: 2009-07-07 13:04:39 UTC

Priority:  minor
Component: body cache
Keywords:  patch

--------------------------------------------------------------------------------
Description:
When foldernames contain chars outside the ascii-range, it depends on $charset how the bcache path is composed. As hcache is utf8 only and independent from $charset, bcache should be, too.

One idea is to convert the path portion to UTF8 and URL-encode it.

--------------------------------------------------------------------------------
2009-06-30 11:16:55 UTC pdmef
* component changed to body cache

--------------------------------------------------------------------------------
2009-06-30 12:30:01 UTC pdmef
* Added comment:
A proposed fix is here: http://bitbucket.org/pdmef/muttfixesmq/src/tip/ticket-3284

Unfortunately, hcache suffers the same problem. The problem is: Unicode folder names in IMAP are UTF-7 encoded. Mutt decodes that to UTF-8 and does charset conversion to $charset. That path is then displayed to the user and used for hcache/bache paths which thus depend on $charset. E.g. gmail used "[Google Mail]/Entwürfe" (German u umlaut, likely this is "[Google Mail]/Drafts" or the like in the english version). With a UTF-8 locale, the umlaut is encoded as 0xC3 0xBC, but with a latin1 charset as 0xFC. If the user switches locales, he ends up with two different bcaches and two different hcaches.

The proposed fix uses URL-like encoding using '+' instead of '%'. Thus, the above folder is always 'Entw+C3+BCrfe' on disk.

--------------------------------------------------------------------------------
2009-07-01 14:28:33 UTC pdmef
* status changed to accepted

--------------------------------------------------------------------------------
2009-07-01 14:28:41 UTC pdmef
* status changed to started

--------------------------------------------------------------------------------
2009-07-04 01:54:34 UTC brendan
* Added comment:
Seems reasonable I suppose. But why use + instead of %? And why not just write the paths as UTF-8? AFAIK windows, mac, and linux can handle it. The worse problem is case-insensitivity. I'm not looking forward to writing a portable path encoder.

--------------------------------------------------------------------------------
2009-07-05 09:35:59 UTC pdmef
* Added comment:
I guess I used + instead of % because it looks nicer. It's been a while that I had to deal with non-ascii in pathnames (with samba) and that wasn't really straightforward. If this is safe now to just use utf8, the fix will be even simpler.

--------------------------------------------------------------------------------
2009-07-06 14:53:29 UTC pdmef
* Added comment:
The patch at bitbucket.org is updated to only do utf-8 encoding.

* keywords changed to patch

--------------------------------------------------------------------------------
2009-07-06 17:12:54 UTC brendan
* Added comment:
This looks pretty good to me.

--------------------------------------------------------------------------------
2009-07-07 13:04:39 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [f161c2f00d84]) Make hcache+bcache paths always UTF-8. Closes #3284.

* resolution changed to fixed
* status changed to closed
