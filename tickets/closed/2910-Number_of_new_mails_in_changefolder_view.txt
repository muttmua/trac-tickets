Ticket:  2910
Status:  closed
Summary: Number of new mails in change-folder view

Reporter: christof
Owner:    brendan

Opened:       2007-06-13 22:00:03 UTC
Last Updated: 2008-10-30 22:13:02 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
If you have new mails in a IMAP mailbox, you can display the count in the folder view with the macro %N. Now you read the mails and you would expect that this counter is reseted. But since version 1.5.15 this isn't (1.5.14 have the expected behavior). Even if you save the box the counter shows the old number of new emails.

If you need some kind of more information or a debug log, i can provide it. Just let me know.

--------------------------------------------------------------------------------
2007-06-13 22:01:32 UTC christof
* version changed to 1.5.15

--------------------------------------------------------------------------------
2007-06-15 13:06:35 UTC drow
* Added attachment mutt-005-drow-unseen-count
* Added comment:
Here's a hacky patch I have been using for this issue.  I don't claim it's right...

--------------------------------------------------------------------------------
2007-06-15 23:53:37 UTC brendan
* Added comment:
This patch appears to remove the ability to distinguish between "new" and "old" unread messages over IMAP (which was finally fixed in 1.5.14 IIRC). You're not supposed to be prompted to open mailboxes that only have old messages.

I think the actual problem is that the mailbox display uses the buffy new count (only new messages), when what it should probably be doing is fetching the unseen count from the IMAP mailbox status cache.

--------------------------------------------------------------------------------
2007-09-27 19:27:17 UTC christof
* Added comment:
In 1.5.16 it also doesn't work.

* version changed to 1.5.16

--------------------------------------------------------------------------------
2007-12-07 19:25:27 UTC brendan
* component changed to IMAP
* milestone changed to 1.6
* owner changed to brendan

--------------------------------------------------------------------------------
2008-09-14 13:57:51 UTC mivok
* Added comment:
This appears to be the same issue reported in #2897

--------------------------------------------------------------------------------
2008-10-30 22:13:02 UTC brendan
* Added comment:
Should be fixed in [ac00273b3a88].

* resolution changed to fixed
* status changed to closed
