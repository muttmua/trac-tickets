Ticket:  2471
Status:  closed
Summary: mutt misinterpretation of ".." while changing dir in the browser

Reporter: stefano.sabatini-lala@poste.it
Owner:    mutt-dev

Opened:       2006-09-04 15:44:16 UTC
Last Updated: 2007-09-05 07:16:47 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
In the browser when changing directory with "c.." or "c../", the command results in a jump in a directory usually different from the parent directory (usually it jumps in the /home or the /home/<username> directory). 
After this happens, the directory indication in the status line reports ".." as the current directory.

Selecting in the browser the line with the "../" results in the correct behaviour.
>How-To-Repeat:
In the browser context:
c<delete the presetted value in the command line>../<enter>

--------------------------------------------------------------------------------
2007-04-30 02:01:00 UTC brendan
* Updated description:
In the browser when changing directory with "c.." or "c../", the command results in a jump in a directory usually different from the parent directory (usually it jumps in the /home or the /home/<username> directory). 
After this happens, the directory indication in the status line reports ".." as the current directory.

Selecting in the browser the line with the "../" results in the correct behaviour.
>How-To-Repeat:
In the browser context:
c<delete the presetted value in the command line>../<enter>
* milestone changed to 1.6

--------------------------------------------------------------------------------
2007-09-05 07:16:47 UTC pdmef
* Added comment:
(In [964658b145df]) Interpret relative paths in browser relative to shown dir, not working
dir (closes #2471).

* resolution changed to fixed
* status changed to closed
