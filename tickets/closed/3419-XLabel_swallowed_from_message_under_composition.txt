Ticket:  3419
Status:  closed
Summary: X-Label swallowed from message under composition

Reporter: madduck
Owner:    mutt-dev

Opened:       2010-06-14 08:20:53 UTC
Last Updated: 2016-08-31 06:59:39 UTC

Priority:  major
Component: mutt
Keywords:  x-label edit_headers

--------------------------------------------------------------------------------
Description:
From http://bugs.debian.org/583251:

I have $edit_headers set. I quite often add headers to messages
I compose. This works fine, except for X-Label. If I add X-Label to
the message, the header seems to get removed after the editor exits.
Re-editing, or saving the message as a draft and inspecting the raw
message both show the absence of the header.


--------------------------------------------------------------------------------
2016-08-31 06:59:39 UTC antonio@dyne.org
* Added comment:
This is fixed in 1.7.0

* resolution changed to fixed
* status changed to closed
