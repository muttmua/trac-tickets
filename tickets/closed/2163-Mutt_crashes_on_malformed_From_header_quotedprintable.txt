Ticket:  2163
Status:  closed
Summary: Mutt crashes on malformed "From:" header (quoted-printable)

Reporter: vincent@vinc17.org
Owner:    mutt-dev

Opened:       2006-01-14 17:45:12 UTC
Last Updated: 2007-03-07 02:59:28 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Mutt crashes on a maildir (see attached file) containing a message with only:

  From: =?iso-8859-1?Q??= <konateaicha@ifrance.com>

(this comes from a spam). The .muttdebug0 file says:

[...]
mh.c:760 maildir_add_to_context(): Considering 
mh.c:769 Adding header structure. Flags:
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-01-15 12:14:56 UTC Michael Tatge <Michael.Tatge@web.de>
* Added comment:
{{{
* On Sat, Jan 14, 2006 vincent@vinc17.org (vincent@vinc17.org) muttered:
> >Number:         2163
> >Synopsis:       Mutt crashes on malformed "From:" header (quoted-printable)
> >Release:        cvs + patches
> >Organization:
> >Environment:    Mac OS X
> >Description:
> Mutt crashes on a maildir (see attached file) containing a message
> with only:
> 
>   From: =?iso-8859-1?Q??= <konateaicha@ifrance.com>

FWIW I cannot reproduce a crash with 1.5.11 or cvs head under linux 2.6.14.3.

Michael
-- 
BOFH excuse #439:

Hot Java has gone cold

PGP-Key-ID: 0xDC1A44DD
Jabber:     init[0]@amessage.de
}}}

--------------------------------------------------------------------------------
2006-01-15 13:58:31 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2006-01-14 19:14:56 +0100, Michael Tatge wrote:
> FWIW I cannot reproduce a crash with 1.5.11 or cvs head under linux 2.6=
.14.3.

Yes, this seems to be due to one of the patches.

--=20
Vincent Lef=E8vre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2006-01-15 14:06:38 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2006-01-14 20:58:31 +0100, Vincent Lefevre wrote:
> On 2006-01-14 19:14:56 +0100, Michael Tatge wrote:
> > FWIW I cannot reproduce a crash with 1.5.11 or cvs head under
> > linux 2.6.14.3.
>=20
> Yes, this seems to be due to one of the patches.

This is due to the namequot patch.

--=20
Vincent Lef=E8vre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2006-01-15 22:22:38 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2006-01-14 21:15:01 +0100, Vincent Lefevre wrote:
>  This is due to the namequot patch.

I've attached a fixed version of this namequot patch. The problem
was that mutt_get_name could return a null pointer.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / SPACES project at LORIA

--LQksG6bCIzRHxTLp
Content-Type: text/plain; charset=us-ascii
Content-Disposition: attachment; filename="patch-1.5.11cvs.tamovl.namequot.1"

Index: PATCHES
=================================RCS file: /home/roessler/cvs/mutt/PATCHES,v
retrieving revision 3.6
diff -d -u -p -r3.6 PATCHES
--- PATCHES	9 Dec 2002 17:44:54 -0000	3.6
+++ PATCHES	15 Jan 2006 03:57:50 -0000
@@ -0,0 +1 @@
+patch-1.5.11cvs.tamovl.namequot.1
Index: copy.c
=================================RCS file: /home/roessler/cvs/mutt/copy.c,v
retrieving revision 3.27
diff -d -u -p -r3.27 copy.c
--- copy.c	21 Oct 2005 04:35:37 -0000	3.27
+++ copy.c	15 Jan 2006 03:57:50 -0000
@@ -982,6 +982,7 @@ static int address_header_decode (char *
   if ((a      return 0;
   
+  rfc822_dequotepersonal_adrlist (a);
   mutt_addrlist_to_local (a);
   rfc2047_decode_adrlist (a);
   
Index: hdrline.c
=================================RCS file: /home/roessler/cvs/mutt/hdrline.c,v
retrieving revision 3.18
diff -d -u -p -r3.18 hdrline.c
--- hdrline.c	10 Jan 2006 19:15:21 -0000	3.18
+++ hdrline.c	15 Jan 2006 03:57:50 -0000
@@ -59,7 +59,11 @@ check_for_mailing_list (ADDRESS *adr, ch
     if (mutt_is_subscribed_list (adr))
     {
       if (pfx && buf && buflen)
-	snprintf (buf, buflen, "%s%s", pfx, mutt_get_name (adr));
+      {
+	char *name +	snprintf (buf, buflen, "%s%s", pfx, name);
+	FREE (&name);
+      }
       return 1;
     }
   }
@@ -102,6 +106,7 @@ static int first_mailing_list (char *buf
 static void make_from (ENVELOPE *hdr, char *buf, size_t len, int do_lists)
 {
   int me;
+  char *name  
   me  
@@ -114,13 +119,16 @@ static void make_from (ENVELOPE *hdr, ch
   }
 
   if (me && hdr->to)
-    snprintf (buf, len, "To %s", mutt_get_name (hdr->to));
+    snprintf (buf, len, "To %s", name    else if (me && hdr->cc)
-    snprintf (buf, len, "Cc %s", mutt_get_name (hdr->cc));
+    snprintf (buf, len, "Cc %s", name    else if (hdr->from)
-    strfcpy (buf, mutt_get_name (hdr->from), len);
+    strfcpy (buf, name    else
     *buf +
+  if (name)
+    FREE (&name);
 }
 
 static void make_from_addr (ENVELOPE *hdr, char *buf, size_t len, int do_lists)
@@ -493,7 +501,11 @@ hdr_format_str (char *dest,
       break;
 
     case 'n':
-      mutt_format_s (dest, destlen, prefix, mutt_get_name (hdr->env->from));
+      {
+	char *name +	mutt_format_s (dest, destlen, prefix, name);
+	FREE (&name);
+      }
       break;
 
     case 'N':
@@ -588,10 +600,13 @@ hdr_format_str (char *dest,
       if (!check_for_mailing_list (hdr->env->to, "To ", buf2, sizeof (buf2)) &&
 	  !check_for_mailing_list (hdr->env->cc, "Cc ", buf2, sizeof (buf2)))
       {
+	char *name  	if (hdr->env->to)
-	  snprintf (buf2, sizeof (buf2), "To %s", mutt_get_name (hdr->env->to));
+	  snprintf (buf2, sizeof (buf2), "To %s", name  	else if (hdr->env->cc)
-	  snprintf (buf2, sizeof (buf2), "Cc %s", mutt_get_name (hdr->env->cc));
+	  snprintf (buf2, sizeof (buf2), "Cc %s", name +	if (name)
+	  FREE (&name);
       }
       mutt_format_s (dest, destlen, prefix, buf2);
       break;
@@ -617,15 +632,22 @@ hdr_format_str (char *dest,
     case 'v':
       if (mutt_addr_is_user (hdr->env->from)) 
       {
+	char *name  	if (hdr->env->to)
-	  mutt_format_s (buf2, sizeof (buf2), prefix, mutt_get_name (hdr->env->to));
+	  mutt_format_s (buf2, sizeof (buf2), prefix, name  	else if (hdr->env->cc)
-	  mutt_format_s (buf2, sizeof (buf2), prefix, mutt_get_name (hdr->env->cc));
+	  mutt_format_s (buf2, sizeof (buf2), prefix, name  	else
 	  *buf2 +	if (name)
+	  FREE (&name);
       }
       else
-	mutt_format_s (buf2, sizeof (buf2), prefix, mutt_get_name (hdr->env->from));
+      {
+	char *name +	mutt_format_s (buf2, sizeof (buf2), prefix, name);
+	FREE (&name);
+      }
       if ((p  	*p        mutt_format_s (dest, destlen, prefix, buf2);
Index: rfc822.c
=================================RCS file: /home/roessler/cvs/mutt/rfc822.c,v
retrieving revision 3.9
diff -d -u -p -r3.9 rfc822.c
--- rfc822.c	17 Sep 2005 20:46:11 -0000	3.9
+++ rfc822.c	15 Jan 2006 03:57:50 -0000
@@ -60,28 +60,6 @@ const char *RFC822Errors[]    "bad address spec"
 };
 
-void rfc822_dequote_comment (char *s)
-{
-  char *w -
-  for (; *s; s++)
-  {
-    if (*s = '\\')
-    {
-      if (!*++s)
-	break; /* error? */
-      *w++ -    }
-    else if (*s !-    {
-      if (w !-	*w -      w++;
-    }
-  }
-  *w -}
-
 void rfc822_free_address (ADDRESS **p)
 {
   ADDRESS *t;
@@ -119,6 +97,8 @@ parse_comment (const char *s,
     }
     else if (*s = '\\')
     {
+      if (*commentlen < commentmax)
+	comment[(*commentlen)++]        if (!*++s)
 	break;
     }
@@ -153,8 +133,8 @@ parse_quote (const char *s, char *token,
       if (!*++s)
 	break;
 
-      if (*tokenlen < tokenmax)
-	token[*tokenlen] +      if (*tokenlen + 1 < tokenmax)
+	token[++(*tokenlen)]      }
     (*tokenlen)++;
     s++;
@@ -451,9 +431,7 @@ ADDRESS *rfc822_parse_adrlist (ADDRESS *
       {
 	if (cur->personal)
 	  FREE (&cur->personal);
-	/* if we get something like "Michael R. Elkins" remove the quotes */
-	rfc822_dequote_comment (phrase);
-	cur->personal +	  cur->personal        }
       if ((ps        {
@@ -531,11 +509,6 @@ rfc822_cat (char *buf, size_t buflen, co
     *pc++      for (; *value && tmplen > 1; value++)
     {
-      if (*value = '\\' || *value = '"')
-      {
-	*pc++ -	tmplen--;
-      }
       *pc++        tmplen--;
     }
@@ -582,40 +555,12 @@ void rfc822_write_address_single (char *
 
   if (addr->personal)
   {
-    if (strpbrk (addr->personal, RFC822Specials))
-    {
-      if (!buflen)
-	goto done;
-      *pbuf++ -      buflen--;
-      for (pc -      {
-	if (*pc = '"' || *pc = '\\')
-	{
-	  if (!buflen)
-	    goto done;
-	  *pbuf++ -	  buflen--;
-	}
-	if (!buflen)
-	  goto done;
-	*pbuf++ -	buflen--;
-      }
-      if (!buflen)
-	goto done;
-      *pbuf++ -      buflen--;
-    }
-    else
-    {
-      if (!buflen)
-	goto done;
-      strfcpy (pbuf, addr->personal, buflen);
-      len -      pbuf +-      buflen --    }
+    if (!buflen)
+      goto done;
+    strfcpy (pbuf, addr->personal, buflen);
+    len +    pbuf ++    buflen - 
     if (!buflen)
       goto done;
@@ -791,6 +736,41 @@ ADDRESS *rfc822_append (ADDRESS **a, ADD
   return tmp;
 }
 
+/* dequote personal name */
+char *rfc822_dequote_personal (char **s)
+{
+  if (*s && **s)
+  {
+    size_t slen +    char *r +    char *p +    if (slen > 2 && **s = '"' && *(*s + slen - 1) = '"')
+    {
+      p++;
+      slen--;
+    }
+    for (; p < *s + slen; p++)
+    {
+      if (*p = '\\')
+        *q++ +      else
+        *q++ +    }
+    *q +    FREE (s);
+    *s +  }
+  return (*s);
+}
+
+/* dequote personal names in adrlist */
+void rfc822_dequotepersonal_adrlist (ADDRESS *a)
+{
+  ADDRESS *cur +  for (; cur; cur +    rfc822_dequote_personal (&cur->personal);
+}
+
 #ifdef TESTING
 int safe_free (void **p)
 {
Index: rfc822.h
=================================RCS file: /home/roessler/cvs/mutt/rfc822.h,v
retrieving revision 3.5
diff -d -u -p -r3.5 rfc822.h
--- rfc822.h	17 Sep 2005 20:46:11 -0000	3.5
+++ rfc822.h	15 Jan 2006 03:57:50 -0000
@@ -52,6 +52,8 @@ void rfc822_write_address (char *, size_
 void rfc822_write_address_single (char *, size_t, ADDRESS *, int);
 void rfc822_free_address (ADDRESS **addr);
 void rfc822_cat (char *, size_t, const char *, const char *);
+char *rfc822_dequote_personal (char **);
+void rfc822_dequotepersonal_adrlist (ADDRESS *);
 
 extern int RFC822Error;
 extern const char *RFC822Errors[];
Index: sort.c
=================================RCS file: /home/roessler/cvs/mutt/sort.c,v
retrieving revision 3.9
diff -d -u -p -r3.9 sort.c
--- sort.c	17 Sep 2005 20:46:11 -0000	3.9
+++ sort.c	15 Jan 2006 03:57:51 -0000
@@ -90,21 +90,28 @@ int compare_subject (const void *a, cons
   return (SORTCODE (rc));
 }
 
+/* needs freeing */
 const char *mutt_get_name (ADDRESS *a)
 {
   ADDRESS *ali;
+  char *tmp  
   if (a)
   {
     if (option (OPTREVALIAS) && (ali -      return ali->personal;
+      tmp      else if (a->personal)
-      return a->personal;
+      tmp      else if (a->mailbox)
-      return (mutt_addr_for_display (a));
+      tmp +    tmp +    if (tmp)
+      return tmp;
   }
   /* don't return NULL to avoid segfault when printing/comparing */
-  return ("");
+  tmp +  *tmp +  return tmp;
 }
 
 int compare_to (const void *a, const void *b)
@@ -117,6 +124,8 @@ int compare_to (const void *a, const voi
   fa    fb    result +  FREE (&fa);
+  FREE (&fb);
   AUXSORT(result,a,b);
   return (SORTCODE (result));
 }
@@ -131,6 +140,8 @@ int compare_from (const void *a, const v
   fa    fb    result +  FREE (&fa);
+  FREE (&fb);
   AUXSORT(result,a,b);
   return (SORTCODE (result));
 }

--LQksG6bCIzRHxTLp--
}}}

--------------------------------------------------------------------------------
2006-01-16 20:49:03 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
* Sun Jan 15 2006 Vincent Lefevre <vincent@vinc17.org>
> On 2006-01-14 21:15:01 +0100, Vincent Lefevre wrote:
> >  This is due to the namequot patch.
> 
> I've attached a fixed version of this namequot patch. The problem
> was that mutt_get_name could return a null pointer.

Thanks! I didn't realize that safe_strdup() could return NULL.

>    /* don't return NULL to avoid segfault when printing/comparing */
> -  return ("");
> +  tmp = safe_malloc (1);
> +  *tmp = '\0';
> +  return tmp;

Great! That's what I intended.

-- 
tamo
}}}

--------------------------------------------------------------------------------
2007-03-07 20:59:28 UTC brendan
* Added comment:
{{{
* On Sat, Jan 14, 2006 vincent@vinc17.org (vincent@vinc17.org) muttered:
 On 2006-01-14 19:14:56 +0100, Michael Tatge wrote:
 On 2006-01-14 20:58:31 +0100, Vincent Lefevre wrote:
 
 * Sun Jan 15 2006 Vincent Lefevre <vincent@vinc17.org>
closed by submitter's request
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:10 UTC 
* Added attachment crash.tar.gz
* Added comment:
crash.tar.gz
