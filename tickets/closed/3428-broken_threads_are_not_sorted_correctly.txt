Ticket:  3428
Status:  closed
Summary: broken threads are not sorted correctly

Reporter: scholz
Owner:    mutt-dev

Opened:       2010-07-16 10:21:23 UTC
Last Updated: 2010-07-31 17:18:06 UTC

Priority:  minor
Component: display
Keywords:  thread broken

--------------------------------------------------------------------------------
Description:
After breaking a thread, the display shows the broken part (now a new thread) still as part of the old. It should sort it according to the date of the new root. 

--------------------------------------------------------------------------------
2010-07-31 17:18:06 UTC me
* Added comment:
I think you want to look at the $strict_threads and $sort_re variables and change them to suit your taste.

Feel free to reopen this bug if these options do not cover what you are referring to.

* resolution changed to wontfix
* status changed to closed
