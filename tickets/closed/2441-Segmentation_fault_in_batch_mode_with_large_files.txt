Ticket:  2441
Status:  closed
Summary: Segmentation fault in batch mode with large files

Reporter: yaelgilad@myrealbox.com
Owner:    mutt-dev

Opened:       2006-08-23 14:14:43 UTC
Last Updated: 2007-03-31 05:59:39 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Sending a large file via command line causes a segmentation fault

There was one important piece of information missing:
The mail sending fails at mutt_invoke_sendmail due to a problem in my mail server (not mutt related),
and then mutt_pager is called, thus starting the previously defined sequence.
>How-To-Repeat:
tail -3000 somefile | mutt -s "Sending something"  user@somewhere.com
>Fix:
The crash happens at the mutt_pager function.
The function uses curses "LINES".
However, since we are running in batch mode, curses was not initialized, and LINES evaluates to zero.
A lot of bad things happen; e.g. lineInfo is allocated with zero bytes, and is thus a NULL pointer

--------------------------------------------------------------------------------
2006-08-24 09:16:22 UTC Michael Tatge <Michael.Tatge@web.de>
* Added comment:
{{{
* On Wed, Aug 23, 2006 yaelgilad@myrealbox.com (yaelgilad@myrealbox.com) muttered:
> >Number:         2441
> >Synopsis:       Segmentation fault in batch mode with large files
> >Release:        mutt-1.4.2.2
> >Environment:
> Windows 2000 with cygwin
> >Description:
> Sending a large file via command line causes a segmentation fault
> >How-To-Repeat:
> tail -3000 somefile | mutt -s "Sending something"  user@somewhere.com

I just tested with with 1.4.2.2 and 1.5.13 under linux. Works fine. Even
10k lines are not problem. I guess this is a windows or cygwin problem.

HTH,

Michael
-- 
... A booming voice says, "Wrong, cretin!", and you notice that you
have turned into a pile of dust.

PGP-Key-ID: 0xDC1A44DD
Jabber:     init[0]@amessage.de
}}}

--------------------------------------------------------------------------------
2006-08-27 12:05:31 UTC paul
* Added comment:
{{{
* On Wed, Aug 23, 2006 yaelgilad@myrealbox.com (yaelgilad@myrealbox.com) muttered:
getting feedback from submitter
}}}

* status changed to new

--------------------------------------------------------------------------------
2006-08-27 12:05:32 UTC paul
* Added comment:
{{{
yaelgilad, did the Cygwin people have anything useful to say about this?
}}}

--------------------------------------------------------------------------------
2007-03-31 05:59:39 UTC brendan
* Added comment:
This is fixed in [359d9fbb5fda]

* Updated description:
Sending a large file via command line causes a segmentation fault

There was one important piece of information missing:
The mail sending fails at mutt_invoke_sendmail due to a problem in my mail server (not mutt related),
and then mutt_pager is called, thus starting the previously defined sequence.
>How-To-Repeat:
tail -3000 somefile | mutt -s "Sending something"  user@somewhere.com
>Fix:
The crash happens at the mutt_pager function.
The function uses curses "LINES".
However, since we are running in batch mode, curses was not initialized, and LINES evaluates to zero.
A lot of bad things happen; e.g. lineInfo is allocated with zero bytes, and is thus a NULL pointer
* priority changed to major
* resolution changed to fixed
* status changed to closed
