Ticket:  2303
Status:  closed
Summary: problem with "ac_cv_lib_ssl_SSL_new" test

Reporter: stopuce111@hotmail.com
Owner:    mutt-dev

Opened:       2006-06-23 10:55:02 UTC
Last Updated: 2007-09-05 00:23:08 UTC

Priority:  minor
Component: build
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
If the shared ssl libraries are installed, the configure script
completes and the build is successful.  However, if only the static libraries
are installed (the default for OpenSSL), the configure script fails with:
        checking for X509_new in -lcrypto... yes
        checking for SSL_new in -lssl... no
        configure: error: Unable to find SSL library

It's strange that the -lcrypto test works while the -lssl test fails since
they are both from the exact same intallation of OpenSSL 0.9.8b.  If the
"else" clause at lines 12580-12583 of the configure script is simply
replaced with one line "ac_cv_lib_ssl_SSL_new=yes" the configure script
seems to finish successfully, so perhaps the problem is only the test?
>How-To-Repeat:
run ./configure with --with-ssl=/usr/local/ssl with only the
static libraries installed (default for OpenSSL 0.9.8b.  (There is no problem if shared libraries are installed.)
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-06-24 08:42:47 UTC Ralf Wildenhues <wildenhues@ins.uni-bonn.de>
* Added comment:
{{{
http://bugs.mutt.org/cgi-bin/gnatsweb.pl?debug=&database=mutt&cmd=view+audit-trail&cmd=view&pr=2303

Hello Henry,

>      checking for X509_new in -lcrypto... yes
>      checking for SSL_new in -lssl... no
>      configure: error: Unable to find SSL library

Please post the relevant parts of config.log that show the failure,
including the link command lines that the tests used.

Cheers,
Ralf
}}}

--------------------------------------------------------------------------------
2006-09-22 15:45:27 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
Hi,

The last notation in this bug is a request from Ralf for you to provide the
relevant section of the config.log file. Have you had chance to re-run the
configure recently...?

Thanks,

-- 
Paul
}}}

--------------------------------------------------------------------------------
2006-09-25 08:33:00 UTC "leavemealone stopuce" <stopuce111@hotmail.com>
* Added comment:
{{{
I thought this was solved and closed.  Very sorry for the misunderstanding.

It was some problem of libz not getting put into $LIBS when building with 
SSL
support.  Thus the test "ac_cv_lib_ssl_SSL_new" needs to include libz or 
test
whether or not SSL was built with libz support.  It only becomes a problem 
when
building a statically linked Mutt.

Easiest fix is to export LIBS="-lz" before running configure.

Regards,
Henry

_________________________________________________________________
Find a local pizza place, music store, museum and morethen map the best 
route!  http://local.live.com
}}}

--------------------------------------------------------------------------------
2006-09-25 10:35:44 UTC "leavemealone stopuce" <stopuce111@hotmail.com>
* Added comment:
{{{
>The last notation in this bug is a request from Ralf for you to provide the
>relevant section of the config.log file. Have you had chance to re-run the

In case someone is receiving this mail.  Mutt version is 1.5.13.

setenv LDFLAGS -static
./configure --prefix=/usr/local --with-slang=/usr/local \
--with-libiconv-prefix=/usr/local --enable-pop --enable-imap \
--with-ssl=/usr/local/ssl

checking for SSL_new in -lssl... no
configure: error: Unable to find SSL library

From config.log:

configure:13649: checking for SSL_new in -lssl
configure:13679: gcc -o conftest -g -O2  -I/usr/local/include 
-I/usr/local/ssl/i
nclude -static -L/usr/local/lib -L/usr/local/ssl/lib conftest.c -lssl 
-lcrypto -
lcrypto -ltermlib  >&5
/usr/local/ssl/lib/libcrypto.a(c_zlib.o)(.text+0x6f): In function 
`zlib_stateful
_init':
: undefined reference to `inflateInit_'
/usr/local/ssl/lib/libcrypto.a(c_zlib.o)(.text+0xb9): In function 
`zlib_stateful
_init':
: undefined reference to `deflateInit_'
/usr/local/ssl/lib/libcrypto.a(c_zlib.o)(.text+0x21a): In function 
`zlib_statefu
l_compress_block':
: undefined reference to `deflate'
/usr/local/ssl/lib/libcrypto.a(c_zlib.o)(.text+0x293): In function 
`zlib_statefu
l_expand_block':
: undefined reference to `inflate'
/usr/local/ssl/lib/libcrypto.a(c_zlib.o)(.text+0x2bf): In function 
`zlib_statefu
l_free_ex_data':
: undefined reference to `inflateEnd'
/usr/local/ssl/lib/libcrypto.a(c_zlib.o)(.text+0x2ca): In function 
`zlib_statefu
l_free_ex_data':
: undefined reference to `deflateEnd'
configure:13685: $? = 1
configure: failed program was:
| /* confdefs.h.  */
[... snip many defines ...]
| /* end confdefs.h.  */
|
| /* Override any gcc2 internal prototype to avoid an error.  */
| #ifdef __cplusplus
| extern "C"
| #endif
| /* We use char because int might match the return type of a gcc2
|    builtin and then its argument prototype would still apply.  */
| char SSL_new ();
| int
| main ()
| {
| SSL_new ();
|   ;
|   return 0;
| }
configure:13711: result: no
configure:13721: error: Unable to find SSL library

_________________________________________________________________
Share your special moments by uploading 500 photos per month to Windows Live 
Spaces  
http://clk.atdmt.com/MSN/go/msnnkwsp0070000001msn/direct/01/?href=http://www.get.live.com/spaces/features
}}}

--------------------------------------------------------------------------------
2006-09-26 04:42:09 UTC Ralf Wildenhues <wildenhues@ins.uni-bonn.de>
* Added comment:
{{{
* leavemealone stopuce wrote on Mon, Sep 25, 2006 at 10:15:03AM CEST:
> 
>  I thought this was solved and closed.  Very sorry for the misunderstanding.

Oh, no, I think it's me to apologize.  You sent me a message privately
(due to some mailing issue), I forgot about it.  Sorry about that.

>  It was some problem of libz not getting put into $LIBS when building
>  with SSL support.  Thus the test "ac_cv_lib_ssl_SSL_new" needs to
>  include libz or test whether or not SSL was built with libz support.
>  It only becomes a problem when building a statically linked Mutt.
>  
>  Easiest fix is to export LIBS="-lz" before running configure.

A patch to fix this would be this one, I guess.

Note I do not know whether libcrypto may also be built without needing
libz (in which case the patch is too strict).

Cheers,
Ralf

	* configure.in: libcrypto depends on libz, so search that, too.
	Fixes #2303.

Index: configure.in
===================================================================
RCS file: /home/roessler/cvs/mutt/configure.in,v
retrieving revision 3.56
diff -u -r3.56 configure.in
--- configure.in	1 Sep 2006 19:26:39 -0000	3.56
+++ configure.in	25 Sep 2006 08:25:03 -0000
@@ -641,15 +642,16 @@
           fi
           saved_LIBS="$LIBS"
 
-          AC_CHECK_LIB(crypto, X509_new,, AC_MSG_ERROR([Unable to find SSL library]))
-          AC_CHECK_LIB(ssl, SSL_new,, AC_MSG_ERROR([Unable to find SSL library]), -lcrypto)
+          AC_CHECK_LIB(z, deflate, , [AC_MSG_ERROR([Unable to find libz])])
+          AC_CHECK_LIB(crypto, X509_new,, [AC_MSG_ERROR([Unable to find SSL library])], -lz)
+          AC_CHECK_LIB(ssl, SSL_new,, [AC_MSG_ERROR([Unable to find SSL library])], -lcrypto -lz)
 
           AC_CHECK_FUNCS(RAND_status RAND_egd)
 
           AC_DEFINE(USE_SSL,1,[ Define if you want support for SSL. ])
           AC_DEFINE(USE_SSL_OPENSSL,1,[ Define if you want support for SSL via OpenSSL. ])
           LIBS="$saved_LIBS"
-          MUTTLIBS="$MUTTLIBS -lssl -lcrypto"
+          MUTTLIBS="$MUTTLIBS -lssl -lcrypto -lz"
           MUTT_LIB_OBJECTS="$MUTT_LIB_OBJECTS mutt_ssl.o"
           need_ssl=yes
}}}

--------------------------------------------------------------------------------
2007-04-07 22:25:57 UTC brendan
* component changed to build
* Updated description:
{{{
If the shared ssl libraries are installed, the configure script
completes and the build is successful.  However, if only the static libraries
are installed (the default for OpenSSL), the configure script fails with:
        checking for X509_new in -lcrypto... yes
        checking for SSL_new in -lssl... no
        configure: error: Unable to find SSL library

It's strange that the -lcrypto test works while the -lssl test fails since
they are both from the exact same intallation of OpenSSL 0.9.8b.  If the
"else" clause at lines 12580-12583 of the configure script is simply
replaced with one line "ac_cv_lib_ssl_SSL_new=yes" the configure script
seems to finish successfully, so perhaps the problem is only the test?
>How-To-Repeat:
run ./configure with --with-ssl=/usr/local/ssl with only the
static libraries installed (default for OpenSSL 0.9.8b.  (There is no problem if shared libraries are installed.)
>Fix:
Unknown
}}}
* milestone changed to 1.6

--------------------------------------------------------------------------------
2007-09-05 00:23:08 UTC brendan
* Added comment:
(In [7df563e4b7fd]) Add libz to crypto libs if available (closes #2303).

* resolution changed to fixed
* status changed to closed
