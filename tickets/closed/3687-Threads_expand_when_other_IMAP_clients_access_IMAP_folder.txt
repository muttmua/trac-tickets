Ticket:  3687
Status:  closed
Summary: Threads expand when other IMAP clients access IMAP folder

Reporter: G11_a
Owner:    mutt-dev

Opened:       2014-05-07 16:05:05 UTC
Last Updated: 2017-12-11 03:48:53 UTC

Priority:  minor
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
when using 'Imapfilter' (for example), to access and then sort an IMAP (using Maildir format) mailbox, mutt displays "Mailbox was externally modified.  Flags may be wrong.", and, expands all threads. If this is done on a non-threaded view, the above error message appears, but, no changes to view are made.   

The same result occurs, when issuing '"mutt -e "push $MACRO"' , if the macro has <sync-mailbox> command in it. If, another window of mutt is open, then, the second window will get the error displayed, and the threads get expanded. ("second window" is the window which was open already).

System: Linux 3.2.0-60-generic (x86_64)
ncurses: ncurses 5.9.20110404 (compiled with 5.9)


--------------------------------------------------------------------------------
2017-11-06 22:08:10 UTC kevin8t8
* Added comment:
changeset:e98ad5446640 in hg tip (post 1.9.1 release) made some improvements to imap syncing and fetch handling, that may alleviate the warning messages in many cases.

Additionally the $uncollapse_new opion may be unset to turn off the uncollapsing behavior.

I realize, due to the age of the ticket, you may have completely given up on this.  However, on the chance you are still using mutt and care about this ticket, would you mind trying hg tip and/or unsetting $uncollapse_new to see if that fixes the problem for you?

Thank you!

--------------------------------------------------------------------------------
2017-12-11 03:48:53 UTC kevin8t8
* Added comment:
Ticket seems to be too old.  I believe the fixes from comment:1 have taken care of this, so am marking this as fixed.

* resolution changed to fixed
* status changed to closed
