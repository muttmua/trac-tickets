Ticket:  3988
Status:  closed
Summary: signed integer overflow in mbox_parse_mailbox

Reporter: josephbisch
Owner:    mutt-dev

Opened:       2018-02-11 03:21:09 UTC
Last Updated: 2018-02-14 16:05:54 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Using a version of mutt compiled with ubsan, the attached mbox file causes the following output from ubsan:

mbox.c:320:15: runtime error: signed integer overflow: 629 + 9223372036854775807 cannot be represented in type 'long'

--------------------------------------------------------------------------------
2018-02-11 03:21:45 UTC josephbisch
* Added attachment testcase.mbox

--------------------------------------------------------------------------------
2018-02-11 17:34:31 UTC kevin8t8
* Added comment:
I'm really not inclined to care about this much.  Vincent?

--------------------------------------------------------------------------------
2018-02-13 19:58:15 UTC vinc17
* Added attachment bug3988.patch

--------------------------------------------------------------------------------
2018-02-13 20:05:57 UTC vinc17
* Added comment:
I think that the attached patch should fix the problem in practice.

Now, this may not be a big problem since an invalid content-length means that the user has edited the mbox file with a text editor or there is a bug in some software.

Note: Obviously, MDA's should remove the "Content-Length:" header if invalid otherwise there is a risk of mail loss (for instance, if a second message is received and the content-length points just after this second message, so that the second message is regarded as part of the first message, which could be a spam).

--------------------------------------------------------------------------------
2018-02-13 20:28:49 UTC vinc17
* Added comment:
Moreover, I suppose that without ubsan, there could be an issue if due to a huge content-length and wrapping, {{{tmploc}}} would point to a "From " of a previous mail, so that the content-length would incorrectly be regarded as valid. I haven't tried, but if this is possible, this could be bad.

--------------------------------------------------------------------------------
2018-02-14 02:26:27 UTC josephbisch
* Added comment:
Regarding comment #3, I don't think it is very plausible in practice. There is the check for the content-length being > 0, so the content-length can't be so huge such that it wraps around by itself. So {{{loc}}} has to be large. So the overall filesize of the mbox has to be large. Maybe on 32-bit, but we are still talking about a huge mbox file.

Regarding comment #2, a website could purposefully provide a specially crafted mbox file, so it doesn't necessarily mean a bug is not a big problem because the file itself is invalid. Though this bug probably isn't a big problem for the reason I outlined in the first paragraph of this comment.

--------------------------------------------------------------------------------
2018-02-14 10:11:18 UTC vinc17
* _comment0 changed to 1518603108451303
* Added comment:
OK on both points.

The bug is now fixed in the master branch, assuming that the mailbox size is less than half the maximum signed offset, which is necessarily true on 64-bit in practice. And on 32-bit, the mbox file has to be huge (more than 1 GB), but this is controlled by the user and there may be other issues with such a huge mailbox on 32-bit (e.g. lack of address space or other limitation).

So I suppose that this fix is sufficient in practice.

--------------------------------------------------------------------------------
2018-02-14 16:05:54 UTC kevin8t8
* Added comment:
Thank you Vincent.  Also, thank you Joseph for reporting these issues.  I apologize for my terse comment - looking again I realize I sounded like an ass.  I saw the range checks just below, but didn't look closely enough that the size of and current location in the mbox matters too.

I'm indisposed this week, so haven't had any time to address those yet, but will soon.

Thanks again.

* resolution changed to fixed
* status changed to closed
