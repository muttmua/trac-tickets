Ticket:  1673
Status:  closed
Summary: Matching signed messages (via ~g) does not work for signed+encrypted eMails

Reporter: Thoralf Klein <masq@berlin.ccc.de>
Owner:    mutt-dev

Opened:       2003-10-20 18:16:37 UTC
Last Updated: 2005-10-07 14:55:33 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4+20030913
Severity: normal

-- Please type your report below this line

Hello,


when using hooks to match messages, mutt seems not to handle pgp/gpg signed
and/or encrypted messages correctly.


From the man page:
[pattern matching in muttrc]
>       ~g           PGP signed messages
>       ~G           PGP encrypted messages
>       ~k           message contains PGP key material


My test case:
> # GPG debug hooks in .muttrc
> message-hook ~g 'my_hdr X-Hook-sig: yes'  # 'PGP signed messages'
> message-hook !~g 'my_hdr X-Hook-sig: no'
> message-hook ~G 'my_hdr X-Hook-enc: yes'  # 'PGP encrypted messages'
> message-hook !~G 'my_hdr X-Hook-enc: no'


So, where's the problem?

A signed message should give something like:
>   sig: yes, enc: no  (and it actually does)

An encrypted message should give:
>   sig: no, enc: yes  (all right, too)

A signed *and* encryped message should give:
>   sig: yes, enc: yes
but it does give:
>   sig: no, enc: yes


Thanks for your attention!


-- System Information
System Version: Linux 2.4.22 i686 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.2/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
Thread model: posix
gcc version 3.3.2 20031005 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.22 (i686) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.14)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.3.rr.compressed.1
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.5.1.cd.edit_threads.9.2
patch-1.3.27.bse.xtitles.1
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.56d

--- Begin /home/masq/.muttrc
set pgp_autosign=yes
set pgp_replyencrypt=yes
set pgp_replysign=yes
set pgp_replysignencrypted=yes
set crypt_timestamp=yes
set pgp_show_unusable=no
set folder=~/Mail
set abort_unmodified=yes
set alias_file=~/.mail_aliases
set arrow_cursor
set attribution="On %d, %n wrote:"
set autoedit
set noconfirmappend
set copy=yes
set delete=yes
set fcc_clear=no
set hdr_format="%2C %Z %{%m/%d} %-20.20F (%4c) %s"
set help
set include
set indent_string="> "
set locale="C"
set nomark_old
set mail_check=10
set mbox=+mbox
unset metoo
set mime_forward
set move=yes
set pager_format="%S [%C/%T] %n (%l, %c) %s"
set pager_index_lines=10
set pager_stop
set postponed=+`date +%Y-%m`/postponed
set print_command="/home/masq/bin/muttprint "
set noprompt_after
set quote_regexp="^[- \t]*[a-zA-Z]*[]>|}%][]>|}]*"
set read_inc=53
unset recall
set record=+`date +%Y-%m`/outbox
set reply_to
set reply_regexp="^((re|aw)(.?[0-9].?)?:[ \t]*)+"
set reverse_alias
set reverse_name
set nosave_empty
set signature="~/.signature"
set sort=threads
set sort_aux=reverse-date-received
set sort_browser=reverse-date
set status_format="%v: %f (%s) [%M/%m] [N=%n,*=%t,D=%d,post=%p,new=%b] %> (%P)"
set tilde
set tmpdir=~/tmp
set nouse_domain
set use_from
set write_inc=20
set collapse_unread
set uncollapse_jump
macro index \cb |urlview\n	# simulate the old browse-url function
bind pager G bottom	# just like vi and less
--- End /home/masq/.muttrc


--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
ignore date delivered-to precedence errors-to in-reply-to user-agent
ignore x-loop x-sender x-mailer x-msmail-priority x-mimeole x-priority
ignore x-accept-language x-authentication-warning thread- priority
bind editor    "\e<delete>"    kill-word
bind editor    "\e<backspace>" kill-word
bind editor     <delete>  delete-char
unset use_domain
unset use_from
set sort=threads
unset write_bcc
unset bounce_delivered
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
set pipe_decode
macro generic <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro index   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro pager   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
open-hook	\\.gz$ "gzip -cd %f > %t"
close-hook	\\.gz$ "gzip -c %t > %f"
append-hook	\\.gz$ "gzip -c %t >> %f"
open-hook	\\.bz2$ "bzip2 -cd %f > %t"
close-hook	\\.bz2$ "bzip2 -c %t > %f"
append-hook	\\.bz2$ "bzip2 -c %t >> %f"
color normal	white black
color attachment brightyellow black
color hdrdefault cyan black
color indicator black cyan
color markers	brightred black
color quoted	green black
color signature cyan black
color status	brightgreen blue
color tilde	blue black
color tree	red black
set pgp_decode_command="gpg  --charset utf-8   --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet  --batch  --output - %f"
set pgp_verify_command="gpg   --status-fd=2 --no-verbose --quiet  --batch  --output - --verify %s %f"
set pgp_decrypt_command="gpg   --status-fd=2 --passphrase-fd 0 --no-verbose --quiet  --batch  --output - %f"
set pgp_sign_command="gpg    --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="gpg   --charset utf-8 --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap gpg  --charset utf-8    --batch  --quiet  --no-verbose --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap gpg  --charset utf-8 --passphrase-fd 0  --batch --quiet  --no-verbose  --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="gpg  --no-verbose --import %f"
set pgp_export_command="gpg   --no-verbose --export --armor %r"
set pgp_verify_key_command="gpg   --verbose --batch  --fingerprint --check-sigs %r"
set pgp_list_pubring_command="gpg   --no-verbose --batch --quiet   --with-colons --list-keys %r" 
set pgp_list_secring_command="gpg   --no-verbose --batch --quiet   --with-colons --list-secret-keys %r" 
set pgp_good_sign="^\\[GNUPG:\\] GOODSIG"
set smime_ca_location="~/.smime/ca-bundle.crt"
set smime_certificates="~/.smime/certificates"
set smime_keys="~/.smime/keys"
set smime_pk7out_command="openssl smime -verify -in %f -noverify -pk7out"
set smime_get_cert_command="openssl pkcs7 -print_certs -in %f"
set smime_get_signer_cert_command="openssl smime -verify -in %f -noverify -signer %c -out /dev/null"
set smime_get_cert_email_command="openssl x509 -in %f -noout -email"
set smime_import_cert_command="/usr/lib/mutt/smime_keys add_cert %f"
set smime_encrypt_command="openssl smime -encrypt -%a -outform DER -in %f %c"
set smime_sign_command="openssl smime -sign -signer %c -inkey %k -passin stdin -in %f -certfile %i -outform DER"
set smime_decrypt_command="openssl smime -decrypt -passin stdin -inform DER -in %f -inkey %k -recip %c"
set smime_verify_command="openssl smime -verify -inform DER -in %s %C -content %f"
set smime_verify_opaque_command="openssl smime -verify -inform DER -in %s %C"
--- End /etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-22 11:52:09 UTC rado
* Added comment:
{{{
Oops, my bad, confirmed "unexpectedness".

However, when looking at the header of such an
signed+encrypted eMail,
I don't see how mutt should know what the content of an
eMail is before
processing it, which it doesn't before viewing it.
}}}

* resolution changed to wontfix
* status changed to closed

--------------------------------------------------------------------------------
2005-09-22 11:52:10 UTC rado
* Added comment:
{{{
Subject changed, change-request.

The behaviour probably won't be changed (can't[? without performance issues]).
}}}

--------------------------------------------------------------------------------
2005-09-22 12:17:21 UTC Thoralf Klein <masq@berlin.ccc.de>
* Added comment:
{{{
Hey Rado,


thanks for answering my two-year-old bug report. :-)


On Wed, Sep 21, 2005 at 06:52:10PM +0200, Rado Smiljanic wrote:
> Synopsis: Matching signed messages (via ~g) does not work for signed+encr=
ypted eMails
>=20
> State-Changed-From-To: feedback->suspended
> State-Changed-By: rado
> State-Changed-When: Wed, 21 Sep 2005 18:52:09 +0200
> State-Changed-Why:
> Oops, my bad, confirmed "unexpectedness".
>=20
> However, when looking at the header of such an
> signed+encrypted eMail,
> I don't see how mutt should know what the content of an
> eMail is before
> processing it, which it doesn't before viewing it.

I agree with you, that it's impossible to say whether a encrypted message
contains a signature or not.  But in my case I was trying to reply to an
encrypted message.  This means, that mutt already did process that message
and sees both: A signed and encrypted message.

Thus, mutt could set the right headers.


> **** Comment added by rado on Wed, 21 Sep 2005 18:52:09 +0200 ****
>  Subject changed, change-request.
>=20
> The behaviour probably won't be changed (can't[? without performance issu=
es]).

Thanks for your patience!
}}}

--------------------------------------------------------------------------------
2005-09-23 09:31:50 UTC rado
* Added comment:
{{{
Hey Rado,
more input from mutt-dev
}}}

* status changed to new

--------------------------------------------------------------------------------
2005-09-23 09:31:51 UTC rado
* Added comment:
{{{
a) my INITIAL (now deleted) statment was not meant for your report,
   sorry for the mistake.
b) 2y for answer: well ... there were some issues with the old bts and lack of man-power. :)
   Some (new) people have cleaned up old reports (from spam)
and now we have a new bts which has its own new issues. :-/
}}}

--------------------------------------------------------------------------------
2005-10-08 08:55:33 UTC rado
* Added comment:
{{{
no input form mutt-dev, it will stay as it is.
}}}

* resolution changed to wontfix
* status changed to closed
