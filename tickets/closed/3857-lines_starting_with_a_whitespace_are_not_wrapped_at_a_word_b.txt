Ticket:  3857
Status:  closed
Summary: lines starting with a whitespace are not wrapped at a word boundary (though $smart_wrap is set)

Reporter: vinc17
Owner:    mutt-dev

Opened:       2016-07-29 14:59:25 UTC
Last Updated: 2017-11-15 22:59:10 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
The attached mailbox shows a message for which some lines are not wrapped at a word boundary, even though {{{$smart_wrap}}} is set. For instance, in a 75-column xterm, the bottom of the message is displayed as (the ↳ characters are the markers for wrapped lines):

{{{
         {  /* GMP_NUMB_BITS <= d < 2*GMP_NUMB_BITS */
+          /* warning: the most significant bit of sb might become the
↳least
+             significant bit of ap[0] below */
           sb = (d == GMP_NUMB_BITS) ? cp[0]
-            : (cp[1] << (2*GMP_NUMB_BITS-d)) | (cp[0] >> (d -
↳GMP_NUMB_BITS));
+            : (cp[1] << (2*GMP_NUMB_BITS - d)) | (cp[0] != 0);
           ap[0] = b0 - (cp[1] >> (d - GMP_NUMB_BITS)) - (sb != 0);
           ap[1] = b1 - (ap[0] >= b0); /* since cp[1] has its most signific
↳ant bit
                                          set, and d-GMP_NUMB_BITS < GMP_NU
↳MB_BITS,

__
}}}

For the first two wrapped lines, wrapping is done at a word boundary as expected, but this is not the case of the last two wrapped lines.

--------------------------------------------------------------------------------
2016-07-29 14:59:48 UTC vinc17
* Added attachment wrapped-lines.mbox
* Added comment:
mailbox

--------------------------------------------------------------------------------
2017-11-14 01:50:48 UTC vinc17
* Added comment:
The issue seems to occur on the lines that start with a whitespace (space or tab character).

* summary changed to lines starting with a whitespace are not wrapped at a word boundary (though $smart_wrap is set)

--------------------------------------------------------------------------------
2017-11-14 03:45:11 UTC kevin8t8
* Added comment:
Thank you for the additional information, Vincent.  This one is near the top of my list, but I've been struggling to get a few things done first.  I will look at it shortly.

--------------------------------------------------------------------------------
2017-11-15 03:41:53 UTC kevin8t8
* Added comment:
changeset:125076e0fdfa added the infinite loop protection inside the loop.

changeset:737102af74eb added the check for a leading space/tab and turned off $smart_wrap in that case.  This was to protect a folded header from being displayed funny.

--------------------------------------------------------------------------------
2017-11-15 04:02:19 UTC kevin8t8
* Added comment:
I'll think about it some more to be sure.  But it seems like both changes are trying to solve the problem of leading spaces followed by a really long word.

Tamo's commit was solving an infinite loop when the NSKIP flag was set.

Rocco's commit was solving a funky display issue for a folded header with a single long word in the fold.

It seems to me that Tamo's commit could just be generalized instead to anytime ch is 0.  I'm attaching an '''untested''' patch, which I'll test (and clean up, etc) some more tomorrow.

--------------------------------------------------------------------------------
2017-11-15 04:03:29 UTC kevin8t8
* Added attachment ticket-3857.patch
* Added comment:
untested patch.

--------------------------------------------------------------------------------
2017-11-15 09:49:03 UTC vinc17
* Added comment:
Not tested either, but BTW, the
{{{
      else
        buf_ptr = buf + cnt; /* a very long word... */
}}}
seems useless. Then the 2 {{{if}}}'s could be joined.

--------------------------------------------------------------------------------
2017-11-15 17:03:50 UTC kevin8t8
* Added comment:
Excellent catch, Vincent!  It looks useless to me too.  I'll break that out into a separate patch just to make the transformation clear in the future.

--------------------------------------------------------------------------------
2017-11-15 22:59:08 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"4240966d41fc3c17f41d2c2815970ae52c4ce8a1" 7182:4240966d41fc]:
{{{
#!CommitTicketReference repository="" revision="4240966d41fc3c17f41d2c2815970ae52c4ce8a1"
Fix $smart_wrap to not be disabled by whitespace-prefixed lines. (closes #3857)

changeset:737102af74eb fixed a folded header display issue with $smart_wrap
by disabling $smart_wrap for lines beginning with whitespace.

Unfortunately, this turns off smart wrapping in the body of an email
too, even when the line has other whitespace breaks in it.

An earlier commit, changeset:125076e0fdfa added an infinite loop fix
when MUTT_PAGER_NSKIP is set, by disabling the smart_wrap if the space
backtracking went to the beginning of the line.  That is, a line
beginning with 1+ whitespace followed by a single long word.

Extend this second commit by always disabling the smart_wrap in that
case, not just when MUTT_PAGER_NSKIP is set.  This also solves the
folded header issue without other side effects.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2017-11-15 22:59:10 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"2ff00d88bcf6b5afc30067369bd8931c8a42fe88" 7183:2ff00d88bcf6]:
{{{
#!CommitTicketReference repository="" revision="2ff00d88bcf6b5afc30067369bd8931c8a42fe88"
Remove useless else branch in the $smart_wrap code. (see #3857)

Thanks to Vincent Lefèvre for noticing the nested else was redundant,
since buf_ptr is already set to "buf + cnt" after the format_line()
call.

This allows us to merge the inner and outer if statement, resulting in
simpler code.
}}}
