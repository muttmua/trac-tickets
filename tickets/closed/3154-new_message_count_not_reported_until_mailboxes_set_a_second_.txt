Ticket:  3154
Status:  closed
Summary: new message count not reported until :mailboxes set a second time

Reporter: agriffis
Owner:    brendan

Opened:       2009-01-22 15:58:49 UTC
Last Updated: 2009-06-07 17:31:20 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
My muttrc builds up the mailboxes list as follows (--dumprc is part of
a wrapper, and I inserted blank lines for clarity):

  $ mutt-hp --dumprc | grep 'mailboxes\|set folder'
  
    macro index,pager y "<change-folder>?<toggle-mailboxes>" "show incoming mailboxes list"
  
    mailboxes imaps://mail.griffis1.net/INBOX
    set folder=imaps://mail.griffis1.net/INBOX
  
    mailboxes imaps://mail.hp.com/INBOX
    set folder=imaps://mail.hp.com/INBOX
    mailboxes +xen
    mailboxes +fedora
    mailboxes +libvir
    mailboxes +thincrust
    mailboxes +linux-ia64
  
    mailboxes imaps://mail.boston.redhat.com/INBOX
    set folder=imaps://mail.boston.redhat.com/INBOX
  
    set spoolfile=imaps://mail.hp.com/INBOX
    set folder=imaps://mail.hp.com/INBOX   # restore to default

So when I start mutt I'm at my HP inbox and mutt has a connection to the HP imap server.  This works well except that the first time I hit 'y' I see a list of empty boxes:

  1   0         imaps://mail.boston.redhat.com/INBOX
  2   0         imaps://mail.griffis1.net/INBOX
  3   0         imaps://mail.hp.com/INBOX
  4   0         imaps://mail.hp.com/INBOX/fedora
  5   0         imaps://mail.hp.com/INBOX/libvir
  6   0         imaps://mail.hp.com/INBOX/linux-ia64
  7   0         imaps://mail.hp.com/INBOX/thincrust
  8   0         imaps://mail.hp.com/INBOX/xen

So now the strange part: If I reset the mailboxes list then reenter this menu, it works right.  This is the same list of commands as shown above from my dumped muttrc:

  :unmailboxes *
  :mailboxes imaps://mail.griffis1.net/INBOX
  :set folder=imaps://mail.griffis1.net/INBOX
  :mailboxes imaps://mail.hp.com/INBOX
  :set folder=imaps://mail.hp.com/INBOX
  :mailboxes +xen
  :mailboxes +fedora
  :mailboxes +libvir
  :mailboxes +thincrust
  :mailboxes +linux-ia64
  :mailboxes imaps://mail.boston.redhat.com/INBOX
  :set folder=imaps://mail.boston.redhat.com/INBOX
  :set folder=imaps://mail.hp.com/INBOX

Now I hit 'q' to return to the index, then 'y' and I see:

  1   0         imaps://mail.boston.redhat.com/INBOX
  2   0         imaps://mail.griffis1.net/INBOX
  3   0         imaps://mail.hp.com/INBOX
  4   31801     imaps://mail.hp.com/INBOX.fedora
  5   15607     imaps://mail.hp.com/INBOX.libvir
  6   14456     imaps://mail.hp.com/INBOX.linux-ia64
  7   213       imaps://mail.hp.com/INBOX.thincrust
  8   35594     imaps://mail.hp.com/INBOX.xen

The zeroes on the first two entries are fine because mutt hasn't opened connections to the other two imap servers yet.

I'm attaching the -d3 output of the sequence listed above (mutt-hp, y, cut-n-paste commands, q, y, q, q).  I removed the parse_alias and snipped the FETCH lines.


--------------------------------------------------------------------------------
2009-01-22 15:59:13 UTC agriffis
* Added attachment .muttdebug0
* Added comment:
mutt-hp -d3 output

--------------------------------------------------------------------------------
2009-01-22 16:02:48 UTC agriffis
* Added comment:
Sorry, I apparently suck at wiki formatting.  Here is the description again, formatted correctly (I hope)

My muttrc builds up the mailboxes list as follows (--dumprc is part of a wrapper, and I inserted blank lines for clarity):

{{{
$ mutt-hp --dumprc | grep 'mailboxes\|set folder'

  macro index,pager y "<change-folder>?<toggle-mailboxes>" "show incoming mailboxes list"
  
  mailboxes imaps://mail.griffis1.net/INBOX
  set folder=imaps://mail.griffis1.net/INBOX
  
  mailboxes imaps://mail.hp.com/INBOX
  set folder=imaps://mail.hp.com/INBOX
  mailboxes +xen
  mailboxes +fedora
  mailboxes +libvir
  mailboxes +thincrust
  mailboxes +linux-ia64
  
  mailboxes imaps://mail.boston.redhat.com/INBOX
  set folder=imaps://mail.boston.redhat.com/INBOX
  
  set spoolfile=imaps://mail.hp.com/INBOX
  set folder=imaps://mail.hp.com/INBOX   # restore to default
}}}

So when I start mutt I'm at my HP inbox and mutt has a connection to the HP imap server.  This works well except that the first time I hit 'y' I see a list of empty boxes:

{{{
 1   0         imaps://mail.boston.redhat.com/INBOX
 2   0         imaps://mail.griffis1.net/INBOX
 3   0         imaps://mail.hp.com/INBOX
 4   0         imaps://mail.hp.com/INBOX/fedora
 5   0         imaps://mail.hp.com/INBOX/libvir
 6   0         imaps://mail.hp.com/INBOX/linux-ia64
 7   0         imaps://mail.hp.com/INBOX/thincrust
 8   0         imaps://mail.hp.com/INBOX/xen
}}}

So now the strange part: If I reset the mailboxes list then reenter this menu, it works right.  This is the same list of commands as shown above from my dumped muttrc:

{{{
:unmailboxes *
:mailboxes imaps://mail.griffis1.net/INBOX
:set folder=imaps://mail.griffis1.net/INBOX
:mailboxes imaps://mail.hp.com/INBOX
:set folder=imaps://mail.hp.com/INBOX
:mailboxes +xen
:mailboxes +fedora
:mailboxes +libvir
:mailboxes +thincrust
:mailboxes +linux-ia64
:mailboxes imaps://mail.boston.redhat.com/INBOX
:set folder=imaps://mail.boston.redhat.com/INBOX
:set folder=imaps://mail.hp.com/INBOX
}}}

Now I hit 'q' to return to the index, then 'y' and I see:

{{{
 1   0         imaps://mail.boston.redhat.com/INBOX
 2   0         imaps://mail.griffis1.net/INBOX
 3   0         imaps://mail.hp.com/INBOX
 4   31801     imaps://mail.hp.com/INBOX.fedora
 5   15607     imaps://mail.hp.com/INBOX.libvir
 6   14456     imaps://mail.hp.com/INBOX.linux-ia64
 7   213       imaps://mail.hp.com/INBOX.thincrust
 8   35594     imaps://mail.hp.com/INBOX.xen
}}}

The zeroes on the first two entries are fine because mutt hasn't opened connections to the other two imap servers yet.

I'm attaching the -d3 output of the sequence listed above (mutt-hp, y, cut-n-paste commands, q, y, q, q).  I removed the parse_alias and snipped the FETCH lines.


--------------------------------------------------------------------------------
2009-01-26 14:35:04 UTC brendan
* Added comment:
This is probably more fallout from me monkeying with imap_delim_chars and IMAP URL canonification. Can you bisect?

* component changed to IMAP
* milestone changed to 1.6
* owner changed to brendan

--------------------------------------------------------------------------------
2009-01-26 17:17:18 UTC Aron Griffis
* Added comment:
{{{
Well, to make something clear, mutt has always shown zero new
messages for my mailboxes.  It's only recently that I discovered
this trick of setting the mailboxes a second time to make the
new-message count show properly.

I went back to an older build I had from mid-last-year,
post-1.5.18 changeset d3ee9644765f

In that version the mailboxes *always* list zero new messages,
even after the unmailboxes/mailboxes sequence given above.

I'll do some bisection to determine when that trick started
working.  I don't think I'll find a rev where it ever worked
exactly right, though.
}}}

--------------------------------------------------------------------------------
2009-01-27 02:40:57 UTC Aron Griffis
* Added comment:
{{{
Brendan, I went back and checked both 1.5.17 and 1.5.18.  Neither
one shows message counts for me.  They've always been zero in my
mailboxes browser.

Bisection shows that the unmailboxes/mailboxes trick fixes the
problem starting with:
    changeset:   5663:73eb20f80066
    branch:      HEAD
    user:        Brendan Cully <brendan@kublai.com>
    date:        Sat Jan 10 21:44:41 2009 -0800
    summary:     Use known connection delimiter in imap_expand_path

So I think we have to look back at the .muttdebug0 I posted to
this bug.  grepping INBOX.fedora, for example, we see:

a0008 STATUS "INBOX.fedora" (UIDNEXT UIDVALIDITY UNSEEN RECENT)^M
42< * STATUS "INBOX.fedora" (RECENT 17045 UIDNEXT 61235 UIDVALIDITY 1152649375 UNSEEN 31801)
INBOX.fedora (UIDVALIDITY: 1152649375, UIDNEXT: 61235) 0 messages, 17045 recent, 31801 unseen
Found INBOX.fedora in buffy list (OV: 0 ON: 0 U: 31801)
42< * LIST (\HasNoChildren) "." "INBOX.fedora"
42< * LIST (\HasNoChildren) "." "INBOX.fedora-xen"
42< * LIST (\HasNoChildren) "." "INBOX.fedora-ia64"
42< * LIST (\HasNoChildren) "." "INBOX.fedora"
42< * LIST (\HasNoChildren) "." "INBOX.fedora-xen"
42< * LIST (\HasNoChildren) "." "INBOX.fedora-ia64"
42< * LIST (\HasNoChildren) "." "INBOX.fedora"
42< * LIST (\HasNoChildren) "." "INBOX.fedora-xen"
42< * LIST (\HasNoChildren) "." "INBOX.fedora-ia64"
a0019 STATUS "INBOX.fedora" (UIDNEXT UIDVALIDITY UNSEEN RECENT)^M
42< * STATUS "INBOX.fedora" (RECENT 17045 UIDNEXT 61235 UIDVALIDITY 1152649375 UNSEEN 31801)
INBOX.fedora (UIDVALIDITY: 1152649375, UIDNEXT: 61235) 0 messages, 17045 recent, 31801 unseen
Found INBOX.fedora in buffy list (OV: 1152649375 ON: 0 U: 31801)
42< * LIST (\HasNoChildren) "." "INBOX.fedora"
42< * LIST (\HasNoChildren) "." "INBOX.fedora-xen"
42< * LIST (\HasNoChildren) "." "INBOX.fedora-ia64"

The only thing that jumps out at me here is the difference
between these two lines:

Found INBOX.fedora in buffy list (OV: 0 ON: 0 U: 31801)
Found INBOX.fedora in buffy list (OV: 1152649375 ON: 0 U: 31801)
}}}

--------------------------------------------------------------------------------
2009-04-09 21:38:06 UTC agriffis
* Added comment:
FYI since I recently pulled mutt.hg, this hack no longer works for me at all.  My IMAP message counts are always zero.

--------------------------------------------------------------------------------
2009-06-07 17:26:09 UTC brendan
* status changed to accepted

--------------------------------------------------------------------------------
2009-06-07 17:31:20 UTC agriffis
* Added comment:
Brendan, this was fixed by http://dev.mutt.org/hg/mutt/rev/7bc332ddd8fc

* resolution changed to fixed
* status changed to closed
