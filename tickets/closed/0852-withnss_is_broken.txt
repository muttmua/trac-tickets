Ticket:  852
Status:  closed
Summary: --with-nss is broken

Reporter: md@Linux.IT (Marco d'Itri)
Owner:    mutt-dev

Opened:       2001-11-03 17:08:25 UTC
Last Updated: 2005-08-02 17:17:45 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.23-1
Severity: normal

Mutt crashes when I run "./mutt -f pops://md:xxxxxx@spock.linux.it/"

BTW, it needs -I/usr/include/mozilla -I/usr/include/mozilla/dbm
and -lfreebl -lswft to compile.

(gdb) where
#0  0x40171a80 in PR_NewNamedMonitor () from /usr/lib/libnspr4.so
#1  0x4016481d in _PR_InitLinker () from /usr/lib/libnspr4.so
#2  0x4016a396 in PR_VersionCheck () from /usr/lib/libnspr4.so
#3  0x4016ab04 in _PR_ImplicitInitialization () from /usr/lib/libnspr4.so
#4  0x401775b4 in PR_GetCurrentThread () from /usr/lib/libnspr4.so
#5  0x40169d60 in PR_GetError () from /usr/lib/libnspr4.so
#6  0x80ae06a in mutt_nss_error (call=0x811edd5 "NSS_Init")
    at mutt_ssl_nss.c:58
#7  0x80ae0c3 in mutt_nss_init () at mutt_ssl_nss.c:72
#8  0x80ae987 in mutt_nss_socket_setup (con=0x817d028) at mutt_ssl_nss.c:374
#9  0x80ad7e5 in mutt_conn_find (start=0x0, account=0xbfffecd8)
    at mutt_socket.c:256
#10 0x80aad0e in pop_open_mailbox (ctx=0x8156668) at pop.c:255
#11 0x8081923 in mx_open_mailbox (
    path=0xbffff8ec "pops://md:xxxxxx@spock.linux.it/", flags=0, pctx=0x0)
    at mx.c:724
#12 0x807a02f in main (argc=3, argv=0xbffffa54) at main.c:840



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2001-11-03 17:08:25 UTC md@Linux.IT (Marco d'Itri)
* Added comment:
{{{
Package: mutt
Version: 1.3.23-1
Severity: normal

Mutt crashes when I run "./mutt -f pops://md:xxxxxx@spock.linux.it/"

BTW, it needs -I/usr/include/mozilla -I/usr/include/mozilla/dbm
and -lfreebl -lswft to compile.

(gdb) where
#0  0x40171a80 in PR_NewNamedMonitor () from /usr/lib/libnspr4.so
#1  0x4016481d in _PR_InitLinker () from /usr/lib/libnspr4.so
#2  0x4016a396 in PR_VersionCheck () from /usr/lib/libnspr4.so
#3  0x4016ab04 in _PR_ImplicitInitialization () from /usr/lib/libnspr4.so
#4  0x401775b4 in PR_GetCurrentThread () from /usr/lib/libnspr4.so
#5  0x40169d60 in PR_GetError () from /usr/lib/libnspr4.so
#6  0x80ae06a in mutt_nss_error (call=0x811edd5 "NSS_Init")
   at mutt_ssl_nss.c:58
#7  0x80ae0c3 in mutt_nss_init () at mutt_ssl_nss.c:72
#8  0x80ae987 in mutt_nss_socket_setup (con=0x817d028) at mutt_ssl_nss.c:374
#9  0x80ad7e5 in mutt_conn_find (start=0x0, account=0xbfffecd8)
   at mutt_socket.c:256
#10 0x80aad0e in pop_open_mailbox (ctx=0x8156668) at pop.c:255
#11 0x8081923 in mx_open_mailbox (
   path=0xbffff8ec "pops://md:xxxxxx@spock.linux.it/", flags=0, pctx=0x0)
   at mx.c:724
#12 0x807a02f in main (argc=3, argv=0xbffffa54) at main.c:840
}}}

--------------------------------------------------------------------------------
2005-08-03 11:17:45 UTC brendan
* Added comment:
{{{
NSS support has been removed in favour of gnutls.
}}}

* resolution changed to fixed
* status changed to closed
