Ticket:  3607
Status:  closed
Summary: support plain text digests

Reporter: jidanni
Owner:    mutt-dev

Opened:       2012-12-12 01:44:52 UTC
Last Updated: 2012-12-12 02:34:28 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
All I know is gnus-summary-enter-digest-group can deal with

$ GET http://www.csl.sri.com/users/risko/risks.txt | procmail

results, whilst mutt's "v" cannot find the individual members.

--------------------------------------------------------------------------------
2012-12-12 02:22:25 UTC aaron@schrab.com
* Added comment:
{{{
At 01:44 -0000 12 Dec 2012, Mutt <fleas@mutt.org> wrote:

So actually, you're saying the procmail is better at this.  But for me, 
using that command results in just a single message in a mailbox.  To 
actually separate it into separate messages you'd need to use the 
formail program from the procmail suite.  You should be able to use mutt 
to read a mailbox produced in that manner as easily as you could use 
gnus, so I don't see how gnus is better about it.

Mutt has never claimed to support plain text digests, such as that.  The 
only digests which mutt supports are messages with the multipart/digest 
MIME type.
}}}

--------------------------------------------------------------------------------
2012-12-12 02:34:28 UTC jidanni
* Added comment:
Yes I am talking about plain text digests. I recall there even is an RFC about them. OK, glad to know. Closing... naaa, I'll leave it as a long tern enhancement, even though the kids don't use plain text digests much these days. Naa... closing anyway still.

* resolution changed to wontfix
* status changed to closed
* summary changed to support plain text digests
