Ticket:  2242
Status:  closed
Summary: Mutt 1.5.9i crashes when reading mail with unicode symbols.

Reporter: johnpol@2ka.mipt.ru
Owner:    mutt-dev

Opened:       2006-05-17 09:31:40 UTC
Last Updated: 2007-03-31 22:27:14 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Mutt crashes when reading attached e-mail.

Program received signal SIGSEGV, Segmentation fault.
0xa7f3b88e in PutAttrChar () from /usr/lib/libncursesw.so.5
(gdb) bt
#0  0xa7f3b88e in PutAttrChar () from /usr/lib/libncursesw.so.5
#1  0xa7f37b7f in EmitRange () from /usr/lib/libncursesw.so.5
#2  0xa7f37ca3 in PutRange () from /usr/lib/libncursesw.so.5
#3  0xa7f38d47 in TransformLine () from /usr/lib/libncursesw.so.5
#4  0xa7f37f98 in doupdate () from /usr/lib/libncursesw.so.5
#5  0xa7f32bb7 in wrefresh () from /usr/lib/libncursesw.so.5
#6  0x0805ef82 in mutt_refresh () at curs_lib.c:62
#7  0x080853f2 in mutt_pager (banner=0x0, fname=0xa7f5aba0 "&#65533;J\004", flags=66, extra=0xafe7d130) at pager.c:1754
#8  0x0805792e in mutt_display_message (cur=0x8141908) at commands.c:211
#9  0x08061b16 in mutt_index_menu () at curs_main.c:1144
#10 0x0807802a in main (argc=135716808, argv=0xafe7e514) at main.c:934
#11 0xa7c751c4 in __libc_start_main () from /lib/libc.so.6
(gdb)
>How-To-Repeat:
mutt -f mbox

hit enter to read the mail
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-05-17 17:08:32 UTC Thomas Dickey <dickey@his.com>
* Added comment:
{{{
yOn Wed, 17 May 2006, johnpol@2ka.mipt.ru wrote:

>> Number:         2242
>> Notify-List:
>> Category:       mutt
>> Synopsis:       Mutt 1.5.9i crashes when reading mail with unicode symbols.
>> Confidential:   no
>> Severity:       serious
>> Priority:       medium
>> Responsible:    mutt-dev
>> State:          open
>> Keywords:
>> Class:          sw-bug
>> Submitter-Id:   net
>> Arrival-Date:   Wed May 17 10:31:40 +0200 2006
>> Originator:     Evgeniy Polyakov
>> Release:        Mutt 1.5.9i
>> Organization:
>> Environment:
> $ mutt -v
> Mutt 1.5.9i (2005-03-13)
> Copyright (C) 1996-2002 Michael R. Elkins and others.
> Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
> Mutt is free software, and you are welcome to redistribute it
> under certain conditions; type `mutt -vv' for details.
>
> System: Linux 2.6.16.1-2ka (i686) [using ncurses 5.2]

The current version of ncurses is 5.5 (last October), and 5.2 is iirc the 
first version to support Unicode.  This is a problem with your environment 
rather than a bug in any recent mutt or ncurses.

-- 
Thomas E. Dickey
http://invisible-island.net
ftp://invisible-island.net
}}}

--------------------------------------------------------------------------------
2006-05-17 23:53:47 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
{{{
Hi,

* johnpol@2ka.mipt.ru [06-05-17 10:31:40 +0200] wrote:

>System: Linux 2.6.16.1-2ka (i686) [using ncurses 5.2]
[...]
>+ENABLE_NLS  +LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR

>>Description:
>Mutt crashes when reading attached e-mail.

>Program received signal SIGSEGV, Segmentation fault.
>0xa7f3b88e in PutAttrChar () from /usr/lib/libncursesw.so.5
>(gdb) bt
>#0  0xa7f3b88e in PutAttrChar () from /usr/lib/libncursesw.so.5
>#1  0xa7f37b7f in EmitRange () from /usr/lib/libncursesw.so.5
>#2  0xa7f37ca3 in PutRange () from /usr/lib/libncursesw.so.5
>#3  0xa7f38d47 in TransformLine () from /usr/lib/libncursesw.so.5
>#4  0xa7f37f98 in doupdate () from /usr/lib/libncursesw.so.5
>#5  0xa7f32bb7 in wrefresh () from /usr/lib/libncursesw.so.5
>#6  0x0805ef82 in mutt_refresh () at curs_lib.c:62
>#7  0x080853f2 in mutt_pager (banner=0x0, fname=0xa7f5aba0 "&#65533;J\004", flags=66, extra=0xafe7d130) at pager.c:1754
>#8  0x0805792e in mutt_display_message (cur=0x8141908) at commands.c:211
>#9  0x08061b16 in mutt_index_menu () at curs_main.c:1144
>#10 0x0807802a in main (argc=135716808, argv=0xafe7e514) at main.c:934
>#11 0xa7c751c4 in __libc_start_main () from /lib/libc.so.6

I cannot reproduce this with CVS HEAD nor 1.5.8 nor 1.4.2.1.

I have two ideas: first ncurses 5.2 is pretty old and as the crash 
happens after calling refresh() inside ncurses it could be its fault and 
not mutt's. Can you try updating the cursing library?

Second: how do you build? You have +LOCALES_HACK enabled which shouldn't 
be necessary on your system I guess. Maybe not enabling it helps?

But still the value for fname when calling mutt_pager() looks very 
strange to me...

   bye, Rocco
-- 
:wq!
}}}

--------------------------------------------------------------------------------
2007-03-27 18:07:29 UTC 
* Added attachment mbox
* Added comment:
mbox

--------------------------------------------------------------------------------
2007-03-31 22:27:14 UTC brendan
* Updated description:
{{{
Mutt crashes when reading attached e-mail.

Program received signal SIGSEGV, Segmentation fault.
0xa7f3b88e in PutAttrChar () from /usr/lib/libncursesw.so.5
(gdb) bt
#0  0xa7f3b88e in PutAttrChar () from /usr/lib/libncursesw.so.5
#1  0xa7f37b7f in EmitRange () from /usr/lib/libncursesw.so.5
#2  0xa7f37ca3 in PutRange () from /usr/lib/libncursesw.so.5
#3  0xa7f38d47 in TransformLine () from /usr/lib/libncursesw.so.5
#4  0xa7f37f98 in doupdate () from /usr/lib/libncursesw.so.5
#5  0xa7f32bb7 in wrefresh () from /usr/lib/libncursesw.so.5
#6  0x0805ef82 in mutt_refresh () at curs_lib.c:62
#7  0x080853f2 in mutt_pager (banner=0x0, fname=0xa7f5aba0 "&#65533;J\004", flags=66, extra=0xafe7d130) at pager.c:1754
#8  0x0805792e in mutt_display_message (cur=0x8141908) at commands.c:211
#9  0x08061b16 in mutt_index_menu () at curs_main.c:1144
#10 0x0807802a in main (argc=135716808, argv=0xafe7e514) at main.c:934
#11 0xa7c751c4 in __libc_start_main () from /lib/libc.so.6
(gdb)
>How-To-Repeat:
mutt -f mbox

hit enter to read the mail
>Fix:
Unknown
}}}
* resolution changed to worksforme
* status changed to closed
