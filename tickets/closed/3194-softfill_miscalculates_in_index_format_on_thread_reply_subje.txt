Ticket:  3194
Status:  closed
Summary: soft-fill miscalculates in index_format on thread reply subjects

Reporter: slovichon
Owner:    mutt-dev

Opened:       2009-02-26 16:40:58 UTC
Last Updated: 2009-12-29 14:13:15 UTC

Priority:  major
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi, I'm using the following in .muttrc on mutt 1.5.18:

  set index_format="%Z %s%*  %-16.16F %4c %{%b %d %y}"

I know 1.5.18 isn't the newest release but I did not see mention of any fix pertaining to this issue in the changelog to 1.5.19.  If this has been solved, please forgive my ignorance.

Anyway, this index_format works great for the first subject in a thread, but it seems to miscalculate by the indent width on replies to the thread.  An example:

N   [Full-disclosure] Buffer Overflow in dnsmap 0.22 - DNS Network Mappe srl              2.3K Feb 25 09
N   `->                                                                  Jason Starks     3.7K Feb 25 09
N     |->[Full-disclosure] Re : Buffer Overflow in dnsmap 0.22 - DNS Network  Pete Licoln      1.4K Feb 
N     `->Re: [Full-disclosure] Buffer Overflow in dnsmap 0.22 - DNS Network M srl              5.5K Feb 
N       |->                                                              Pete Licoln      0.2K Feb 25 09

Here, the amount shifted over where "Author" starts appears to be the same amount of the indent from the ASCII characters to show thread structure.  Have not time to craft a patch, sorry.

--------------------------------------------------------------------------------
2009-02-26 16:43:27 UTC slovichon
* Added comment:
Sorry, didn't use the WikiFormatting, here is the example with that:

{{{
N   [Full-disclosure] Buffer Overflow in dnsmap 0.22 - DNS Network Mappe srl              2.3K Feb 25 09
N   `->                                                                  Jason Starks     3.7K Feb 25 09
N     |->[Full-disclosure] Re : Buffer Overflow in dnsmap 0.22 - DNS Network  Pete Licoln      1.4K Feb 
N     `->Re: [Full-disclosure] Buffer Overflow in dnsmap 0.22 - DNS Network M srl              5.5K Feb 
N       |->                                                              Pete Licoln      0.2K Feb 25 09
N       | `->                                                            srl              1.3K Feb 25 09
N       `->                                                              Jason Starks     6.4K Feb 25 09
}}}

--------------------------------------------------------------------------------
2009-02-26 16:58:00 UTC dgc
* cc changed to dgc+mutt-bugs@uchicago.edu
* Added comment:
The soft-fill feature regressed in [9414b9dd36db], addressing ticket #3035.  I tried to fix it in [1de934f1d618] but the patch was incomplete. (It solved a few test cases but I later realized that I had a lot of messages it didn't work for.)
Haven't had time to spend on it since, but I thought I'd add the historical context.

--------------------------------------------------------------------------------
2009-02-27 11:06:09 UTC slovichon
* Added comment:
This patch seems to address the problem for me but I have not tested it under a wide variety of circumstances; someone more familiar with the code will need to determine if it's too intrusive.  I stole the idea from print_enriched_string().

{{{
--- protos.h.orig	Fri Feb 27 05:56:55 2009
+++ protos.h	Fri Feb 27 05:56:55 2009
@@ -357,7 +357,7 @@ int mutt_search_command (int, int);
 int mutt_smtp_send (const ADDRESS *, const ADDRESS *, const ADDRESS *,
                     const ADDRESS *, const char *, int);
 #endif
-int mutt_wstr_trunc (const char *, size_t, size_t, size_t *);
+int mutt_wstr_trunc (const char *, size_t, size_t, size_t *, int);
 int mutt_charlen (const char *s, int *);
 int mutt_strwidth (const char *);
 int mutt_compose_menu (HEADER *, char *, size_t, HEADER *);
--- muttlib.c.orig	Fri Feb 27 05:45:27 2009
+++ muttlib.c	Fri Feb 27 05:59:58 2009
@@ -1236,13 +1236,13 @@ void mutt_FormatString (char *dest,		/* output buffer 
 	    /* \0-terminate dest for length computation in mutt_wstr_trunc() */
 	    *wptr = 0;
 	    /* make sure right part is at most as wide as display */
-	    len = mutt_wstr_trunc (buf, destlen, COLS, &wid);
+	    len = mutt_wstr_trunc (buf, destlen, COLS, &wid, 0);
 	    /* truncate left so that right part fits completely in */
-	    wlen = mutt_wstr_trunc (dest, destlen - len, col + pad, &col);
+	    wlen = mutt_wstr_trunc (dest, destlen - len, col + pad, &col, 1);
 	    wptr = dest + wlen;
 	  }
 	  if (len + wlen > destlen)
-	    len = mutt_wstr_trunc (buf, destlen - wlen, COLS - col, NULL);
+	    len = mutt_wstr_trunc (buf, destlen - wlen, COLS - col, NULL, 0);
 	  memcpy (wptr, buf, len);
 	  wptr += len;
 	  wlen += len;
@@ -1281,13 +1281,13 @@ void mutt_FormatString (char *dest,		/* output buffer 
 	    /* \0-terminate dest for length computation in mutt_wstr_trunc() */
 	    *wptr = 0;
 	    /* make sure right part is at most as wide as display */
-	    len = mutt_wstr_trunc (buf, destlen, COLS, &wid);
+	    len = mutt_wstr_trunc (buf, destlen, COLS, &wid, 0);
 	    /* truncate left so that right part fits completely in */
-	    wlen = mutt_wstr_trunc (dest, destlen - len, col + pad, &col);
+	    wlen = mutt_wstr_trunc (dest, destlen - len, col + pad, &col, 1);
 	    wptr = dest + wlen;
 	  }
 	  if (len + wlen > destlen)
-	    len = mutt_wstr_trunc (buf, destlen - wlen, COLS - SidebarWidth - col, NULL);
+	    len = mutt_wstr_trunc (buf, destlen - wlen, COLS - SidebarWidth - col, NULL, 0);
 	  memcpy (wptr, buf, len);
 	  wptr += len;
 	  wlen += len;
@@ -1351,7 +1351,7 @@ void mutt_FormatString (char *dest,		/* output buffer 
 	}
 	
 	if ((len = mutt_strlen (buf)) + wlen > destlen)
-	  len = mutt_wstr_trunc (buf, destlen - wlen, COLS - col, NULL);
+	  len = mutt_wstr_trunc (buf, destlen - wlen, COLS - col, NULL, 0);
 
 	memcpy (wptr, buf, len);
 	wptr += len;
--- curs_lib.c.orig	Fri Feb 27 05:55:45 2009
+++ curs_lib.c	Fri Feb 27 05:56:35 2009
@@ -865,7 +865,7 @@ void mutt_paddstr (int n, const char *s)
 
 /* See how many bytes to copy from string so its at most maxlen bytes
  * long and maxwid columns wide */
-int mutt_wstr_trunc (const char *src, size_t maxlen, size_t maxwid, size_t *width)
+int mutt_wstr_trunc (const char *src, size_t maxlen, size_t maxwid, size_t *width, int arboreal)
 {
   wchar_t wc;
   int w = 0, l = 0, cl;
@@ -882,6 +882,8 @@ int mutt_wstr_trunc (const char *src, size_t maxlen, s
   {
     if (cl == (size_t)(-1) || cl == (size_t)(-2))
       cw = cl = 1;
+    else if (arboreal) /* hack */
+      cw = 1;
     else
       cw = wcwidth (wc);
     if (cl + l > maxlen || cw + w > maxwid)

}}}

--------------------------------------------------------------------------------
2009-02-27 18:52:56 UTC slovichon
* Added comment:
I realize this is a bad patch, but I thought I'd provide it as a possible starting point for someone more seriously investigating this problem.  Cheers.

--------------------------------------------------------------------------------
2009-06-30 14:17:23 UTC pdmef
* component changed to display

--------------------------------------------------------------------------------
2009-12-29 14:13:15 UTC brendan
* Added comment:
Fixed in [31881f38ca1e]

* resolution changed to fixed
* status changed to closed
