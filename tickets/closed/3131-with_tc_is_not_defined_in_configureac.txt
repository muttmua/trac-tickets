Ticket:  3131
Status:  closed
Summary: with_tc is not defined in configure.ac

Reporter: tamo
Owner:    mutt-dev

Opened:       2008-10-23 05:15:32 UTC
Last Updated: 2008-11-16 04:27:02 UTC

Priority:  major
Component: build
Keywords:  

--------------------------------------------------------------------------------
Description:
"configure --with-tokyocabinet=/home/tamo/tcb" doesn't work
because configure.ac uses $with_tc instead of $with_tokyocabinet.

Maybe you forgot to declare with_tc like

 AC_ARG_WITH(tokyocabinet, AC_HELP_STRING([--without-tokyocabinet],
   [Don't use tokyocabinet even if it is available]),with_tc=$withval)


--------------------------------------------------------------------------------
2008-11-16 04:27:02 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [8087be7178cd]) Use with_tokyocabinet exclusively (with_tc is undefined).
Closes #3131.

* resolution changed to fixed
* status changed to closed
