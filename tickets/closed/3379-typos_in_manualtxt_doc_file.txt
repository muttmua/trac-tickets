Ticket:  3379
Status:  closed
Summary: typos in manual.txt doc file

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2010-02-07 01:25:21 UTC
Last Updated: 2010-02-11 07:31:41 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/547739

It seems that there are some typos in manual.xml.head, the attached patch should fix the problem

--------------------------------------------------------------------------------
2010-02-07 01:26:17 UTC antonio@dyne.org
* Added attachment 547739-manual-typos.patch

--------------------------------------------------------------------------------
2010-02-11 07:31:41 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [b7ec848af36b]) Fix a typo (closes #3379)

* resolution changed to fixed
* status changed to closed
