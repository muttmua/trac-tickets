Ticket:  3986
Status:  closed
Summary: send-hook for Cc and Bcc executed too late

Reporter: alahouze
Owner:    mutt-dev

Opened:       2018-01-23 08:30:19 UTC
Last Updated: 2018-01-25 21:49:41 UTC

Priority:  major
Component: mutt
Keywords:  send-hook my_hdr

--------------------------------------------------------------------------------
Description:
Hi,

I have a hook to initialize Cc on new mails only:

{{{ send-hook !~Q 'my_hdr Cc: Contact Example <contact@example.com>' }}}


When I create a new mail for the first time, Cc header is not initialized.

When I create a second new mail, it is.

I also have a folder hook to reset send hook (I have a multi account configuration, each one in a separate folder). Let's say account1 for the one I want Cc on new mails and account2 for the other one.

1. I make a new mail on account1: Cc is not initialized but should be
2. I make another new mail on account1: Cc is initialized
3. Then I make a mail on account2: Cc is initialized but should not be
4. Finally I make another new mail on account2: Cc is not initialized

It seems the hook is run after the mail edition.

I am using neomutt, so I discussed about it with flatcap on #neomutt@freenode who confirmed the bug for Cc and Bcc headers, and confirmed it on upstream mutt too and so he asked me to open a new ticket here, so here it is.

--------------------------------------------------------------------------------
2018-01-23 15:50:38 UTC kevin8t8
* Added comment:
Please see https://muttmua.gitlab.io/mutt/manual-dev.html#send-hook the second note:

  send-hook's are only executed once after getting the initial list of recipients. Adding a recipient after replying or editing the message will not cause any send-hook to be executed, similarly if $autoedit is set (as then the initial list of recipients is empty). Also note that my_hdr commands which modify recipient headers, or the message's subject, don't have any effect on the current message when executed from a send-hook.


--------------------------------------------------------------------------------
2018-01-23 17:52:44 UTC kevin8t8
* Added comment:
Just to clarify my previous comment before I close this ticket.  The send-hook's purpose is to change settings based on the recipients of a message, not to set such recipients.

If you need suggestions on how to make your desired changes, I would advise emailing mutt-users.  They are a creative bunch, and perhaps better informed than whence you were told to report this as a bug.

One quick suggestion would be to try making the changes in a macro instead:
{{{
# Override 'm' to set a Cc header
macro index,pager m "<enter-command>my_hdr Cc: Contact Example <contact@example.com><enter><mail>"
# Clean up the cc header so it doesn't appear for replies
send-hook . unmy_hdr cc
}}}

You could override 'm' as the above does, or use another key sequence just for those cases you want the cc.  Again, mutt-users may have better ideas.

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2018-01-25 21:49:41 UTC alahouze
* Added comment:
Actually, the macro must be on "compose", and I added a `send-hook '!~Q' "push m"` too, which initializes the headers like I want when I create a new mail.
