Ticket:  1840
Status:  closed
Summary: full-header edit shows "illegal" header like "Return-Path" first time but drops it later

Reporter: Lukasz Stelmach <steelman@post.pl>
Owner:    mutt-dev

Opened:       2004-03-28 06:05:40 UTC
Last Updated: 2009-06-11 16:35:17 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.1i
Severity: normal

-- Please type your report below this line

When I edit mail with headers (e.g. "E" in compose menu) Return-Path header 
appears only the first time. When i save edited-with-headers message
and edit-it-with-headers again this header is not there. Since
I use dialup connection and i don't have globally available dns
name for my system i *need* to use this header to tell qmail what
to say to another MTA with MAIL FROM command.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-slackware-linux/3.2.3/specs
Configured with: ../gcc-3.2.3/configure --prefix=/usr --enable-shared --enable-threads=posix --enable-__cxa_atexit --disable-checking --with-gnu-ld --verbose --target=i486-slackware-linux --host=i486-slackware-linux
Thread model: posix
gcc version 3.2.3

- CFLAGS
-Wall -pedantic -O2 -march=i386 -mcpu=i686

-- Mutt Version Information

Mutt 1.4.1i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.24 (i686) [using ncurses 5.3]
Parametry kompilacji:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  +LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc/mutt"
EXECSHELL="/bin/sh"
-MIXMASTER
Aby powiadomiæ autorów, proszê pisaæ na <mutt-dev@mutt.org>.
Aby zg³osiæ b³±d u¿yj programu flea(1).



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-03-28 06:05:40 UTC Lukasz Stelmach <steelman@post.pl>
* Added comment:
{{{
Package: mutt
Version: 1.4.1i
Severity: normal

-- Please type your report below this line

When I edit mail with headers (e.g. "E" in compose menu) Return-Path header 
appears only the first time. When i save edited-with-headers message
and edit-it-with-headers again this header is not there. Since
I use dialup connection and i don't have globally available dns
name for my system i *need* to use this header to tell qmail what
to say to another MTA with MAIL FROM command.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-slackware-linux/3.2.3/specs
Configured with: ../gcc-3.2.3/configure --prefix=/usr --enable-shared --enable-threads=posix --enable-__cxa_atexit --disable-checking --with-gnu-ld --verbose --target=i486-slackware-linux --host=i486-slackware-linux
Thread model: posix
gcc version 3.2.3

- CFLAGS
-Wall -pedantic -O2 -march=i386 -mcpu=i686

-- Mutt Version Information

Mutt 1.4.1i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.24 (i686) [using ncurses 5.3]
Parametry kompilacji:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  +LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc/mutt"
EXECSHELL="/bin/sh"
-MIXMASTER
Aby powiadomiæ autorów, proszê pisaæ na <mutt-dev@mutt.org>.
Aby zg³osiæ b³±d u¿yj programu flea(1).
}}}

--------------------------------------------------------------------------------
2004-03-30 23:26:54 UTC David Yitzchak Cohen <lists+mutt_bugs@bigfatdave.com>
* Added comment:
{{{
On Tue, Mar 30, 2004 at 06:15:33AM EST, Alain Bench wrote:
>  On Saturday, March 27, 2004 at 1:05:40 PM +0100, Lukasz Stelmach wrote:

> > When I edit mail with headers (e.g. "E" in compose menu) Return-Path
> > header appears only the first time. When i save edited-with-headers
> > message and edit-it-with-headers again this header is not there.
> 
>     Not a bug, but a feature: The sender is not supposed to set a
> Return-Path header. Where applicable, the final delivering MTA sets it
> containing the enveloppe MAIL FROM.

Question: Why is it there the first time, then?

- Dave [who's slightly confused here]

-- 
Uncle Cosmo, why do they call this a word processor?
It's simple, Skyler.  You've seen what food processors do to food, right?

Please visit this link:
http://rotter.net/israel
}}}

--------------------------------------------------------------------------------
2004-03-31 07:15:33 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Lukasz, thanks for the report.

On Saturday, March 27, 2004 at 1:05:40 PM +0100, Lukasz Stelmach wrote:

> When I edit mail with headers (e.g. "E" in compose menu) Return-Path
> header appears only the first time. When i save edited-with-headers
> message and edit-it-with-headers again this header is not there.

   Not a bug, but a feature: The sender is not supposed to set a
Return-Path header. Where applicable, the final delivering MTA sets it
containing the enveloppe MAIL FROM.


> Since I use dialup connection and i don't have globally available dns
> name for my system i *need* to use this header to tell qmail what to
> say to another MTA with MAIL FROM command.

   Use the -f option in $sendmail, or better set $envelope_from.


Bye!	Alain.
-- 
I read mutt-dev and Bug Tracker: No need to CC me, thank you.
}}}

--------------------------------------------------------------------------------
2004-04-01 09:15:09 UTC David Yitzchak Cohen <lists+mutt_bugs@bigfatdave.com>
* Added comment:
{{{
On Wed, Mar 31, 2004 at 12:40:00PM EST, Lukasz Stelmach wrote:
>  Byla godzina 12:26:54 w Tuesday 30 March, gdy do autobusu wsiadl kanar
>  i wrzasnal:"David Yitzchak Cohen!!! Bilecik do kontroli!!!" A on(a) na to:
> > On Tue, Mar 30, 2004 at 06:15:33AM EST, Alain Bench wrote:
> >>  On Saturday, March 27, 2004 at 1:05:40 PM +0100, Lukasz Stelmach wrote:

> >>> When I edit mail with headers (e.g. "E" in compose menu) Return-Path
> >>> header appears only the first time. When i save edited-with-headers
> >>> message and edit-it-with-headers again this header is not there.
> >>     Not a bug, but a feature: The sender is not supposed to set a
> >> Return-Path header. Where applicable, the final delivering MTA sets it
> >> containing the enveloppe MAIL FROM.
> > Question: Why is it there the first time, then?
> 
> I put it there with my_hdr. *Probably* I have done it this way
> because there was no envelope_from or i've had missed it. Or for
> some, unthinkable now, reason i've had wanted return-path to be
> different than from.

In that case, the interesting question becomes:
Why doesn't Mutt notice a bad header the first time you edit your message?
That shouldn't happen, AFAICT. . .

> Czym sie cieplo David...

whatever that means ;-P

- Dave

-- 
Uncle Cosmo, why do they call this a word processor?
It's simple, Skyler.  You've seen what food processors do to food, right?

Please visit this link:
http://rotter.net/israel
}}}

--------------------------------------------------------------------------------
2004-04-01 13:40:00 UTC Lukasz Stelmach <steelman@post.pl>
* Added comment:
{{{
Byla godzina 12:26:54 w Tuesday 30 March, gdy do autobusu wsiadl kanar
i wrzasnal:"David Yitzchak Cohen!!! Bilecik do kontroli!!!" A on(a) na to:

> On Tue, Mar 30, 2004 at 06:15:33AM EST, Alain Bench wrote:
>>  On Saturday, March 27, 2004 at 1:05:40 PM +0100, Lukasz Stelmach wrote:
>>> When I edit mail with headers (e.g. "E" in compose menu) Return-Path
>>> header appears only the first time. When i save edited-with-headers
>>> message and edit-it-with-headers again this header is not there.
>>     Not a bug, but a feature: The sender is not supposed to set a
>> Return-Path header. Where applicable, the final delivering MTA sets it
>> containing the enveloppe MAIL FROM.
> Question: Why is it there the first time, then?

I put it there with my_hdr. *Probably* I have done it this way
because there was no envelope_from or i've had missed it. Or for
some, unthinkable now, reason i've had wanted return-path to be
different than from.

Czym sie cieplo David...
-- 
|/       |_,  _   .-  --,  Ju¿ z ka¿dej strony pe³zn±, potworne ¿±dze
|__ |_|. | \ |_|. ._' /_.         Bêdê uprawia³ nierz±d, za pieni±ze
}}}

--------------------------------------------------------------------------------
2005-08-27 10:54:02 UTC rado
* Added comment:
{{{
detailed subject
}}}

--------------------------------------------------------------------------------
2009-06-11 16:35:17 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [fc60d44a5b22]) Don't allow setting Return-Path: header via my_hdr

Mutt has use_envelope_from/envelope_from_address for that purpose.
Closes #1840.

* resolution changed to fixed
* status changed to closed
