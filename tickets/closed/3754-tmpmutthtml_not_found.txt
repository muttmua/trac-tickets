Ticket:  3754
Status:  closed
Summary: /tmp/mutt.html not found

Reporter: jidanni
Owner:    mutt-dev

Opened:       2015-05-03 04:15:31 UTC
Last Updated: 2017-10-18 11:18:19 UTC

Priority:  trivial
Component: browser
Keywords:  

--------------------------------------------------------------------------------
Description:
Looking at
{{{
 I     1 <no description>                                                                            [multipa/alternativ, 7bitC?, >, 9.9K]
 I     2 ├─><no description>                                                                          [text/plain, quotedC?, utf-8>, 1.3K]
 I     3 └─><no description>                                                                           [text/html, quotedC?, utf-8>, 8.3K]
}}}
with the cursor on line 3, I hit "v",
which calls $BROWSER file:///tmp/mutt.html
which is not found by the browser.

--------------------------------------------------------------------------------
2015-05-03 04:18:43 UTC jidanni
* Added comment:
Oops I meant I hit RET (ENTER), not v.

--------------------------------------------------------------------------------
2015-05-03 04:41:01 UTC jidanni
* Added comment:
I bet it is some kind of race condition, where you don't wait long enough for the browser to find /tmp/mutt.html before doing
{{{
unlink("/tmp/mutt.html")                = 0
}}}
which in fact strace(1) shows happend twice.

Anyway, I tried RET on several messages' attachments and only once was the file found. A second  test run could not find the file, hence must be a race condition.

--------------------------------------------------------------------------------
2015-05-03 15:02:16 UTC kevin8t8
* Added comment:
Please take a look at "How to view HTML attachments in a new tab in firefox:" in http://dev.mutt.org/trac/wiki/MuttFaq/Attachment and see if that answers your question.  Let us know if that helps.

--------------------------------------------------------------------------------
2015-05-04 03:01:55 UTC jidanni
* _comment0 changed to 1430708601764639
* Added comment:
Indeed http://dev.mutt.org/trac/wiki/MuttFaq/Attachment mentions a race
condition.

1. It is not only present in firefox, so perhaps change "How to view HTML attachments in a new tab in firefox:".

2. Why not just remove all the temp files when mutt exits? That will eliminate all race conditions. Otherwise since many people's computers are slower than the developers', bugs will continue to be reported.

--------------------------------------------------------------------------------
2017-10-18 11:18:19 UTC kevin8t8
* Added comment:
Original reporter is apparently ignorant of what a wiki is, and my computer is about 9 years old, so I'm not sure what he is suggesting in his second point.  Closing this ticket out.

* resolution changed to wontfix
* status changed to closed
