Ticket:  3138
Status:  closed
Summary: IMAP: leaf folders in LSUB are not recognized correctly (was: problem with backslash as folder separator)

Reporter: uspoerlein
Owner:    brendan

Opened:       2008-11-26 11:13:13 UTC
Last Updated: 2009-01-06 00:14:20 UTC

Priority:  minor
Component: IMAP
Keywords:  imap folder separator backslash leaf

--------------------------------------------------------------------------------
Description:
Hi,

I'm using mutt 1.5.17 on Ubuntu versus a Lotus Domino 6.5.6 (don't ask). While Thunderbird seems to cope very well with the IMAP quirks, mutt is interpreting the backslashes used to separate the folder structure one time too often.

I subscribed to several folders, adn the mutt view is displaying something like:

{{{

 1     IMAP                                  Drafts
 2     IMAP                                  Gemeinsame Ordner\

}}}

This is ok and true, \ is the path separator. I can even browse into the folder, but not open any subfolders, because the \ at the end is not take verbatim, but interpreted. A tcpdump of the IMAP session shows the problem very nicely:

{{{

a0004 LSUB "" "*"
* LIST (\Noselect) "\\" ""
* LSUB () "\\" Drafts
* LSUB () "\\" "Gemeinsame Ordner"
...
* LSUB () "\\" {35}
Gemeinsame Ordner\Mailpool IT\Trash
* LSUB () "\\" {35}
Gemeinsame Ordner\Mailpool IT\Inbox
a0004 OK LSUB completed
...
a0036 STATUS "Gemeinsame OrdnerMailpool ITTrash" (UIDNEXT UIDVALIDITY UNSEEN RECENT)
a0037 STATUS "Gemeinsame OrdnerMailpool ITInbox" (UIDNEXT UIDVALIDITY UNSEEN RECENT)
a0036 NO STATUS Folder not found in IMAP name space
a0037 NO STATUS Folder not found in IMAP name space

}}}

setting the folders up with the 'mailboxes' command works when using double backslash like this:

{{{

mailboxes \
"+" \
"+Gemeinsame Ordner\\Mailpool IT\\Trash" \
"+Gemeinsame Ordner\\Mailpool IT\\Inbox"

}}}

--------------------------------------------------------------------------------
2009-01-04 20:21:08 UTC brendan
* milestone changed to 1.6

--------------------------------------------------------------------------------
2009-01-05 00:58:47 UTC brendan
* Added comment:
I can't reproduce this with tip. I don't have access to a Domino server, but I've injected a \-separate string into the LSUB parser in a debugger and it both parses and emits it correctly later. Can you try again with tip, or 1.5.19 (probably out tomorrow night)?

* status changed to accepted

--------------------------------------------------------------------------------
2009-01-05 00:58:55 UTC brendan
* status changed to infoneeded

--------------------------------------------------------------------------------
2009-01-05 10:38:34 UTC uspoerlein
* Added comment:
I no longer have access to the Domino server and the original trace. But managed to reproduce another bug by using dovecot.

It is really easy to set up, use the following namespace definition:


{{{
namespace private {
  prefix =
  inbox = yes
  separator = "\"
}
}}}

And then fool around. One thing I noticed, that is different to the original test (I'm using 1.5.18 right here), is that mutt now issues multiple LSUB "" "%" instead of LSUB "" "*" so no sub-folder view will be returned. Using these individual LSUBs seems to have worked around the problem. There is still a nit though. It will not SELECT the folder, when using Return on a leaf folder. You have to use Space to actually enter the folder. Looks like it doesn't "get" that it's a leaf folder.

The dump shows this:


{{{
a0006 LSUB "" "%"
* LSUB () "\\" "ls3"
* LSUB () "\\" "testing"
a0006 OK Lsub completed.
a0007 LSUB "" "testing"
* LSUB () "\\" "testing"
a0007 OK Lsub completed.
a0008 LSUB "" "testing\\%"
* LSUB () "\\" {9}
testing\a
* LSUB () "\\" {9}
testing\b
a0008 OK Lsub completed.
}}}

This is ok, a and b are leaf folders. 


{{{
a0011 LSUB "" "testing\a"
a0011 OK Lsub completed.
a0012 LSUB "" "testing\\a%"
* LSUB () "\\" {9}
testing\a
a0012 OK Lsub completed.
a0013 CLOSE
a0014 SELECT "testing\\a"
a0013 OK Close completed.
* FLAGS (\Answered \Flagged \Deleted \Seen \Draft)
* OK [PERMANENTFLAGS (\Answered \Flagged \Deleted \Seen \Draft \*)] Flags permitted.
* 0 EXISTS
* 0 RECENT
* OK [UIDVALIDITY 1231150924] UIDs valid
* OK [UIDNEXT 1] Predicted next UID
a0014 OK [READ-WRITE] Select completed.
}}}

This is not ok, hitting return on "testing.a." (that's how it's displayed) will issue LSUBs (a0011 seems totally bogus). Only forcing the SELECT by using space will open the folder. This is a regression against 1.5.17 and happens regardless of the folder separator. I just checked with a "." separator.

Is this related to #3129? Is dovecot to blame? I'm using dovecot 1.1.7 and mutt 1.5.18 right now. I will retest once 1.5.19 has hit the FreeBSD ports tree (shouldn't take too long).

Cheers,
Uli

* keywords changed to imap folder separator backslash leaf
* status changed to assigned
* summary changed to IMAP: leaf folders in LSUB are not recognized correctly (was: problem with backslash as folder separator)
* version changed to 1.5.18

--------------------------------------------------------------------------------
2009-01-06 00:14:20 UTC brendan
* Added comment:
I've just tested tip on dovecot configured as you described and everything works as well with a \ delimiter as without. Mutt is expected to generate an LSUB request on testing\a when you press `<enter>`. The reason is that `LSUB "" "testing\\%"` returns:
{{{
* LSUB () "\\" {9}
testing\a
}}}
In particular, there's no `(\NoInferiors)` tag after LSUB. Note that this is typical when you've switched to the subscription browser with `<toggle-subscribed>`, and that if you were in the regular browser (toggle back), you'd get a `\NoInferiors` tag and `<enter>` would open the folder.

I think `<toggle-subscribed>` is what's causing confusion about `LSUB "" "%"` vs `LSUB "" "*"` too. The former is (and has always been) generated only by the subscription browser, while the latter is generated when `imap_check_subscribed` is set in order to add subscribed mailboxes to the `mailboxes` list.

Thanks for the additional data, especially the dovecot recipe. I'm closing this bug since setting the delimiter to \ doesn't change mutt's behaviour.

* resolution changed to fixed
* status changed to closed
