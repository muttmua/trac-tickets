Ticket:  3463
Status:  closed
Summary: message incorrectly displayed: bad scrolling position

Reporter: vinc17
Owner:    mutt-dev

Opened:       2010-10-13 11:36:43 UTC
Last Updated: 2016-08-08 20:18:13 UTC

Priority:  major
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
Mutt displayed a message as if it were completely empty (with no headers), but with a strange %P value. See attached screenshot, in particular the (211) corresponding to the %P value. AFAIK, this is the first time such a problem occurs.

I use Mutt based on rev 6161 with some patches (but no patches should be related to the scrolling position).

--------------------------------------------------------------------------------
2010-10-13 11:37:08 UTC vinc17
* Added attachment message.png
* Added comment:
screenshot

--------------------------------------------------------------------------------
2010-10-13 11:38:38 UTC vinc17
* Added comment:
I forgot to say: I couldn't scroll upwards. That's why "Le début du message est affiché." ("Top of message is shown." in English) is displayed.

--------------------------------------------------------------------------------
2010-10-13 13:39:34 UTC vinc17
* Added comment:
I could reproduce the problem on a different message. This time I got (-86) for the %P value.

--------------------------------------------------------------------------------
2010-10-13 13:44:26 UTC vinc17
* Added comment:
It was from the same Mutt instance, which had been running for 259 hours. In case there has been some memory corruption, I've quit Mutt and restarted it.

--------------------------------------------------------------------------------
2010-10-13 14:18:18 UTC me
* Added comment:
{{{
Can you sanitize the message enough to produce a test case?
}}}

--------------------------------------------------------------------------------
2010-10-14 07:42:59 UTC vinc17
* Added comment:
The problem is that the bug is not reproducible. Just after it occurred (on both messages), I tried to reproduce it, trying to remember what I had done just before, but without any success. As the bug occurred on two different messages (from two different senders), I don't think it is related to some particular message. Both messages came from the same mailing-list, with many headers (in particular, "Received:" ones, but note that I wasn't displaying all headers, so that when such messages are displayed, they look as normal messages -- the first one at least could fit in the terminal window). The incorrect behavior could come from some corrupt data due to some other unknown bug.

--------------------------------------------------------------------------------
2010-11-21 09:20:33 UTC vinc17
* Added comment:
This has just happened again, on a different mailbox, a few minutes after opening it and copying a message to it. I tried to reproduce the problem by redoing about the same operations I had done, but without success.

--------------------------------------------------------------------------------
2010-12-20 09:07:06 UTC vinc17
* Added comment:
The problem occurred again yesterday, in a different mailbox (the folder for the mutt-dev mailing-list). As it had never occurred before October, I'd say that the problem is due to some change from September or October.

--------------------------------------------------------------------------------
2016-08-08 20:18:13 UTC kevin8t8
* Added comment:
Whoops, forgot to add the close header to the commit.

Commit 7abc19ad2d10 most probably fixes this issue, so I'm going to mark this closed.  Please re-open if it turns out not to be the case.

* Updated description:
Mutt displayed a message as if it were completely empty (with no headers), but with a strange %P value. See attached screenshot, in particular the (211) corresponding to the %P value. AFAIK, this is the first time such a problem occurs.

I use Mutt based on rev 6161 with some patches (but no patches should be related to the scrolling position).
* resolution changed to fixed
* status changed to closed
