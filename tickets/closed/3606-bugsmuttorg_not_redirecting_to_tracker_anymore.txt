Ticket:  3606
Status:  closed
Summary: bugs.mutt.org not redirecting to tracker anymore!

Reporter: jidanni
Owner:    mutt-dev

Opened:       2012-12-12 01:41:13 UTC
Last Updated: 2012-12-12 06:02:16 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:

{{{
$ GET -USP http://bugs.mutt.org/
GET http://bugs.mutt.org/
User-Agent: lwp-request/6.03 libwww-perl/6.04

200 OK
<html>
<head>
<title>Mutt development</title>
<link rel="shortcut icon" href="favicon.ico"/>
</head>

<body>
<p>
Read the <a href="doc/manual.html">manual</a>, generated nightly from CVS.
</p>
<p>
Browse the <a href="hg/">source</a>.
<p>
<p>
Get a nightly <a href="nightlies/">snapshot</a>.
</body>
</html>
}}}


--------------------------------------------------------------------------------
2012-12-12 06:02:16 UTC brendan
* Added comment:
thanks for the report. This was probably an accident when moving the wiki over to dev.mutt.org, and should be fixed now.

* resolution changed to fixed
* status changed to closed
