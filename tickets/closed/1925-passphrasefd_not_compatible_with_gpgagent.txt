Ticket:  1925
Status:  closed
Summary: --passphrase-fd not compatible with gpg-agent

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2004-10-22 08:25:27 UTC
Last Updated: 2005-08-02 16:50:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: ?
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#277646.
Please Cc all your replies to 277646@bugs.debian.org .]

From: Marco d'Itri <md@Linux.IT>
Subject: --passphrase-fd not compatible with gpg-agent
Date: Thu, 21 Oct 2004 15:00:50 +0200

Fix:

s/ --passphrase-fd 0/ %?p?--passphrase-fd 0?/

-- 
ciao, |
Marco | [8680 cosw4hS/v4ijQ]


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-10-22 08:25:27 UTC Marco d'Itri <md@linux.it>
* Added comment:
{{{
Package: mutt
Version: ?
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#277646.
Please Cc all your replies to 277646@bugs.debian.org .]

From: Marco d'Itri <md@Linux.IT>
Subject: --passphrase-fd not compatible with gpg-agent
Date: Thu, 21 Oct 2004 15:00:50 +0200

Fix:

s/ --passphrase-fd 0/ %?p?--passphrase-fd 0?/

-- 
ciao, |
Marco | [8680 cosw4hS/v4ijQ]
}}}

--------------------------------------------------------------------------------
2005-08-03 10:50:00 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed
