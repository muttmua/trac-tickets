Ticket:  3049
Status:  closed
Summary: c<ret> won't work on the spoolfile in a solid way

Reporter: calmar
Owner:    mutt-dev

Opened:       2008-04-21 22:45:19 UTC
Last Updated: 2008-05-14 11:05:26 UTC

Priority:  minor
Component: mutt
Keywords:  spoolfile change

--------------------------------------------------------------------------------
Description:
when I get a new mail into the =inbox (set as spoolfile=+inbox)

and 
mailboxes ! +inbox
#mailboxes =inbox

(I tried some combinations without any success),

then I can't c<ret> unless when I do that just right after getting the message after a new message in =inbox.

When I press anything (beeing in another folder), i can't change anymore to =inbox, unless pressing c!<ret> etc.

my muttrc http://www.calmar.ws/tmp/.mutt/muttrc



--------------------------------------------------------------------------------
2008-05-01 14:48:25 UTC Alain Bench
* Added comment:
{{{
Hello,

 On Monday, April 21, 2008 at 22:45:19 -0000, Mutt wrote:

>| mailboxes ! +inbox
> when I get a new mail into the =inbox (set as spoolfile=+inbox) [...]
> then I can't c<ret> unless when I do that just right after getting the
> message after a new message in =inbox.

    I can't reproduce this problem: When pressing [c], the prompt
defaults to "=inbox" as soon and as long as the inbox is considered new.
Pressing <Enter> then opens the inbox.

    Maybe something is reading the inbox, or messing with its access or
modification timestamps?


> my muttrc http://www.calmar.ws/tmp/.mutt/muttrc

    404


Bye!	Alain.
}}}

--------------------------------------------------------------------------------
2008-05-14 01:02:45 UTC calmar
* Added comment:
Hi Alain,

> 
>     Maybe something is reading the inbox, or messing with its access or
> modification timestamps?
> 

Yeah, true, that seems to be the thing - the mail-checking script what reads the inbox did it.


Sorry no bug in that case.

I just need to find out, what to do beeing able to read that file without changing the access-time or reseting it with 'touch' or whatever.

Cheers
marco

--------------------------------------------------------------------------------
2008-05-14 09:03:35 UTC pdmef
* Added comment:
Ok, so I'm closing the ticket.

From the description I assume you're using mbox. You can use "fixatime" from:

ftp://ftp.mutt.org/pub/mutt/contrib

to fixup atime of mbox folders with new mail to make mutt's new mail detection working again. You need to compile it on your own, but that should be easy.

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2008-05-14 11:05:26 UTC calmar
* Added comment:
great, thanks a lot for the fix-a-time link! ;)
