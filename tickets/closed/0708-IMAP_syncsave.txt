Ticket:  708
Status:  closed
Summary: IMAP sync/save

Reporter: Daniel Lashua <djl@djlpc.irngtx.tel.gte.com>
Owner:    mutt-dev

Opened:       2001-07-27 23:54:51 UTC
Last Updated: 2005-07-24 19:59:47 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.20-1
Severity: normal

-- Please type your report below this line

When using IMAP, if a message is altered (i.e. marked as replied, etc) and then it is saved to another mailbox on the SAME IMAP server (possibly on another IMAP server or in a local mailbox... untested) the message is saved in the state that it is in on the server. Any changes that have been made to the message that have not been synced to the IMAP server are not reflected. This becomes slightly annoying when performing very standard activities. For example, the following:

new mail comes in.
read it
save it to another mailbox
in that mailbox, the message is still marked as new.

reply to a message
save it to another mailbox
in that mailbox, the message is not marked as replied.


in order to work around this... one must:

new mail comes in
read it.
sync the mail box
save it to another mailbox
in that mailbox, the message is marked as read, as it should be



I can give more information about this problem if needed.


-- System Information
Debian Release: testing/unstable
Kernel Version: Linux djlpc 2.4.4-686 #1 Sun Apr 29 12:22:40 EST 2001 i686 unknown

Versions of the packages mutt depends on:
ii  exim           3.31-1         Exim Mailer
ii  libc6          2.2.3-7        GNU C Library: Shared libraries and Timezone
ii  libncurses5    5.2.20010318-3 Shared libraries for terminal handling
ii  libsasl7       1.5.24-6.1     Authentication abstraction library.
ii  libssl0.9.6    0.9.6b-1       SSL shared libraries
ii  exim           3.31-1         Exim Mailer
	^^^ (Provides virtual package mail-transport-agent)

-- Mutt Version Information

Mutt 1.3.20i (2001-07-24)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.4-686 [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-07-25 13:59:47 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed
