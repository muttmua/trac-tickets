Ticket:  2026
Status:  closed
Summary: mailboxes command ignores password

Reporter: ismail@kde.org.tr
Owner:    mutt-dev

Opened:       2005-08-07 19:24:41 UTC
Last Updated: 2005-08-08 19:29:24 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
if you use mailboxes imap://username:password@host/INBOX when you open mailbox mutt ignores password and asks you again.
>How-To-Repeat:
Add mailboxes imap://username:password@host/INBOX with valid info and try to open it.
>Fix:
use account-hook for password.
}}}

--------------------------------------------------------------------------------
2005-08-09 13:29:24 UTC brendan
* Added comment:
{{{
Actually this appears to work fine. You may have confused
the mailboxes command with the spoolfile variable. If
imap_passive is unset and a mailboxes command contains an
IMAP URL with a username and password, mutt will connect to
poll for mail, without demanding a password.
}}}

* resolution changed to fixed
* status changed to closed
