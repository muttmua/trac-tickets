Ticket:  2972
Status:  closed
Summary: Have auto_view use its own command instead of consulting mailcap

Reporter: john
Owner:    mutt-dev

Opened:       2007-10-20 12:12:26 UTC
Last Updated: 2007-10-21 02:15:30 UTC

Priority:  trivial
Component: mutt
Keywords:  auto_view mailcap

--------------------------------------------------------------------------------
Description:
I honestly don't know how feasible this is going to be from a backward compatibility standpoint, since rewriting the current auto_view syntax would break a lot of people's configs, but here goes:

I think it would be a good thing if one could specify a mailcap-style command definition for a given MIME type, to be used (only) when auto_view-ing something.  That probably doesn't make a whole lot of sense, so let me explain the limitation in the current behaviour and contrast it with an example of how things would work differently with this feature enabled:

The way things work now, say I wanted to set up an auto_view handler for JPEG files; it would have to be something with text output, of course, but perhaps I made a script that would run !ImageMagick's "identify" command on it, as well as something to dump any EXIF metadata.  The details aren't important; the point is that I have a script that takes a JPEG file as input and produces useful textual output.  :)  I can define "auto_view image/jpeg" in my .muttrc, and then configure Mutt's mailcap file to run this command, and then everything works when I bring up the message that the JPEG file is attached to.  Unfortunately, this means that when I open the attachment individually (for example, by hitting "v" and then selecting it), I still get the text output when what I want is to spawn an image viewer to actually view the picture.

What I'm looking for is something like this:

{{{auto_view image/jpeg "my_script ; copiousoutput"}}}

Whatever's between the quotes is expected to be in mailcap format, and would get handled by the same code that currently handles that stuff.  The idea is that when an attachment is being displayed by auto_view, it gets sent to the specialized handler defined there...but when it's being individually opened, it gets the normal mailcap handling (in this case, it would be opened with an image viewer like qiv).

--------------------------------------------------------------------------------
2007-10-20 15:19:26 UTC Aron Griffis
* Added comment:
{{{
Mutt wrote:  [Sat Oct 20 2007, 08:12:29AM EDT]
>  I can define "auto_view
>  image/jpeg" in my .muttrc, and then configure Mutt's mailcap file to run
>  this command, and then everything works when I bring up the message that
>  the JPEG file is attached to.  Unfortunately, this means that when I open
>  the attachment individually (for example, by hitting "v" and then
>  selecting it), I still get the text output when what I want is to spawn an
>  image viewer to actually view the picture.

I think you can get the behavior you want by defining one mailcap
entry with copiousoutput and one without, for example:

image/*; display %s
image/*; convert -geometry 80x1000 %s pbm:- | pbmtoascii ; copiousoutput

(Just an example... you probably want test= to check $DISPLAY, etc)

Aron
}}}

--------------------------------------------------------------------------------
2007-10-20 16:13:13 UTC Michael Tatge
* Added comment:
{{{
* On Sat, Oct 20, 2007 Mutt wrote:
> #2972: Have auto_view use its own command instead of consulting mailcap
>  I think it would be a good thing if one could specify a mailcap-style
>  command definition for a given MIME type, to be used (only) when
>  auto_view-ing something.

Isn't that already the case? The mailcap entry that has "copiousoutput"
is used for auto_view.
I use this for text/html:

# used when (v)iewing
text/html; links -dump-charset ISO-8859-15 '%s'; nametemplate=%s.html; needsterminal

# for autoview
text/html; lynx -stdin -dump -force_html ; copiousoutput; needsterminal

Also see the wiki (http://wiki.mutt.org/?MuttFaq/Attachment)

HTH,

Michael
}}}

--------------------------------------------------------------------------------
2007-10-21 02:15:30 UTC john
* Added comment:
Yikes, I could have sworn I did the research on this before opening this bug; apparently not.  Sorry to have bothered you guys; yes, this is indeed the case and in fact it's even documented in section 5 of TFM.  :(

* resolution changed to invalid
* status changed to closed
