Ticket:  2022
Status:  closed
Summary: expansion of shell environment $VAR doesn't like digits in VARnames.

Reporter: rado@math.uni-hamburg.de
Owner:    mutt-dev

Opened:       2005-08-02 19:32:24 UTC
Last Updated: 2005-08-06 20:09:08 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
When using shell "VAR1" and "VAR2", mutt fails to expand the values of those vars (via "$") because they contain "1" and "2" in the name. Instead mutt tries to/ expands "VAR" plain.
>How-To-Repeat:
shell:
export VAR=bla VAR1=var1 VAR2=var2

muttrc:
set signature=$VAR2

results in signature=bla instead of =var2
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-08-06 19:47:41 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
[How-To-Repeat]
> shell:
> export VAR=bla VAR1=var1 VAR2=var2
> 
> muttrc:
> set signature=$VAR2
> 
> results in signature=bla instead of =var2



The problem is at the 275th line of init.c:
> 	for (pc = tok->dptr; isalpha ((unsigned char) *pc) || *pc == '_'; pc++)
> 	  ;

Currently mutt supports only alphabets and underscore
for variable names.
Maybe we can use isalnum() instead of isalpha().
But you can workaround it with ${VAR1} and ${VAR2}.

-- 
tamo
}}}

--------------------------------------------------------------------------------
2005-08-07 14:09:08 UTC brendan
* Added comment:
{{{
[How-To-Repeat]
Fixed in CVS (used isalnum per tamo's suggestion).
}}}

* resolution changed to fixed
* status changed to closed
