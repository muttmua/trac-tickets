Ticket:  2020
Status:  closed
Summary: crypt-gpgme.c: impossible to make good translation for some languages

Reporter: ttakah@lapis.plala.or.jp
Owner:    mutt-dev

Opened:       2005-08-02 09:10:03 UTC
Last Updated: 2005-08-09 17:25:52 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
In crypt-gpgme.c:
>      fprintf (fp, "%s ......: ", idx ? _(" aka") :_("Name"));

In some languages, this is not correctly-translatable.

At first, the number of dots should be different between languages.
----> fix: _("%s ......: ")

Next, the number of dots should be different between "aka" and "Name".
----> fix: if(idx)fprintf(..._(" aka"));else fprintf(..._("Name"));

>How-To-Repeat:
build mutt with --enable-nls --enable-gpgme options.
>Fix:
See the attached patch for "dot_format" macro.

Note: In addition to fixing this problem,
this patch enables translation for other symbols
such as "[?]" and "," because these can look strange
in the midst of non-European language messages.
For example, Japanese language has its own delimiter
in place of ",".

Note2: This patch concatenates some messages because
some languages have completely different order of words
for a message, e.g. "Error getting key information: xxx\n"
could be "getting key information: xxx error\n" in such
languages.

See "info gettext 'Preparing Strings'" for the detail.
http://info2html.sourceforge.net/cgi-bin/info2html-demo/info2html?(gettext.info.gz)Preparing%2520Strings

}}}

--------------------------------------------------------------------------------
2005-08-10 11:25:52 UTC brendan
* Added comment:
{{{
Applied, thanks.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2005-08-10 14:44:02 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
> Synopsis: crypt-gpgme.c: impossible to make good translation for some languages

The previous patch was a big change.
A smaller version is here.

-- 
tamo

--zYM0uCDKw75PZbzx
Content-Type: text/plain; charset=us-ascii
Content-Disposition: attachment; filename="patch-1.5.9cvs.tamo.gpgme-trans.1"

--- ../mutt/crypt-gpgme.c	Tue Aug  2 16:11:37 2005
+++ ./crypt-gpgme.c	Tue Aug  9 14:29:57 2005
@@ -2952,7 +2952,7 @@
         continue;
 
       s = uid->uid;
-      fprintf (fp, "%s ......: ", idx ? _(" aka") :_("Name"));
+      fputs (idx ? _(" aka ......: ") :_("Name ......: "), fp);
       if (uid->invalid)
         {
           fputs (_("[Invalid]"), fp);
@@ -2975,7 +2975,7 @@
 #else
       strftime (shortbuf, sizeof shortbuf, "%c", tm);
 #endif
-      fprintf (fp, "Valid From : %s\n", shortbuf);
+      fprintf (fp, _("Valid From : %s\n"), shortbuf);
     }
   
   if (key->subkeys && (key->subkeys->expires > 0))
@@ -2988,7 +2988,7 @@
 #else
       strftime (shortbuf, sizeof shortbuf, "%c", tm);
 #endif
-      fprintf (fp, "Valid To ..: %s\n", shortbuf);
+      fprintf (fp, _("Valid To ..: %s\n"), shortbuf);
     }
 
   if (key->subkeys)
@@ -3001,25 +3001,25 @@
   if (key->subkeys)
     aval = key->subkeys->length;
 
-  fprintf (fp, "Key Type ..: %s, %lu bit %s\n", s2, aval, s);
+  fprintf (fp, _("Key Type ..: %s, %lu bit %s\n"), s2, aval, s);
 
-  fprintf (fp, "Key Usage .: ");
+  fprintf (fp, _("Key Usage .: "));
   delim = "";
 
   if (key_check_cap (key, KEY_CAP_CAN_ENCRYPT))
     {
       fprintf (fp, "%s%s", delim, _("encryption"));
-      delim = ", ";
+      delim = _(", ");
     }
   if (key_check_cap (key, KEY_CAP_CAN_SIGN))
     {
       fprintf (fp, "%s%s", delim, _("signing"));
-      delim = ", ";
+      delim = _(", ");
     }
   if (key_check_cap (key, KEY_CAP_CAN_CERTIFY))
     {
       fprintf (fp, "%s%s", delim, _("certification"));
-      delim = ", ";
+      delim = _(", ");
     }
   putc ('\n', fp);
 
@@ -3058,7 +3058,7 @@
     {
       s = key->issuer_serial;
       if (s)
-	fprintf (fp, "Serial-No .: 0x%s\n", s);
+	fprintf (fp, _("Serial-No .: 0x%s\n"), s);
     }
 
   if (key->issuer_name)
@@ -3066,7 +3066,7 @@
       s = key->issuer_name;
       if (s)
 	{
-	  fprintf (fp, "Issued By .: ");
+	  fprintf (fp, _("Issued By .: "));
 	  parse_and_print_user_id (fp, s);
 	  putc ('\n', fp);
 	}
@@ -3085,7 +3085,7 @@
           putc ('\n', fp);
           if ( strlen (s) == 16)
             s += 8; /* display only the short keyID */
-          fprintf (fp, "Subkey ....: 0x%s", s);
+          fprintf (fp, _("Subkey ....: 0x%s"), s);
 	  if (subkey->revoked)
             {
               putc (' ', fp);
@@ -3118,7 +3118,7 @@
 #else
               strftime (shortbuf, sizeof shortbuf, "%c", tm);
 #endif
-              fprintf (fp, "Valid From : %s\n", shortbuf);
+              fprintf (fp, _("Valid From : %s\n"), shortbuf);
             }
 
 	  if (subkey->expires > 0)
@@ -3131,7 +3131,7 @@
 #else
               strftime (shortbuf, sizeof shortbuf, "%c", tm);
 #endif
-              fprintf (fp, "Valid To ..: %s\n", shortbuf);
+              fprintf (fp, _("Valid To ..: %s\n"), shortbuf);
             }
 
 	  if (subkey)
@@ -3144,25 +3144,25 @@
 	  else
 	    aval = 0;
 
-          fprintf (fp, "Key Type ..: %s, %lu bit %s\n", "PGP", aval, s);
+          fprintf (fp, _("Key Type ..: %s, %lu bit %s\n"), "PGP", aval, s);
 
-          fprintf (fp, "Key Usage .: ");
+          fprintf (fp, _("Key Usage .: "));
           delim = "";
 
 	  if (subkey->can_encrypt)
             {
               fprintf (fp, "%s%s", delim, _("encryption"));
-              delim = ", ";
+              delim = _(", ");
             }
           if (subkey->can_sign)
             {
               fprintf (fp, "%s%s", delim, _("signing"));
-              delim = ", ";
+              delim = _(", ");
             }
           if (subkey->can_certify)
             {
               fprintf (fp, "%s%s", delim, _("certification"));
-              delim = ", ";
+              delim = _(", ");
             }
           putc ('\n', fp);
         }

--zYM0uCDKw75PZbzx--
}}}

--------------------------------------------------------------------------------
2007-04-03 16:42:59 UTC 
* Added attachment patch-1.5.9.tamo.transl.1
* Added comment:
patch-1.5.9.tamo.transl.1
