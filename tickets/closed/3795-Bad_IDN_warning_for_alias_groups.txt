Ticket:  3795
Status:  closed
Summary: "Bad IDN" warning for alias groups

Reporter: chdiza
Owner:    brendan

Opened:       2015-11-25 04:04:01 UTC
Last Updated: 2015-11-25 05:52:47 UTC

Priority:  major
Component: SMTP
Keywords:  

--------------------------------------------------------------------------------
Description:
Pulled after the 4-part SMTP enhancement patch.  This is OS X 10.11.1 with Xcode 7.1.1.

After building mutt and trying to run it, when it's starting up it's barfing on my aliases file.  This file is fine, I didn't change it.  What I see is:

{{{
Error in /Users/chdiza/.mutt/aliases, line 106: Warning: Bad IDN 'jjones' in alias 'friends'.

Error in /Users/chdiza/.mutt/aliases, line 108: Warning: Bad IDN 'asmith' in alias 'colleagues'.
}}}

These two cases have the following in common.  The aliases jjones and asmith are both the *first* alias in a  group alias.  E.g.:
{{{
alias friends jjones, rjohnson, fadams
alias colleagues asmith, jpeters, bgomez
}}}

--------------------------------------------------------------------------------
2015-11-25 04:21:00 UTC kevin8t8
* Added comment:
Thanks for testing this quickly.  I see my goof up, I just need a little time to figure out the best way to fix it.

--------------------------------------------------------------------------------
2015-11-25 05:09:25 UTC kevin8t8
* Added comment:
I'm attaching an alpha patch.  It gets rid of the error but I need to test it a bit more before I can push it.

--------------------------------------------------------------------------------
2015-11-25 05:09:46 UTC kevin8t8
* Added attachment ticket-3795.patch

--------------------------------------------------------------------------------
2015-11-25 05:52:33 UTC kevin8t8
* Added comment:
Okay, this look okay in my testing.  I'm pushing it up.  Thank you again for testing and reporting the error so quickly.

--------------------------------------------------------------------------------
2015-11-25 05:52:47 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [94186a96ca17f4d348fd563e4b2ded3386180bad]:
{{{
#!CommitTicketReference repository="" revision="94186a96ca17f4d348fd563e4b2ded3386180bad"
Fix bad idn error on local mailboxes.  (closes #3795)

Commit 831abf39d53a pulled the mbox_to_udomain() call inside the
conversion functions.  Unfortunately, this causes local (user only)
mailboxes to be considered conversion errors instead of just skipping
them.

Revert mbox_to_udomain() back to using a static buffer and pull back
into the mutt_addrlist_to_local/intl() functions.

Pass the user and domain into the conversion functions instead of the address.
}}}

* resolution changed to fixed
* status changed to closed
