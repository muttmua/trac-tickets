Ticket:  3406
Status:  closed
Summary: folder_format %d should reference date_format

Reporter: rubix
Owner:    me

Opened:       2010-04-16 14:03:58 UTC
Last Updated: 2010-08-06 18:46:35 UTC

Priority:  trivial
Component: browser
Keywords:  folder_format date_format

--------------------------------------------------------------------------------
Description:
It would simplify date formatting across variables, assuming people want that.

--------------------------------------------------------------------------------
2010-04-16 21:23:05 UTC me
* Added comment:
Proposed fix posted to mutt-dev for review: http://marc.info/?l=mutt-dev&m=127145163816668&w=2

* component changed to browser
* owner changed to me
* status changed to accepted

--------------------------------------------------------------------------------
2010-08-06 18:46:35 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [317338b0d490]) add %D format string to $folder_format to expand the time based on $date_format.

properly set the locale for LC_TIME prior to calling strftime()
closes #1734
closes #3406

* resolution changed to fixed
* status changed to closed
