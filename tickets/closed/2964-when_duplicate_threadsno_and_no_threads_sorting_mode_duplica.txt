Ticket:  2964
Status:  closed
Summary: when $duplicate_threads='no' and no threads sorting mode duplicates can't be found

Reporter: m.m
Owner:    mutt-dev

Opened:       2007-09-26 15:48:50 UTC
Last Updated: 2009-02-16 14:45:11 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I able to use the ~= pattern search only when $duplicate_threads=yes and sorting mode by threads.
The first condition seems to be a bug, according to the documentation, while the second seems like a 'known fact', although not documented.

It is a bug because when $duplicate_threads is 'no' duplicate messages couldn't be found.

I checked this on a maildir folder.

(I checked on .1.5.11, and 1.5.16 (last available version))

--------------------------------------------------------------------------------
2009-02-16 14:45:11 UTC pdmef
* Added comment:
Dup of #2711.

* resolution changed to duplicate
* status changed to closed
