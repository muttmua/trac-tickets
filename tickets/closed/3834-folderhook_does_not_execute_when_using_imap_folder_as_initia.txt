Ticket:  3834
Status:  closed
Summary: folder-hook does not execute when using imap folder as initial mailbox

Reporter: craftyguy
Owner:    brendan

Opened:       2016-04-24 21:29:24 UTC
Last Updated: 2016-04-25 17:35:55 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
When an imap folder is configured as the initial mailbox (e.g. using spool= in muttrc), a folder-hook that should execute on this folder does not run. If I (c)hange to it afterward, the folder-hook will execute successfully. In my example I've attached, the folder-hook should execute on any imap folder, and should cause all threads to be collapsed. When the imap folder is set as spool, this folder-hook will not run when mutt is first started.

If I set spool to be a local mailbox, then (c)hange to the imap folder, the folder-hook will run.

--------------------------------------------------------------------------------
2016-04-24 21:30:23 UTC craftyguy
* Added attachment muttrc
* Added comment:
minimal muttrc that reproduces issue

--------------------------------------------------------------------------------
2016-04-24 22:39:45 UTC craftyguy
* version changed to 1.6.0

--------------------------------------------------------------------------------
2016-04-25 02:03:29 UTC kevin8t8
* Added comment:
Sorry but this is working for me.  I took your sample muttrc, changed the folder to point to my imap server, and the threads are being collapsed.

Have you tried it using 'mutt -n' to make sure the system muttrc isn't doing something funny?

Also, just to confirm that you are using 1.6.0.  I made a few changes separating macro and keyboard buffering between 1.5.24 and 1.6.0.  If you are using an earlier version of mutt and are being prompted for a certificate, it might goof things up.

--------------------------------------------------------------------------------
2016-04-25 02:14:57 UTC craftyguy
* Added comment:
Ah! So yes, you're right, this actually works as expected. I have been launching mutt with a keybinding that essentially maps to this: uxrvt --title 'mutt mail' -e mutt
For some reason when mutt is run this way, the folder-hook does not execute. 

--------------------------------------------------------------------------------
2016-04-25 17:30:42 UTC kevin8t8
* Added comment:
So if you run "mutt" directly from a shell, it runs the folder-hook.  But if you run "uxrvt --title 'mutt mail' -e mutt" via a key binding, the folder-hook doesn't run?  That is rather odd, but seems likely to be some kind of alias or other configuration issue.

I'm going to mark this as works-for-me, but if you find out there is some kind of mutt bug involved here, please feel free to re-open the ticket.

* resolution changed to worksforme
* status changed to closed

--------------------------------------------------------------------------------
2016-04-25 17:35:55 UTC craftyguy
* Added comment:
That's correct. Yes, odd behavior.. Thanks for the hint there, I'll re-open if I narrow it down to something mutt-specific!
