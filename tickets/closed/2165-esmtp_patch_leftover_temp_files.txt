Ticket:  2165
Status:  closed
Summary: esmtp patch: leftover temp files

Reporter: wildenhues@ins.uni-bonn.de
Owner:    mutt-dev

Opened:       2006-01-16 13:47:11 UTC
Last Updated: 2006-01-23 00:56:49 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
With Brendan's esmtp patch, there are occasional leftover temp files.

I see that in send_message and in _mutt_bounce_message,
  mutt_invoke_sendmail is responsible for unlinking the message file passed from the variable 'tempfile' (done in send_msg),
  mix_send_message does unlink, too,
  mutt_smtp_send however does not unlink it

It seems that unlinking semantics are not completely uniform (the unlinking is done in some but not all error paths in above functions), so please add the corresponding removal to the mutt_smtp_send as intended.
>How-To-Repeat:
invoke mutt with esmtp patch, write message, send, quit, look into TMPDIR.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-01-23 18:56:49 UTC brendan
* Added comment:
{{{
I've applied your patch, thanks.
}}}

* resolution changed to fixed
* status changed to closed
