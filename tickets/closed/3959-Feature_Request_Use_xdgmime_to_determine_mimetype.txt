Ticket:  3959
Status:  closed
Summary: Feature Request: Use xdg-mime to determine mime-type

Reporter: boruch
Owner:    mutt-dev

Opened:       2017-08-02 10:42:10 UTC
Last Updated: 2017-08-19 15:36:32 UTC

Priority:  minor
Component: MIME
Keywords:  

--------------------------------------------------------------------------------
Description:
For systems that have package xdg-utils available/installed, program xdg-mime can return a mime-type for a file without needing to identify it by filename extension. For example:

 $ xdg-mime query filetype ~/patch_patch-patch

returns:  text/x-patch

OTOH, mutt (v1.7) recognizes the file as plain text.

Also, by default, mutt recognizes files with extension .patch as mime-type text/x-diff, which is close but not exact.

Alternatively, package 'file' includes the command-line program file, which also has a mime-type option:

  $ file --mime-type ~/patch_patch-patch

returns:  text/x-diff



--------------------------------------------------------------------------------
2017-08-03 09:24:06 UTC vinc17
* Added comment:
Replying to [ticket:3959 boruch]:
> Also, by default, mutt recognizes files with extension .patch as mime-type text/x-diff, which is close but not exact.

The MIME type associated with .patch is platform dependent. Under Debian, this comes from /etc/mime.types provided by the mime-support package. But the end user can also change the default.

FYI, I also suggested the use of an external program to determine the MIME type, in the thread "corrupted attachments" in mutt-dev in June 2017: https://www.mail-archive.com/mutt-dev@mutt.org/msg12926.html

--------------------------------------------------------------------------------
2017-08-19 15:36:32 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"190e778db4d6c5c764ba225115c71ad6de88b62e" 7131:190e778db4d6]:
{{{
#!CommitTicketReference repository="" revision="190e778db4d6c5c764ba225115c71ad6de88b62e"
Add option to run command to query attachment mime type. (closes #2933) (closes #3959)

Add $mime_type_query_command to specify a command to run if the
attachment extension is not in the mime.types file.

Add $mime_type_query_first to allow the query command to be run before
the mime.types lookup.
}}}

* resolution changed to fixed
* status changed to closed
