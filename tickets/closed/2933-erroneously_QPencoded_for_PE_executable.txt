Ticket:  2933
Status:  closed
Summary: erroneously QP-encoded for PE executable

Reporter: hubertwslin
Owner:    mutt-dev

Opened:       2007-07-19 02:56:06 UTC
Last Updated: 2017-08-19 15:36:32 UTC

Priority:  major
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
I sent the sample file attached with Mutt -a, but after saving the attachment to a file, the md5 checksum to the file is changed. Both mutt-1.4.2.3-1.fc6 on fc6 and mutt-1.5.14-4.fc7 on fedora 7 have the same issue.

Original md5 sum: 0e37c055d7b3184d3530db1e06b948d5
Newly saved md5sum: b649d4991851c2757734432ce3a0c773


WARNING! The attached is virus infected, please handle with care.

--------------------------------------------------------------------------------
2007-07-19 02:58:30 UTC hubertwslin
* Added attachment sample.zip
* Added comment:
sample file (please unzip it before the test, virus infected, handle with care)

--------------------------------------------------------------------------------
2007-07-19 03:08:17 UTC hubertwslin
* Added comment:
FYI. No anti-virus product is installed during the test.

--------------------------------------------------------------------------------
2009-06-12 12:05:37 UTC pdmef
* Added comment:
After downloading the file for testing, I get b649d4991851c2757734432ce3a0c773 which remains after sending and saving the file. Are you really sure you got the right file for testing?

* status changed to infoneeded_new

--------------------------------------------------------------------------------
2014-12-14 04:45:20 UTC kevin8t8
* Added comment:
I also see the md5sum of the unzipped file as b649d4991851c2757734432ce3a0c773.

However, I think the problem hubertwslin was experiencing was that mutt was setting the content type to text/plain.  Since the file has no extension, mutt has to scan the file and take a guess.  If I don't adjust the content type, then sending the file (as a text attachment) does indeed corrupt it, so I think that's what this was about.

Taking a look at mutt_make_file_attach(), the key lines are
{{{
    if (info->lobin == 0 || (info->lobin + info->hibin + info->ascii)/ info->lobin >= 10)
    {
      att->type = TYPETEXT;
      att->subtype = safe_strdup ("plain");
    }
    else
    {
      att->type = TYPEAPPLICATION;
      att->subtype = safe_strdup ("octet-stream");
    }
}}}

I did a quick scan, and it looks like this file has (approximately):
hibin: 848351
ascii: 21851
lobin: 33596
which gives (l+h+a)/l of about 27 (~4% lobin characters).  So the number of "hibin" characters are what is causing the wrong guess in this case.

I wonder if it would be better to be more conservative when guessing, and count hibin characters as "binary" too. (l+h+a)/(l+h) would be about 1 (~97% lobin + hibin), which would have given the correct content type here.  Does anyone have an opinion on that?

--------------------------------------------------------------------------------
2014-12-16 21:17:47 UTC kevin8t8
* cc changed to kevin@8t8.us
* Added comment:
Attaching a proposed patch.  The patch treats hibin characters as binary for this calculation.

* keywords changed to patch

--------------------------------------------------------------------------------
2014-12-16 21:18:03 UTC kevin8t8
* Added attachment consider-hbin-as-binary.patch

--------------------------------------------------------------------------------
2017-07-21 00:32:04 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"a533c22715c822803a7d384de3a7c81a66a31a11" 7110:a533c22715c8]:
{{{
#!CommitTicketReference repository="" revision="a533c22715c822803a7d384de3a7c81a66a31a11"
When guessing an attachment type, don't allow text/plain if there is a null character. (see #2933)

Type text/plain should not contain any null characters.  Slightly
improve the type guesser by forcing an attachment with any null
characters to be application/octet-stream.

Note the type guesser could use much more improvement, but this is an
easy and obvious fix.
}}}

--------------------------------------------------------------------------------
2017-08-19 15:36:32 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"190e778db4d6c5c764ba225115c71ad6de88b62e" 7131:190e778db4d6]:
{{{
#!CommitTicketReference repository="" revision="190e778db4d6c5c764ba225115c71ad6de88b62e"
Add option to run command to query attachment mime type. (closes #2933) (closes #3959)

Add $mime_type_query_command to specify a command to run if the
attachment extension is not in the mime.types file.

Add $mime_type_query_first to allow the query command to be run before
the mime.types lookup.
}}}

* resolution changed to fixed
* status changed to closed
