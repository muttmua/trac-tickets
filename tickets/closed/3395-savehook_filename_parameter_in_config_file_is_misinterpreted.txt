Ticket:  3395
Status:  closed
Summary: save-hook filename parameter in config file is misinterpreted

Reporter: daniell
Owner:    mutt-dev

Opened:       2010-03-27 11:04:25 UTC
Last Updated: 2010-03-28 09:31:51 UTC

Priority:  minor
Component: mutt
Keywords:  save-hook folder filename muttrc

--------------------------------------------------------------------------------
Description:
Given a save-hook entry like this in .muttrc:[[BR]]
save-hook '~sSPAM' '=sa.spam'

In mutt when finding a message with that subject, pressing a 's' (save-message) results in '~/Mail/sa.spam' being offered as default filename, although it should be '=sa.spam'.
This only happens when this save-hook entry is interpreted from the config file. When setting it inside mutt (after pressing ':' and entering the same exact command as above), it acts correct, and offers the '=sa.spam' directory as default for saving this kind of messages.

--------------------------------------------------------------------------------
2010-03-27 21:23:50 UTC Gary Johnson
* Added comment:
{{{
On 2010-03-27, Mutt wrote:

My guess is that you are setting the 'folder' variable in your
.muttrc _after_ you define the save-hook.  From the mutt manual
(version 1.4.2.2):

    6.3.48.  folder

    Type: path
    Default: "~/Mail"

    Specifies the default location of your mailboxes.  A `+' or `='
    at the beginning of a pathname will be expanded to the value of
    this variable.  Note that if you change this variable from the
    default value you need to make sure that the assignment occurs
    before you use `+' or `=' for any other variables since
    expansion takes place during the `set' command.

If that is the problem, then to avoid it, set the 'folder' variable
early in your .muttrc and before you use either the '+' or '='
shortcut.

HTH,
Gary
}}}

--------------------------------------------------------------------------------
2010-03-28 09:31:51 UTC daniell
* Added comment:
Wow, you're totally right.[[BR]]
I'm sorry for the noise...

* resolution changed to invalid
* status changed to closed
