Ticket:  1834
Status:  closed
Summary: send-hook matching limitations for new mails with $autoedit=yes

Reporter: Jay Maynard <jmaynard@conmicro.cx>
Owner:    mutt-dev

Opened:       2004-03-21 20:42:47 UTC
Last Updated: 2009-04-20 16:37:12 UTC

Priority:  minor
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6i
Severity: normal

-- Please type your report below this line
The send-hook line in my local config file below is ineffective. The From:
address is not changed when a destination address at gentoo.org is
specified.


-- System Information
System Version: Linux thebrain 2.4.21-alpha-r3 #1 Mon Feb 16 11:50:19 CST 2004 alpha EV56  GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/alpha-unknown-linux-gnu/3.3.2/specs
Configured with: /var/tmp/portage/gcc-3.3.2-r2/work/gcc-3.3.2/configure --prefix=/usr --bindir=/usr/alpha-unknown-linux-gnu/gcc-bin/3.3 --includedir=/usr/lib/gcc-lib/alpha-unknown-linux-gnu/3.3.2/include --datadir=/usr/share/gcc-data/alpha-unknown-linux-gnu/3.3 --mandir=/usr/share/gcc-data/alpha-unknown-linux-gnu/3.3/man --infodir=/usr/share/gcc-data/alpha-unknown-linux-gnu/3.3/info --enable-shared --host=alpha-unknown-linux-gnu --target=alpha-unknown-linux-gnu --with-system-zlib --enable-languages=c,c++,f77,objc --enable-threads=posix --enable-long-long --disable-checking --enable-cstdio=stdio --enable-clocale=generic --enable-__cxa_atexit --enable-version-specific-runtime-libs --with-gxx-include-dir=/usr/lib/gcc-lib/alpha-unknown-linux-gnu/3.3.2/include/g++-v3 --with-local-prefix=/usr/local --enable-shared --enable-nls --without-included-gettext --disable-multilib
Thread model: posix
gcc version 3.3.2 20031022 (Gentoo Linux 3.3.2-r2, propolice)

- CFLAGS
-Wall -pedantic -Wall -pedantic -O2 -mcpu=ev56 -pipe

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.21-alpha-r3 (alpha) [using slang 10409]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
-USE_FCNTL  +USE_FLOCK
+USE_POP  -USE_IMAP  -IMAP_EDIT_THREADS  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
-HAVE_REGCOMP  +USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Maildir"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc/mutt"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.6.dw.mbox-hook.1
rr.compressed
patch-1.5.5.1.cd.edit_threads.9.5

--- Begin /home/jmaynard/.muttrc
set abort_unmodified=yes	# automatically abort replies if I don't
set arrow_cursor		# use -> instead of hiliting the whole line
set autoedit			# go to the editor right away when composing
set noconfirmappend		# don't ask me if i want to append to mailboxes
set delete=yes			# purge deleted messages without asking
set edit_hdrs			# let me edit the message header when composing
set hdr_format="%4C %Z %{%m/%d} %-15.15F (%4c) %s" # format of the index
set hdrs			# include `my_hdr' lines in outgoing messages
set help			# show the help lines
set include			# always include messages when replying
set mail_check=10		# how often to poll for new mail
set mbox_type=mbox		#turn off maildir, dammit
set move=no			# don't ask about moving messages, just do it
set pager_index_lines=6		# how many index lines to show in the pager
set postponed=+postponed	# mailbox to store postponed messages in
set print=yes			# ask me if I really want to print messages
set noprompt_after	# ask me for a command after the external pager exits
set read_inc=25			# show progress when reading a mailbox
set record=			# default location to save outgoing mail (Fcc:)
set reply_to			# always use reply-to if present
set reply_regexp="^(re:[ \t]*)+"# how to identify replies in the subject:
set reverse_name		# use my address as it appears in the message
set nosave_empty		# remove files when no messages are left
set sendmail="/usr/lib/sendmail -oi -oem"	# how to deliver mail
set signature="~/.signature"	# file which contains my signature
set spoolfile='/var/spool/mail/jmaynard'	# where my new mail is located
set nouse_domain		# don't qualify local addresses with $domain
set write_inc=25		# show progress while writing mailboxes
ignore *		# this means "ignore all lines by default"
unignore	from: subject to cc mail-followup-to \
		date x-mailer x-url # this shows how nicely wrap long lines
mono quoted bold
bind generic "\e<" first-entry	# emacs-like bindings for moving to top/bottom
bind generic \e> last-entry
bind generic { top-page
bind generic } bottom-page
bind generic \177 last-entry
macro index \cb |urlview\n	# simulate the old browse-url function
macro index S s+spam\n
macro pager S s+spam\n
bind pager G bottom	# just like vi and less
send-hook '~t gentoo\.org' 'my_hdr From: Jay Maynard <jmaynard@gentoo.org>'
alias mutt-dev Mutt Development List <mutt-dev@cs.hmc.edu> # power users
alias mutt-users Mutt User List <mutt-users@cs.hmc.edu>
alias mutt-announce Mutt Announcement List <mutt-announce@cs.hmc.edu>
alias wmaker WindowMaker Mailing List <wmaker@eosys.com>
mailboxes ! +mutt-dev +mutt-users +open-pgp +wmaker +hurricane +vim +ietf
hdr_order date from subject to cc
lists mutt-dev mutt-users
auto_view application/x-gunzip
--- End /home/jmaynard/.muttrc


--- Begin /etc/mutt/Muttrc
set mbox_type=Maildir
set folder=~/.maildir
set spoolfile=~/.maildir/
set record=~/.maildir-sent/
unset mbox
set move=no
set index_format="%4C %Z %{%b %d} %-16.16L  %s"
--- End /etc/mutt/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-03-21 20:42:47 UTC jmaynard@conmicro.cx
* Added comment:
{{{
Package: mutt
Version: 1.5.6i
Severity: normal

-- Please type your report below this line
The send-hook line in my local config file below is ineffective. The From:
address is not changed when a destination address at gentoo.org is
specified.


-- System Information
System Version: Linux thebrain 2.4.21-alpha-r3 #1 Mon Feb 16 11:50:19 CST 2004 alpha EV56  GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/alpha-unknown-linux-gnu/3.3.2/specs
Configured with: /var/tmp/portage/gcc-3.3.2-r2/work/gcc-3.3.2/configure --prefix=/usr --bindir=/usr/alpha-unknown-linux-gnu/gcc-bin/3.3 --includedir=/usr/lib/gcc-lib/alpha-unknown-linux-gnu/3.3.2/include --datadir=/usr/share/gcc-data/alpha-unknown-linux-gnu/3.3 --mandir=/usr/share/gcc-data/alpha-unknown-linux-gnu/3.3/man --infodir=/usr/share/gcc-data/alpha-unknown-linux-gnu/3.3/info --enable-shared --host=alpha-unknown-linux-gnu --target=alpha-unknown-linux-gnu --with-system-zlib --enable-languages=c,c++,f77,objc --enable-threads=posix --enable-long-long --disable-checking --enable-cstdio=stdio --enable-clocale=generic --enable-__cxa_atexit --enable-version-specific-runtime-libs --with-gxx-include-dir=/usr/lib/gcc-lib/alpha-unknown-linux-gnu/3.3.2/include/g++-v3 --with-local-prefix=/usr/local --enable-shared --enable-nls --without-included-gettext --disable-multilib
Thread model: posix
gcc version 3.3.2 20031022 (Gentoo Linux 3.3.2-r2, propolice)

- CFLAGS
-Wall -pedantic -Wall -pedantic -O2 -mcpu=ev56 -pipe

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.21-alpha-r3 (alpha) [using slang 10409]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
-USE_FCNTL  +USE_FLOCK
+USE_POP  -USE_IMAP  -IMAP_EDIT_THREADS  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
-HAVE_REGCOMP  +USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Maildir"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc/mutt"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.6.dw.mbox-hook.1
rr.compressed
patch-1.5.5.1.cd.edit_threads.9.5

--- Begin /home/jmaynard/.muttrc
set abort_unmodified=yes	# automatically abort replies if I don't
set arrow_cursor		# use -> instead of hiliting the whole line
set autoedit			# go to the editor right away when composing
set noconfirmappend		# don't ask me if i want to append to mailboxes
set delete=yes			# purge deleted messages without asking
set edit_hdrs			# let me edit the message header when composing
set hdr_format="%4C %Z %{%m/%d} %-15.15F (%4c) %s" # format of the index
set hdrs			# include `my_hdr' lines in outgoing messages
set help			# show the help lines
set include			# always include messages when replying
set mail_check=10		# how often to poll for new mail
set mbox_type=mbox		#turn off maildir, dammit
set move=no			# don't ask about moving messages, just do it
set pager_index_lines=6		# how many index lines to show in the pager
set postponed=+postponed	# mailbox to store postponed messages in
set print=yes			# ask me if I really want to print messages
set noprompt_after	# ask me for a command after the external pager exits
set read_inc=25			# show progress when reading a mailbox
set record=			# default location to save outgoing mail (Fcc:)
set reply_to			# always use reply-to if present
set reply_regexp="^(re:[ \t]*)+"# how to identify replies in the subject:
set reverse_name		# use my address as it appears in the message
set nosave_empty		# remove files when no messages are left
set sendmail="/usr/lib/sendmail -oi -oem"	# how to deliver mail
set signature="~/.signature"	# file which contains my signature
set spoolfile='/var/spool/mail/jmaynard'	# where my new mail is located
set nouse_domain		# don't qualify local addresses with $domain
set write_inc=25		# show progress while writing mailboxes
ignore *		# this means "ignore all lines by default"
unignore	from: subject to cc mail-followup-to \
		date x-mailer x-url # this shows how nicely wrap long lines
mono quoted bold
bind generic "\e<" first-entry	# emacs-like bindings for moving to top/bottom
bind generic \e> last-entry
bind generic { top-page
bind generic } bottom-page
bind generic \177 last-entry
macro index \cb |urlview\n	# simulate the old browse-url function
macro index S s+spam\n
macro pager S s+spam\n
bind pager G bottom	# just like vi and less
send-hook '~t gentoo\.org' 'my_hdr From: Jay Maynard <jmaynard@gentoo.org>'
alias mutt-dev Mutt Development List <mutt-dev@cs.hmc.edu> # power users
alias mutt-users Mutt User List <mutt-users@cs.hmc.edu>
alias mutt-announce Mutt Announcement List <mutt-announce@cs.hmc.edu>
alias wmaker WindowMaker Mailing List <wmaker@eosys.com>
mailboxes ! +mutt-dev +mutt-users +open-pgp +wmaker +hurricane +vim +ietf
hdr_order date from subject to cc
lists mutt-dev mutt-users
auto_view application/x-gunzip
--- End /home/jmaynard/.muttrc


--- Begin /etc/mutt/Muttrc
set mbox_type=Maildir
set folder=~/.maildir
set spoolfile=~/.maildir/
set record=~/.maildir-sent/
unset mbox
set move=no
set index_format="%4C %Z %{%b %d} %-16.16L  %s"
--- End /etc/mutt/Muttrc
}}}

--------------------------------------------------------------------------------
2004-04-04 06:36:00 UTC Christoph Berg <cb@heim-d.uni-sb.de>
* Added comment:
{{{
Re: jmaynard@conmicro.cx in <20040321164247.64AD3260065@thebrain.conmicro.cx>
> The send-hook line in my local config file below is ineffective. The From:
> address is not changed when a destination address at gentoo.org is
> specified.

The problem is "set autoedit". From manual.txt:

 Note: the send-hook's are only executed ONCE after getting the initial
 list of recipients.  Adding a recipient after replying or editing the
 message will NOT cause any send-hook to be executed.  Also note that
 my_hdr commands which modify recipient headers, or the message's
 subject, don't have any effect on the current message when executed
 from a send-hook.

This means that send-hooks cannot be used with $autoedit set. The manual
should be updated to reflect that.

> --- Begin /home/jmaynard/.muttrc
> set autoedit			# go to the editor right away when composing
> send-hook '~t gentoo\.org' 'my_hdr From: Jay Maynard <jmaynard@gentoo.org>'
> --- End /home/jmaynard/.muttrc

Christoph
-- 
cb@df7cb.de | http://www.df7cb.de/
}}}

--------------------------------------------------------------------------------
2004-04-04 11:24:08 UTC Christoph Berg <cb@heim-d.uni-sb.de>
* Added comment:
{{{
Re: To jmaynard@conmicro.cx in <20040403103600.GA2404@df7cb.de>
> This means that send-hooks cannot be used with $autoedit set. The manual
> should be updated to reflect that.

Here's the patch: (the bit about Mutt entering the compose menu before
starting the editor was wrong, too, if I haven't mis-tuned my config
here)

diff -ur ../MUTT/mutt/doc/manual.sgml.head mutt/doc/manual.sgml.head
--- ../MUTT/mutt/doc/manual.sgml.head	2004-02-13 16:08:32.000000000 +0100
+++ mutt/doc/manual.sgml.head	2004-04-03 17:20:07.000000000 +0200
@@ -485,7 +485,7 @@
in greater detail in the next chapter <ref id="forwarding_mail"
name="``Forwarding and Bouncing Mail''">.
 
-Mutt will then enter the <em/compose/ menu and prompt you for the
+When sending a message, Mutt will prompt you for the
recipients to place on the ``To:'' header field.  Next, it will ask
you for the ``Subject:'' field for the message, providing a default if
you are replying to or forwarding a message.  See also <ref id="askcc"
@@ -1414,7 +1414,8 @@
 
<bf/Note:/ the send-hook's are only executed ONCE after getting the initial
list of recipients.  Adding a recipient after replying or editing the
-message will NOT cause any send-hook to be executed.  Also note that
+message will NOT cause any send-hook to be executed, similarly if <ref
+id="autoedit" name="&dollar;autoedit"> is set.  Also note that
my_hdr commands which modify recipient headers, or the message's
subject, don't have any effect on the current message when executed
from a send-hook.
diff -ur ../MUTT/mutt/init.h mutt/init.h
--- ../MUTT/mutt/init.h	2004-02-13 16:08:31.000000000 +0100
+++ mutt/init.h	2004-04-03 17:19:56.000000000 +0200
@@ -240,6 +240,9 @@
  ** editing the body of your message.
  ** .pp
  ** Also see ``$$fast_reply''.
+  ** .pp
+  ** \fBNote:\fP when this option is set, send-hooks do not work when
+  ** composing a new message, as the initial list of recipients is empty.
  */
  { "auto_tag",		DT_BOOL, R_NONE, OPTAUTOTAG, 0 },
  /*


Christoph
-- 
cb@df7cb.de | http://www.df7cb.de/
}}}

--------------------------------------------------------------------------------
2004-04-07 03:03:36 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# no followups
close 1460
# not an upstream bug
close 1830
tags 1229 stable
merge 1235 1708 1815
tags 1652 patch
tags 1834 patch
tags 1838 patch
tags 1845 patch
}}}

--------------------------------------------------------------------------------
2004-04-13 14:02:17 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2004-04-03 17:24:08 +0200, Christoph Berg wrote:

>> This means that send-hooks cannot be used with $autoedit set.
>> The manual should be updated to reflect that.

> Here's the patch: (the bit about Mutt entering the compose menu before
> starting the editor was wrong, too, if I haven't mis-tuned my config
> here)

Technically, this documentation change is wrong: Send-hooks are
executed when composing a message, but there isn't terribly much
they can depend on.  (Note, however, that you could still have hooks
matching subject tags that distinguish replies and forwarded
messages, while a ~A hook could be used for newly composed
messages.)

Regards,
-- 
Thomas Roessler			      <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2004-04-13 15:11:52 UTC Christoph Berg <cb@heim-d.uni-sb.de>
* Added comment:
{{{
Re: Thomas Roessler in <20040412190217.GB5807@raktajino.does-not-exist.org>
> Technically, this documentation change is wrong: Send-hooks are
> executed when composing a message, but there isn't terribly much
> they can depend on.  (Note, however, that you could still have hooks
> matching subject tags that distinguish replies and forwarded
> messages, while a ~A hook could be used for newly composed
> messages.)

Hello,

I wanted to keep the note short, but it should of course be correct -
here is an updated version.

New patch:

diff -ur ../MUTT/mutt/doc/manual.sgml.head mutt/doc/manual.sgml.head
--- ../MUTT/mutt/doc/manual.sgml.head	2004-04-12 21:55:25.000000000 +0200
+++ mutt/doc/manual.sgml.head	2004-04-12 21:56:48.000000000 +0200
@@ -485,7 +485,7 @@
in greater detail in the next chapter <ref id="forwarding_mail"
name="``Forwarding and Bouncing Mail''">.
 
-Mutt will then enter the <em/compose/ menu and prompt you for the
+When sending a message, Mutt will prompt you for the
recipients to place on the ``To:'' header field.  Next, it will ask
you for the ``Subject:'' field for the message, providing a default if
you are replying to or forwarding a message.  See also <ref id="askcc"
@@ -1414,7 +1414,8 @@
 
<bf/Note:/ the send-hook's are only executed ONCE after getting the initial
list of recipients.  Adding a recipient after replying or editing the
-message will NOT cause any send-hook to be executed.  Also note that
+message will NOT cause any send-hook to be executed, similarly if <ref
+id="autoedit" name="&dollar;autoedit"> is set.  Also note that
my_hdr commands which modify recipient headers, or the message's
subject, don't have any effect on the current message when executed
from a send-hook.
diff -ur ../MUTT/mutt/init.h mutt/init.h
--- ../MUTT/mutt/init.h	2004-04-12 21:55:24.000000000 +0200
+++ mutt/init.h	2004-04-12 22:07:36.000000000 +0200
@@ -240,6 +240,10 @@
  ** editing the body of your message.
  ** .pp
  ** Also see ``$$fast_reply''.
+  ** .pp
+  ** \fBNote:\fP when this option is set, you cannot use send-hooks that depend
+  ** on the recipients when composing a new (non-reply) message, as the initial
+  ** list of recipients is empty.
  */
  { "auto_tag",		DT_BOOL, R_NONE, OPTAUTOTAG, 0 },
  /*

Difference to the previous patch:

diff -u mutt/init.h mutt/init.h
--- mutt/init.h	2004-04-03 17:19:56.000000000 +0200
+++ mutt/init.h	2004-04-12 22:07:36.000000000 +0200
@@ -241,8 +241,9 @@
  ** .pp
  ** Also see ``$$fast_reply''.
  ** .pp
-  ** \fBNote:\fP when this option is set, send-hooks do not work when
-  ** composing a new message, as the initial list of recipients is empty.
+  ** \fBNote:\fP when this option is set, you cannot use send-hooks that depend
+  ** on the recipients when composing a new (non-reply) message, as the initial
+  ** list of recipients is empty.
  */
  { "auto_tag",		DT_BOOL, R_NONE, OPTAUTOTAG, 0 },
  /*

Christoph
-- 
cb@df7cb.de | http://www.df7cb.de/
}}}

--------------------------------------------------------------------------------
2005-08-17 13:25:06 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Christoph,

 On Monday, April 12, 2004 at 10:11:52 PM +0200, Christoph Berg wrote:

> Note: the send-hook's are only executed ONCE after getting the initial
> list of recipients. Adding a recipient after replying or editing the
> message will NOT cause any send-hook to be executed, similarly if
> $autoedit is set.

    Hum... "similarly if $autoedit is set", that's not clear nor
specific enough. May I suggest:

| [...] Adding a recipient later, while editing the message in $editor
| or in the compose menu, will NOT cause any send-hook to be executed.
| And when composing a new mail with $autoedit set, the said initial
| list of recipients is empty, and thus when send-hooks are executed
| they can't match future recipients (unless the pattern is ".*").


Bye!	Alain.
-- 
Mutt muttrc tip to send mails in best adapted first necessary and sufficient
charset (version for Western Latin-1/Latin-9/CP-850/CP-1252 terminal users):
set send_charset="us-ascii:iso-8859-1:iso-8859-15:windows-1252:utf-8"
}}}

--------------------------------------------------------------------------------
2005-08-17 13:50:23 UTC ab
* Added comment:
{{{
Hello Christoph,
 Retitle "send-hook to change From: doesn't work in 1.5.6i" to "send-hook matching limitations for new mails with $autoedit=yes" and tag doc-bug.
}}}

--------------------------------------------------------------------------------
2009-04-20 16:37:12 UTC Christoph Berg <cb@df7cb.de>
* Added comment:
(In [39fee3a9d034]) Better document that some send-hooks can't work with $autoedit. Closes #1834.

* resolution changed to fixed
* status changed to closed
