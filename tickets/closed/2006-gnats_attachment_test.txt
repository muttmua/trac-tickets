Ticket:  2006
Status:  closed
Summary: gnats attachment test

Reporter: brendan@kublai.com
Owner:    mutt-dev

Opened:       2005-07-28 22:12:10 UTC
Last Updated: 2005-07-28 22:19:25 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Attachment handling in gnats is a mess.
>How-To-Repeat:
>Fix:
the attached file may help
}}}

--------------------------------------------------------------------------------
2005-07-29 16:19:25 UTC brendan
* Added comment:
{{{
tlr has taken over the testing.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:42:58 UTC 
* Added attachment gnatsfix.pl
* Added comment:
gnatsfix.pl
