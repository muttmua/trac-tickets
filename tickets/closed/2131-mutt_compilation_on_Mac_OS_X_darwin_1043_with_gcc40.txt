Ticket:  2131
Status:  closed
Summary: mutt compilation on Mac OS X (darwin) 10.4.3 with gcc-4.0

Reporter: a.lathrop@gmail.com
Owner:    mutt-dev

Opened:       2005-11-09 05:53:27 UTC
Last Updated: 2005-11-12 07:07:45 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Mutt 1.4.2.1i fails to compile giving error:

Error: Target com.apple.build returned: shell command "cd
"/opt/local/var/db/dports/build/file._opt_local_var_db_dports_sources_rsync.rsync.opendarwin.org_dpupdate_dports_mail_mutt/work/mutt-1.4.2.1"
&& make all" returned error 2
Command output: cd .. \
  && CONFIG_FILES=doc/Makefile CONFIG_HEADERS= /bin/sh ./config.status
config.status: creating doc/Makefile
config.status: executing default-1 commands
config.status: executing default-2 commands
gcc -E -I. -I.. -I/opt/local/include -I.. -DSYSCONFDIR=\"\"
-DBINDIR=\"/opt/local/bin\" -DHAVE_CONFIG_H=1 -I/opt/local/include -I../intl
-D_MAKEDOC -C  ../init.h | ../makedoc -m |         \
        cat ./muttrc.man.head - ./muttrc.man.tail\
        > muttrc.man
touch stamp-doc-man
test -f manual.html || make manual.html || cp ./manual*.html ./
test -f manual.txt || make manual.txt || cp ./manual.txt ./
Making all in contrib
cd .. \
  && CONFIG_FILES=contrib/Makefile CONFIG_HEADERS= /bin/sh ./config.status
config.status: creating contrib/Makefile
config.status: executing default-1 commands
config.status: executing default-2 commands
make[2]: Nothing to be done for `all'.
cd . \
  && CONFIG_FILES=Makefile CONFIG_HEADERS= /bin/sh ./config.status
config.status: creating Makefile
config.status: executing default-1 commands
config.status: executing default-2 commands
gcc -DPKGDATADIR=\"/opt/local/share/mutt\" -DSYSCONFDIR=\"/opt/local/etc\"    
-DBINDIR=\"/opt/local/bin\" -DMUTTLOCALEDIR=\"/opt/local/share/locale\" 
-DHAVE_CONFIG_H=1 -I. -I.  -Iintl  -I/opt/local/include -I./intl
-I/opt/local/include  -g -O2 -c addrbook.c
In file included from mutt_menu.h:23,
                 from addrbook.c:20:
keymap.h:83: error: array type has incomplete element type
make[2]: *** [addrbook.o] Error 1
make[1]: *** [all-recursive] Error 1
make: *** [all-recursive-am] Error 2

-----

changing 'extern struct mapping_t Menus[];'
to
'extern struct mapping_t *Menus;'
in keymap.h:83, and making a similar change in keymap.c:31 will allow mutt to compile fine; however it causes a segfault at runtime.

this is outlined in the bug report here:
http://bugzilla.opendarwin.org/show_bug.cgi?id=4297

the problem is the same whether one is compiling directly from source, or when attempting to use darwinports
'sudo port install mutt'



>How-To-Repeat:
simply typing 'make' after doing './configure' produces the error
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-11-13 01:07:45 UTC brendan
* Added comment:
{{{
Fixed in 1.5.11 (or possibly earlier).
}}}

* resolution changed to fixed
* status changed to closed
