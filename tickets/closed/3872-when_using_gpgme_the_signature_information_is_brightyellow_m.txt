Ticket:  3872
Status:  closed
Summary: when using gpgme, the signature information is brightyellow making it difficult to read

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2016-09-11 06:54:34 UTC
Last Updated: 2016-09-20 22:52:58 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
How to reproduce:

(1) set crypt_use_gpgme=yes
(2) open mutt and check a signed email, the verification output is brightyellow

While:
(1) set crypt_use_gpgme=no
(2) open mutt and check a signed email, the verification output is the same color as the text.

We would like to get the second behavior for gpgme too, because otherwise the text is too difficult to read

--------------------------------------------------------------------------------
2016-09-11 17:20:16 UTC antonio@dyne.org
* Added comment:
I've investigated and I'm providing you with the same patch that I'll add to the debian package.

It seems that the output of pgp.c is written with state_puts(), while the output of crypt-gpgme.c has state_attach_puts().

It might make sense to use state_attach_puts() for the lines with [--] but we still want to preserve the same behavior of pgp.c for the content.

Another option is to provide a feature to have a different color for crypto output (it might be called crypto and we could provide state_crypto_puts()); I might provide a patch for this particular feature next weekend (I'll file a separate bug for that).

--------------------------------------------------------------------------------
2016-09-11 17:20:34 UTC antonio@dyne.org
* Added attachment 837372-do-not-color-gpgme-output.patch

--------------------------------------------------------------------------------
2016-09-20 02:57:25 UTC kevin8t8
* Added comment:
Thanks Antonio.  The analysis looks correct to me.  We only want to tag the "[--..." output with the attachment marker.

I'll take a closer look at the patch tomorrow.

--------------------------------------------------------------------------------
2016-09-20 22:52:58 UTC Antonio Radici <antonio@dyne.org>
* Added comment:
In [changeset:"405cbc43c3acc6cec944110f4a8d8b0b1977d4f8" 6793:405cbc43c3ac]:
{{{
#!CommitTicketReference repository="" revision="405cbc43c3acc6cec944110f4a8d8b0b1977d4f8"
Use body color for gpgme output. (closes #3872)

When switching from pgp_* commands to crypt_use_gpgme=yes, Peter
Colberg noticed that the output was colored 'brightyellow'.

The issue is that crypt-gpgme.c uses state_attach_puts in various
places where it should use state_puts to maintain compatibility with
the previous behavior in pgp.c.
}}}

* resolution changed to fixed
* status changed to closed
