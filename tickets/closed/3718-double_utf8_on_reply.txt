Ticket:  3718
Status:  closed
Summary: double utf-8 on "reply"

Reporter: powerman
Owner:    mutt-dev

Opened:       2014-12-10 16:41:37 UTC
Last Updated: 2014-12-17 22:36:04 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I've just received email (in Russian), which looks correctly inside mutt, and it headers also looks good,
but when I press "r" to reply (using vim as $EDITOR) I got double-encoded utf-8 unreadable text in vim
(actually it may be something else, not double-encoded utf-8, I didn't tried to decode it, but looks very similar).

I've bisect this email: removed all extra headers, multipart, base64, replaced all Russian letters with А(A),
all English letters with Z, and removed most of email content. Now I've ~2KB mbox file (attached) which have this issue.
If I remove one more letter from content (I've tried to remove first, last and one in the middle) then this issue disappears.

So, to see this bug you should:
{{{
$ /usr/bin/mutt -f /tmp/mutt-vim-reply-bug.mbox
<Enter> (enter mbox)
<Enter> (view email)
r (reply)
y (quote)
}}}
and then in Vim I get something like:
{{{
>  п░п░п░п░п░п░ п░п░п░п░п░ …
}}}
instead of
{{{
>  АААААА ААААА …
}}}

If I just open this mbox file in vim - it opens just fine, so I suppose this is bug in mutt.

I'm using Gentoo Linux, rxvt-unicode-9.20, mutt-1.5.22, vim-7.4.542.

--------------------------------------------------------------------------------
2014-12-10 16:42:00 UTC powerman
* Added attachment mutt-vim-reply-bug.mbox

--------------------------------------------------------------------------------
2014-12-14 00:00:11 UTC kevin8t8
* Added comment:
Sorry I can't duplicate your problem, it's showing up correctly for me (I'm also using vim and rxvt-unicode).

When editing the reply, what does vim show for
{{{
:set encoding? fileencoding? fileencodings?
}}}

When you save and exit the editor, what does mutt show for the message encoding in the attachments list?

Are you using any filters between mutt and vim (what is editor set to?)

Lastly, what's the default locale for your system?

--------------------------------------------------------------------------------
2014-12-14 04:30:47 UTC powerman
* Added comment:
Replying to [comment:1 kevin8t8]:
> When editing the reply, what does vim show for 
{{{
encoding=utf-8
fileencoding=koi8-r
fileencodings=ucs-bom,utf-8,koi8-r,default
}}}

In ~/.vimrc I've this:
{{{
set fileencodings=ucs-bom,utf-8,koi8-r,default
}}}

If I remove "koi8-r" from that list then reply text in vim looks correctly, but:
* I can't ":wq" to save it - vim says option 'readonly' is enabled, not sure why
* there are one extra symbol at end of quoted line: "?" (ascii 3F)
* vim settings in this case are:
{{{
encoding=utf-8
fileencoding=
fileencodings=ucs-bom,utf-8,default
}}}

> When you save and exit the editor, what does mutt show for the message encoding in the attachments list?
{{{
[text/plain, quoted, utf-8, 2.1K]
}}}

Also, if I view unsent reply's text in mutt there are extra "\320" at end of quoted line, but except that text looks correctly.

> Are you using any filters between mutt and vim (what is editor set to?)

$EDITOR=/usr/bin/vi (which is symlink to vim).
I don't use any filters in between, but I have some code executed on `autocmd FileType mail` - disabling it doesn't affect this issue.

> Lastly, what's the default locale for your system?
{{{
$ locale -a
C
en_US
en_US.iso88591
en_US.utf8
POSIX
ru_RU.utf8
$ locale 
LANG=ru_RU.utf8
LC_CTYPE="ru_RU.utf8"
LC_NUMERIC=C
LC_TIME="ru_RU.utf8"
LC_COLLATE="ru_RU.utf8"
LC_MONETARY="ru_RU.utf8"
LC_MESSAGES="ru_RU.utf8"
LC_PAPER="ru_RU.utf8"
LC_NAME="ru_RU.utf8"
LC_ADDRESS="ru_RU.utf8"
LC_TELEPHONE="ru_RU.utf8"
LC_MEASUREMENT="ru_RU.utf8"
LC_IDENTIFICATION="ru_RU.utf8"
LC_ALL=
}}}

--------------------------------------------------------------------------------
2014-12-14 05:35:17 UTC kevin8t8
* Added comment:
So, something is causing vim to first reject the file as utf-8 and accept it as koi8-r.  In the first case, encoding=utf-8 means vim has tried to use iconv to convert the koi8-r to utf-8 internally (and will convert it back when you write it out.)  So that's why the file looks garbled.

In the second case, where you remove koi8-r as an option, the empty 'fileencoding' indicates vim doesn't like any of the choices in fileencodings.  In that case, vim shouldn't do any conversion at all, although I don't know why that last "?" is there.

I wonder if you have something in your attribution, or signature, or From line that is koi8-r?  Have you tried paring down your muttrc and using 'mutt -n' and seeing if there is some setting there causing a non utf-8 character to be added in the reply?

--------------------------------------------------------------------------------
2014-12-14 07:58:54 UTC powerman
* Added comment:
Replying to [comment:3 kevin8t8]:
> So, something is causing vim to first reject the file as utf-8 and accept it as koi8-r.

Yeah. I've used this to get unmodified reply content passed from mutt to vim:
{{{
$ EDITOR="cp -t ." /usr/bin/mutt -n -f /tmp/mutt-vim-reply-bug.mbox
}}}
I'll attach this file, but, in short, looks like last symbol of quoted line is broken:
{{{
$ cat /tmp/mutt-vim-reply-bug.mbox
...
АААА ААА А ААААААА АА АААААААААА АААААА, ААА АААААААА А ААААААААААААА
$ cat mutt-home-1000-14115-1022183079317066769
...
АААААА ААА А ААААААА АА АААААААААА АААААА, ААА АААААААА А ААААААААААААÐ
}}}
(second output contain two more "A" at beginning because this is quoted line and it begins with extra two symbols "> ", thus shifting cat output).

According to hexdump output last symbol of quoted line was output only partially - first of two bytes was included while second byte was lost:

{{{
$ hexdump-esr /tmp/mutt-vim-reply-bug.mbox
...
0970  90 d0 90 d0 90 d0 90 d0  90 d0 90 d0 90 d0 90 d0  ........ ........
0980  90 d0 90 d0 90 d0 90 0a  0a                       ........ .
$ hexdump-esr mutt-home-1000-14115-1022183079317066769
...
0840  90 d0 90 d0 90 d0 90 d0  90 d0 90 d0 0a 0a 0a 2d  ........ .......-
0850  2d 20 0a 09 09 09 57 42  52 2c 20 41 6c 65 78 2e  - ....WB R, Alex.
0860  0a                                                .
}}}
Last symbol is bytes 0985-0986 in mbox file and only byte 084B in reply file.

> I wonder if you have something in your attribution, or signature, or From line that is koi8-r?

No.

> Have you tried paring down your muttrc and using 'mutt -n' and seeing if there is some setting there causing a non utf-8 character to be added in the reply?

As you see above `-n` didn't make any difference. Also I've tried to remove ~/.muttrc and run mutt -n - no difference too, so my configuration isn't affect this issue.

--------------------------------------------------------------------------------
2014-12-14 07:59:37 UTC powerman
* Added attachment mutt-home-1000-14115-1022183079317066769

--------------------------------------------------------------------------------
2014-12-14 17:34:14 UTC kevin8t8
* Added comment:
Thank you for the detailed analysis.  It looks like second attachment you added was edited and then saved (adding the "Hi!" at the top).  Would you mind uploading a pristine version of the file, as output by mutt and before vim has modified it?  (Just copy the file out of /tmp).

--------------------------------------------------------------------------------
2014-12-14 18:50:38 UTC powerman
* Added comment:
It wasn't edited. The "Hi!" part was added by feature provided by signin-signoff.patch applied to my mutt. If you think this bug with utf-8 may happens because of this patch - I can try to recompile mutt without it. I'll attach both my extra patches, so you can check them.

--------------------------------------------------------------------------------
2014-12-14 18:51:06 UTC powerman
* Added attachment signinoff-1.5.21.patch

--------------------------------------------------------------------------------
2014-12-14 18:51:13 UTC powerman
* Added attachment smime.patch

--------------------------------------------------------------------------------
2014-12-14 23:03:07 UTC kevin8t8
* Added comment:
I don't see a problem with the patches, however I'm still unable to duplicate this problem.  It looks like an off-by-one error in including the reply, but there's a lot of code involved there, and a lot of different paths.

Would you mind compiling a vanilla 1.5.23 release tarball, and then run it with "-n -F /dev/null" and see if the problem still occurs?

--------------------------------------------------------------------------------
2014-12-17 21:06:04 UTC powerman
* Added comment:
Thanks for hint about vanilla - I've checked vanilla 1.5.22 and it doesn't have this issue!
Then I take a closer look at Gentoo ebuild for mutt and found a dozen Gentoo-specific patches applied with comment "the big feature patches that upstream doesn't want to include, but nearly every distro has due to their usefulness", one of which (07-quote.patch, I'll attach it) introduce this bug.
Thanks for your help, I'll report this issue to Gentoo.

--------------------------------------------------------------------------------
2014-12-17 21:06:30 UTC powerman
* Added attachment 07-quote.patch

--------------------------------------------------------------------------------
2014-12-17 21:25:07 UTC powerman
* Added comment:
https://bugs.gentoo.org/show_bug.cgi?id=532890

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2014-12-17 22:36:04 UTC kevin8t8
* Added comment:
I added this note to the gentoo bug, but just to record it here too: the patch uses a buffer of length 2048 to hold each line.  His sample email had a single line of length 2047.  So when the patch added "> " the snprintf was truncating the last byte.
