Ticket:  2143
Status:  closed
Summary: sort-mailbox should also work in pager

Reporter: andreas.amann@tyndall.ie
Owner:    mutt-dev

Opened:       2005-11-28 13:10:52 UTC
Last Updated: 2010-08-06 00:04:30 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
I often need to change the sort order of the messages while in the pager, i.e. I want to put the following into muttrc:
bind pager o sort-mailbox
bind pager O sort-reverse

The following patch solves this problem for me, but I don't have enough understanding of the source, to check if its correct:
diff -u mutt-1.5.11/curs_main.c mutt-1.5.11.aa/curs_main.c
--- mutt-1.5.11/curs_main.c     2005-09-07 09:19:43.000000000 +0100
+++ mutt-1.5.11.aa/curs_main.c  2005-11-28 12:33:26.000000000 +0000
@@ -928,6 +928,12 @@
            set_option (OPTSEARCHINVALID);
          }
        }
+       if (menu->menu == MENU_PAGER)
+        {
+          op = OP_DISPLAY_MESSAGE;
+          continue;
+        }
+
        break;

       case OP_TAG:
diff -u mutt-1.5.11/functions.h mutt-1.5.11.aa/functions.h
--- mutt-1.5.11/functions.h     2005-07-24 17:56:42.000000000 +0100
+++ mutt-1.5.11.aa/functions.h  2005-11-28 12:17:24.000000000 +0000
@@ -186,6 +186,8 @@
   { "search-next",     OP_SEARCH_NEXT,                 "n" },
   { "next-thread",     OP_MAIN_NEXT_THREAD,            "\016" },
   { "next-subthread",  OP_MAIN_NEXT_SUBTHREAD,         "\033n" },
+  { "sort-mailbox",            OP_SORT,                        "o" },
+  { "sort-reverse",            OP_SORT_REVERSE,                "O" },
   { "print-message",   OP_PRINT,                       "p" },
   { "previous-thread", OP_MAIN_PREV_THREAD,            "\020" },
   { "previous-subthread",OP_MAIN_PREV_SUBTHREAD,       "\033p" },
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2007-03-27 18:07:26 UTC 
* Added attachment patch
* Added comment:
patch

--------------------------------------------------------------------------------
2010-08-06 00:04:30 UTC me
* Added comment:
fixed in rev 0666d9588eac

* Updated description:
{{{
I often need to change the sort order of the messages while in the pager, i.e. I want to put the following into muttrc:
bind pager o sort-mailbox
bind pager O sort-reverse

The following patch solves this problem for me, but I don't have enough understanding of the source, to check if its correct:
diff -u mutt-1.5.11/curs_main.c mutt-1.5.11.aa/curs_main.c
--- mutt-1.5.11/curs_main.c     2005-09-07 09:19:43.000000000 +0100
+++ mutt-1.5.11.aa/curs_main.c  2005-11-28 12:33:26.000000000 +0000
@@ -928,6 +928,12 @@
            set_option (OPTSEARCHINVALID);
          }
        }
+       if (menu->menu == MENU_PAGER)
+        {
+          op = OP_DISPLAY_MESSAGE;
+          continue;
+        }
+
        break;

       case OP_TAG:
diff -u mutt-1.5.11/functions.h mutt-1.5.11.aa/functions.h
--- mutt-1.5.11/functions.h     2005-07-24 17:56:42.000000000 +0100
+++ mutt-1.5.11.aa/functions.h  2005-11-28 12:17:24.000000000 +0000
@@ -186,6 +186,8 @@
   { "search-next",     OP_SEARCH_NEXT,                 "n" },
   { "next-thread",     OP_MAIN_NEXT_THREAD,            "\016" },
   { "next-subthread",  OP_MAIN_NEXT_SUBTHREAD,         "\033n" },
+  { "sort-mailbox",            OP_SORT,                        "o" },
+  { "sort-reverse",            OP_SORT_REVERSE,                "O" },
   { "print-message",   OP_PRINT,                       "p" },
   { "previous-thread", OP_MAIN_PREV_THREAD,            "\020" },
   { "previous-subthread",OP_MAIN_PREV_SUBTHREAD,       "\033p" },
>How-To-Repeat:
>Fix:
}}}
* milestone changed to 
* resolution changed to fixed
* status changed to closed
