Ticket:  3417
Status:  closed
Summary: limiting query with unmatched [ causes crash

Reporter: grobian
Owner:    mutt-dev

Opened:       2010-06-12 21:09:29 UTC
Last Updated: 2010-08-08 17:21:59 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
see http://bugs.gentoo.org/show_bug.cgi?id=323613

press l
type [Bug
and press enter

A segfault (linux, osx) or a insufficient memory error (solaris) will occur.

cause: changeset 05cec973f0d97065709f2ec55a42b39797995eff
a similar fix for another case in the code was made in 25e12863c521c6cb0e0c1f197c7e84def57b9ec8

The attached patch fixes the crash.

--------------------------------------------------------------------------------
2010-06-12 21:10:08 UTC grobian
* Added attachment mutt-1.5.20-crash-on-invalid-limit-pattern.patch

--------------------------------------------------------------------------------
2010-08-08 17:21:59 UTC Fabian Groffen <grobian@gentoo.org>
* Added comment:
(In [d4d703e21cdd]) fix crash in limit when user enters invalid regexp

closes #3417

* resolution changed to fixed
* status changed to closed
