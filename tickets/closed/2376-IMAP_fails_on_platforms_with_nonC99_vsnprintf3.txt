Ticket:  2376
Status:  closed
Summary: IMAP fails on platforms with non-C99 vsnprintf(3)

Reporter: holger@zedat.fu-berlin.de
Owner:    mutt-dev

Opened:       2006-07-23 03:22:26 UTC
Last Updated: 2006-08-08 23:49:48 UTC

Priority:  major
Component: IMAP
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
When using IMAP on IRIX (and possibly other platforms), Mutt 1.5.12 hangs forever while "Fetching message headers...".  This happens because longer IMAP commands are truncated to 127 characters prior to sending them; whereupon Mutt will wait forever for the server response to the incomplete command.

The truncation occurs because, in order to calculate the necessary size of the buffer holding the IMAP command, mutt_buffer_printf() relies on vsnprintf(3) returning the number of characters which _would_ have been written to a sufficiently sized buffer, see muttlib.c:1438.  However, (at least) on IRIX, vsnprintf(3) returns the actual length of the output string, which leads to mutt_buffer_printf() not recognizing that the size of the buffer in question would have to be increased.  Instead, the IMAP command is truncated to fit into the (default) buffer size of 128.

Apart from that, the [v]snprintf(3) replacement functions provided in snprintf.c return the length of the actual output string just as the IRIX implementation does, so without modification, they cannot be used either.

Of course, the [v]snprintf(3) problem might also arise elsewhere in the code, I haven't checked.
>How-To-Repeat:
Try to use IMAP on IRIX.
>Fix:
The attached patch fixes the return value of the replacement functions in snprintf.c and changes configure.in so that the replacements are used if the system's [v]snprintf(3) implementation doesn't return the correct length (or if it doesn't truncate the output appropriately, which happened on Solaris 7 IIRC).
}}}

--------------------------------------------------------------------------------
2006-07-28 06:36:14 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Holger, and much thanks for the report, analysis, and patch.

 On Sunday, July 23, 2006 at 4:22:26 +0200, Holger Weiss wrote:

> When using IMAP on IRIX (and possibly other platforms), Mutt 1.5.12
> hangs forever while "Fetching message headers...".

    Same hang happens on a Linux libc5 platform. Problem solved by your
c99_vsnprintf.diff patch. Thanks again :-).

| --- mutt/PATCHES	Tue Nov  6 19:59:33 2001
| +++ mutt/PATCHES	Tue Nov  6 19:59:42 2001
| @@ -1,0 +1 @@
| +patch-1.5.12.hw.c99_vsnprintf.1


Bye!	Alain.
-- 
Followups to bug reports are public, and should go to bug-any@bugs.mutt.org,
and the reporter (unless he is a member of mutt-dev). Do not CC mutt-dev mailing
list, the BTS does it already. Do not send to mutt-dev only, your writings would
not be tracked. Do not remove the "mutt/nnnn:" tag from subject.
}}}

--------------------------------------------------------------------------------
2006-08-04 00:35:26 UTC ab
* Added comment:
{{{
Hello Holger, and much thanks for the report, analysis, and patch.
 tags patch
}}}

--------------------------------------------------------------------------------
2006-08-09 17:49:48 UTC brendan
* Added comment:
{{{
Patch applied, thanks very much.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:23 UTC 
* Added attachment c99_vsnprintf.diff
* Added comment:
c99_vsnprintf.diff
