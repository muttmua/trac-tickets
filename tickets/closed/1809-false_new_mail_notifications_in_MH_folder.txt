Ticket:  1809
Status:  closed
Summary: false new mail notifications in MH folder

Reporter: Moshe Kaminsky <kaminsky@math.huji.ac.il>
Owner:    mutt-dev

Opened:       2004-02-18 05:25:21 UTC
Last Updated: 2007-04-03 01:44:10 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6i
Severity: normal

-- Please type your report below this line

I get false "new mail in ..." notifications. I use MH folders. Checking 
the output of flists and the .mh_sequences files shows there is no new 
mail. When I actually change to the named folder, I don't see any new 
mail.

Thanks,
Moshe

-- System Information
System Version: Linux kaminsky 2.6.3-rc3-gentoo #1 Mon Feb 16 08:02:01 IST 2004 i686 AMD Duron(tm) Processor AuthenticAMD GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.2/specs
Configured with: /usr/tmp/portage/gcc-3.3.2-r7/work/gcc-3.3.2/configure --prefix=/usr --bindir=/usr/i686-pc-linux-gnu/gcc-bin/3.3 --includedir=/usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.2/include --datadir=/usr/share/gcc-data/i686-pc-linux-gnu/3.3 --mandir=/usr/share/gcc-data/i686-pc-linux-gnu/3.3/man --infodir=/usr/share/gcc-data/i686-pc-linux-gnu/3.3/info --enable-shared --host=i686-pc-linux-gnu --target=i686-pc-linux-gnu --with-system-zlib --enable-languages=c,c++,f77,objc,java --enable-threads=posix --enable-long-long --disable-checking --enable-cstdio=stdio --enable-clocale=generic --enable-__cxa_atexit --enable-version-specific-runtime-libs --with-gxx-include-dir=/usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.2/include/g++-v3 --with-local-prefix=/usr/local --enable-shared --enable-nls --without-included-gettext --x-includes=/usr/X11R6/include --x-libraries=/usr/X11R6/lib --enable-interpreter --enable-java-awt=xlib --with-x --disable-multilib
Thread model: posix
gcc version 3.3.2 20040119 (Gentoo Linux 3.3.2-r7, propolice-3.3-7)

- CFLAGS
-Wall -pedantic -Wall -pedantic -O2 -march=athlon-tbird -funroll-loops -fprefetch-loop-arrays -pipe -fomit-frame-pointer

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.3-rc3-gentoo (i686) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
-USE_FCNTL  +USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
-HAVE_REGCOMP  +USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc/mutt"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.6.dw.mbox-hook.1
rr.compressed
patch-1.5.5.1.cd.edit_threads.9.5


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2007-04-03 01:44:10 UTC brendan
* Added comment:
Fixed in [15f8a55220a7]

* Updated description:
{{{
Package: mutt
Version: 1.5.6i
Severity: normal

-- Please type your report below this line

I get false "new mail in ..." notifications. I use MH folders. Checking 
the output of flists and the .mh_sequences files shows there is no new 
mail. When I actually change to the named folder, I don't see any new 
mail.

Thanks,
Moshe

-- System Information
System Version: Linux kaminsky 2.6.3-rc3-gentoo #1 Mon Feb 16 08:02:01 IST 2004 i686 AMD Duron(tm) Processor AuthenticAMD GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.2/specs
Configured with: /usr/tmp/portage/gcc-3.3.2-r7/work/gcc-3.3.2/configure --prefix=/usr --bindir=/usr/i686-pc-linux-gnu/gcc-bin/3.3 --includedir=/usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.2/include --datadir=/usr/share/gcc-data/i686-pc-linux-gnu/3.3 --mandir=/usr/share/gcc-data/i686-pc-linux-gnu/3.3/man --infodir=/usr/share/gcc-data/i686-pc-linux-gnu/3.3/info --enable-shared --host=i686-pc-linux-gnu --target=i686-pc-linux-gnu --with-system-zlib --enable-languages=c,c++,f77,objc,java --enable-threads=posix --enable-long-long --disable-checking --enable-cstdio=stdio --enable-clocale=generic --enable-__cxa_atexit --enable-version-specific-runtime-libs --with-gxx-include-dir=/usr/lib/gcc-lib/i686-pc-linux-gnu/3.3.2/include/g++-v3 --with-local-prefix=/usr/local --enable-shared --enable-nls --without-included-gettext --x-includes=/usr/X11R6/include --x-libraries=/usr/X11R6/lib --enable-interpreter --enable-java-awt=xlib --with-x --disable-multilib
Thread model: posix
gcc version 3.3.2 20040119 (Gentoo Linux 3.3.2-r7, propolice-3.3-7)

- CFLAGS
-Wall -pedantic -Wall -pedantic -O2 -march=athlon-tbird -funroll-loops -fprefetch-loop-arrays -pipe -fomit-frame-pointer

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.3-rc3-gentoo (i686) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
-USE_FCNTL  +USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
-HAVE_REGCOMP  +USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc/mutt"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.6.dw.mbox-hook.1
rr.compressed
patch-1.5.5.1.cd.edit_threads.9.5


>How-To-Repeat:
>Fix:
}}}
* resolution changed to fixed
* status changed to closed
