Ticket:  2917
Status:  closed
Summary: fixed sections of format=flowed messages can wrap prematurely (78 cols)

Reporter: jhawk
Owner:    mutt-dev

Opened:       2007-06-21 00:42:55 UTC
Last Updated: 2007-10-15 09:01:15 UTC

Priority:  major
Component: display
Keywords:  wrap flowed fixed

--------------------------------------------------------------------------------
Description:
mutt wraps long lines in a format/flowed message, but does not fill the paragraphs they are in, resulting in awkward-to-read paragraphs with several long lines followed by a very short line followed by more long lines.

This is with the default $wrap=0. Setting $wrap=80 seems to fix this example, but it fails with $wrap=79 or $wrap=-1.

I was under the impression this was supposed to have been looked at extensively recently, but I can't seem to find evidence of it.

My sample is 7 lines of "lorem ipsum" text. In the example attachment, mutt displays 5 long lines, 1 line with 1 word, and then 2 full lines.

Example rendering:
{{{
From jhawk@MIT.EDU Wed Jun 20 20:25:48 2007
From: John Hawkinson <jhawk@MIT.EDU>
Subject: Format flowed test

Lorem ipsum dolor sit amet, consectetaur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur.  Excepteur sint occaecat cupidatat
non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum Et
harumd und.
}}}


--------------------------------------------------------------------------------
2007-06-21 00:43:27 UTC jhawk
* Added attachment mutt-flowed.mbox

--------------------------------------------------------------------------------
2007-06-21 07:27:27 UTC pdmef
* Added comment:
This is not a mutt bug (I'd say) but a badly prepared format=flowed message. The reason is that all lines do ''not'' end in a trailing space, i.e. ''all'' lines are fixed.

As a result, mutt takes each invidual line and breaks it at spaces to fit {{{$wrap}}}. When there's only one word one a single line, mutt cannot continue with the content from the next line since it's not allowed to: the current one doesn't end in space.

For verification, please take the example mail and append a space to all lines: mutt now properly renders the text with nearly any value of {{{$wrap}}}.

* component changed to display

--------------------------------------------------------------------------------
2007-06-21 13:20:09 UTC John Hawkinson
* Added comment:
{{{
pdmef <fleas@mutt.org> wrote on Thu, 21 Jun 2007
at 07:27:28 -0000 in <046.f9ed8ea55e761b771e4c040f11574e3a@mutt.org>:

>  This is not a mutt bug (I'd say) but a badly prepared format=flowed
>  message. The reason is that all lines do ''not'' end in a trailing space,
>  i.e. ''all'' lines are fixed.

I think you are misinterpretting.

format=flowed messages are permitted to have fixed lines and flowed
lines. mutt should not wrap the fixed lines, especially when they are
under 80 columns in an 80-column window and wrap=0.

The particular message that I was looking at, from which I extracted
this test case, had both fixed paragraphs and flowed paragraphs. The
flowed paragraphs were displayed properly by mutt, but the fixed were
not.

If its of interest, the message was sent with

User-Agent: Internet Messaging Program (IMP) H3 (4.0.3)

and I seem to encounter them with some regularity. :(


Even were it the case that this were a badly-formatted format=flowed
message (which it is not), mutt should still try to display it
properly, in the "be liberal in what you accept" vein.

--jhawk
}}}

--------------------------------------------------------------------------------
2007-06-21 15:01:20 UTC pdmef
* Added comment:
Replying to [comment:2 John Hawkinson]:

> format=flowed messages are permitted to have fixed lines and flowed
> lines. mutt should not wrap the fixed lines, especially when they are
> under 80 columns in an 80-column window and wrap=0.

By default, if you don't set $wrap, the limit is 77. I don't recall
exactly why it's that number (maybe to leave some room below 80 for
more quoting levels or for \r\n or...).

The point is that when you explicitly set wrap to something other than
0, you ask mutt to display text not wider than your limit. I'm afraid
in these cases mutt will have to break all lines (all fixed-format mails,
all fixed lines in flowed-format mails) in order to do what you configured
it to.
 
> Even were it the case that this were a badly-formatted format=flowed
> message (which it is not), mutt should still try to display it
> properly, in the "be liberal in what you accept" vein.

Though I agree on the last part, I still think the mail is badly formatted.

I don't see the point of composing a paragraph of text as fixed lines in a
format=flowed mail while hoping and requesting everything to render well
on the receiver's side.

Being allowed to reflow text also includes to render it narrower than the
sender intended and possibly causing ugly rendering. What if you were viewing the
message on a device with less than 80 columns? It will look ugly again unless
the sender adds proper spaces to make up reflowable paragraphs.

Anyway, I'm kind of stuck on providing proposals on how to fix it, i.e. provide
better defaults that fit most people...

--------------------------------------------------------------------------------
2007-06-21 15:13:07 UTC brendan
* Added comment:
$wrap defaulting to 77 columns was because the majority opinion on mutt-dev IIRC was that by default mutt should use narrow columns for readability reasons. I think this was one of the reasons it tooks so long for the new flowed handler to be applied. I think it had to default to 0 because otherwise on small screens the wrong thing would be done? I can't remember precisely why, but it could be dug up out of the mutt-dev archive I suppose.

--------------------------------------------------------------------------------
2007-06-21 16:11:20 UTC Kyle Wheeler
* Added comment:
{{{
On Thursday, June 21 at 03:01 PM, quoth Mutt:
> The point is that when you explicitly set wrap to something 
> other than 0, you ask mutt to display text not wider than your 
> limit. I'm afraid in these cases mutt will have to break all 
> lines (all fixed-format mails, all fixed lines in flowed-format 
> mails) in order to do what you configured it to.

No, that's wrong. Fixed lines in flowed-format mails are 
*specifically* fixed. Mutt can break them if they're too long, but 
it's not allowed to rewrap them if they can be displayed as-is.

>> Even were it the case that this were a badly-formatted 
>> format=flowed message (which it is not), mutt should still try 
>> to display it properly, in the "be liberal in what you accept" 
>> vein.
>
> Though I agree on the last part, I still think the mail is badly 
> formatted.

It is not. From RFC 2646, 4.1:

     A Format=Flowed message consists of zero or more paragraphs,
     each containing one or more flowed lines followed by one fixed
     line. The usual case is a series of flowed text lines with
     blank (empty) fixed lines between them.

     Any number of fixed lines can appear between paragraphs.

> I don't see the point of composing a paragraph of text as fixed 
> lines in a format=flowed mail while hoping and requesting 
> everything to render well on the receiver's side.

My RFC quotes up above are good examples of why you would want 
that. If those lines were "flowed", the indenting I put in front 
of them would make them rather unreadable. Instead, I removed the 
space from the end of each line of indented text, so that it would 
not be flowed. Check out what it would look like if I 
hadn't:

     A Format=Flowed message consists of zero or more paragraphs, 
     each containing one or more flowed lines followed by one fixed 
     line. The usual case is a series of flowed text lines with 
     blank (empty) fixed lines between them.

Here's another example: let's say I wanted to include a small 
shell script or bit of code in an email. I would say:

     #!/bin/sh
     echo this is a script!
     echo this demonstrates why some things in a \
         format=flowed message should not be flowed

But if I leave the spaces at the end and allow it to be flowed:

     #!/bin/sh 
     echo this is a script! 
     echo this demonstrates why some things in a \ 
         format=flowed message should not be flowed

Thus, the fact that there are spaces at the end is IMPORTANT to 
correct display of the email. The mailer should not simply ignore 
them and assume that all lines in a format=flowed message are 
permitted to be flowed.

> Being allowed to reflow text also includes to render it narrower 
> than the sender intended and possibly causing ugly rendering. 
> What if you were viewing the message on a device with less than 
> 80 columns? It will look ugly again unless the sender adds 
> proper spaces to make up reflowable paragraphs.

The sender has chosen to explicitly decide what lines can and 
cannot be flowed, and may have very good reasons for that. The 
mailer should respect that information, and not disregard it just 
because SOME parts of the message need can be reformatted.

~Kyle
}}}

--------------------------------------------------------------------------------
2007-06-21 16:29:37 UTC Kyle Wheeler
* Added comment:
{{{
The really interesting thing about this particular case is that in 
the example mbox attached to the bug, the line that gets rewrapped 
(the fifth one) is 78 characters long. More to the point, it gets 
wrapped EVEN IF $wrap IS SET TO 79! And *that*, I think, is 
probably wrong.

~Kyle
}}}

--------------------------------------------------------------------------------
2007-06-21 16:33:06 UTC John Hawkinson
* Added comment:
{{{
Rocco Rutte <fleas@mutt.org> wrote on Thu, 21 Jun 2007
at 15:01:21 -0000 in <046.765e9bc5d83e60986ba2007a91262198@mutt.org>:

>  By default, if you don't set $wrap, the limit is 77.

Whoa. That's weird. Can we update the docs? They say the default
is "0" and they don't say what that means, but the implication is
that the its the same as a negative value of zero:

   When set to a negative value, mutt will wrap text so that there are
   $wrap characters of empty space on the right side of the terminal.

Unforatunately, it seems like one of the ways to solve my problem
would be for me to set wrap=80. I'd really like to set wrap=-0 ("negative
zero"), but that doesn't seem supported.

>  The point is that when you explicitly set wrap to something other than
>  0, you ask mutt to display text not wider than your limit. I'm afraid

Right. Normally I don't set wrap and would prefer not to.

>  I don't see the point of composing a paragraph of text as fixed lines in a
>  format=flowed mail while hoping and requesting everything to render well
>  on the receiver's side.

Well, one answer is that sometimes users type things where the
spacing matters, and sometimes they do not. It's reasonable for
a mail client to try to apply a heuristic and format the
special lines as "fixed" and the others as "flowed".

> What if you were viewing the message on a device with less than 
> 80 columns?

Then you as the receiver have a special problem and will need to take
special action.. But that vast majority of people have >=80 cols
and their viewing shouldn't be broken.

>  Anyway, I'm kind of stuck on providing proposals on how to fix it,
>  i.e.  provide better defaults that fit most people...

It seems to me that it comes back to the 77 issue...many mail clients
will format for 80 columns, and if you wrap everything between 77 and 80,
you're going to have problems. This is independent of format=flowed.



> Comment (by brendan):
> 
>  $wrap defaulting to 77 columns was because the majority opinion on mutt-
>  dev IIRC was that by default mutt should use narrow columns for
>  readability reasons. I think this was one of the reasons it tooks so long
>  for the new flowed handler to be applied. I think it had to default to 0
>  because otherwise on small screens the wrong thing would be done? I can't
>  remember precisely why, but it could be dug up out of the mutt-dev archive
>  I suppose.

Now I'm even more confused. Is this about composition rather than
reading? It seems clear that taking 80-column-formatted text and
wrapping those lines that exceed 77 columns will produce ugly-looking
text a lot of the time. Surely that's not the intent?

Kyle Wheeler responded to mutt-dev (found in the archive) and
it didn't make it into trac, but I wanted to note that
RFC2646 is obsoleted by RFC3676 (I don't think anything
of relevance changes). 

I'm not having too much like searching in the mutt-dev archives
(are they available in mbox format somewhere?), so I'll pause for
now.

--jhawk
}}}

--------------------------------------------------------------------------------
2007-06-21 17:09:43 UTC pdmef
* Added comment:
Replying to [comment:7 John Hawkinson]:

> Rocco Rutte <fleas@mutt.org> wrote on Thu, 21 Jun 2007
> >  By default, if you don't set $wrap, the limit is 77.
> 
> Whoa. That's weird. Can we update the docs? They say the default
> is "0" and they don't say what that means, but the implication is
> that the its the same as a negative value of zero:

I didn't mean the value for $wrap but an internal limit being used
in case $wrap=0. The old handler for format=flowed didn't flow lines
and so $wrap defaults to 0 and the new f=f handler will flow the lines
up to the internal limit (just for clarification that the limit makes
sense, there's no other way except a new variable whether to generally
flow format=flowed mails to what width that I know of now...)

> > What if you were viewing the message on a device with less than 
> > 80 columns?
> 
> Then you as the receiver have a special problem and will need to take
> special action.. But that vast majority of people have >=80 cols
> and their viewing shouldn't be broken.

Just that one has more than 80 characters doesn't mean he's willing to
use it for text.
  
> Kyle Wheeler responded to mutt-dev (found in the archive) and
> it didn't make it into trac, but I wanted to note that
> RFC2646 is obsoleted by RFC3676 (I don't think anything
> of relevance changes). 

The new f=f handler is supposed to support RfC3676 except for this
bug maybe... :)

--------------------------------------------------------------------------------
2007-06-21 17:20:09 UTC pdmef
* Added comment:
Replying to [comment:5 Kyle Wheeler]:
> On Thursday, June 21 at 03:01 PM, quoth Mutt:

> > The point is that when you explicitly set wrap to something 
> > other than 0, you ask mutt to display text not wider than your 
> > limit. I'm afraid in these cases mutt will have to break all 
> > lines (all fixed-format mails, all fixed lines in flowed-format 
> > mails) in order to do what you configured it to.
> 
> No, that's wrong. Fixed lines in flowed-format mails are 
> *specifically* fixed. Mutt can break them if they're too long, but 
> it's not allowed to rewrap them if they can be displayed as-is.

Didn't I write that? The problem with this bug is not mutt incorrectly
rewraps lines but that it could be more clever to break overly long lines.

And yes, with $wrap=0 and the internal limit of 77 the text is too long.

A quick look at the code is that mutt even uses 1 char less, though I don't
know why off-hand.

> Check out what it would look like if I hadn't:

Okay, code and quotes are valid examples of why you want that.

> The sender has chosen to explicitly decide what lines can and 
> cannot be flowed, and may have very good reasons for that. The 
> mailer should respect that information, and not disregard it just 
> because SOME parts of the message need can be reformatted.

The problem is about breaking, not flowing.

--------------------------------------------------------------------------------
2007-06-21 17:21:53 UTC pdmef
* Added comment:
Replying to [comment:6 Kyle Wheeler]:

> The really interesting thing about this particular case is that in 
> the example mbox attached to the bug, the line that gets rewrapped 
> (the fifth one) is 78 characters long. More to the point, it gets 
> wrapped EVEN IF $wrap IS SET TO 79! And *that*, I think, is 
> probably wrong.

The code has an extra -1 and checks for the length being lower than
the wrapping width, i.e. 79-1 < 78 fails... which might explain it.

--------------------------------------------------------------------------------
2007-06-21 17:35:50 UTC Kyle Wheeler
* Added comment:
{{{
On Thursday, June 21 at 05:20 PM, quoth Mutt:
>> No, that's wrong. Fixed lines in flowed-format mails are 
>> *specifically* fixed. Mutt can break them if they're too long, 
>> but it's not allowed to rewrap them if they can be displayed 
>> as-is.
>
> Didn't I write that? The problem with this bug is not mutt 
> incorrectly rewraps lines but that it could be more clever to 
> break overly long lines.

If you did, then I misunderstood you, sorry.

How should mutt be more clever? I read this bug as requesting that 
mutt pretend that long lines should be wrapped as if they ended in 
a space (i.e. to have the paragraph "filled in"). If that's not 
the issue... then the bug report is misleading. If that IS the 
issue, though, I disagree with it. Mutt should obey the formatting 
specified in the message, not make guesses and overrule it.

~Kyle
}}}

--------------------------------------------------------------------------------
2007-06-21 17:47:58 UTC pdmef
* Added comment:
Replying to [comment:11 Kyle Wheeler]:

> How should mutt be more clever? I read this bug as requesting that 
> mutt pretend that long lines should be wrapped as if they ended in 
> a space (i.e. to have the paragraph "filled in").

Now I understand why you quoted the RfC. It's really not about that.

The example mail (and the mailbox) contain just a message where an
f=f mail contains only fixed lines that mutt breaks because it thinks
they're too long (triggered by the internal 77-1 limit).

With $wrap>=80 and/or manually adding spaces everything is fine.

--------------------------------------------------------------------------------
2007-06-21 18:54:06 UTC John Hawkinson
* Added comment:
{{{
Mutt <fleas@mutt.org> wrote on Thu, 21 Jun 2007
at 17:20:09 -0000 in <046.1d123600f7cd5cc9bbaf6c0979dd2c00@mutt.org>:

> Didn't I write that? The problem with this bug is not mutt
> incorrectly rewraps lines but that it could be more clever to break
> overly long lines.

No, this is wrong!

The problem is that a fixed portion of a format=flowed message gets
wrapped when $wrap=0. And it looks bad. Mutt should not have wrapped it,
it should have kept it fixed.

It is possible that there are example cases of messages that should
be wrapped when $wrap=0 that are not being wrapped, but this is not
one of them and I'd want to see a case before speculating on a fix.

> Just that one has more than 80 characters doesn't mean he's willing
> to use it for text.

I wrote ">=80" and I meant >=. That is, if you have =80, mutt
should behave reasonably. It is not behaving reasonably for
a format=flowed message with a fixed portion that is formatted to
less than 80 columns.

>  The problem is about breaking, not flowing.

But it's about breaking when it should not, it is not about
breaking in the wrong place, etc.

Mutt <fleas@mutt.org> wrote on Thu, 21 Jun 2007
at 17:21:53 -0000 in <046.d4e21f1656a0ab6249453d6b36e2de9c@mutt.org>:


>  > How should mutt be more clever? I read this bug as requesting that
>  > mutt pretend that long lines should be wrapped as if they ended in
>  > a space (i.e. to have the paragraph "filled in").

No, that's definitely not good default behavior.
If someone sends me a fixed-formatted message that fits in 80 columns,
and I have not set $wrap, I do not want mutt to wrap it. I want it
to show it to me as the originator intended.

--jhawk
}}}

--------------------------------------------------------------------------------
2007-06-21 23:51:47 UTC jhawk
* keywords changed to wrap flowed fixed
* summary changed to fixed sections of format=flowed messages can wrap prematurely (78 cols)

--------------------------------------------------------------------------------
2007-10-15 09:01:15 UTC pdmef
* Added comment:
(In [35e5c34b7e91]) f=f: Print standalone fixed lines as-is (closes #2917).

* resolution changed to fixed
* status changed to closed
