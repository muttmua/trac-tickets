Ticket:  3815
Status:  closed
Summary: [PATCH] Debian buildfix for Hurd

Reporter: flatcap
Owner:    mutt-dev

Opened:       2016-03-19 15:59:03 UTC
Last Updated: 2016-11-03 22:52:30 UTC

Priority:  minor
Component: build
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Trivial downstream patch.

Hurd is missing a definition for PATH_MAX.

--------------------------------------------------------------------------------
2016-03-19 15:59:36 UTC flatcap
* Added attachment debian1.patch
* Added comment:
define PATH_MAX

--------------------------------------------------------------------------------
2016-03-19 16:04:31 UTC flatcap
* keywords changed to patch

--------------------------------------------------------------------------------
2016-10-22 15:28:51 UTC grobian
* Added comment:
I would like to see this go in, possibly as
{{{
#if !defined(PATH_MAX) && defined(_POSIX_PATH_MAX)
}}}

--------------------------------------------------------------------------------
2016-11-03 22:52:30 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"45023e44c92caf3e545b18d6bc2eaec707b6b61b" 6836:45023e44c92c]:
{{{
#!CommitTicketReference repository="" revision="45023e44c92caf3e545b18d6bc2eaec707b6b61b"
Define PATH_MAX, it's missing on the GNU Hurd. (closes #3815)

I believe this patch originally came from Debian.

Modified based on a suggestion from Fabian Groffen.
}}}

* resolution changed to fixed
* status changed to closed
