Ticket:  2937
Status:  closed
Summary: mutt segfaulted when I pressed 'i' from within the pager

Reporter: sashang
Owner:    mutt-dev

Opened:       2007-08-03 04:42:07 UTC
Last Updated: 2007-12-08 23:11:22 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
mutt segfaulted when I pressed 'i' to exit the pager. This is the error message I received.

error: unknown op 180 (report this error).Segmentation fault

Output from mutt -v:
Mutt 1.4.2.2i (2006-07-14)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.18-1.2798.fc6 (i686) [using ncurses 5.5]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +USE_GSS  +USE_SSL  +USE_SASL
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER


--------------------------------------------------------------------------------
2007-12-07 19:27:12 UTC brendan
* Added comment:
Can you reproduce this with 1.5.17? Is mutt patched (sounds like it might be)?

--------------------------------------------------------------------------------
2007-12-08 23:10:39 UTC Sashan Govender
* Added comment:
{{{
No can't reproduce it with 1.5.16 (the version I'm using) anymore. At
the time, I couldn't reproduce it with the version (1.4.2.2i) in the
bug report either.

On Dec 8, 2007 6:27 AM, Mutt <fleas@mutt.org> wrote:
> #2937: mutt segfaulted when I pressed 'i' from within the pager
>
> Comment (by brendan):
>
>  Can you reproduce this with 1.5.17? Is mutt patched (sounds like it might
>  be)?
>
> --
> Ticket URL: <http://dev.mutt.org/trac/ticket/2937#comment:1>
>
>


}}}

--------------------------------------------------------------------------------
2007-12-08 23:11:22 UTC brendan
* Added comment:
Thanks. I'm closing this report then.

* resolution changed to worksforme
* status changed to closed
