Ticket:  3133
Status:  closed
Summary: mutt_md5 hangs when compiled by gcc 4.3 with optimization

Reporter: raorn
Owner:    mutt-dev

Opened:       2008-11-09 22:19:55 UTC
Last Updated: 2008-11-16 03:55:07 UTC

Priority:  critical
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Due to redefinition of __attribute__ in md5.h mutt_md5 hangs when calling memcpy().  I've updated md5.{h,c} to latest version from gnulib (patch attached).

--------------------------------------------------------------------------------
2008-11-09 22:21:36 UTC raorn
* Added attachment 0001-md5.h-md5.c-updated-to-latest-version-from-gnulib.patch

--------------------------------------------------------------------------------
2008-11-10 00:30:07 UTC raorn
* Added comment:
gcc 4.3 with -O2 optimization and -D_FORTIFY_SOURCE=1 or 2.

--------------------------------------------------------------------------------
2008-11-16 03:55:07 UTC brendan
* Added comment:
Thanks, I've applied this in [af87aa1846be].

* resolution changed to fixed
* status changed to closed
