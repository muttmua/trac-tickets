Ticket:  2152
Status:  closed
Summary: new read mail in IMAP folder not seen

Reporter: muttbug@spodhuis.demon.nl
Owner:    mutt-dev

Opened:       2005-12-15 12:39:50 UTC
Last Updated: 2005-12-16 20:30:05 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
View an IMAP folder.  View a mail in an IMAP folder.  Leave the mutt client in the message view.

Using a second instance of mutt connecting to the same IMAP server, view that folder; reply to that message.  Read the new mail, so that it's no longer flagged N.  Quit.

Go back to the first mutt client.  It won't pick up that there is new email, whether with sync-mailbox or with imap-fetch-mail.

If reply with first client, then when mutt picks up the second new mail it will also see the previous one.
>How-To-Repeat:
Example output from a raw IMAP session in EXAMINE state on the same folder:

a5 NOOP
* 29 EXISTS
* 2 RECENT
* 28 FETCH (FLAGS (\Recent \Answered \Seen))

Note that there's no unseen, as in:
  * OK [UNSEEN 25]
so mutt fails to pick up on the new value of the EXISTS tag.
>Fix:
Perhaps (possibly wrong), in imap.c:imap_check_mailbox(), cache idata->seqno before the NOOP, then after imap_cmd_finish() if idata->seqno has changed then return M_NEW_MAIL.
}}}

--------------------------------------------------------------------------------
2005-12-15 18:52:43 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Thursday, 15 December 2005 at 19:38, Phil Pennock wrote:
> On 2005-12-15 at 18:27 +0100, Brendan Cully wrote:
> > Synopsis: new read mail in IMAP folder not seen
> > 
> > **** Comment added by brendan on Thu, 15 Dec 2005 18:27:32 +0100 ****
> >  I can't reproduce this with current CVS. The mail arrives in the first mutt (not marked new) as soon as I leave the message view (with debian cyrus 2.1.18). A longer trace would be needed...
> 
> Updated to CVS.
> 
> The ChangeLog is informative (latest "2005-12-14 17:29:57").
> 
> Trying the new code and looking at debug traces ...  the new approach is
> to poll every mailbox, always?  That will cause hideous performance

not new. That code hasn't changed in years. It's not related to the
bug you described either - polling other mailboxes is done completely
differently from polling the selected one.

> problems for anyone using UW imapd as a front-end onto a regular mbox
> folder, since that will involve recalculations for every folder (hence
> the strenuous Thou Shalt Nots in RFC3501).  I use Cyrus (mostly), so am
> happy enough with the behaviour, but a friend will murder me if I try to
> run this against his UW server.

It polls every mailbox you've told it to poll using a mailboxes
command or imap_check_subscribed. This is usually much less than the
total set of mailboxes you can access. Right now it's only asking for
RECENT, which only a silly mail server should have a hard time
calculating (MESSAGES is a different story), and there's not much you
can do to make it better. You either run STATUS on other mailboxes, or
hold them open. UW doesn't like that either, since it locks the
mailbox. UW is not a good IMAP server, or at least mbox is a very bad
back end.

But make sure you drop mail_check down to something sensible
(ie 60+). 5 is insane for IMAP. And for everything else, really.

I'm currently playing with a rewrite that uses UIDNEXT instead of
RECENT to work around the ephemeral nature of the RECENT flag, but it
will actually be marginally more expensive for the server.

> Is the imap-fetch-mail command expected to disappear?  Because it in
> itself is not causing the mail fetch (as far as I can figure out); it's
> being independently determined elsewhere by the STATUS polls.

it's a silly function, but it'll probably hang around a bit
longer. New mail in the current mailbox is checked with NOOP or IDLE,
which should be very cheap.
}}}

--------------------------------------------------------------------------------
2005-12-15 23:24:06 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Thursday, 15 December 2005 at 23:22, Phil Pennock wrote:
> On 2005-12-15 at 10:52 -0800, Brendan Cully wrote:
> > not new. That code hasn't changed in years. It's not related to the
> > bug you described either - polling other mailboxes is done completely
> > differently from polling the selected one.
> 
> In that case, one of your recent changes has had a major impact on how
> well this works, because I'd bound <Esc>$ through necessity.  So gratz
> there.  Sorry for not knowing, but this is the first time that I've had
> cause to build a +DEBUG mutt.

In hindsight it strikes me that the problem was probably that your
$timeout was much too high. The new IDLE code isn't dependent on
$timeout, but NOOP was. $timeout has a default which isn't suitable
for IMAP.

> > I'm currently playing with a rewrite that uses UIDNEXT instead of
> > RECENT to work around the ephemeral nature of the RECENT flag, but it
> > will actually be marginally more expensive for the server.
> 
> I'm interested in helping to test this; would it be best for me to join
> mutt-dev@ ?

Sure, I'll be posting the announcement there when I've got it working
to my satisfaction locally.

> >         New mail in the current mailbox is checked with NOOP or IDLE,
> > which should be very cheap.
> 
> Not guaranteed to return status updates, though, IIRC.  I guess that's
> why you're not getting rid of imap-fetch-mail immediately though, to
> wait to see just how brain-dead some of the smaller servers are?

It's because not everything supports IDLE...
}}}

--------------------------------------------------------------------------------
2005-12-16 05:41:01 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Thu, Dec 15, 2005 at 01:39:50PM +0100, muttbug@spodhuis.demon.nl wrote:

> Using a second instance of mutt connecting to the same IMAP server, view
> that folder; reply to that message.  Read the new mail, so that it's no
> longer flagged N.  Quit.
> 
> Go back to the first mutt client.  It won't pick up that there is new
> email, whether with sync-mailbox or with imap-fetch-mail.

Just to clarify - you mean that the first new mutt does not show any mail
which arrives in the folder after that point? Or simply that it doesn't say
"New mail in  mailbox"?

If the latter, surely that's because there *isn't* any new mail? I would
have thought that your penultimate step in the first paragraph made it
not-new.

-- 
Paul

--6TrnltStXW4iwmi0
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.1 (GNU/Linux)

iD8DBQFDoXJtP9fOqdxRstoRAgwOAJ9oTkwRlg3aol0b+ZbBQDUbBSXyuACdEh6q
Ksi/mxEh5NEUsjC+Om15l68=BGOJ
-----END PGP SIGNATURE-----

--6TrnltStXW4iwmi0--
}}}

--------------------------------------------------------------------------------
2005-12-16 07:35:12 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Thu, Dec 15, 2005 at 04:21:05PM +0100, Phil Pennock wrote:

>   different mutt GNATS ticket didn't make it into the ticketing system
>   and I want to make sure you see this -- feel free to LART if
>   appropriate ]

I'm pretty sure it'll arrive, but a Cc is no problem.

> The former.  I can not get mutt to acknowledge the existance of new "seen"
> email until mutt sees a "new" email. As soon as mutt determines that
> there's a 'new' mail, it picks up _all_ the additional emails not visible
> in the index.

Ick. You're in luck, in that the IMAP developer seems to be back online
temporarily...

A couple of things you might want to check and post:

a) can you still reproduce the problem with the current CVS? The server
details should be on www.mutt.org
b) what patches have Gentoo applied? If there's anything which looks like it
touches IMAP, it might also be worth testing with a clean copy of 1.5.11

Brendan might also want a debug trace - if mutt -v shows a +DEBUG, run mutt
with "mutt -d 4" and send the ~/.muttdebug0 file (assuming it's not too
big!). I can't remember if mutt sanitises the output WRT passwords, so you
might want to check before attaching...

-- 
Paul

"Morpheus: There is a difference between knowing the RFC
 and following the RFC"          - Jeff Gostin

--wzJLGUyc3ArbnUjN
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.1 (GNU/Linux)

iD8DBQFDoY0wP9fOqdxRstoRAqEWAJ9Kb4S8wQk/VTRdayo81mGa2ciegwCdFQRz
ba2px3nATHv9QHLJPBrlhe4=Uyhw
-----END PGP SIGNATURE-----

--wzJLGUyc3ArbnUjN--
}}}

--------------------------------------------------------------------------------
2005-12-16 09:21:05 UTC Phil Pennock <muttbug@spodhuis.demon.nl>
* Added comment:
{{{
[ Paul, sorry for overriding Reply-To: but a previous reply to a
  different mutt GNATS ticket didn't make it into the ticketing system
  and I want to make sure you see this -- feel free to LART if
  appropriate ]

On 2005-12-15 at 14:45 +0100, Paul Walker wrote:
> Subject: Re: mutt/2152: new read mail in IMAP folder not seen

>  Just to clarify - you mean that the first new mutt does not show any mail
>  which arrives in the folder after that point? Or simply that it doesn't say
>  "New mail in  mailbox"?

The former.  I can not get mutt to acknowledge the existance of new
"seen" email until mutt sees a "new" email.

Ie, it won't show in the folder index.

I bind <Esc>$ to imap-fetch-mail and throw $ and <Esc>$ at the server,
to no effect.

As soon as mutt determines that there's a 'new' mail, it picks up _all_
the additional emails not visible in the index.

-Phil
}}}

--------------------------------------------------------------------------------
2005-12-16 11:27:32 UTC brendan
* Added comment:
{{{
I can't reproduce this with current CVS. The mail arrives in the first mutt (not marked new) as soon as I leave the message view (with debian cyrus 2.1.18). A longer trace would be needed...
}}}

--------------------------------------------------------------------------------
2005-12-16 12:38:53 UTC Phil Pennock <muttbug@spodhuis.demon.nl>
* Added comment:
{{{
On 2005-12-15 at 18:27 +0100, Brendan Cully wrote:
> Synopsis: new read mail in IMAP folder not seen
> 
> **** Comment added by brendan on Thu, 15 Dec 2005 18:27:32 +0100 ****
>  I can't reproduce this with current CVS. The mail arrives in the first mutt (not marked new) as soon as I leave the message view (with debian cyrus 2.1.18). A longer trace would be needed...

Updated to CVS.

The ChangeLog is informative (latest "2005-12-14 17:29:57").

Trying the new code and looking at debug traces ...  the new approach is
to poll every mailbox, always?  That will cause hideous performance
problems for anyone using UW imapd as a front-end onto a regular mbox
folder, since that will involve recalculations for every folder (hence
the strenuous Thou Shalt Nots in RFC3501).  I use Cyrus (mostly), so am
happy enough with the behaviour, but a friend will murder me if I try to
run this against his UW server.

Is the imap-fetch-mail command expected to disappear?  Because it in
itself is not causing the mail fetch (as far as I can figure out); it's
being independently determined elsewhere by the STATUS polls.

Thanks,
}}}

--------------------------------------------------------------------------------
2005-12-16 16:22:21 UTC Phil Pennock <muttbug@spodhuis.demon.nl>
* Added comment:
{{{
Okay, because I'm silly I tried replying using Thunderbird on my wife's
Windows box and when it wouldn't let me adjust the From: I saved to
Drafts and ... it disappeared into a black hole.  Let's try again,
using, uhm, "mutt".  *coughs*

On 2005-12-15 at 10:52 -0800, Brendan Cully wrote:
> not new. That code hasn't changed in years. It's not related to the
> bug you described either - polling other mailboxes is done completely
> differently from polling the selected one.

In that case, one of your recent changes has had a major impact on how
well this works, because I'd bound <Esc>$ through necessity.  So gratz
there.  Sorry for not knowing, but this is the first time that I've had
cause to build a +DEBUG mutt.

>          UW is not a good IMAP server, or at least mbox is a very bad
> back end.

There be religious flame-war dragons.  ;^)  But I'm inclined to agree
with you, hence choosing to deploy Cyrus at work.

> But make sure you drop mail_check down to something sensible
> (ie 60+). 5 is insane for IMAP. And for everything else, really.

*CHOKES*

Thank you for pointing that option out to me.  I'd not noticed it
before.  "Insane" ... again, I am in complete agreement.

> I'm currently playing with a rewrite that uses UIDNEXT instead of
> RECENT to work around the ephemeral nature of the RECENT flag, but it
> will actually be marginally more expensive for the server.

I'm interested in helping to test this; would it be best for me to join
mutt-dev@ ?

[ imap-fetch-mail ]
> it's a silly function, but it'll probably hang around a bit
> longer.

Okay, I won't worry too much about awkwardness in a transitional tool,
then.

>         New mail in the current mailbox is checked with NOOP or IDLE,
> which should be very cheap.

Not guaranteed to return status updates, though, IIRC.  I guess that's
why you're not getting rid of imap-fetch-mail immediately though, to
wait to see just how brain-dead some of the smaller servers are?

Thanks,
-Phil
}}}

--------------------------------------------------------------------------------
2005-12-16 20:24:12 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Friday, 16 December 2005 at 12:22, Phil Pennock wrote:
> On 2005-12-15 at 15:24 -0800, Brendan Cully wrote:
> > In hindsight it strikes me that the problem was probably that your
> > $timeout was much too high. The new IDLE code isn't dependent on
> > $timeout, but NOOP was. $timeout has a default which isn't suitable
> > for IMAP.
> 
> Was set to 60 seconds in some variable I set years ago.  Now amended to
> 15 seconds, per (your) manual note.
> 
> How does this affect my results though, since I was explicitly pressing
> <Esc>$ for imap-fetch-mail, so $timeout shouldn't have been an issue,
> should it?

No - if you weren't getting new mail even after calling
imap-fetch-mail, something else was wrong. But I think I'm going to
close this bug anyway, since it seems to have gone away.
}}}

--------------------------------------------------------------------------------
2005-12-17 05:22:54 UTC Phil Pennock <muttbug@spodhuis.demon.nl>
* Added comment:
{{{
On 2005-12-15 at 15:24 -0800, Brendan Cully wrote:
> In hindsight it strikes me that the problem was probably that your
> $timeout was much too high. The new IDLE code isn't dependent on
> $timeout, but NOOP was. $timeout has a default which isn't suitable
> for IMAP.

Was set to 60 seconds in some variable I set years ago.  Now amended to
15 seconds, per (your) manual note.

How does this affect my results though, since I was explicitly pressing
<Esc>$ for imap-fetch-mail, so $timeout shouldn't have been an issue,
should it?

> > I'm interested in helping to test this; would it be best for me to join
> > mutt-dev@ ?
> 
> Sure, I'll be posting the announcement there when I've got it working
> to my satisfaction locally.

Subscribed, thanks.

> > >         New mail in the current mailbox is checked with NOOP or IDLE,
> > > which should be very cheap.
> > 
> > Not guaranteed to return status updates, though, IIRC.  I guess that's
> > why you're not getting rid of imap-fetch-mail immediately though, to
> > wait to see just how brain-dead some of the smaller servers are?
> 
> It's because not everything supports IDLE...

Sorry, I was both unclear and stupid.  I was thinking that NOOP provided
an opportunity for an untagged response without guaranteeing that the
server bothers to track new mails, but of course the server is required
to do so during an EXAMINE/SELECT so the opportunity is sufficient.

Thanks,
-Phil
}}}

--------------------------------------------------------------------------------
2005-12-17 14:30:05 UTC brendan
* Added comment:
{{{
On 2005-12-15 at 18:27 +0100, Brendan Cully wrote:
 On Thursday, 15 December 2005 at 19:38, Phil Pennock wrote:
 Okay, because I'm silly I tried replying using Thunderbird on my wife's
 On Thursday, 15 December 2005 at 23:22, Phil Pennock wrote:
 [ Paul, sorry for overriding Reply-To: but a previous reply to a
 On 2005-12-15 at 15:24 -0800, Brendan Cully wrote:
 On Friday, 16 December 2005 at 12:22, Phil Pennock wrote:
CVS appears to have resolved the issue. And this bug log is drifting far off 
course...
}}}

* resolution changed to fixed
* status changed to closed
