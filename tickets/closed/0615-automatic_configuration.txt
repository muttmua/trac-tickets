Ticket:  615
Status:  closed
Summary: automatic configuration

Reporter: Joseph Laughlin <joe@big.seattleu.edu>
Owner:    mutt-dev

Opened:       2001-05-20 22:23:00 UTC
Last Updated: 2005-09-22 13:51:39 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.2.5i
Severity: wishlist

-- Please type your report below this line
An automatic configuration would be nice.. maybe setup 'configure' to ask you questions like if you want to want to use POP or SMTP or something like that...  




-- Mutt Version Information

Mutt 1.2.5i (2000-07-28)
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-09 08:33:30 UTC rado
* Added comment:
{{{
What's the benefit of an "interactive configure" over reading "configure --help" and applying desired flags.
}}}

--------------------------------------------------------------------------------
2005-08-21 07:08:10 UTC rado
* Added comment:
{{{
Repeat for mutt-dev, reminder for sender:
Why "interactive configure" when you can read "configure --help" and apply desired flags? Isn't the help helpful enough?
}}}

--------------------------------------------------------------------------------
2005-09-23 07:51:39 UTC rado
* Added comment:
{{{
no response for 4 weeks.
}}}

* resolution changed to fixed
* status changed to closed
