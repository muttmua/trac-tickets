Ticket:  1901
Status:  closed
Summary: mutt: reply-hook not documented in Reference chapter of Mutt manual

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2004-06-15 14:11:05 UTC
Last Updated: 2005-08-12 09:35:11 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6-20040523+2
Severity: minor

[NOTE: this bug report has been submitted to the debian BTS as Bug#254294.
Please Cc all your replies to 254294@bugs.debian.org .]

From: Branden Robinson <branden@debian.org>
Subject: mutt: reply-hook not documented in Reference chapter of Mutt manual
Date: Mon, 14 Jun 2004 01:46:53 -0500

The configuration command "reply-hook" is documented in section 3.18 but
there's no mention of it in chapter 6 ("Reference"), which should be
comprehensive.

-- System Information:
Debian Release: testing/unstable
  APT prefers unstable
  APT policy: (500, 'unstable'), (500, 'testing')
Architecture: powerpc (ppc)
Kernel: Linux 2.4.25-powerpc
Locale: LANG=C, LC_CTYPE=en_US.UTF-8

Versions of packages mutt depends on:
ii  libc6                       2.3.2.ds1-13 GNU C Library: Shared libraries an
ii  libgnutls10                 1.0.4-3      GNU TLS library - runtime library
ii  libidn11                    0.4.1-1      GNU libidn library, implementation
ii  libncursesw5                5.4-4        Shared libraries for terminal hand
ii  libsasl2                    2.1.18-4.1   Authentication abstraction library
ii  postfix [mail-transport-age 2.1.1-5      A high-performance mail transport 

-- no debconf information


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-13 03:35:11 UTC ab
* Added comment:
{{{
Lacking doc written by Adeodato Simó last year: Thanks! 
Debian brother Bug#254294 closed long ago. Closing 
mutt/1901 now. Thank you for the report Branden!
}}}

* resolution changed to fixed
* status changed to closed
