Ticket:  2183
Status:  closed
Summary: Messages marked as old even though mark_old set to no

Reporter: db-mutt@earth.li
Owner:    mutt-dev

Opened:       2006-02-11 14:44:25 UTC
Last Updated: 2007-04-02 03:16:25 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
I get my mail using fetchmail, which passes it to exim to deliver locally (via procmail).
After recently upgrading to mutt 1.5.9i, all my new mail is being marked as 'O' instead of 'N', even though it is new and even though mark_old is set to no in my .muttrc.  The mark_old setting appears to be working normally for existing mail - ie if I mark a mail as N then switch folders and back, it remains marked as N.

.fetchamilrc:
poll the.earth.li
proto pop3
user db
pass <removed>
fetchall
pass8bits
ssl

.procmailrc:
MAILDIR=$HOME/Mail

:0:
* ^TO.*ircuk-(announce|discuss)@scoutlink.net.*
inbox

:0:
* ^TOdavidball@lycosmail.com.*
lycosmail

:0:
* ^TOdb-x.*@earth.li.*
lycosmail

:0:
inbox

>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-02-12 22:57:19 UTC Derek Martin <invalid@pizzashack.org>
* Added comment:
{{{
> >Description:
> I get my mail using fetchmail, which passes it to exim to deliver
> locally (via procmail).  

It's hard to say if  this would affect the problem you're writing
about, but you could just have fetchmail send the mail to procmail
directly:

   mda "/usr/bin/procmail -f -"

That should be a bit more efficient.  And I suppose it's possible that 
it might even fix your problem...  It might be the MDA that's
mistakenly marking the mail as old.

> After recently upgrading to mutt 1.5.9i, all my new mail is being
> marked as 'O' instead of 'N', even though it is new and even though
> mark_old is set to no in my .muttrc.  The mark_old setting appears
> to be working normally for existing mail - ie if I mark a mail as N
> then switch folders and back, it remains marked as N.

Another thing to try: update to mutt 1.5.11.  It fixes lots of bugs in
1.5.9.

-- 
Derek D. Martin    http://www.pizzashack.org/   GPG Key ID: 0xDFBEAD02
-=-=-=-=-
This message is posted from an invalid address.  Replying to it will result in
undeliverable mail.  Sorry for the inconvenience.  Thank the spammers.


--d6Gm4EdcadzBjdND
Content-Type: application/pgp-signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.1 (GNU/Linux)

iD8DBQFD72jvHEnASN++rQIRAmhJAJ4zcOH/++RZjKx9mPc0wMKK0jl9HwCgsWZu
5LVoYbSwCnj80KjMVh4682Y=
=eaAq
-----END PGP SIGNATURE-----

--d6Gm4EdcadzBjdND--
}}}

--------------------------------------------------------------------------------
2006-02-13 11:10:51 UTC David Ball <db-mutt@earth.li>
* Added comment:
{{{
On Sun, Feb 12, 2006, Derek Martin wrote:
> > >Description:
> > I get my mail using fetchmail, which passes it to exim to deliver
> > locally (via procmail).  
> 
> It's hard to say if  this would affect the problem you're writing
> about, but you could just have fetchmail send the mail to procmail
> directly:
> 
>    mda "/usr/bin/procmail -f -"
> 
> That should be a bit more efficient.  And I suppose it's possible that 
> it might even fix your problem...  It might be the MDA that's
> mistakenly marking the mail as old.

Sadly this makes no difference.

Looking into this more, it appears to be the POP3 server at the other 
end which is inserting "Status: O" headers into the mail.  If I add 
'dropstatus' to the fetchmail config, then all the messages are marked 
as new (including, of course, ones that I have read on the server, so 
this doesn't actually solve the problem).

I'm contacting the server admin to see if there has been a change to the 
config/upgrade of the POP3 server.  Alternatively it may be a change in 
behaviour in mutt to honour 'Status: O' even if mark_old is set to no - 
do you know if there has been such a change?

Either way, it doesn't look like mutt is at fault. :)  Although, it 
might be nice to have a config option to always ignore 'Status: O'...

Thanks,


	David


-- 
David Ball <db-mutt@earth.li>
}}}

--------------------------------------------------------------------------------
2006-02-13 14:13:25 UTC Nicolas Rachinsky <mutt-devel-0@ml.turing-complete.org>
* Added comment:
{{{
* David Ball <db-mutt@earth.li> [2006-02-12 21:05 +0100]:
>  Looking into this more, it appears to be the POP3 server at the other 
>  end which is inserting "Status: O" headers into the mail.  If I add 
>  'dropstatus' to the fetchmail config, then all the messages are marked 
>  as new (including, of course, ones that I have read on the server, so 
>  this doesn't actually solve the problem).

Delete/rename the Status header on delivery.

:0 fw
* ^Status:
| formail -b -R "Status" "X-disabled-Status"

Nicolas

-- 
http://www.rachinsky.de/nicolas
}}}

--------------------------------------------------------------------------------
2007-03-27 18:07:27 UTC 
* Added attachment .muttrc
* Added comment:
.muttrc

--------------------------------------------------------------------------------
2007-04-02 03:16:25 UTC brendan
* Added comment:
Appears to be an upstream bug.

* Updated description:
{{{
I get my mail using fetchmail, which passes it to exim to deliver locally (via procmail).
After recently upgrading to mutt 1.5.9i, all my new mail is being marked as 'O' instead of 'N', even though it is new and even though mark_old is set to no in my .muttrc.  The mark_old setting appears to be working normally for existing mail - ie if I mark a mail as N then switch folders and back, it remains marked as N.

.fetchamilrc:
poll the.earth.li
proto pop3
user db
pass <removed>
fetchall
pass8bits
ssl

.procmailrc:
MAILDIR=$HOME/Mail

:0:
* ^TO.*ircuk-(announce|discuss)@scoutlink.net.*
inbox

:0:
* ^TOdavidball@lycosmail.com.*
lycosmail

:0:
* ^TOdb-x.*@earth.li.*
lycosmail

:0:
inbox

>How-To-Repeat:
>Fix:
Unknown
}}}
* resolution changed to wontfix
* status changed to closed
