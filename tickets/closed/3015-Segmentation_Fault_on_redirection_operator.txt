Ticket:  3015
Status:  closed
Summary: Segmentation Fault on redirection operator

Reporter: mathewpv
Owner:    mutt-dev

Opened:       2008-01-14 03:39:55 UTC
Last Updated: 2008-07-01 08:18:58 UTC

Priority:  major
Component: SMTP
Keywords:  segmentation fault, smtp

--------------------------------------------------------------------------------
Description:
'''Segmentation fault occurs when using the < operator on command line:
'''
~/temp>/'''usr/bin/mutt  address@dom.ain <Testbody'''

Looking up localhost...

Connecting to localhost...

Authenticating (LOGIN)...

Segmentation fault

'''How ever if'''

~/temp>'''/usr/bin/mutt  address@dom.ain'''

is  used and we go through the normal(ui) mode  it is seen that the mail is sent successfully

Linux distribution is Debian testing

Further info:
   I am using smtps (port 465) for sending mail

~/temp>'''mutt -v'''

Mutt 1.5.17 (2007-11-01)
Copyright (C) 1996-2007 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.22-3-686 (i686)
ncurses: ncurses 5.6.20071013 (compiled with 5.6)
libidn: 1.1 (compiled with 1.1)
hcache backend: GDBM version 1.8.3. 10/15/2002 (built Apr 24 2006 03:25:20)
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE +USE_FCNTL  -USE_FLOCK   +USE_INODESORT  +USE_POP  +USE_IMAP  +USE_SMTP  -USE_GSS  -USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO +HAVE_REGCOMP  -USE_GNU_REGEX +HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET +HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM +CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME -EXACT_ADDRESS  -SUN_ATTACHMENT +ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR +HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE -ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

patch-1.5.13.cd.ifdef.2
patch-1.5.13.cd.purge_message.3.4
patch-1.5.13.nt+ab.xtitles.4
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.6.dw.maildir-mtime.1
patch-1.5.8.hr.sensible_browser_position.3

Initially it was thought that it could be a MTA problem with the service provider but the following reply from a user almost confirms it other wise...

''I tried different versions of MUTT.''

''After a couple of hours I solved the problem for me :) .''

''It seems that the version 1.5.16 won't cause such a problem.''







--------------------------------------------------------------------------------
2008-01-18 02:55:09 UTC mathewpv
* Added comment:
The bug has been traced to line 232,233 of smtp.c in smtp_send function

    
{{{
ret = snprintf (buf, sizeof (buf), "MAIL FROM:<%s>",
                    EnvFrom ? EnvFrom->mailbox : from->mailbox);

}}}

It seems that from or from->mailbox may not be defined in all cases

--------------------------------------------------------------------------------
2008-01-18 09:24:26 UTC pdmef
* Added comment:
> The bug has been traced to line 232,233 of smtp.c in smtp_send function     
> {{{
> ret = snprintf (buf, sizeof (buf), "MAIL FROM:<%s>",
>                     EnvFrom ? EnvFrom->mailbox : from->mailbox);
> 
> }}}
> It seems that from or from->mailbox may not be defined in all cases

So the question is what the SMTP code should use as fallback (the sendmail code
seems to omit the '-f addr' part in that case)...

Use username (or 'nobody' if user can't be determined) as mailbox and hostname
(or 'localhost' if hostname can't be determined) as domain part?

Since the sendmail-calling code explicetely tests for 'from' being exactly one address,
I guess it's an invalid assumption that a message always has a sender address? Though
I'd be interested as to when this happens (and why) since mutt should IMHO always generate
some sender for any outgoing message...

Rocco

* keywords changed to segmentation fault, smtp
* milestone changed to 1.6

--------------------------------------------------------------------------------
2008-01-18 09:53:05 UTC P V Mathew
* Added comment:
{{{
Mutt wrote:
> #3015: Segmentation Fault on redirection operator
>
> Changes (by pdmef):
>
>   * keywords:  segmentation fault => segmentation fault, smtp
>   * milestone:  => 1.6
>
> Comment:
>
>  > The bug has been traced to line 232,233 of smtp.c in smtp_send function
>  > {{{
>  > ret = snprintf (buf, sizeof (buf), "MAIL FROM:<%s>",
>  >                     EnvFrom ? EnvFrom->mailbox : from->mailbox);
>  >
>  > }}}
>  > It seems that from or from->mailbox may not be defined in all cases
>
>  So the question is what the SMTP code should use as fallback (the sendmail
>  code
>  seems to omit the '-f addr' part in that case)...
>
>  Use username (or 'nobody' if user can't be determined) as mailbox and
>  hostname
>  (or 'localhost' if hostname can't be determined) as domain part?
>
>  Since the sendmail-calling code explicetely tests for 'from' being exactly
>  one address,
>  I guess it's an invalid assumption that a message always has a sender
>  address? Though
>   
may be... mutt should exit grace fully giving some proper error message.
segmentation fault looks terrible(well, at least to a end user!)
>  I'd be interested as to when this happens (and why) since mutt should IMHO
>  always generate
>  some sender for any outgoing message...
>  Rocco
>
>   
I tried to  determine that, but  the code is too complicated to me.
I will make some more attempts....

Mathew
}}}

--------------------------------------------------------------------------------
2008-01-18 16:07:00 UTC Rocco Rutte
* Added comment:
{{{
Hi,

* Mutt wrote:

>> Since the sendmail-calling code explicetely tests for 'from' being 
>> exactly one address, I guess it's an invalid assumption that a 
>> message always has a sender address?

> may be... mutt should exit grace fully giving some proper error message.

Exiting is just as bad if you ask me. IMHO it should have some fallback 
and try that one and display the error in case the server returns one.

> segmentation fault looks terrible(well, at least to a end user!)

ACK.

>> I'd be interested as to when this happens (and why) since mutt should 
>> IMHO always generate some sender for any outgoing message...

> I tried to  determine that, but  the code is too complicated to me.

I didn't mean finding the bug in the code (if there's any) but rather 
any usage pattern this happens on.

Rocco
}}}

--------------------------------------------------------------------------------
2008-01-26 10:26:44 UTC P V Mathew
* Added comment:
{{{
Mutt wrote:
> #3015: Segmentation Fault on redirection operator
>
> Comment (by Rocco Rutte):
>
>  {{{
>  Hi,
>
>  * Mutt wrote:
>
>  >> Since the sendmail-calling code explicetely tests for 'from' being
>  >> exactly one address, I guess it's an invalid assumption that a
>  >> message always has a sender address?
>
>  > may be... mutt should exit grace fully giving some proper error message.
>
>  Exiting is just as bad if you ask me. IMHO it should have some fallback
>  and try that one and display the error in case the server returns one.
>
>  > segmentation fault looks terrible(well, at least to a end user!)
>
>  ACK.
>
>  >> I'd be interested as to when this happens (and why) since mutt should
>  >> IMHO always generate some sender for any outgoing message...
>
>  > I tried to  determine that, but  the code is too complicated to me.
>
>  I didn't mean finding the bug in the code (if there's any) but rather
>  any usage pattern this happens on.
>
>  Rocco
>  }}}
>
>   
In my case msg->env->from exists and is defined until block of lines  
1474 to 1489 in send.c

1. even in batch mode control enters this block as default value of 
OPT_COPY is M_YES

2. is the variable killfrom being re-used?

3. in my case, killfrom is true even before entry in this block. Hence 
the address gets freed at line 1486.

4. if i change the line 1474 to :

    if (!fcc[0] && !(flags & (SENDPOSTPONED)) && !(flags & SENDBATCH))

from the existing :

  if (!fcc[0] && !(flags & (SENDPOSTPONED)) && (!(flags & SENDBATCH) || 
(quadoption (OPT_COPY) & 0x1)))

no more seg fault occurs and the mail gets send properly.

regards
mathew p v


}}}

--------------------------------------------------------------------------------
2008-07-01 08:18:58 UTC brendan
* Added comment:
Fixed in [a2e8f6fab8d3].

* component changed to SMTP
* resolution changed to fixed
* status changed to closed
