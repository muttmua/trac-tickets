Ticket:  3085
Status:  closed
Summary: mailbox corruption when deleting some attachment

Reporter: rtc
Owner:    mutt-dev

Opened:       2008-06-30 03:03:59 UTC
Last Updated: 2008-07-01 08:12:48 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
If I delete some attachment and close the mailbox, mutt corruptes it: The mail from which I deleted the attachment gets the empty line removed that separates header and body.

--------------------------------------------------------------------------------
2008-06-30 03:04:23 UTC rtc
* Added attachment mutt-copybug.patch
* Added comment:
patch that fixes the problem

--------------------------------------------------------------------------------
2008-07-01 08:10:31 UTC rtc
* Added comment:
(In [49fe0292b503]) When deleting attachments, always print newline separating header from body.
Closes #3085.

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2008-07-01 08:12:48 UTC brendan
* Added comment:
Patch applied, thanks.

* milestone changed to 1.6
* priority changed to major
* version changed to 1.5.18
