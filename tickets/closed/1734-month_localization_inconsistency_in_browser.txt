Ticket:  1734
Status:  closed
Summary: month localization inconsistency in browser

Reporter: Alain Bench <veronatif@free.fr>
Owner:    mutt-dev

Opened:       2003-12-11 10:03:07 UTC
Last Updated: 2010-08-06 18:46:35 UTC

Priority:  minor
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.5.1i
Severity: normal
Keywords: patch

My dear ALL,

    Mutt built with NLS. Environment LC_TIME=fr_FR. The browser shows
French month names when called first (like with "mutt -y"), but English
when called from a mailbox (like with "<change-folder>?").

    Seems everywhere a localized date is needed Mutt sets desired
locale, uses it, and resets it just after to C. But browser doesn't set
nor reset LC_TIME. So it seems browser was designed to print English C
dates (fixed month/day order confirms). But in main(), when ENABLE_NLS,
there is a setlocale(LC_ALL, "") without reset to C.

    The attached patch fixes the case in favour of $LC_TIME, and after
use resets to C in browser and in main().

    If solving case in favour of C is prefered: Cut out the 3 first
hunks, apply only last part to main.c. But IMHO localize is better. And
in fact the strftime() format itself should be localized, or user
setable, or both. I use "%_d %b %_H:%M" and "%_d %b  %Y".


Bye!	Alain.
-- 
Everything about locales on Sven Mascheck's excellent site at new
location <URL:http://www.in-ulm.de/~mascheck/locale/>. The little tester
utility is at <URL:http://www.in-ulm.de/~mascheck/locale/checklocale.c>.
}}}

--------------------------------------------------------------------------------
2003-12-11 10:03:07 UTC Alain Bench <veronatif@free.fr>
* Added attachment patch-1.4.ab.browser_date_locale.2
* Added comment:
patch-1.4.ab.browser_date_locale.2

--------------------------------------------------------------------------------
2003-12-17 06:11:48 UTC Michael Elkins <me@sigpipe.org>
* Added comment:
{{{
On 2003-12-10, Alain Bench wrote:
>     Seems everywhere a localized date is needed Mutt sets desired
> locale, uses it, and resets it just after to C. But browser doesn't set
> nor reset LC_TIME. So it seems browser was designed to print English C
> dates (fixed month/day order confirms). But in main(), when ENABLE_NLS,
> there is a setlocale(LC_ALL, "") without reset to C.

Does anyone recall why LC_TIME is set back to C after every use of
time?

me
}}}

--------------------------------------------------------------------------------
2003-12-18 02:38:52 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2003-12-16 22:11:48 -0800, Michael Elkins wrote:

> Does anyone recall why LC_TIME is set back to C after every use
> of time?

Not really.  I believe, though, that the date parsing routines we
use expect to run in C locale; it might be that the current approach
is easier than switching back to C locale whereever we parse dates.

-- 
Thomas Roessler			      <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2003-12-26 14:41:33 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# feature, not bug
close 1731
tags 844 patch
tags 1004 patch
tags 1296 patch
tags 1488 patch
tags 1637 patch
merge 1644 1685
tags 1644 patch
tags 1644 fixed
tags 1716 patch
tags 1716 fixed
tags 1721 patch
tags 1734 patch
-- broken threading: is #1116 fixed?
}}}

--------------------------------------------------------------------------------
2005-10-01 05:05:23 UTC ab
* Added comment:
{{{
deduppe unform, upload patch-1.4.ab.browser_date_locale.2
fixing this inconsistency and using LC_TIME for date
localization in browser.
}}}

--------------------------------------------------------------------------------
2010-08-06 18:46:35 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [317338b0d490]) add %D format string to $folder_format to expand the time based on $date_format.

properly set the locale for LC_TIME prior to calling strftime()
closes #1734
closes #3406

* resolution changed to fixed
* status changed to closed
