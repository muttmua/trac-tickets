Ticket:  3552
Status:  closed
Summary: .data section is unnecessarily large

Reporter: danf
Owner:    mutt-dev

Opened:       2011-11-30 19:27:24 UTC
Last Updated: 2011-12-03 19:14:22 UTC

Priority:  minor
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Many structs used in mutt are actually constant but are defined without the 'const' keyword. This can slow initialization (slightly) in some environments due to extra copying and increases the amount of writable RAM required at run-time, which can be significant on non-MMU systems. Using const can also increase the opportunities for compiler optimization.

The attached patch marks many such structures as const. On my test x86 build, this reduces the size of .data by over 50%.

--------------------------------------------------------------------------------
2011-11-30 19:28:35 UTC danf
* Added attachment mutt-1.5.21hg-size.diff
* Added comment:
Patch to add const keyword

--------------------------------------------------------------------------------
2011-12-03 19:14:22 UTC Dan Fandrich <dan@coneharvesters.com>
* Added comment:
(In [9e756d1adb76]) Declare many structures const (closes #3552)

Many structs used in mutt are actually constant but are defined
without the 'const' keyword. This can slow initialization (slightly)
in some environments due to extra copying and increases the amount of
writable RAM required at run-time, which can be significant on non-MMU
systems. Using const can also increase the opportunities for compiler
optimization.

The attached patch marks many such structures as const. On my test x86
build, this reduces the size of .data by over 50%.

* resolution changed to fixed
* status changed to closed
