Ticket:  3738
Status:  closed
Summary: no error when attempt to send multipart message encrypted / signed using PGP traditional

Reporter: wyardley
Owner:    mutt-dev

Opened:       2015-02-19 23:43:14 UTC
Last Updated: 2015-12-03 23:25:36 UTC

Priority:  major
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
If I try to send a message using inline signing, but the message contains attachments, it fails to send, which of course is expected. However, it doesn't produce an error beyond 'Mail not sent.'. While I understand all the reasons it's not possible for this to work, it would be nice if a more specific error could be given.

--------------------------------------------------------------------------------
2015-12-03 23:25:36 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [bce2a0e71bf62521f6e6d31fd7386c46da3b457e]:
{{{
#!CommitTicketReference repository="" revision="bce2a0e71bf62521f6e6d31fd7386c46da3b457e"
Provide a better prompt and error for inline PGP with attachments.  (closes #3738)

Change mutt_protect() to check for text/plain before trying to invoke
crypt_pgp_traditional_encryptsign().  This way, mutt can provide a bit
more specific prompt and error message.

Since pgp_mime_auto says it will prompt in the event of any failure,
keep the more generic prompt after the encryptsign call too.
}}}

* resolution changed to fixed
* status changed to closed
