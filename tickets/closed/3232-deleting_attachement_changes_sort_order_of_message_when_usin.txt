Ticket:  3232
Status:  closed
Summary: deleting attachement changes sort order of message when using 'date-received'

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-05-19 22:31:22 UTC
Last Updated: 2016-08-31 07:03:21 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from: http://bugs.debian.org/529250

{{{
1. sort mailbox with sort=date-received

2. delete attachement from an old message

3. sync mailbox ($)

4. watch that message jump to the front of the mailbox

(5. profit?)

}}}

--------------------------------------------------------------------------------
2009-07-31 16:41:25 UTC pdmef
* Added comment:
What type of folder do you observe this with?

--------------------------------------------------------------------------------
2009-09-08 05:09:57 UTC antonio@dyne.org
* Added comment:
the reporter says that the bug "is reproducible with an imap mailbox"

* version changed to 1.5.20

--------------------------------------------------------------------------------
2016-08-31 07:03:21 UTC antonio@dyne.org
* Added comment:
fixed on 1.7.0-1

* resolution changed to fixed
* status changed to closed
