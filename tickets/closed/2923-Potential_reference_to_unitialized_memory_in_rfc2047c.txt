Ticket:  2923
Status:  closed
Summary: Potential reference to unitialized memory in rfc2047.c

Reporter: mj
Owner:    me

Opened:       2007-06-25 18:14:43 UTC
Last Updated: 2010-08-24 23:40:55 UTC

Priority:  minor
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
When I was trying to use the RFC 2047 decoder from Mutt 1.5.16 in my toy project, I noticed that when the rfc2047_decode_word() function returns an error, the destination buffer is not initialized and the caller (rfc2047_decode()) happily calls strlen() on it. I believe that this cannot happen normally in the current version of Mutt, because inputs causing this function to fail never pass the checks in find_encoded_word(), but I still think that it should be fixed to avoid problems in the future.

--------------------------------------------------------------------------------
2010-08-24 23:17:27 UTC me
* Added attachment decode-word-fallback.diff
* Added comment:
proposed fix

--------------------------------------------------------------------------------
2010-08-24 23:18:31 UTC me
* Added comment:
the attached patch falls back to copying the raw string in the event it can't be decoded.

* owner changed to me
* status changed to accepted

--------------------------------------------------------------------------------
2010-08-24 23:18:40 UTC me
* status changed to started

--------------------------------------------------------------------------------
2010-08-24 23:18:52 UTC me
* keywords changed to patch

--------------------------------------------------------------------------------
2010-08-24 23:40:55 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [92b02f77e780]) detect failure to decode word and copy raw string instead; avoids calling strlen() on uninitialized memory

closes #2923

* resolution changed to fixed
* status changed to closed
