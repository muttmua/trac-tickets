Ticket:  3507
Status:  closed
Summary: Mutt ignores SIGWINCH when built with --enable-imap

Reporter: vinc17
Owner:    mutt-dev

Opened:       2011-03-10 11:59:48 UTC
Last Updated: 2011-03-30 14:45:10 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When I configure Mutt with "./configure --enable-imap", it ignores SIGWINCH under some occasions (see below how to reproduce the bug). No such problem when I just do "./configure". It seems that this problem is recent.

1. Open a 80x40 xterm.[[BR]]
2. Run screen in it.[[BR]]
3. Run "mutt -F /dev/null".[[BR]]
4. Detach the session.[[BR]]
5. Open a 80x60 xterm.[[BR]]
6. Reattach the session with "screen -r".

Result: There are 20 blank lines above the help line.

According to strace, Mutt received a SIGWINCH as expected, but it has apparently been ignored.

{{{
[...]
13:02:41 stat("/home/vlefevre/postponed", 0x7fff8f92ef10) = -1 ENOENT (No such file or directory)
13:02:41 write(1, "\33[H\33[39;49m\33[37m\33[40m\33[7mq:Quit "..., 220) = 220
13:02:41 rt_sigaction(SIGINT, {0x464ff0, [], SA_RESTORER, 0x7f045d0161e0}, NULL, 8) = 0
13:02:41 write(1, "\33[?1h\33=", 7)     = 7
13:02:41 poll([{fd=0, events=POLLIN}], 1, 300000) = ? ERESTART_RESTARTBLOCK (To be restarted)
13:02:51 --- SIGWINCH (Window changed) @ 0 (0) ---
13:02:51 rt_sigreturn(0x1c)             = -1 EINTR (Interrupted system call)
13:02:51 rt_sigaction(SIGINT, {0x464ff0, [], SA_RESTORER|SA_RESTART, 0x7f045d0161e0}, NULL, 8) = 0
13:02:51 ioctl(0, SNDCTL_TMR_TIMEBASE or TCGETS, {B38400 opost isig -icanon -echo ...}) = 0
13:02:51 rt_sigaction(SIGINT, {0x464ff0, [], SA_RESTORER, 0x7f045d0161e0}, NULL, 8) = 0
13:02:51 poll([{fd=0, events=POLLIN}], 1, 300000) = 1 ([{fd=0, revents=POLLIN}])
[...]
}}}

This can also sometimes be reproduced when increasing the xterm window (height or width).

{{{
Mutt 1.5.21+22 (8d0281f79b21) (2010-12-30)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.37-2-amd64 (x86_64)
ncurses: ncurses 5.8.20110307 (compiled with 5.8)
libidn: 1.20 (compiled with 1.20)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  +USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_SMTP
-USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  +HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.
}}}

--------------------------------------------------------------------------------
2011-03-10 12:11:36 UTC vinc17
* Added comment:
The bug was introduced in:
{{{
changeset:   6166:047bd501d6db
branch:      HEAD
user:        Brendan Cully <brendan@kublai.com>
date:        Mon Feb 21 23:12:52 2011 -0800
summary:     Lower $imap_keepalive default to 300 seconds
}}}

--------------------------------------------------------------------------------
2011-03-10 12:19:57 UTC vinc17
* Added comment:
I suppose the bug was there previously, but wasn't visible with the default time values. And 6166:047bd501d6db changed one of the default values...

If I understand correctly, the default Timeout value is 600. 6166:047bd501d6db lowered ImapKeepalive from 900 to 300. In keymap.c, the test ImapKeepalive >= i becomes false, hence the problem, IMHO. So, I'd say that the bug is here in keymap.c:
{{{
        while (ImapKeepalive && ImapKeepalive < i)
        {
          timeout (ImapKeepalive * 1000);
          tmp = mutt_getch ();
          timeout (-1);
          if (tmp.ch != -2)
            /* something other than timeout */
            goto gotkey;
          i -= ImapKeepalive;
          imap_keepalive ();
        }
}}}

--------------------------------------------------------------------------------
2011-03-12 04:46:21 UTC me
* Added comment:
{{{
On Thu, Mar 10, 2011 at 12:19:57PM -0000, Mutt wrote:

I don't see an obvious bug in that snippet.  Have you verified the bug
goes away if you set imap_keepalive > timeout?  mutt_getch() should
return -1 when SIGWINCH is received, which will break out of the loop
and jump down to the gotkey label, and then return -1.


}}}

--------------------------------------------------------------------------------
2011-03-12 12:34:33 UTC vinc17
* Added comment:
Replying to [comment:3 me]:
> Have you verified the bug goes away if you set imap_keepalive > timeout?  

Yes. And if I set it back to 300 (< timeout), the bug reappears.

--------------------------------------------------------------------------------
2011-03-12 16:30:33 UTC me
* Added comment:
{{{
On Sat, Mar 12, 2011 at 12:34:33PM -0000, Mutt wrote:

This patch fixes the problem.  mutt_getch() returns {-2,OP_NULL} for
both a timeout and a SIGWINCH, so the loop around imap_keepalive() needs
to be a bit smarter.

diff -r 8d0281f79b21 keymap.c
--- a/keymap.c	Mon Mar 07 10:17:59 2011 -0600
+++ b/keymap.c	Sat Mar 12 08:34:45 2011 -0800
@@ -435,8 +435,11 @@
  	  timeout (ImapKeepalive * 1000);
  	  tmp = mutt_getch ();
  	  timeout (-1);
-	  if (tmp.ch != -2)
-	    /* something other than timeout */
+	  /* If a timeout was not received, or the window was resized, exit the
+	   * loop now.  Otherwise, continue to loop until reaching a total of
+	   * $timeout seconds.
+	   */
+	  if (tmp.ch != -2 || SigWinch)
  	    goto gotkey;
  	  i -= ImapKeepalive;
  	  imap_keepalive ();
}}}

--------------------------------------------------------------------------------
2011-03-30 14:45:10 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [f7160c94ff70]) fix bug where SIGWICH is ignored when $imap_keepalive < $timeout

closes #3507

* resolution changed to fixed
* status changed to closed
