Ticket:  3325
Status:  closed
Summary: attachment type misdetection for small .tar.gz

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-09-03 23:29:54 UTC
Last Updated: 2009-09-07 04:22:13 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi,
we have found that there is an attachment type misdetection for a small .tar.gz file, you can see the complete bug report on http://bugs.debian.org/541241.

I had a look at your code and in sendlib.c the mutt_make_file_attach function will take care of looking up the mime type, it will use 'mutt_lookup_mime_type', then based on the extension it will report the mimetype back to mutt. Unfortunately .gz, .bz2 and other compressed formats are not classified as mime types because they are 'encodings, so you won't find anything in /etc/mime.types

In that case (i.e.: no content-type found in mime-type) you're trying to guess if it's a binary file or not with this check

{{{
    if (info->lobin == 0 || (info->lobin + info->hibin + info->ascii)/ info->lobin >= 10)
    {
      /*
       * Statistically speaking, there should be more than 10% "lobin"
       * chars if this is really a binary file...
       */
      att->type = TYPETEXT;
      att->subtype = safe_strdup ("plain");
    }
    else
    {
      att->type = TYPEAPPLICATION;
      att->subtype = safe_strdup ("octet-stream");
    }
}}}

in this particular case this small .tar.gz file has the following info data:
{{{
(gdb) print *info
$5 = {hibin = 43584, lobin = 8102, crlf = 522, ascii = 29871, linemax = 2790, space = 0, binary = 1, from = 0, dot = 0, cr = 1}
(gdb) 
}}}

Unfortunately this results in a percentage of lobin chars slightly bigger than 10:
{{{
>>> (43584+8102+29871)/8102.0
10.066279930881263
>>> 
}}}

so mutt believes that the attachment is not a binary file.

How could you fix this?
there are some options: you could link to libmagic and let libmagic cares about the mime-types, otherwise you could add a check on mutt_lookup_mime_type (sendlib.c) and cover the major encoding by assigning them the content type of application/octet-stream (.gz, .zip and .bz2 should be enough).

If you want I can propose a patch, probably libmagic is the best option but I suppose that you don't want to introduce another dependency

Cheers
Antonio

--------------------------------------------------------------------------------
2009-09-04 16:24:51 UTC Derek Martin
* Added attachment part0001.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
On Thu, Sep 03, 2009 at 11:29:55PM -0000, Mutt wrote:

Who said they're encodings?  I don't see them listed in the
Content-Transfer-Encodings section of the MIME RFCs, nor do I see them
mentioned anywhere pertaining to "encoded words", and I don't see any
other part of the RFCs that might suggest they should be classified as
encodings.  

Classifying these as encodings seems impractically pedantic at best,
but more likely just plain wrong.  They are application data, specific
to their respective compression applications.  Note also that while
not an officially recognized IANA MIME type, on platforms other than
debian and derivatives, gzip does indeed appear in the MIME type
listings.  This makes sense: once the e-mail is "decoded" and the
attachment saved to a file, the data is still gzipped.  It is in fact
a non-text attachment, as was intended to be hanled by MIME.  It
requires an "application" to use the data.  It's no more an encoding
than HTML and MP3 are encodings (which, in fact, they are, though not
in the sense of the MIME RFCs as far as I can tell).

I think this is not a bug, but a system misconfiguration based on
bogus policy.
}}}

--------------------------------------------------------------------------------
2009-09-04 17:54:50 UTC antonio@dyne.org
* Added comment:
Hi Derek,
thanks for your clear explanation; I will resolve this bug because it's clear that it is not a mutt bug and I will follow-up this on the Debian side.

Cheers
Antonio

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2009-09-07 01:03:54 UTC vinc17
* Added comment:
Replying to [comment:1 Derek Martin]:
> Who said they're encodings?  I don't see them listed in the
> Content-Transfer-Encodings section of the MIME RFCs, nor do I see them
> mentioned anywhere pertaining to "encoded words", and I don't see any
> other part of the RFCs that might suggest they should be classified as
> encodings.

FYI they are encodings for HTTP ([http://www.rfc-editor.org/rfc/rfc2616.txt RFC 2616]) to allow transparent decompression. Unfortunately this isn't supported for the mail (e.g. there's no standard way to tell the MUA that some attachment is compressed text/plain so that it can display the uncompressed text as if it were not compressed in the first place).

--------------------------------------------------------------------------------
2009-09-07 04:22:13 UTC Derek Martin
* Added attachment part0001.2.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
That's fine and dandy, but the HTTP protocol deals with them directly,
no need to treat them specially in terms of MIME.  How HTTP treats
compression is not relevant to MIME processing of e-mail.  MIME has
been embraced in many places, but its primary function is still to
tell e-mail clients how to handle MIME attachments according to RFC
2045 and its cousins.  You can't break mail because someone later
decided that compression was an "encoding" rather than an "application
data type" in some other context...
}}}
