Ticket:  3207
Status:  closed
Summary: Conform to XDG Base Directory Specification

Reporter: wraichia
Owner:    mutt-dev

Opened:       2009-03-23 14:56:02 UTC
Last Updated: 2016-11-14 02:46:39 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
http://standards.freedesktop.org/basedir-spec/basedir-spec-0.5.html

It'd be nice if mutt conformed to freedesktop's XDG Base Directory Specification.

If all applications conformed to this, it'd make backing up important config files, while ignoring large directories such as ~/.mozilla, fast and easy.

--------------------------------------------------------------------------------
2009-03-23 17:12:48 UTC pdmef
* Added comment:
All config/data files in mutt can be configured to conform to this standard. Mutt even supports the use environment variables. Symlinks can help to. I don't think there's anything wrong with including a sample muttrc conforming to that standard.

What won't work is conditional expressions when one of the environment variables isn't set.

--------------------------------------------------------------------------------
2013-10-25 16:02:25 UTC muffins
* Added attachment patch-1.5.22.mo.xdg.3

--------------------------------------------------------------------------------
2013-10-25 16:03:10 UTC muffins
* Added comment:
http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html

Here is a patch to tell mutt to look in the usual locations according to the XDG spec for `muttrc`. The checks happen after checking `$HOME` and `$HOME/.mutt` for backward compatibility.

--------------------------------------------------------------------------------
2013-10-25 21:02:50 UTC me
* Added comment:
{{{
On Fri, Oct 25, 2013 at 04:03:10PM -0000, Mutt wrote:

I'd been wondering if we shouldn't add support for this as well, 
but since it was associated with the freedesktop.org (GUI) 
project, I wasn't sure it was applicable to Mutt.

I looked into what policy Debian and Fedora have, and Debian does 
encourage all upstream software to adopt the XDG [1].

Debian/Ubuntu has a libxdg-basedir package that is much more 
involved, and perhaps overkill (the attached patch is very 
simple).

[1] https://wiki.debian.org/UpstreamGuide#User_home_directories

So I think this patch should be applied.
}}}

--------------------------------------------------------------------------------
2013-10-25 21:18:08 UTC dgc
* Added comment:
I'm pretty suspicious of anything that smells Linux-specific, but this looks harmless enough and doesn't mandate any new behavior. +0.

--------------------------------------------------------------------------------
2013-11-07 19:54:20 UTC muffins
* Added comment:
Whether XDG is Linux specific is irrelevant, as the "base directory spec" only specifies a few default paths.  Additionally, the standard is independent of any X feature; many cli tools already support the standard in some capacity.  To name a few: git, htop, inkscape, morituri, mpd, newsbeuter, pianobar, cower, pulseaudio, ranger, quvi, systemd.

--------------------------------------------------------------------------------
2013-11-11 14:35:50 UTC jpacner
* Added comment:
Replying to [comment:6 muffins]: +1

--------------------------------------------------------------------------------
2016-11-14 02:46:39 UTC Damien Riegel <damien.riegel@gmail.com>
* Added comment:
In [changeset:"42fee7585faeef1c1abec1d127a7bb292605383a" 6841:42fee7585fae]:
{{{
#!CommitTicketReference repository="" revision="42fee7585faeef1c1abec1d127a7bb292605383a"
search muttrc file according to XDG Base Specification (closes #3207)

First of all, the MUTT_VERSION symbol is now concatenated (when
possible) at compile time.

Then, the logic to find the config file has been changed a bit to remove
unnecessary calls to access(), so now each possible locations for the
config file is only tested once, and it stops as soon as a valid one has
been found. So instead of:

  access("/home/dkc/.muttrc-1.7.1", F_OK) = -1 ENOENT (No such file or directory)
  access("/home/dkc/.muttrc", F_OK)       = 0
  access("/home/dkc/.muttrc", F_OK)       = 0
  access("/home/dkc/.muttrc", F_OK)       = 0
  [... Tests for Muttrc ... ]
  access("/home/dkc/.muttrc", F_OK)       = 0

We now have:

  access("/home/dkc/.muttrc-1.7+13 (f658e517960e)", F_OK) = -1 ENOENT (No such file or directory)
  access("/home/dkc/.muttrc", F_OK)       = 0

It also cleans up the case where -F is passed on the command line but
points to a non-existent file by moving the error path closer to the
actual fail condition.

Finally, it adds partial support for the XDG Base Directory
Specification. mutt will now try to locate its config at:

  $XDG_CONFIG_HOME/mutt/muttrc-MUTT_VERSION
  $XDG_CONFIG_HOME/mutt/muttrc.

If XDG_CONFIG_HOME is not set, it will use '~/.config' as a default.
}}}

* resolution changed to fixed
* status changed to closed
