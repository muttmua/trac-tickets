Ticket:  2115
Status:  closed
Summary: reply-hook not working with "From_" header

Reporter: krys.power@gmail.com
Owner:    mutt-dev

Opened:       2005-10-17 22:12:21 UTC
Last Updated: 2005-10-21 06:10:23 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
reply-hook '~h "From xxx@zzz.yy"' 'set signature=~/.sig'
does not work.
However
save-hook '~h "From xxx@zzz.yy"' +aaa
does work

I do not mean "From:" but "From" header. Therse two are different headers.
>How-To-Repeat:
paste
reply-hook '~h "From xxx@zzz.yy"' 'set signature=~/.sig'
into ~/.muttrc
>Fix:
}}}

--------------------------------------------------------------------------------
2005-10-19 01:49:54 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2005-10-17 23:12:22 +0200, New Mutt PR wrote:

> I do not mean "From:" but "From" header. Therse two are different headers.

The key thing to understand is that there is no such thing as a
"From" header: It's not a header.  It's not something that even
exists for every mailbox format.  Don't rely on its presence.  If it
works in header patterns, that's an accident.

Try the Return-Path header instead.  If that's matched
inconsistently, that's a problem worth investigating.

Regards,
--=20
Thomas Roessler =B7 Personal soap box at <http://log.does-not-exist.org/>.
}}}

--------------------------------------------------------------------------------
2005-10-22 00:10:23 UTC ab
* Added comment:
{{{
On 2005-10-17 23:12:22 +0200, New Mutt PR wrote:

    Thanks for the report, Krystian. Unreproducable: Your
example works as expected, both with 1.5.9 and 1.5.11, as
soon as a the From_ separator is present in the mail.

BTW that are regexps: Better use '~h "^From xxx@zzz\\.yy"'
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2005-10-22 00:10:24 UTC ab
* Added comment:
{{{
retitled s/From/From_/
}}}
