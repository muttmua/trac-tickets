Ticket:  3680
Status:  closed
Summary: unary operator warning during configure

Reporter: evgeni
Owner:    mutt-dev

Opened:       2014-03-08 21:26:46 UTC
Last Updated: 2017-11-06 22:25:15 UTC

Priority:  minor
Component: build
Keywords:  

--------------------------------------------------------------------------------
Description:
When configuring with "--with-idn", the following warning is issued:

{{{
checking for idna_to_ascii_from_locale... no
../configure: line 12285: test: =: unary operator expected
}}}

The attached patch fixes this.

--------------------------------------------------------------------------------
2014-03-08 21:27:25 UTC evgeni
* Added attachment 0001-Fix-a-unary-operator-warning-during-configure.patch

--------------------------------------------------------------------------------
2017-11-06 22:25:15 UTC kevin8t8
* Added comment:
Thank you for the ticket and patch.

The root cause of this was actually the $ac_cv_search_STRINGPREP_CHECK_VERSION variable name, and was fixed in changeset:c7f116b6dc20

* resolution changed to fixed
* status changed to closed
