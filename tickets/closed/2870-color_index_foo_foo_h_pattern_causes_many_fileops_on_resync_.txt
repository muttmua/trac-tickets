Ticket:  2870
Status:  closed
Summary: color index foo foo ~h pattern causes many fileops on resync only

Reporter: marc_soft@merlins.org
Owner:    mutt-dev

Opened:       2007-03-29 16:43:53 UTC
Last Updated: 2007-04-30 01:55:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Executive summary: 
I am using header_cache="~/Maildir/" and set maildir_header_cache_verify = no
Each "color index ~h" rule does not cause any stats in the Maildir/.snd/
directory when I _open_ the folder (apparently, the header cache is used
for that). 
However each of my sent messages is stated and opened four times when
mutt resyncs the already opened folder, that is 4 times per ~h line that
is processed from my muttrc

Details:
I had a bunch of rules like these:
color index     brightgreen     black           '! ~p'
color index     black           brightblue      '~C afscv'
color index     brightwhite     black           '~p'

Those rules are ok, mutt processes them quickly enough (a few seconds)
whether I open a folder, or mutt resyncs it.

However, rules like these are causing significant delays on auto resyncs
(i.e. when an open folder is externally modified). 12 rules like the
ones below caused a resync time of 5mn for a 40,000 message folder
color index    black           brightyellow    '~h List-Id:.*bugtraq'
color index    brightblue      black           '~h hits\=3'
color index    brightblue      brightyellow    '~h post\ from'

mutt can process ~f, ~C and ~p from the header cache, but not ~h. Or at
least it can't do it on resync apparently (unless the colors are cached
in the header cache file, which makes for a fast open, but a slow resync?)

There is still a clear problem. Just adding a single:
> color index    black           brightyellow    '~h List-Id:.*bugtraq'

causes 4 stats and opens for _each_ message (again, on resync only)
> open("/home/merlin/Maildir//.snd/cur/1162956827.15796_57.magic:2,S", O_RDONLY|O_LARGEFILE) = 3
> fstat64(3, {st_mode=S_IFREG|0600, st_size=1914, ...}) = 0
> open("/home/merlin/Maildir//.snd/cur/1162956827.15796_57.magic:2,S", O_RDONLY|O_LARGEFILE) = 3
> fstat64(3, {st_mode=S_IFREG|0600, st_size=1914, ...}) = 0
> open("/home/merlin/Maildir//.snd/cur/1162956827.15796_57.magic:2,S", O_RDONLY|O_LARGEFILE) = 3
> fstat64(3, {st_mode=S_IFREG|0600, st_size=1914, ...}) = 0
> open("/home/merlin/Maildir//.snd/cur/1162956827.15796_57.magic:2,S", O_RDONLY|O_LARGEFILE) = 3
> fstat64(3, {st_mode=S_IFREG|0600, st_size=1914, ...}) = 0

If I add a second ~h line, I get 8 fstat and open calls _per_ message.
Now, that explains why I was seeing 5mn rsync times.

The unanswered questions are:
1) why can mutt use the header cache to run the ~h rules when the folder
   is opened, but not when it's resynced

2) Why does adding a message to a folder cause mutt to ignore its entire
   cache and rebuild a brand new one at resync time, scanning all messages
   multiple times, when at folder open time, mutt looks smart enough to use
   the cache for already cached messages, and only open/index/scan whatever
   few messages that weren't in the cache already?

3) 4 fstat and open calls per message in the folder? That's obviously at
   least 3 too many, but most likely 4 times too many.

>How-To-Repeat:
Take 40,000 message ~/Maildir/.snd folder, open from mutt, send a random email that gets fcced to that same folder, see how long mutt takes to reopen the .snd folder.
Repeat operation by adding 10 "color index red black ~h fooheader" rules in muttrc
Verify fileops with strace -e trace=file mutt
>Fix:
my current workaround has been to remove all ~h color rules.

Fixes would be:
1) mutt should never open a stat the same file 4 times in a row
2) mutt resync code looks very suboptimal compared to folder open code.
   If it were to only parse new messages in the folder (messages not in
   the cache), it would effectively fix this problem.
}}}

--------------------------------------------------------------------------------
2007-03-30 02:44:04 UTC Michael Elkins <me@sigpipe.org>
* Added comment:
{{{
On Thu, Mar 29, 2007 at 05:43:53PM +0200, marc_soft@merlins.org wrote:
> The unanswered questions are:
> 1) why can mutt use the header cache to run the ~h rules when the folder
>    is opened, but not when it's resynced
> 
> 2) Why does adding a message to a folder cause mutt to ignore its entire
>    cache and rebuild a brand new one at resync time, scanning all messages
>    multiple times, when at folder open time, mutt looks smart enough to use
>    the cache for already cached messages, and only open/index/scan whatever
>    few messages that weren't in the cache already?

Unfortunately, it is possible that a message in the cur/ subdirectory is
modified in another Mutt which would be undetectable without a full
rescan.  Operations which rewrite the message header such as
edit-message or {break,join}-threads would cause the cache to be bad.
You can't rely on the times from stat() because Mutt uses utime() to
restore the times after an edit.

Mutt assumes the cache is valid when the mailbox is open, so it only
scans for messaages not already in the cache.

> Fixes would be:
> 1) mutt should never open a stat the same file 4 times in a row

I am not sure why that would happen, but I agree.

> 2) mutt resync code looks very suboptimal compared to folder open code.
>    If it were to only parse new messages in the folder (messages not in
>    the cache), it would effectively fix this problem.

This won't work, as described above.

me
}}}

--------------------------------------------------------------------------------
2007-03-30 03:48:44 UTC Marc MERLIN <marc_soft@merlins.org>
* Added comment:
{{{
On Fri, Mar 30, 2007 at 04:25:04AM +0200, Michael Elkins wrote:
>  > 2) Why does adding a message to a folder cause mutt to ignore its entire
>  >    cache and rebuild a brand new one at resync time, scanning all messages
>  >    multiple times, when at folder open time, mutt looks smart enough to use
>  >    the cache for already cached messages, and only open/index/scan whatever
>  >    few messages that weren't in the cache already?
>  
>  Unfortunately, it is possible that a message in the cur/ subdirectory is
>  modified in another Mutt which would be undetectable without a full
>  rescan.  Operations which rewrite the message header such as
>  edit-message or {break,join}-threads would cause the cache to be bad.
>  You can't rely on the times from stat() because Mutt uses utime() to
>  restore the times after an edit.
>  
>  Mutt assumes the cache is valid when the mailbox is open, so it only
>  scans for messaages not already in the cache.

That makes sense, but given the time penalty that this creates, would it be
reasonable to have an option like
set     maildir_header_cache_verify = no
(which is what I thought that option did to start with), which would disable
the full resync behaviour? I think most people would be happy to trade not
editing the same folder from two mutts at the same time vs several minutes
of rsync each time anything is modified.

Also, I'm confused, wouldn't modifying a message from a second mutt, update
the cache anyway? (maybe not, but even if not, I'd be happy to have a "do
not do full resyncs option")
  
>  > Fixes would be:
>  > 1) mutt should never open a stat the same file 4 times in a row
>  
>  I am not sure why that would happen, but I agree.

although come to think of it, what really impacted me was actually the fact
that mutt would do this for each and every index color line too, so let's
say 40 stats and open per message if I had index color ~h lines.

If just that bug is fixed, I think it would alleviate most of the slowdown
problems, even if mutt still does full resyncs.

Does that sound reasonable?

Thanks
Marc
-- 
"A mouse is a device used to point at the xterm you want to type in" - A.S.R.
Microsoft is to operating systems & security ....
                                      .... what McDonalds is to gourmet cooking
Home page: http://marc.merlins.org/
}}}

--------------------------------------------------------------------------------
2007-03-30 21:58:53 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2007-03-30 04:25:04 +0200, Michael Elkins wrote:
>  Unfortunately, it is possible that a message in the cur/ subdirectory is
>  modified in another Mutt which would be undetectable without a full
>  rescan.  Operations which rewrite the message header such as
>  edit-message or {break,join}-threads would cause the cache to be bad.
>  You can't rely on the times from stat() because Mutt uses utime() to
>  restore the times after an edit.

You could look at the ctime, for the file systems that support it
(this could be an option).
}}}

--------------------------------------------------------------------------------
2007-04-30 01:55:00 UTC brendan
* Added comment:
This looks like a duplicate of #1216, and should be fixed by [73894f3f1943].

* Updated description:
{{{
Executive summary: 
I am using header_cache="~/Maildir/" and set maildir_header_cache_verify = no
Each "color index ~h" rule does not cause any stats in the Maildir/.snd/
directory when I _open_ the folder (apparently, the header cache is used
for that). 
However each of my sent messages is stated and opened four times when
mutt resyncs the already opened folder, that is 4 times per ~h line that
is processed from my muttrc

Details:
I had a bunch of rules like these:
color index     brightgreen     black           '! ~p'
color index     black           brightblue      '~C afscv'
color index     brightwhite     black           '~p'

Those rules are ok, mutt processes them quickly enough (a few seconds)
whether I open a folder, or mutt resyncs it.

However, rules like these are causing significant delays on auto resyncs
(i.e. when an open folder is externally modified). 12 rules like the
ones below caused a resync time of 5mn for a 40,000 message folder
color index    black           brightyellow    '~h List-Id:.*bugtraq'
color index    brightblue      black           '~h hits\=3'
color index    brightblue      brightyellow    '~h post\ from'

mutt can process ~f, ~C and ~p from the header cache, but not ~h. Or at
least it can't do it on resync apparently (unless the colors are cached
in the header cache file, which makes for a fast open, but a slow resync?)

There is still a clear problem. Just adding a single:
> color index    black           brightyellow    '~h List-Id:.*bugtraq'

causes 4 stats and opens for _each_ message (again, on resync only)
> open("/home/merlin/Maildir//.snd/cur/1162956827.15796_57.magic:2,S", O_RDONLY|O_LARGEFILE) = 3
> fstat64(3, {st_mode=S_IFREG|0600, st_size=1914, ...}) = 0
> open("/home/merlin/Maildir//.snd/cur/1162956827.15796_57.magic:2,S", O_RDONLY|O_LARGEFILE) = 3
> fstat64(3, {st_mode=S_IFREG|0600, st_size=1914, ...}) = 0
> open("/home/merlin/Maildir//.snd/cur/1162956827.15796_57.magic:2,S", O_RDONLY|O_LARGEFILE) = 3
> fstat64(3, {st_mode=S_IFREG|0600, st_size=1914, ...}) = 0
> open("/home/merlin/Maildir//.snd/cur/1162956827.15796_57.magic:2,S", O_RDONLY|O_LARGEFILE) = 3
> fstat64(3, {st_mode=S_IFREG|0600, st_size=1914, ...}) = 0

If I add a second ~h line, I get 8 fstat and open calls _per_ message.
Now, that explains why I was seeing 5mn rsync times.

The unanswered questions are:
1) why can mutt use the header cache to run the ~h rules when the folder
   is opened, but not when it's resynced

2) Why does adding a message to a folder cause mutt to ignore its entire
   cache and rebuild a brand new one at resync time, scanning all messages
   multiple times, when at folder open time, mutt looks smart enough to use
   the cache for already cached messages, and only open/index/scan whatever
   few messages that weren't in the cache already?

3) 4 fstat and open calls per message in the folder? That's obviously at
   least 3 too many, but most likely 4 times too many.

>How-To-Repeat:
Take 40,000 message ~/Maildir/.snd folder, open from mutt, send a random email that gets fcced to that same folder, see how long mutt takes to reopen the .snd folder.
Repeat operation by adding 10 "color index red black ~h fooheader" rules in muttrc
Verify fileops with strace -e trace=file mutt
>Fix:
my current workaround has been to remove all ~h color rules.

Fixes would be:
1) mutt should never open a stat the same file 4 times in a row
2) mutt resync code looks very suboptimal compared to folder open code.
   If it were to only parse new messages in the folder (messages not in
   the cache), it would effectively fix this problem.
}}}
* resolution changed to duplicate
* status changed to closed
