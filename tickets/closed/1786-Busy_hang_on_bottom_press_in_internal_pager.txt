Ticket:  1786
Status:  closed
Summary: Busy hang on "bottom" press in internal pager

Reporter: J.H.M.Dassen (Ray) <fsmla@xinara.org>
Owner:    mutt-dev

Opened:       2004-01-31 09:32:50 UTC
Last Updated: 2005-08-08 17:35:02 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.5.1-200401
Severity: normal

-- Please type your report below this line

Here's a repeatable "hang" scenario for sid's mutt on i386:
- start "mutt -n -F /dev/null -f borked" (borked being the mailbox included
  below)
- ":bind pager G bottom"
- Press enter to view the message
- Press "G"
Mutt is now in a busy loop and hangs. Attaching an strace to it shows it
busy with an occasional mremap() and numerous llseek()s.

Here's the "borked" mailbox, bzip2-ed and uuencoded:

begin 644 borked.bz2.uue
M0EIH.3%!629362@/``(`!+__A\7X4$!3________^K____!YG[D`?____^`/
M;=]QXH$/7>(]VB@Z:CL>[>8VU:E59#9[LJ*)``!T`DD(1B::F`1IIHQ`&DU/
M$RGHI^I/(]`IO335&:ALU30Q#0T\AD09#0`:`:C()A-"-4]-0]331FIHT&@`
M:`-`!H``!H`````TT#30-3(5/2&:30``:,@:``T-``-!H````````-`:`2$B
M31)C5-I'E&&IZ@:9`S2`&GJ:&@T/4/4!H!H`````!H`(,`F(P3)A!A,3`F`(
M81@(8$P)A,$8``":&$Q-,"!(D!`FIB9#(F1)[4T%-&F)@FC)IY-0#(,(PF(,
MC3)H;4#(#(R8)'>+CG6GT#*`"M=_5+PQE@O)AX>/-$'/,?^;R5J+$E.I(XS8
M%`:0%D\(+G[.0<J7[%X\V$(N#@,DP3=P199BB;&,I0H6IQ-[J-[W8D[!YI3-
M!K9>^-BZXS,EWCC*,MM[4.#?4T[>A_'*E&QC;&VV#/,T?\=?(ER(:(0C3QP^
M![.2\\C5'LR<POT]7G#&-6:NYW9R:8M3%1M'X5AT+VF\LZ9=VJ^/CW,NMG>]
M@)E..9K'XH9F15@B,N4L@P3.WT&%#3ODT<]RBUG(4[5G.)OI/K=O,\]N>UUP
MJ#-KT3(/QR8409?VX="2XUZ4OQR?6@`@5LXL2\M8[#8H=FJ"`2&N%A;5+*#[
M3\%-`]C&XT@AZ4HT.->TS81Z7/2NHGB.Z4]KN=JMLFW20]5L.)4`.P<\\)`U
M?XAHI0AHL'1YL81TRNPA;%%Z=\*#I!3,%NMMYI*NV]\*$8WU*8F^K9#7N\4&
MM!<CP211#G#LVN56J825R+PF4(*%Q.0P@)F0P!'6!.`F:<./+5AP4NE7T`H)
MT&-,@E93O!GD")@Y3!0N)P*I'L75G,\%(ETDBH,.B!L":0UP8YZ;$A2E`-TR
M'"_@'H4(NB;)4`W3SG$1D!.H&[,+,&*)V/O"`-X"J[#'NE983S(?E`?.L)#J
M"$XF!J3/4TFQ:PT,%3'-K#7)$(,F54QDA16L,9'$FB,`*"Q)(6ETLFIF)18D
MM]HR"3%FO.;J@?<)%L;30?:X%YJ5.\M!&ZPS,VYFV_1LQ)+I9+8IXN)ZEJ^#
M[T$8FY>)W^G^)5OCJ^X]'D.HMK@.@Z3O((4<;MIG):7&P>MF<\9>&?/Z/$"W
M&2R3>%Q;(+O>]F]'OVR0/#W],I+J7.2*%E^%H[OFR^=ZP)[M'H9P^/P20L11
M(9^U2X#Q&]/$OM;R$<E&K)$*-3='#(_GJUF95Z]?"O?FP[4_I#(A`9:1#1:8
M/)`=!R5@'24A,\9F8=XZTI$Q$:^P'M#/B*HR)-T)K%FIFQB>&IM[>ED[3,F8
M&:;`8TFO/S;#U,/0$OWA6H=)Z=F;%\4/66#9'#>WK!JC`UY(@BD?`7#L.E6_
M;&WSC5+:,.$B@UT@>%%U$;\+*^`F2VQ$@R`X;6@LV/L!V8=)I9;S`HF0*,TO
M8REK8+D1N@L-+@LTK6)H2(FH##4MXABC(3"J.81:9!]B7<:D]+P`_CO7C"1<
M4HGH3U<F;GP3#^@WZ`3A9_9#UB'8BIF(E7*_!<>&%1H+81(&AO:R2Q0[`R@U
MSW`P8"T7P8\[+ZC$P.(#2A`&$1$50!"`K%^*E>=RLE:@,IPFF.R&L2V&,A\(
M5;<PXK1EJ)QE#6+F5O)V7^WP/+T'ME%!]HX<^@=_(1@[["13I[SOJ^-2B@)H
M]HW\_<S:.8P,WEN+9ZS[D2WL?J#'<]:I&G/FFK[,RDJ"=&/H8=]EQQ%`L-Z_
M>!^B;&U&:8Q$I*`Z;*,ZG4U%T,PP,-[(LT57%4OYQVC9)288BO8?R'P,Y^?6
MH?`UE9'M7D=UIM3/;-2@SL&,/AR4@^"4*S8V#9DO6P]=CT4P=Z<XRW1\.C21
M!JU4<6VJOJ9`R!C(??_5-^>#@:5N<9!UT92(ICC0N#,P5@\SK`3=F;EE^0]!
M.#Y&-`W3F)`^5:YB+?(L[Q#0^(G5!VT>N&`AG(."-8M;XB+1RU2B565PG,6C
M@(B7>&$:YJ3'I67TU"L6A>>HI7C@B>@>2E/(8)"8Y0^8+837`;OF,J&_6*\>
M<4_.0]R+5#/+<5O:L%5:&*6IU3.\>SA@QW6;(-%)P?!K:M88R62"!V(8,"M<
M)(EOPU\15>,P-.?*YC['DXLW--QVW$U(6S(-V`=2=)"Y=)2W9[7@$<J/`*GV
M+N_4(N$6D@N(]#5AA0GZ[&>]CV!)G^(BDZ5"Z#$D;$YHZ@CGY>><E5AY5KIT
M+Y%'-)3B=4V(M@UPMV@D78ZZ[J-M*>NGC0/UI'R/83DI"B7'XA-/]KXIEJ4T
M.9=)KY,]NMJU7R5U1;#U9(@5154N<B,T0I,R9"7Z9`$`O)7C)"&A&5ZNAXS4
M:;%DW.:7]?[\&_'3R;W[-//>AW6<8$7-NMR6$/PE"^92:$4+F.,14HMF+H6-
M,+&$@[,P%=ITA]L?HR;(KONHFOC+#",H%[3J^.UNTYHW55(*K-ND!_0Z!*7D
M&@K)"C7[?=#/40R&2&(^K_!8RKJ<BM'?W5[[[%K%XP<KR]N._XVODZ"RN0;U
M0N0SVJK'AT+F4Q]EI)40F[SC1^U<F`B8(4PW!Q0D"<0GS,,#>5P%][<,EP.4
MT(;-5?BA`2W0X((_*.LU/?:<F&V8I[%/VPS7U_T+K4_`2/[JU*/K:B??GP^Q
MW?DV.MZNL];53ZG+V5JL-(S)DO@3*=O*2B,BXD2:$,-D[<T2ZY5T'%&+DFT6
M26UF[_46D_S%0AB;5>MNNM`U;;=1GMOEWMXQZ<\_)T6,>K?&A;9H9EC7B(KH
M&P^<?#Y5!^U?RA^SH_:--,%AS7-B]B\?KL1^ZBI#-`BRF(EV?776'470:+.7
M'I4L#SU+`TV<]IX%2G40W[1F'N$FC3I&Z0`ZZBN15^@4%JG+*C.6N6<7QC7]
M49,*1O*5G3"]V\8X7JCJ)2X/N$S+S55#!F>QC2L91AB5$9:J"@AIKF(K(\`Y
M_L3DO3#K#U?6I;PR'B@F&FG1GW"\U4.%!2H8",7%'5-N\S4*)I6=7L@LGAUE
M"+&[7#5::)VI!,1P.^$&TSA!2B(_V0,N71K;;-5I=(40DTCA2B%/)8)LYVLL
MIL&V$$72`UQEIK%>ETIJ]DH8]I4\XH`DY_G(6`OHY7U%B_SUI&1S!!H2$/NJ
MM2@I,G290TDJZ[GZ,5F`M)WCM#(9,M;/;FQR4(B["1Z\1V3EO\R8U=M.TD24
MCU,-7%.NH?.NKTE[Z37-=&)?\1`B0?1%"4F2RA6XE;5+T++$?OCW"])1J#%]
M#,77.&0VX,:@^`C<76Q8).%M21Q4JX'-P5M%,]&5P0?I"/*$)F$9YVWV:&/?
M5>[=;H<(8-N)3D(%?&@9SW663W.M03VW^1*?8V]`=9J&8K(*O4?3P#,Z#/;9
M<BF#^)"T061Z_`ZA6R?"#-K5R)I+?K)JJQ&-B=E%E&IH2<99S989CDDP&\XV
MXP)&3/7Z:,1=IC8TG(61F?(.R,-@NU%')GN\%$UDS$2TR3)B[[2.MK>+NO@O
M-LQ(E2T,X=<PC+WZVBMKAGE.WOA8;9=L5L+3$Y%IB%+:,-JZ31-KZ@?2\&T)
ML4%=YD%0EV\."P0>;OZ.G^SX!+@EQX1R8Y*PSQ>9@=?F#V;HZ4K^KA-GE'GZ
MB/']5IEU1%?`%AB^\4H[MENL9@CT,!D%<"->:!47^=OD'+##A039&B'^12B#
M4YJ3A0X0<&,:;C?*FVNX_OM/9W"]V-IC[D9W5$+V/(.66\D>5:J:TCQ8GBH#
M!T4)`RPW*5S!1UF(#$0C_%=`90LN36N6/54+%I'6^0(T&6JX8:G1M1J!T'TC
M68\62[,#)A=@WA#(8FD&<OIE;O#TC8W#>!N2)93<(CI9-XFB#0PF!`(VDYEK
M3L<S*&+5ZQ1$N?6KDJH8D_6LR]K2FIPE#,=RA+,B%4*Q[QMM_*9IH*%=8X%)
MR(L+1UIEYJPP#(-Q/&J*%"^G+6IE!<9#S#\U.C=/7#PW=W7YI]QW/*?U86=L
M'WVC3)J#PFRM196.K`MUQDL,>*/(<SC@IFO"=-.NI7@0P("*D*%`,@!O\$I<
MN44"IPV[0EJ4(:F,D.4IP-LJU+.RDXU:I&7907M(6A@69KL!LBUOPW?T_EWJ
MO7<"32D+0,<0;G"V`^27F#*3;:I"A,:M*"<B%+LT4AR@]&&4EG?C9WW]R-AC
M>>-9FQ/U*QL0=!79MWUEJ7*$%]U=L.U,9PB4AK-JR9:G`8P@XQC!FB)KG3=F
MO$Y".##6#0CI0+/=:QG*X$EJ8C(%[3(TE(8EM\0.YDD2#17R\,RR8[$=^T)Y
M9%`EF]X7*R*RD@XRG*?3"GFJ0U[F]KK&\A^D7D>E,SWJA!+LB#G%$[#9IY8*
M3J572RTX`F`26#`DS-4R%6@L&E.\:4*42KY^,CQSH\Q%AN,H"38TG**OTSES
MU*=6R-;F(S%P1$)C35%%(")OOIY].B1QS4:3-0/"]6>5G:Y=JO@C79JVVV1P
MQR-M2ILS93K>AOGF`]3N&1T0H['TT5UX!E7>5[92[G-O"W-A@Y*:G$?.;R;+
M5R.8%L&9S5+)DF6"NE%,HZ'9A>I^8S>65XACM14/5G:H%X4I8WPU*H:/R-L8
MS(PA6[%+QW\<98DW*T"S2LTP*8&.%E4S=QW6FNF.1DTC]'Q(%H)@<6L4+O@L
MT!#(:38(H:(SXS[<RDJN<EUF$]E9.BTL2WV7(Q.XE@>UK,8S"@M9P@[:;*A2
MIE+_V+[WDWW1U(:\TB-SSKHG.0%I!0W!!+.D@-SE$JS-U,'.O+8>%K!A#@S3
M+%B^RNX#P)BO)&#-B88L-+V)-,6X>YE5T`8;&AWT*]LRQ*J*`HSNHG*^=ALI
MTSI9GC6ZQN[$/!DFF-\YC-&9/'=@MENDS@BN:B9C:O*`T1(W"\PSP)B=ZTT=
MR)S(MPVA8<^ZY@X9UM1K)6!M<JX0I]@B;Q9#?:N<)2<KV%;E4S8U-TX3,Y08
MA5=1R-&<1E^6/INA4T8TP=PFL9034,:+"\C2.M`_<PRW2ELW^'`<0,@T4[[F
IUQD[W5&F,"DW#-T#'-I)RIT3+;^G<3&-C&,+^\E_^+N2*<*$@4!X`!``
`
end


-- System Information
System Version: Linux zensunni 2.4.25-pre7 #1 SMP Fri Jan 23 22:49:36 CET 2004 i686 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
Thread model: posix
gcc version 3.3.3 20040125 (prerelease) (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.5.1+cvs20040105i (2003-11-05)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.25-pre7 (i686) [using ncurses 5.3] [using libidn 0.3.4 (compiled with 0.3.4)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.5.1.tt.compat.1-cl
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.3.rr.compressed.1
patch-1.5.4.helmersson.incomplete_multibyte
patch-1.5.4.fw.maildir_inode_sort
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.5.1.cd.edit_threads.9.2
patch-1.3.27.bse.xtitles.1
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.56d


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-02-02 12:29:32 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
I can't reproduce that with vanilla 1.5.6 (to be).

On 2004-01-30 16:32:50 +0100, J.H.M.Dassen wrote:
> From: "J.H.M.Dassen" <fsmla@xinara.org>
> To: submit@bugs.guug.de
> Cc: mutt@packages.debian.org
> Date: Fri, 30 Jan 2004 16:32:50 +0100
> Subject: bug#1786: mutt-1.5.5.1+cvs20040105i: Busy hang on "bottom" press in internal pager
> Reply-To: "J.H.M.Dassen" <fsmla@xinara.org>, 1786@bugs.guug.de
> Orignal-Sender: "J.H.M. Dassen (Ray)" <ray@xinara.org>
> X-Spam-Level: 
> 
> Package: mutt
> Version: 1.5.5.1-200401
> Severity: normal
> 
> -- Please type your report below this line
> 
> Here's a repeatable "hang" scenario for sid's mutt on i386:
> - start "mutt -n -F /dev/null -f borked" (borked being the mailbox included
>   below)
> - ":bind pager G bottom"
> - Press enter to view the message
> - Press "G"
> Mutt is now in a busy loop and hangs. Attaching an strace to it shows it
> busy with an occasional mremap() and numerous llseek()s.
> 
> Here's the "borked" mailbox, bzip2-ed and uuencoded:
> 
> begin 644 borked.bz2.uue
> M0EIH.3%!629362@/``(`!+__A\7X4$!3________^K____!YG[D`?____^`/
> M;=]QXH$/7>(]VB@Z:CL>[>8VU:E59#9[LJ*)``!T`DD(1B::F`1IIHQ`&DU/
> M$RGHI^I/(]`IO335&:ALU30Q#0T\AD09#0`:`:C()A-"-4]-0]331FIHT&@`
> M:`-`!H``!H`````TT#30-3(5/2&:30``:,@:``T-``-!H````````-`:`2$B
> M31)C5-I'E&&IZ@:9`S2`&GJ:&@T/4/4!H!H`````!H`(,`F(P3)A!A,3`F`(
> M81@(8$P)A,$8``":&$Q-,"!(D!`FIB9#(F1)[4T%-&F)@FC)IY-0#(,(PF(,
> MC3)H;4#(#(R8)'>+CG6GT#*`"M=_5+PQE@O)AX>/-$'/,?^;R5J+$E.I(XS8
> M%`:0%D\(+G[.0<J7[%X\V$(N#@,DP3=P199BB;&,I0H6IQ-[J-[W8D[!YI3-
> M!K9>^-BZXS,EWCC*,MM[4.#?4T[>A_'*E&QC;&VV#/,T?\=?(ER(:(0C3QP^
> M![.2\\C5'LR<POT]7G#&-6:NYW9R:8M3%1M'X5AT+VF\LZ9=VJ^/CW,NMG>]
> M@)E..9K'XH9F15@B,N4L@P3.WT&%#3ODT<]RBUG(4[5G.)OI/K=O,\]N>UUP
> MJ#-KT3(/QR8409?VX="2XUZ4OQR?6@`@5LXL2\M8[#8H=FJ"`2&N%A;5+*#[
> M3\%-`]C&XT@AZ4HT.->TS81Z7/2NHGB.Z4]KN=JMLFW20]5L.)4`.P<\\)`U
> M?XAHI0AHL'1YL81TRNPA;%%Z=\*#I!3,%NMMYI*NV]\*$8WU*8F^K9#7N\4&
> MM!<CP211#G#LVN56J825R+PF4(*%Q.0P@)F0P!'6!.`F:<./+5AP4NE7T`H)
> MT&-,@E93O!GD")@Y3!0N)P*I'L75G,\%(ETDBH,.B!L":0UP8YZ;$A2E`-TR
> M'"_@'H4(NB;)4`W3SG$1D!.H&[,+,&*)V/O"`-X"J[#'NE983S(?E`?.L)#J
> M"$XF!J3/4TFQ:PT,%3'-K#7)$(,F54QDA16L,9'$FB,`*"Q)(6ETLFIF)18D
> MM]HR"3%FO.;J@?<)%L;30?:X%YJ5.\M!&ZPS,VYFV_1LQ)+I9+8IXN)ZEJ^#
> M[T$8FY>)W^G^)5OCJ^X]'D.HMK@.@Z3O((4<;MIG):7&P>MF<\9>&?/Z/$"W
> M&2R3>%Q;(+O>]F]'OVR0/#W],I+J7.2*%E^%H[OFR^=ZP)[M'H9P^/P20L11
> M(9^U2X#Q&]/$OM;R$<E&K)$*-3='#(_GJUF95Z]?"O?FP[4_I#(A`9:1#1:8
> M/)`=!R5@'24A,\9F8=XZTI$Q$:^P'M#/B*HR)-T)K%FIFQB>&IM[>ED[3,F8
> M&:;`8TFO/S;#U,/0$OWA6H=)Z=F;%\4/66#9'#>WK!JC`UY(@BD?`7#L.E6_
> M;&WSC5+:,.$B@UT@>%%U$;\+*^`F2VQ$@R`X;6@LV/L!V8=)I9;S`HF0*,TO
> M8REK8+D1N@L-+@LTK6)H2(FH##4MXABC(3"J.81:9!]B7<:D]+P`_CO7C"1<
> M4HGH3U<F;GP3#^@WZ`3A9_9#UB'8BIF(E7*_!<>&%1H+81(&AO:R2Q0[`R@U
> MSW`P8"T7P8\[+ZC$P.(#2A`&$1$50!"`K%^*E>=RLE:@,IPFF.R&L2V&,A\(
> M5;<PXK1EJ)QE#6+F5O)V7^WP/+T'ME%!]HX<^@=_(1@[["13I[SOJ^-2B@)H
> M]HW\_<S:.8P,WEN+9ZS[D2WL?J#'<]:I&G/FFK[,RDJ"=&/H8=]EQQ%`L-Z_
> M>!^B;&U&:8Q$I*`Z;*,ZG4U%T,PP,-[(LT57%4OYQVC9)288BO8?R'P,Y^?6
> MH?`UE9'M7D=UIM3/;-2@SL&,/AR4@^"4*S8V#9DO6P]=CT4P=Z<XRW1\.C21
> M!JU4<6VJOJ9`R!C(??_5-^>#@:5N<9!UT92(ICC0N#,P5@\SK`3=F;EE^0]!
> M.#Y&-`W3F)`^5:YB+?(L[Q#0^(G5!VT>N&`AG(."-8M;XB+1RU2B565PG,6C
> M@(B7>&$:YJ3'I67TU"L6A>>HI7C@B>@>2E/(8)"8Y0^8+837`;OF,J&_6*\>
> M<4_.0]R+5#/+<5O:L%5:&*6IU3.\>SA@QW6;(-%)P?!K:M88R62"!V(8,"M<
> M)(EOPU\15>,P-.?*YC['DXLW--QVW$U(6S(-V`=2=)"Y=)2W9[7@$<J/`*GV
> M+N_4(N$6D@N(]#5AA0GZ[&>]CV!)G^(BDZ5"Z#$D;$YHZ@CGY>><E5AY5KIT
> M+Y%'-)3B=4V(M@UPMV@D78ZZ[J-M*>NGC0/UI'R/83DI"B7'XA-/]KXIEJ4T
> M.9=)KY,]NMJU7R5U1;#U9(@5154N<B,T0I,R9"7Z9`$`O)7C)"&A&5ZNAXS4
> M:;%DW.:7]?[\&_'3R;W[-//>AW6<8$7-NMR6$/PE"^92:$4+F.,14HMF+H6-
> M,+&$@[,P%=ITA]L?HR;(KONHFOC+#",H%[3J^.UNTYHW55(*K-ND!_0Z!*7D
> M&@K)"C7[?=#/40R&2&(^K_!8RKJ<BM'?W5[[[%K%XP<KR]N._XVODZ"RN0;U
> M0N0SVJK'AT+F4Q]EI)40F[SC1^U<F`B8(4PW!Q0D"<0GS,,#>5P%][<,EP.4
> MT(;-5?BA`2W0X((_*.LU/?:<F&V8I[%/VPS7U_T+K4_`2/[JU*/K:B??GP^Q
> MW?DV.MZNL];53ZG+V5JL-(S)DO@3*=O*2B,BXD2:$,-D[<T2ZY5T'%&+DFT6
> M26UF[_46D_S%0AB;5>MNNM`U;;=1GMOEWMXQZ<\_)T6,>K?&A;9H9EC7B(KH
> M&P^<?#Y5!^U?RA^SH_:--,%AS7-B]B\?KL1^ZBI#-`BRF(EV?776'470:+.7
> M'I4L#SU+`TV<]IX%2G40W[1F'N$FC3I&Z0`ZZBN15^@4%JG+*C.6N6<7QC7]
> M49,*1O*5G3"]V\8X7JCJ)2X/N$S+S55#!F>QC2L91AB5$9:J"@AIKF(K(\`Y
> M_L3DO3#K#U?6I;PR'B@F&FG1GW"\U4.%!2H8",7%'5-N\S4*)I6=7L@LGAUE
> M"+&[7#5::)VI!,1P.^$&TSA!2B(_V0,N71K;;-5I=(40DTCA2B%/)8)LYVLL
> MIL&V$$72`UQEIK%>ETIJ]DH8]I4\XH`DY_G(6`OHY7U%B_SUI&1S!!H2$/NJ
> MM2@I,G290TDJZ[GZ,5F`M)WCM#(9,M;/;FQR4(B["1Z\1V3EO\R8U=M.TD24
> MCU,-7%.NH?.NKTE[Z37-=&)?\1`B0?1%"4F2RA6XE;5+T++$?OCW"])1J#%]
> M#,77.&0VX,:@^`C<76Q8).%M21Q4JX'-P5M%,]&5P0?I"/*$)F$9YVWV:&/?
> M5>[=;H<(8-N)3D(%?&@9SW663W.M03VW^1*?8V]`=9J&8K(*O4?3P#,Z#/;9
> M<BF#^)"T061Z_`ZA6R?"#-K5R)I+?K)JJQ&-B=E%E&IH2<99S989CDDP&\XV
> MXP)&3/7Z:,1=IC8TG(61F?(.R,-@NU%')GN\%$UDS$2TR3)B[[2.MK>+NO@O
> M-LQ(E2T,X=<PC+WZVBMKAGE.WOA8;9=L5L+3$Y%IB%+:,-JZ31-KZ@?2\&T)
> ML4%=YD%0EV\."P0>;OZ.G^SX!+@EQX1R8Y*PSQ>9@=?F#V;HZ4K^KA-GE'GZ
> MB/']5IEU1%?`%AB^\4H[MENL9@CT,!D%<"->:!47^=OD'+##A039&B'^12B#
> M4YJ3A0X0<&,:;C?*FVNX_OM/9W"]V-IC[D9W5$+V/(.66\D>5:J:TCQ8GBH#
> M!T4)`RPW*5S!1UF(#$0C_%=`90LN36N6/54+%I'6^0(T&6JX8:G1M1J!T'TC
> M68\62[,#)A=@WA#(8FD&<OIE;O#TC8W#>!N2)93<(CI9-XFB#0PF!`(VDYEK
> M3L<S*&+5ZQ1$N?6KDJH8D_6LR]K2FIPE#,=RA+,B%4*Q[QMM_*9IH*%=8X%)
> MR(L+1UIEYJPP#(-Q/&J*%"^G+6IE!<9#S#\U.C=/7#PW=W7YI]QW/*?U86=L
> M'WVC3)J#PFRM196.K`MUQDL,>*/(<SC@IFO"=-.NI7@0P("*D*%`,@!O\$I<
> MN44"IPV[0EJ4(:F,D.4IP-LJU+.RDXU:I&7907M(6A@69KL!LBUOPW?T_EWJ
> MO7<"32D+0,<0;G"V`^27F#*3;:I"A,:M*"<B%+LT4AR@]&&4EG?C9WW]R-AC
> M>>-9FQ/U*QL0=!79MWUEJ7*$%]U=L.U,9PB4AK-JR9:G`8P@XQC!FB)KG3=F
> MO$Y".##6#0CI0+/=:QG*X$EJ8C(%[3(TE(8EM\0.YDD2#17R\,RR8[$=^T)Y
> M9%`EF]X7*R*RD@XRG*?3"GFJ0U[F]KK&\A^D7D>E,SWJA!+LB#G%$[#9IY8*
> M3J572RTX`F`26#`DS-4R%6@L&E.\:4*42KY^,CQSH\Q%AN,H"38TG**OTSES
> MU*=6R-;F(S%P1$)C35%%(")OOIY].B1QS4:3-0/"]6>5G:Y=JO@C79JVVV1P
> MQR-M2ILS93K>AOGF`]3N&1T0H['TT5UX!E7>5[92[G-O"W-A@Y*:G$?.;R;+
> M5R.8%L&9S5+)DF6"NE%,HZ'9A>I^8S>65XACM14/5G:H%X4I8WPU*H:/R-L8
> MS(PA6[%+QW\<98DW*T"S2LTP*8&.%E4S=QW6FNF.1DTC]'Q(%H)@<6L4+O@L
> MT!#(:38(H:(SXS[<RDJN<EUF$]E9.BTL2WV7(Q.XE@>UK,8S"@M9P@[:;*A2
> MIE+_V+[WDWW1U(:\TB-SSKHG.0%I!0W!!+.D@-SE$JS-U,'.O+8>%K!A#@S3
> M+%B^RNX#P)BO)&#-B88L-+V)-,6X>YE5T`8;&AWT*]LRQ*J*`HSNHG*^=ALI
> MTSI9GC6ZQN[$/!DFF-\YC-&9/'=@MENDS@BN:B9C:O*`T1(W"\PSP)B=ZTT=
> MR)S(MPVA8<^ZY@X9UM1K)6!M<JX0I]@B;Q9#?:N<)2<KV%;E4S8U-TX3,Y08
> MA5=1R-&<1E^6/INA4T8TP=PFL9034,:+"\C2.M`_<PRW2ELW^'`<0,@T4[[F
> IUQD[W5&F,"DW#-T#'-I)RIT3+;^G<3&-C&,+^\E_^+N2*<*$@4!X`!``
> `
> end
> 
> 
> -- System Information
> System Version: Linux zensunni 2.4.25-pre7 #1 SMP Fri Jan 23 22:49:36 CET 2004 i686 GNU/Linux
> 
> -- Build environment information
> 
> (Note: This is the build environment installed on the system
> muttbug is run on.  Information may or may not match the environment
> used to build mutt.)
> 
> - gcc version information
> cc
> Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.3/specs
> Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
> Thread model: posix
> gcc version 3.3.3 20040125 (prerelease) (Debian)
> 
> - CFLAGS
> -Wall -pedantic -g -O2
> 
> -- Mutt Version Information
> 
> Mutt 1.5.5.1+cvs20040105i (2003-11-05)
> Copyright (C) 1996-2002 Michael R. Elkins and others.
> Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
> Mutt is free software, and you are welcome to redistribute it
> under certain conditions; type `mutt -vv' for details.
> 
> System: Linux 2.4.25-pre7 (i686) [using ncurses 5.3] [using libidn 0.3.4 (compiled with 0.3.4)]
> Compile options:
> -DOMAIN
> -DEBUG
> -HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
> +USE_FCNTL  -USE_FLOCK
> +USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
> +HAVE_REGCOMP  -USE_GNU_REGEX  
> +HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
> +HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
> +CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
> +ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
> +HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
> ISPELL="/usr/bin/ispell"
> SENDMAIL="/usr/sbin/sendmail"
> MAILPATH="/var/mail"
> PKGDATADIR="/usr/share/mutt"
> SYSCONFDIR="/etc"
> EXECSHELL="/bin/sh"
> MIXMASTER="mixmaster"
> To contact the developers, please mail to <mutt-dev@mutt.org>.
> To report a bug, please use the flea(1) utility.
> 
> patch-1.5.5.1.tt.compat.1-cl
> patch-1.5.4.vk.pgp_verbose_mime
> patch-1.5.3.rr.compressed.1
> patch-1.5.4.helmersson.incomplete_multibyte
> patch-1.5.4.fw.maildir_inode_sort
> patch-1.3.23.1.ametzler.pgp_good_sign
> patch-1.5.3.Md.gpg_status_fd
> patch-1.4.Md.gpg-agent
> patch-1.5.3.Md.etc_mailname_gethostbyname
> patch-1.5.1.cd.edit_threads.9.2
> patch-1.3.27.bse.xtitles.1
> Md.use_debian_editor
> Md.muttbug
> patch-1.4.admcd.gnutlsdlopen.53d
> patch-1.4.admcd.gnutlsbuild.53d
> patch-1.4.admcd.gnutls.56d
> 

-- 
Thomas Roessler			      <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2005-08-07 17:28:42 UTC brendan
* Added comment:
{{{
Can you reproduce this with current vanilla mutt code?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-09 10:51:48 UTC brendan
* Added comment:
{{{
No longer reproducible. Thanks for the feedback.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2005-08-09 10:53:53 UTC "J.H.M. Dassen (Ray)" <fsmla@xinara.org>
* Added comment:
{{{
On Sun, Aug 07, 2005 at 00:28:42 +0200, Brendan Cully wrote:
> Can you reproduce this with current vanilla mutt code?

I don't have a vanilla build handy currently, but the problem is no longer
reproducible with Debian sid's 1.5.9-2 package, so feel free to close this
PR.

Ray
-- 
A Microsoft Certified System Engineer is to information technology as a
McDonalds Certified Food Specialist is to the culinary arts.
	Michael Bacarella commenting on the limited value of certification.
}}}
