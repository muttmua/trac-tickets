Ticket:  2187
Status:  closed
Summary: can make CVS crash with particular mailboxes..

Reporter: charlie@rubberduck.com
Owner:    mutt-dev

Opened:       2006-02-24 05:18:50 UTC
Last Updated: 2006-09-21 22:48:22 UTC

Priority:  critical
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
EXC_BAD_ACCESS (0x0001)
KERN_PROTECTION_FAILURE (0x0002) at 0x0000003c

Thread 0 Crashed:
0    mx_update_context + 64 (mx.c:1640)
1    imap_read_headers + 2572 (message.c:340)
2    imap_open_mailbox + 1592 (imap.c:737)
3    mx_open_mailbox + 1308 (mx.c:698)
4    mutt_index_menu + 8736 (curs_main.c:1129)
5    main + 3972 (main.c:967)
6    _start + 340 (crt.c:272)
7    start + 60
>How-To-Repeat:
with headercache - open some of my larger mailboxes... will update as more testing on my side comes to hand. Just thought Brendan may have a clue as to the trace, first...
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-08-16 12:30:23 UTC brendan
* Added comment:
{{{
Still getting these?
}}}

--------------------------------------------------------------------------------
2006-08-17 14:20:57 UTC Charlie Allom <charlie@rubberduck.com>
* Added comment:
{{{
On Tue, Aug 15, 2006 at 07:30:23PM +0200, Brendan Cully wrote:
> Synopsis: can make CVS crash with particular mailboxes..
> 
> **** Comment added by brendan on Tue, 15 Aug 2006 19:30:23 +0200 ****
>  Still getting these?

not for ages, thanks.


-- 
 hail eris
 http://rubberduck.com/
}}}

--------------------------------------------------------------------------------
2006-09-22 16:48:22 UTC paul
* Added comment:
{{{
On Tue, Aug 15, 2006 at 07:30:23PM +0200, Brendan Cully wrote:
no longer seen
}}}

* resolution changed to fixed
* status changed to closed
