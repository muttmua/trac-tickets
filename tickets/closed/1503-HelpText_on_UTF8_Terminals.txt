Ticket:  1503
Status:  closed
Summary: Help-Text on UTF-8 Terminals

Reporter: Daniel Meidlinger <meidlidl@faveve.uni-stuttgart.de>
Owner:    mutt-dev

Opened:       2003-03-22 01:52:06 UTC
Last Updated: 2007-03-14 20:49:26 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: normal

-- Please type your report below this line

On UTF-8 terminals the help-text is splited on a wrong position, when
help-description-string contains non US-ASCII characters.

/-------10--------20--- COLUMNS=78 ---40--UTF-8 Terminal--60--------70-------\
j           next-undeleted        Springe zur nächsten ungelöschten N
+                                 achricht
\-------10--------20--- COLUMNS=78 ---40--------50--------60--------70-------/

On ISO-8859-x terminals is works correct.

/-------10--------20--- COLUMNS=78 ---40--- ISO-8859-15 Terminal ---70-------\
j           next-undeleted        Springe zur nächsten ungelöschten Nachricht
\-------10--------20--- COLUMNS=78 ---40--------50--------60--------70-------/

It seams that the length of an help-description-string is counted in bytes
and not in UTF-8 characters.

mutt-1.5.4 has the same problem. (Here I originaly found the problem.)

/-------10--------20--- COLUMNS=80 ---40--UTF-8 Terminal--60--------70--------8\
^K          extract-keys           Extrahiere unterstützte öffentliche S
+                                  chlüssel
\-------10--------20--- COLUMNS=80 ---40--------50--------60--------70--------8/

/-------10--------20--- COLUMNS=80 ---40--- ISO-8859-15 Terminal ---70--------8\
^K          extract-keys           Extrahiere unterstützte öffentliche Schlüssel
\-------10--------20--- COLUMNS=80 ---40--------50--------60--------70--------8/

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/3.2/specs
Configured with: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --libdir=/usr/lib --enable-languages=c,c++,f77,objc,java,ada --enable-libgcj --with-gxx-include-dir=/usr/include/g++ --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit i486-suse-linux
Thread model: posix
gcc version 3.2

- CFLAGS
-Wall -pedantic -Wall -O2 -march=i586 -mcpu=i686 -fmessage-length=0 -pipe -I. -D_GNU_SOURCE

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins und andere.
Mutt übernimmt KEINERLEI GEWÄHRLEISTUNG. Starten Sie `mutt -vv', um
weitere Details darüber zu erfahren. Mutt ist freie Software. 
Sie können es unter bestimmten Bedingungen weitergeben; starten Sie
`mutt -vv' für weitere Details.

System: Linux 2.4.19-4GB (i686) [using ncurses 5.2]
Einstellungen bei der Compilierung:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, verwenden Sie bitte das Programm flea(1).

patch-1.3.99.rr.compressed.1


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-03-22 09:25:10 UTC Edmund GRIMLEY EVANS <edmundo@rano.org>
* Added comment:
{{{
Vincent Lefevre <vincent@vinc17.org>:

> No, this is not an encoding problem. The characters are recoded as
> expected. It seems that for Mutt, 1 char (in UTF-8) = 1 character.
> Therfore, strings are split at a wrong position (in the middle of a
> word) and there are display problems concerning the cursor position.

If you have this problem with UTF-8 in all contexts, even for
displaying the text of a message, then your Mutt is badly configured
or linked with a screen library that doesn't understand UTF-8. If you
have this problem only with the help text, then you might have
discovered a new bug.

Edmund
}}}

--------------------------------------------------------------------------------
2003-03-22 09:33:10 UTC Marco d'Itri <md@Linux.IT>
* Added comment:
{{{
On Mar 21, Daniel Meidlinger <meidlidl@faveve.uni-stuttgart.de> wrote:

>On UTF-8 terminals the help-text is splited on a wrong position, when
>help-description-string contains non US-ASCII characters.
There is not much you can do about this, the manual charset is not UTF-8
so if you want to read it in a different environment it has to be
recoded.

-- 
ciao,
Marco
}}}

--------------------------------------------------------------------------------
2003-03-22 09:59:58 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Fri, Mar 21, 2003 at 16:33:10 +0100, Marco d'Itri wrote:
> On Mar 21, Daniel Meidlinger <meidlidl@faveve.uni-stuttgart.de> wrote:
>  >On UTF-8 terminals the help-text is splited on a wrong position, when
>  >help-description-string contains non US-ASCII characters.

I can confirm the bug.

> There is not much you can do about this, the manual charset is not UTF-8
> so if you want to read it in a different environment it has to be
> recoded.

No, this is not an encoding problem. The characters are recoded as
expected. It seems that for Mutt, 1 char (in UTF-8) = 1 character.
Therfore, strings are split at a wrong position (in the middle of a
word) and there are display problems concerning the cursor position.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2003-03-22 12:09:15 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Fri, Mar 21, 2003 at 17:25:10 +0000, Edmund GRIMLEY EVANS wrote:
> If you have this problem with UTF-8 in all contexts, even for
> displaying the text of a message, then your Mutt is badly configured
> or linked with a screen library that doesn't understand UTF-8. If you
> have this problem only with the help text, then you might have
> discovered a new bug.

I have this problem only with the help text.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2003-03-22 15:50:35 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On Fri, Mar 21, 2003 at 19:09:15 +0100, Vincent Lefevre wrote:
> On Fri, Mar 21, 2003 at 17:25:10 +0000, Edmund GRIMLEY EVANS wrote:
> > If you have this problem with UTF-8 in all contexts, even for
> > displaying the text of a message, then your Mutt is badly configured
> > or linked with a screen library that doesn't understand UTF-8. If you
> > have this problem only with the help text, then you might have
> > discovered a new bug.
> 
> I have this problem only with the help text.

BTW, looking at how the string is split, one knows that the problem
comes from Mutt (and not the terminal). More precisely, here's what
I get:

<Esc>U      undelete-subthread     récupérer tous les messages de la s
 +                                  ous-discussion

instead of

<Esc>U      undelete-subthread     récupérer tous les messages de la
+                                  sous-discussion

I think that the following happens: There are 2 two-byte characters in
the string (é and è), and it seems that Mutt cuts the string two bytes
too late; in this case, this adds two characters: " s"... and the line
has 82 characters instead of 80, so that the following line is shifted
by two characters to the right.

Mutt also forgets to erase characters (the same bug, IMHO). For
instance, with a 67-column terminal:

<Esc><Tab>  previous-new    aller au nouveau message précédent   ch

instead of

<Esc><Tab>  previous-new    aller au nouveau message précédent

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/> - 100%
validated (X)HTML - Acorn Risc PC, Yellow Pig 17, Championnat International
des Jeux Mathématiques et Logiques, TETRHEX, etc.
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2005-07-25 17:12:55 UTC brendan
* Added comment:
{{{
Can't reproduce with current CVS.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2005-08-01 11:33:00 UTC brendan
* Added comment:
{{{
Vincent can reproduce.
}}}

* status changed to new

--------------------------------------------------------------------------------
2007-03-10 07:26:56 UTC Christoph Berg <cb@df7cb.de>
* Added comment:
{{{
Hi,

mutt/1503 is still present in the current version. Stepan provides
some screenshots in Debian bug #328921: (See http://bugs.debian.org/32892=
1)

----- Forwarded message from Stepan Golosunov <stepan@golosunov.pp.ru> --=
---

Date: Sun, 18 Sep 2005 13:43:38 +0500
From: Stepan Golosunov <stepan@golosunov.pp.ru>
Reply-To: Stepan Golosunov <stepan@golosunov.pp.ru>, 328921@bugs.debian.o=
rg
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: Bug#328921: mutt: problems with utf-8 terminals

Package: mutt
Version: 1.5.9-2
Severity: normal
Tags: l10n

In ru_RU.UTF-8 locale strings are sometimes incorrectly wrapped (see
utf8-1.png vs. koi8r-1.png) and menu line is truncated (utf8-2.png vs.
koi8r-2.png). I guess these problems are caused by incorrect string lengt=
hs
counting.

----- End forwarded message -----

I could also reproduce it here with de_DE.UTF-8 and
macro index t tag-thread "=E4=E4=E4=E4=E4=E4=E4=E4=E4=E4=E4=E4=E4 =E4=E4=E4=
=E4=E4=E4=E4=E4=E4=E4=E4 =E4=E4=E4=E4=E4=E4=E4=E4=E4=E4"
(The help line looks correct though, desite an umlaut being present.)

Christoph
--=20
cb@df7cb.de | http://www.df7cb.de/
}}}

--------------------------------------------------------------------------------
2007-03-14 12:37:49 UTC brendan
* Added comment:
{{{
Hi,
 
Takashi's patch applied, thanks.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-03-14 13:32:37 UTC cb
* Added comment:
{{{
bug still present
}}}

* status changed to new

--------------------------------------------------------------------------------
2007-03-14 13:32:38 UTC cb
* Added comment:
{{{
It's still broken. Again using a silly macro to test:

Wide terminal:
t                      M tag-thread                      ääääääääääääa ääääääääääääääääääääääääa ääääääääääääääääääääää

Smaller terminal:
t                  M tag-thread                  ääääääääääääa äääääääääää
                                                 äääääääääääääa ääääääääääääääääääääää

It broke the middle word in two parts.
}}}

--------------------------------------------------------------------------------
2007-03-14 16:24:42 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2007-03-13 21:32:37 +0100, Christoph Berg wrote:
> **** Comment added by cb on Tue, 13 Mar 2007 21:32:37 +0100 ****
>  It's still broken. Again using a silly macro to test:

Yes, the patch didn't change anything here.

--=20
Vincent Lef=E8vre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / Arenaire project (LIP, ENS-Lyon)
}}}

--------------------------------------------------------------------------------
2007-03-15 01:37:22 UTC TAKIZAWA Takashi <taki@luna.email.ne.jp>
* Added comment:
{{{
This cause has mistaken the width of the column for the octet length. 
The attached patch fixes this. 

On Fri, Mar 09, 2007 at 02:25:02PM +0100,
 Christoph Berg wrote:

> The following reply was made to PR mutt/1503; it has been noted by GNATS.
> 
> From: Christoph Berg <cb@df7cb.de>
> To: Mutt Bugs <bug-any@bugs.mutt.org>
> Cc: 
> Subject: Bug#328921: mutt/1503: problems with utf-8 terminals
> Date: Fri, 9 Mar 2007 14:26:56 +0100
> 
>  Hi,
>  
>  mutt/1503 is still present in the current version. Stepan provides
>  some screenshots in Debian bug #328921: (See http://bugs.debian.org/32892=
>  1)

-- 
TAKIZAWA Takashi
http://www.emaillab.org/


--gKMricLos+KVdGMg
Content-Type: text/plain; charset=us-ascii
Content-Disposition: attachment; filename="patch-1.5.14.tt.help-strwidth.1"

diff -r f4d3704813fd help.c
--- a/help.c	Tue Mar 13 10:58:34 2007 +0100
+++ b/help.c	Wed Mar 14 01:09:50 2007 +0900
@@ -183,7 +183,7 @@ static void format_line (FILE *f, int is
   {
     col_a = COLS > 83 ? (COLS - 32) >> 2 : 12;
     col_b = COLS > 49 ? (COLS - 10) >> 1 : 19;
-    col = pad (f, mutt_strlen(t1), col_a);
+    col = pad (f, mutt_strwidth(t1), col_a);
   }
 
   if (ismacro > 0)
@@ -196,7 +196,7 @@ static void format_line (FILE *f, int is
     if (!split)
     {
       col += print_macro (f, col_b - col - 4, &t2);
-      if (mutt_strlen (t2) > col_b - col)
+      if (mutt_strwidth (t2) > col_b - col)
 	t2 = "...";
     }
   }
@@ -223,7 +223,7 @@ static void format_line (FILE *f, int is
 	SKIPWS(t3);
 
 	/* FIXME: this is completely wrong */
-	if ((n = mutt_strlen (t3)) > COLS - col)
+	if ((n = mutt_strwidth (t3)) > COLS - col)
 	{
 	  n = COLS - col;
 	  for (col_a = n; col_a > 0 && t3[col_a] != ' '; col_a--) ;

--gKMricLos+KVdGMg--
}}}

--------------------------------------------------------------------------------
2007-03-15 11:51:38 UTC Christoph Berg <cb@df7cb.de>
* Added comment:
{{{
Re: TAKIZAWA Takashi 2007-03-14 <E1HRXqS-0005SR-2A@trithemius.gnupg.org>
>  I'm sorry. 
>  The previous patch functions partial.

Sorry for having been a bit blunt - your patch was correct, only the
breakage was larger than expected.

>  The attached patch fixes the rest of problem.

Confirmed. Nice work!

Christoph
-- 
cb@df7cb.de | http://www.df7cb.de/
}}}

--------------------------------------------------------------------------------
2007-03-15 12:49:26 UTC brendan
* Added comment:
{{{
On 2007-03-13 21:32:37 +0100, Christoph Berg wrote:
 
 Re: TAKIZAWA Takashi 2007-03-14 <E1HRXqS-0005SR-2A@trithemius.gnupg.org>
Takashi's patch applied again.
Stay dead, please.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-03-16 02:59:10 UTC TAKIZAWA Takashi <taki@luna.email.ne.jp>
* Added comment:
{{{
I'm sorry. 
The previous patch functions partial.
The attached patch fixes the rest of problem.

On Tue, Mar 13, 2007 at 09:32:37PM +0100,
 Christoph Berg wrote:

> **** Comment added by cb on Tue, 13 Mar 2007 21:32:37 +0100 ****
>  It's still broken. Again using a silly macro to test:
> 
> Wide terminal:
> t                      M tag-thread                      ????????????a ????????????????????????a ??????????????????????
> 
> Smaller terminal:
> t                  M tag-thread                  ????????????a ???????????
>                                                  ?????????????a ??????????????????????
> 
> It broke the middle word in two parts.
> 
> 

-- 
TAKIZAWA Takashi
http://www.emaillab.org/


--VbJkn9YxBvnuCH5J
Content-Type: text/plain; charset=us-ascii
Content-Disposition: attachment; filename="patch-1.5.14.tt.help-wrap-width.1"

diff -r 99d92c8219f4 help.c
--- a/help.c	Tue Mar 13 22:41:34 2007 -0700
+++ b/help.c	Thu Mar 15 02:41:33 2007 +0900
@@ -147,6 +147,38 @@ static int print_macro (FILE *f, int max
   return (maxwidth - n);
 }
 
+static int get_wrapped_width (const char *t, size_t wid)
+{
+  wchar_t wc;
+  size_t k;
+  size_t m, n;
+  size_t len = mutt_strlen (t);
+  const char *s = t;
+  mbstate_t mbstate;
+
+  memset (&mbstate, 0, sizeof (mbstate));
+  for (m = wid, n = 0;
+       len && (k = mbrtowc (&wc, s, len, &mbstate)) && (n <= wid);
+       s += k, len -= k)
+  {
+    if (*s == ' ')
+      m = n;
+    if (k == (size_t)(-1) || k == (size_t)(-2))
+    {
+      k = (k == (size_t)(-1)) ? 1 : len;
+      wc = replacement_char ();
+    }
+    if (!IsWPrint (wc))
+      wc = '?';
+    n += wcwidth (wc);
+  }
+  if (n > wid)
+    n = m;
+  else
+    n = wid;
+  return n;
+}
+
 static int pad (FILE *f, int col, int i)
 {
   char fmt[8];
@@ -221,15 +253,7 @@ static void format_line (FILE *f, int is
       if (ismacro >= 0)
       {
 	SKIPWS(t3);
-
-	/* FIXME: this is completely wrong */
-	if ((n = mutt_strwidth (t3)) > COLS - col)
-	{
-	  n = COLS - col;
-	  for (col_a = n; col_a > 0 && t3[col_a] != ' '; col_a--) ;
-	  if (col_a)
-	    n = col_a;
-	}
+	n = get_wrapped_width (t3, n);
       }
 
       print_macro (f, n, &t3);

--VbJkn9YxBvnuCH5J--
}}}
