Ticket:  1638
Status:  closed
Summary: Fails silently if $EDITOR fails

Reporter: Artur R.Czechowski <arturcz@hell.pl>
Owner:    mutt-dev

Opened:       2003-09-14 09:26:15 UTC
Last Updated: 2007-04-03 00:18:17 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
From: Matt Zimmerman <mdz@debian.org>
Subject: Fails silently if $EDITOR fails
Date: Mon, 8 Sep 2003 12:45:11 -0400
}}}
If $EDITOR fails, an error message is printed, but there is no pause before
the screen is redrawn, so it passes unnoticed, and the user is dumped back
to a command prompt with no error message.

typescript attached.
{{{
Script started on Mon 08 Sep 2003 12:43:23 PM EDT=0A=0D=1B[0m=1B[27m=1B[24m=
=1B[Jthere% =1B[KT=08TERM=3Dvt100 EDITOR=3Dnonexistent mutt -F /dev/null fo=
o=0D=0D=0A=1B(B=1B)0=1B[1;24r=1B[m=0F=1B[?7h=1B[?1h=1B=3D=1B[24;1H=0D=1B[?1=
l=1B>=1B[1;24r=1B[m=0F=1B[?7h=1B[H=1B[J=1B[23BTo: foo=1B[?1h=1B=3D=0DSubjec=
t: foo=0D=1B[J=1B[24;1H=0D=1B[?1l=1B>sh: line 1: nonexistent: command not f=
ound=0D=0A=1B[?1h=1B=3D=1B[?1h=1B=3D=1B[1;24r=1B[m=0F=1B[?7h=1B[H=1B[J=1B[2=
3BAborted unmodified message.=0D=1B[J=1B[24;1H=0D=1B[?1l=1B>=0D=1B[0m=1B[27=
m=1B[24m=1B[Jthere% =1B[K=0D=0D=0A=0AScript done on Mon 08 Sep 2003 12:43:2=
9 PM EDT=0A
}}}

--------------------------------------------------------------------------------
2007-04-03 00:03:21 UTC brendan
* component changed to display
* Updated description:
{{{
From: Matt Zimmerman <mdz@debian.org>
Subject: Fails silently if $EDITOR fails
Date: Mon, 8 Sep 2003 12:45:11 -0400
}}}
If $EDITOR fails, an error message is printed, but there is no pause before
the screen is redrawn, so it passes unnoticed, and the user is dumped back
to a command prompt with no error message.

typescript attached.
{{{
Script started on Mon 08 Sep 2003 12:43:23 PM EDT=0A=0D=1B[0m=1B[27m=1B[24m=
=1B[Jthere% =1B[KT=08TERM=3Dvt100 EDITOR=3Dnonexistent mutt -F /dev/null fo=
o=0D=0D=0A=1B(B=1B)0=1B[1;24r=1B[m=0F=1B[?7h=1B[?1h=1B=3D=1B[24;1H=0D=1B[?1=
l=1B>=1B[1;24r=1B[m=0F=1B[?7h=1B[H=1B[J=1B[23BTo: foo=1B[?1h=1B=3D=0DSubjec=
t: foo=0D=1B[J=1B[24;1H=0D=1B[?1l=1B>sh: line 1: nonexistent: command not f=
ound=0D=0A=1B[?1h=1B=3D=1B[?1h=1B=3D=1B[1;24r=1B[m=0F=1B[?7h=1B[H=1B[J=1B[2=
3BAborted unmodified message.=0D=1B[J=1B[24;1H=0D=1B[?1l=1B>=0D=1B[0m=1B[27=
m=1B[24m=1B[Jthere% =1B[K=0D=0D=0A=0AScript done on Mon 08 Sep 2003 12:43:2=
9 PM EDT=0A
}}}

--------------------------------------------------------------------------------
2007-04-03 00:18:17 UTC brendan
* Added comment:
Fixed in [527589b0b7dd]

* resolution changed to fixed
* status changed to closed
