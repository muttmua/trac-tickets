Ticket:  2120
Status:  closed
Summary: lack of status redraw on resorting empty folder

Reporter: Samuel Tardieu <sam@rfc1149.net>
Owner:    mutt-dev

Opened:       2005-10-27 23:06:05 UTC
Last Updated: 2008-05-14 13:19:25 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{

If you change the sorting method (o-r, o-t for example)
while mutt has opened an empty mailbox, the new sorting
method will not be displayed until another event causes the
status bars to be redrawed.

The following tiny patch fixes this.

  Sam

>How-To-Repeat:
>Fix:
Samuel's patch works OK. AB.
}}}

--------------------------------------------------------------------------------
2007-03-27 18:07:25 UTC 
* Added attachment patch-1.5.11.st.empty_box_sort_redraw.1.gz
* Added comment:
patch-1.5.11.st.empty_box_sort_redraw.1.gz

--------------------------------------------------------------------------------
2008-05-14 13:19:25 UTC Samuel Tardieu <sam@rfc1149.net>
* Added comment:
(In [5518355bc6da]) Always update status bar after sorting method changes (closes #2120).

* resolution changed to fixed
* status changed to closed
