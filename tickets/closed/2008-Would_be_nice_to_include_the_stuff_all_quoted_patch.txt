Ticket:  2008
Status:  closed
Summary: Would be nice to include the "stuff_all_quoted" patch

Reporter: kyle-mutt-dev@memoryhole.net
Owner:    mutt-dev

Opened:       2005-07-29 07:46:21 UTC
Last Updated: 2007-04-10 18:11:15 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Replying to format-flowed messages is formatted in a technically correct, but un-visually-pleasing manner. Gary Johnson's patch adds a config option to tell mutt to improve the readability of format-flowed formatting in both on-screen and in replied messages, as allowed by RFC 2646.
>How-To-Repeat:
View a format-flowed message (or reply to one).
>Fix:
Apply this patch. There are editor-specific work-arounds for replying, but they are frequently incomplete (it's hard to do the correct thing in all circumstances) and obviously do not affect regular message viewing.

--------------------------------------------------------------------------------
2007-03-27 18:07:22 UTC 
* Added attachment patch-1.5.5.1.gj.stuff_all_quoted.3
* Added comment:
patch-1.5.5.1.gj.stuff_all_quoted.3

--------------------------------------------------------------------------------
2007-04-07 23:42:17 UTC brendan
* Updated description:
Replying to format-flowed messages is formatted in a technically correct, but un-visually-pleasing manner. Gary Johnson's patch adds a config option to tell mutt to improve the readability of format-flowed formatting in both on-screen and in replied messages, as allowed by RFC 2646.
>How-To-Repeat:
View a format-flowed message (or reply to one).
>Fix:
Apply this patch. There are editor-specific work-arounds for replying, but they are frequently incomplete (it's hard to do the correct thing in all circumstances) and obviously do not affect regular message viewing.
* milestone changed to 1.6
* version changed to 

--------------------------------------------------------------------------------
2007-04-10 18:11:15 UTC brendan
* Added comment:
The new f=f handler space-stuffs all quoted strings.

* resolution changed to fixed
* status changed to closed
