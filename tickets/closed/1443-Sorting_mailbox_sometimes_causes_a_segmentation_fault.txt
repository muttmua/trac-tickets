Ticket:  1443
Status:  closed
Summary: Sorting mailbox sometimes causes a segmentation fault.

Reporter: Miham KEREKES <miham@bibl.u-szeged.hu>
Owner:    mutt-dev

Opened:       2003-01-26 11:03:04 UTC
Last Updated: 2005-09-06 17:31:07 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.3-1
Severity: important

-- Please type your report below this line
Sorting mailbox causes sometimes segmentation fault, and under certain 
circumstances it also causes a mail-loss (actually, I lost 2 or 3 mails
via this way, when all the mails in inbox were to be deleted, but there
were a new mail which comes between last check and deletion, a sign
appeared for a second says: "1 kept, xx deleted", but immediatelly after
comes the Segmentation fault. 
All the other programs works well on my computer, and i've also checked 
the system w/ memtest86 for a whole day.

-- System Information
System Version: Linux xenon 2.4.19-grsec #1 Mon Nov 25 08:25:24 CET 2002 i686 unknown unknown GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011002 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.3i (2002-12-17)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.19-grsec (i686) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +HAVE_SMIME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.1.cd.edit_threads.9.2
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.3.27.bse.xtitles.1
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.55d
patch-1.5.3.rr.compressed.1

--- Begin /home/poweruser/miham/.muttrc
set folder=~/mail		# where i keep my mailboxes
set locale="hu_HU"
set from="Miham KEREKES <miham@bibl.u-szeged.hu>"
set alias_file=~/.mail_aliases	# where I keep my aliases
set allow_8bit			# never do Q-P encoding on legal 8-bit chars
unset arrow_cursor		# use -> instead of hiliting the whole line
unset ascii_chars		# use ASCII instead of ACS chars for threads
unset autoedit			# go to the editor right away when composing
set charset="ISO-8859-2"	# character set for your terminal
set send_charset="iso-8859-2"
set noconfirmappend		# don't ask me if i want to append to mailboxes
set copy=yes			# always save a copy of outgoing messages
set delete=yes			# purge deleted messages without asking
unset edit_hdrs			# let me edit the message header when composing
set editor="vim"
set hdr_format="%4C %Z %{%m/%d} %-15.15F (%4c) %s" # format of the index
set help			# show the help lines
set history=200			# number of lines of history to remember
set include			# always include messages when replying
set mailcap_path="~/.mailcap:/etc/mailcap"
set mail_check=1		# how often to poll for new mail
set mbox=+mbox		# where to store read messages
unset metoo			# remove my address when replying
set mime_fwd			# use message/rfc822 type to forward messages
set move=yes			# don't ask about moving messages, just do it
set pager_context=0		# no. of lines of context to give when scrolling
set pager_index_lines=6		# how many index lines to show in the pager
set pager_stop			# don't move to the next message on next-page
set postponed=+postponed	# mailbox to store postponed messages in
set noprompt_after	# ask me for a command after the external pager exits
set read_inc=10			# show progress when reading a mailbox
set record=+sent-mail		# default location to save outgoing mail
unset reply_to			# always use reply-to if present
set reply_regexp="^(re:[ \t]*)+"# how to identify replies in the subject:
set reverse_name		# use my address as it appears in the message
unset nosave_empty		# remove files when no messages are left
set signature="~/.signature"	# file which contains my signature
set sort=last-date		# primary sorting method
set tilde			# virtual lines to pad blank lines in the pager
set timeout=10			# timeout for prompt in the index menu
set tmpdir=/tmp		# where to store temp files
set write_inc=25		# show progress while writing mailboxes
set nobeep
set rfc2047_parameters
ignore *		# this means "ignore all lines by default"
unignore	from: subject to cc mail-followup-to \
		date x-mailer x-url # this shows how nicely wrap long lines
color normal white default
color hdrdefault red default
color quoted brightblue default
color signature red default
color indicator brightyellow red
color error brightred default
color status yellow blue
color tree magenta default	# the thread tree in the index menu
color tilde magenta default
color message brightcyan default
color markers brightcyan default
color attachment brightmagenta default
color search default green	# how to hilite search patterns in the pager
color header brightred default ^(From|Subject):
color body magenta default "(ftp|http)://[^ ]+"	# point out URLs
color body magenta default [-a-z_0-9.]+@[-a-z_0-9.]+	# e-mail addresses
color underline brightgreen default
mono quoted bold
charset-hook US-ASCII     ISO-8859-2
charset-hook x-unknown    ISO-8859-2
charset-hook ISO-8859-1	  ISO-8859-2
charset-hook windows-1250 CP1250
charset-hook windows-1251 CP1251
charset-hook windows-1252 CP1252
charset-hook windows-1253 CP1253
charset-hook windows-1254 CP1254
charset-hook windows-1255 CP1255
charset-hook windows-1256 CP1256
charset-hook windows-1257 CP1257
charset-hook windows-1258 CP1258
macro index S "b spambox@xenon.bibl.u-szeged.hu^Myd"
macro index X "| spamassassin -r"
macro pager X "| spamassassin -r"
my_hdr From: Miham KEREKES <miham@bibl.u-szeged.hu>
hdr_order date from to cc subject
auto_view application/x-gunzip
source ~/.muttrc-local	# config commands local to this site
source ~/.mail_aliases
bind index v display-message
bind index p previous-undeleted
bind index n next-undeleted
bind index ' ' next-page
bind index c mail
bind index g change-folder
bind index w search
bind index y print-message
bind index x sync-mailbox
bind index $ sort-mailbox
bind index G group-reply
macro index z ltagged\r
bind pager p previous-undeleted
bind pager n next-undeleted
bind pager ' ' next-page
bind pager g change-folder
bind pager c mail
bind pager w search
bind pager y print-message
bind pager \n noop	# PINE prints "No default action for this menu."
bind pager up previous-line
bind pager down next-line
bind pager G group-reply
bind pager <Up> previous-line
bind pager <Down> next-line
bind compose \cx send-message
bind pager d delete-message
bind pager u undelete-message
macro index <f3> g!^M
macro pager <f3> g!^M
macro index <f2> g?\t
macro pager <f2> g?\t
set nosave_name
set postponed=~/postponed-msgs
set pgp_verify_sig="ask-yes"
--- End /home/poweruser/miham/.muttrc


--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
ignore date delivered-to precedence errors-to in-reply-to user-agent
ignore x-loop x-sender x-mailer x-msmail-priority x-mimeole x-priority
ignore x-accept-language x-authentication-warning thread- priority
bind editor    "\e<delete>"    kill-word
bind editor    "\e<backspace>" kill-word
bind editor     <delete>  delete-char
unset use_domain
unset use_from
set sort=threads
unset write_bcc
unset bounce_delivered
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
set pipe_decode
macro generic <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro index   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro pager   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
open-hook	\\.gz$ "gzip -cd %f > %t"
close-hook	\\.gz$ "gzip -c %t > %f"
append-hook	\\.gz$ "gzip -c %t >> %f"
open-hook	\\.bz2$ "bzip2 -cd %f > %t"
close-hook	\\.bz2$ "bzip2 -c %t > %f"
append-hook	\\.bz2$ "bzip2 -c %t >> %f"
color normal	white black
color attachment brightyellow black
color hdrdefault cyan black
color indicator black cyan
color markers	brightred black
color quoted	green black
color signature cyan black
color status	brightgreen blue
color tilde	blue black
color tree	red black
set pgp_decode_command="/usr/bin/gpg  --charset utf-8   --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet  --batch  --output - %f"
set pgp_verify_command="/usr/bin/gpg   --status-fd=2 --no-verbose --quiet  --batch  --output - --verify %s %f"
set pgp_decrypt_command="/usr/bin/gpg   --status-fd=2 --passphrase-fd 0 --no-verbose --quiet  --batch  --output - %f"
set pgp_sign_command="/usr/bin/gpg    --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="/usr/bin/gpg   --charset utf-8 --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg  --charset utf-8    --batch  --quiet  --no-verbose --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg  --charset utf-8 --passphrase-fd 0  --batch --quiet  --no-verbose  --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="/usr/bin/gpg  --no-verbose --import -v %f"
set pgp_export_command="/usr/bin/gpg   --no-verbose --export --armor %r"
set pgp_verify_key_command="/usr/bin/gpg   --verbose --batch  --fingerprint --check-sigs %r"
set pgp_list_pubring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-keys %r" 
set pgp_list_secring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-secret-keys %r" 
set pgp_good_sign="^\\[GNUPG:\\] GOODSIG"
set smime_ca_location="~/.smime/ca-bundle.crt"
set smime_certificates="~/.smime/certificates"
set smime_keys="~/.smime/keys"
set smime_pk7out_command="openssl smime -verify -in %f -noverify -pk7out"
set smime_get_cert_command="openssl pkcs7 -print_certs -in %f"
set smime_get_signer_cert_command="openssl smime -verify -in %f -noverify -signer %c -out /dev/null"
set smime_get_cert_email_command="openssl x509 -in %f -noout -email"
set smime_import_cert_command="/usr/lib/mutt/smime_keys add_cert %f"
set smime_encrypt_command="openssl smime -encrypt -%a -outform DER -in %f %c"
set smime_sign_command="openssl smime -sign -signer %c -inkey %k -passin stdin -in %f -outform DER"
set smime_decrypt_command="openssl smime -decrypt -passin stdin -inform DER -in %f -inkey %k -recip %c"
set smime_verify_command="openssl smime -verify -inform DER -in %s %C -content %f"
set smime_verify_opaque_command="openssl smime -verify -inform DER -in %s %C"
--- End /etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-01-26 11:03:04 UTC Miham KEREKES <miham@bibl.u-szeged.hu>
* Added comment:
{{{
Package: mutt
Version: 1.5.3-1
Severity: important

-- Please type your report below this line
Sorting mailbox causes sometimes segmentation fault, and under certain 
circumstances it also causes a mail-loss (actually, I lost 2 or 3 mails
via this way, when all the mails in inbox were to be deleted, but there
were a new mail which comes between last check and deletion, a sign
appeared for a second says: "1 kept, xx deleted", but immediatelly after
comes the Segmentation fault. 
All the other programs works well on my computer, and i've also checked 
the system w/ memtest86 for a whole day.

-- System Information
System Version: Linux xenon 2.4.19-grsec #1 Mon Nov 25 08:25:24 CET 2002 i686 unknown unknown GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011002 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.3i (2002-12-17)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.19-grsec (i686) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +HAVE_SMIME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.1.cd.edit_threads.9.2
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.3.27.bse.xtitles.1
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.55d
patch-1.5.3.rr.compressed.1

--- Begin /home/poweruser/miham/.muttrc
set folder=~/mail		# where i keep my mailboxes
set locale="hu_HU"
set from="Miham KEREKES <miham@bibl.u-szeged.hu>"
set alias_file=~/.mail_aliases	# where I keep my aliases
set allow_8bit			# never do Q-P encoding on legal 8-bit chars
unset arrow_cursor		# use -> instead of hiliting the whole line
unset ascii_chars		# use ASCII instead of ACS chars for threads
unset autoedit			# go to the editor right away when composing
set charset="ISO-8859-2"	# character set for your terminal
set send_charset="iso-8859-2"
set noconfirmappend		# don't ask me if i want to append to mailboxes
set copy=yes			# always save a copy of outgoing messages
set delete=yes			# purge deleted messages without asking
unset edit_hdrs			# let me edit the message header when composing
set editor="vim"
set hdr_format="%4C %Z %{%m/%d} %-15.15F (%4c) %s" # format of the index
set help			# show the help lines
set history=200			# number of lines of history to remember
set include			# always include messages when replying
set mailcap_path="~/.mailcap:/etc/mailcap"
set mail_check=1		# how often to poll for new mail
set mbox=+mbox		# where to store read messages
unset metoo			# remove my address when replying
set mime_fwd			# use message/rfc822 type to forward messages
set move=yes			# don't ask about moving messages, just do it
set pager_context=0		# no. of lines of context to give when scrolling
set pager_index_lines=6		# how many index lines to show in the pager
set pager_stop			# don't move to the next message on next-page
set postponed=+postponed	# mailbox to store postponed messages in
set noprompt_after	# ask me for a command after the external pager exits
set read_inc=10			# show progress when reading a mailbox
set record=+sent-mail		# default location to save outgoing mail
unset reply_to			# always use reply-to if present
set reply_regexp="^(re:[ \t]*)+"# how to identify replies in the subject:
set reverse_name		# use my address as it appears in the message
unset nosave_empty		# remove files when no messages are left
set signature="~/.signature"	# file which contains my signature
set sort=last-date		# primary sorting method
set tilde			# virtual lines to pad blank lines in the pager
set timeout=10			# timeout for prompt in the index menu
set tmpdir=/tmp		# where to store temp files
set write_inc=25		# show progress while writing mailboxes
set nobeep
set rfc2047_parameters
ignore *		# this means "ignore all lines by default"
unignore	from: subject to cc mail-followup-to \
		date x-mailer x-url # this shows how nicely wrap long lines
color normal white default
color hdrdefault red default
color quoted brightblue default
color signature red default
color indicator brightyellow red
color error brightred default
color status yellow blue
color tree magenta default	# the thread tree in the index menu
color tilde magenta default
color message brightcyan default
color markers brightcyan default
color attachment brightmagenta default
color search default green	# how to hilite search patterns in the pager
color header brightred default ^(From|Subject):
color body magenta default "(ftp|http)://[^ ]+"	# point out URLs
color body magenta default [-a-z_0-9.]+@[-a-z_0-9.]+	# e-mail addresses
color underline brightgreen default
mono quoted bold
charset-hook US-ASCII     ISO-8859-2
charset-hook x-unknown    ISO-8859-2
charset-hook ISO-8859-1	  ISO-8859-2
charset-hook windows-1250 CP1250
charset-hook windows-1251 CP1251
charset-hook windows-1252 CP1252
charset-hook windows-1253 CP1253
charset-hook windows-1254 CP1254
charset-hook windows-1255 CP1255
charset-hook windows-1256 CP1256
charset-hook windows-1257 CP1257
charset-hook windows-1258 CP1258
macro index S "b spambox@xenon.bibl.u-szeged.hu^Myd"
macro index X "| spamassassin -r"
macro pager X "| spamassassin -r"
my_hdr From: Miham KEREKES <miham@bibl.u-szeged.hu>
hdr_order date from to cc subject
auto_view application/x-gunzip
source ~/.muttrc-local	# config commands local to this site
source ~/.mail_aliases
bind index v display-message
bind index p previous-undeleted
bind index n next-undeleted
bind index ' ' next-page
bind index c mail
bind index g change-folder
bind index w search
bind index y print-message
bind index x sync-mailbox
bind index $ sort-mailbox
bind index G group-reply
macro index z ltagged\r
bind pager p previous-undeleted
bind pager n next-undeleted
bind pager ' ' next-page
bind pager g change-folder
bind pager c mail
bind pager w search
bind pager y print-message
bind pager \n noop	# PINE prints "No default action for this menu."
bind pager up previous-line
bind pager down next-line
bind pager G group-reply
bind pager <Up> previous-line
bind pager <Down> next-line
bind compose \cx send-message
bind pager d delete-message
bind pager u undelete-message
macro index <f3> g!^M
macro pager <f3> g!^M
macro index <f2> g?\t
macro pager <f2> g?\t
set nosave_name
set postponed=~/postponed-msgs
set pgp_verify_sig="ask-yes"
--- End /home/poweruser/miham/.muttrc


--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
ignore date delivered-to precedence errors-to in-reply-to user-agent
ignore x-loop x-sender x-mailer x-msmail-priority x-mimeole x-priority
ignore x-accept-language x-authentication-warning thread- priority
bind editor    "\e<delete>"    kill-word
bind editor    "\e<backspace>" kill-word
bind editor     <delete>  delete-char
unset use_domain
unset use_from
set sort=threads
unset write_bcc
unset bounce_delivered
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
set pipe_decode
macro generic <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro index   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro pager   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
open-hook	\\.gz$ "gzip -cd %f > %t"
close-hook	\\.gz$ "gzip -c %t > %f"
append-hook	\\.gz$ "gzip -c %t >> %f"
open-hook	\\.bz2$ "bzip2 -cd %f > %t"
close-hook	\\.bz2$ "bzip2 -c %t > %f"
append-hook	\\.bz2$ "bzip2 -c %t >> %f"
color normal	white black
color attachment brightyellow black
color hdrdefault cyan black
color indicator black cyan
color markers	brightred black
color quoted	green black
color signature cyan black
color status	brightgreen blue
color tilde	blue black
color tree	red black
set pgp_decode_command="/usr/bin/gpg  --charset utf-8   --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet  --batch  --output - %f"
set pgp_verify_command="/usr/bin/gpg   --status-fd=2 --no-verbose --quiet  --batch  --output - --verify %s %f"
set pgp_decrypt_command="/usr/bin/gpg   --status-fd=2 --passphrase-fd 0 --no-verbose --quiet  --batch  --output - %f"
set pgp_sign_command="/usr/bin/gpg    --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="/usr/bin/gpg   --charset utf-8 --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg  --charset utf-8    --batch  --quiet  --no-verbose --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg  --charset utf-8 --passphrase-fd 0  --batch --quiet  --no-verbose  --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="/usr/bin/gpg  --no-verbose --import -v %f"
set pgp_export_command="/usr/bin/gpg   --no-verbose --export --armor %r"
set pgp_verify_key_command="/usr/bin/gpg   --verbose --batch  --fingerprint --check-sigs %r"
set pgp_list_pubring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-keys %r" 
set pgp_list_secring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-secret-keys %r" 
set pgp_good_sign="^\\[GNUPG:\\] GOODSIG"
set smime_ca_location="~/.smime/ca-bundle.crt"
set smime_certificates="~/.smime/certificates"
set smime_keys="~/.smime/keys"
set smime_pk7out_command="openssl smime -verify -in %f -noverify -pk7out"
set smime_get_cert_command="openssl pkcs7 -print_certs -in %f"
set smime_get_signer_cert_command="openssl smime -verify -in %f -noverify -signer %c -out /dev/null"
set smime_get_cert_email_command="openssl x509 -in %f -noout -email"
set smime_import_cert_command="/usr/lib/mutt/smime_keys add_cert %f"
set smime_encrypt_command="openssl smime -encrypt -%a -outform DER -in %f %c"
set smime_sign_command="openssl smime -sign -signer %c -inkey %k -passin stdin -in %f -outform DER"
set smime_decrypt_command="openssl smime -decrypt -passin stdin -inform DER -in %f -inkey %k -recip %c"
set smime_verify_command="openssl smime -verify -inform DER -in %s %C -content %f"
set smime_verify_opaque_command="openssl smime -verify -inform DER -in %s %C"
--- End /etc/Muttrc
}}}

--------------------------------------------------------------------------------
2003-02-13 02:29:18 UTC Rocco Rutte <s1118644@mail.inf.tu-dresden.de>
* Added comment:
{{{
Hi,

* Miham KEREKES [03-01-25 18:03:04 +0100] wrote:

> Sorting mailbox causes sometimes segmentation fault, and
> under certain circumstances it also causes a mail-loss
> (actually, I lost 2 or 3 mails via this way, when all the
> mails in inbox were to be deleted, but there were a new
> mail which comes between last check and deletion, a sign
> appeared for a second says: "1 kept, xx deleted", but
> immediatelly after comes the Segmentation fault.  All the
> other programs works well on my computer, and i've also
> checked the system w/ memtest86 for a whole day.

I exactly discovered the same behaviour and live with it for
quite a while. The code producing the problem seems to be
native mutt so that further checks added to thread.c should
solve the problem.

I reported this problem in July 2002:

 <http://marc.theaimsgroup.com/?l=mutt-dev&m=102806629300418&w=2>

  bye, Rocco
-- 
:wq!
}}}

--------------------------------------------------------------------------------
2005-09-05 00:54:16 UTC brendan
* Added comment:
{{{
This should be fixed in 1.5.10. Can you give it a try and report back?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-09-07 11:31:07 UTC brendan
* Added comment:
{{{
Submitter no longer experiences the problem. Thanks for the report and the 
feedback.
}}}

* resolution changed to fixed
* status changed to closed
