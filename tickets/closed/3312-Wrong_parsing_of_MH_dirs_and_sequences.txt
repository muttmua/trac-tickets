Ticket:  3312
Status:  closed
Summary: Wrong parsing of MH dirs and sequences

Reporter: JohanD
Owner:    mutt-dev

Opened:       2009-07-31 11:50:03 UTC
Last Updated: 2009-08-04 10:49:56 UTC

Priority:  major
Component: maildir/mh
Keywords:  MH unseen sequence patch

--------------------------------------------------------------------------------
Description:
As described in the debian bug tracking system (http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=538128), mutt version 1.5.20 blocked when opening a MH folder.

The mutt ticket #3308 seems to solve the opening of an MH folder.
But mutt still failed to hanlde correctly the MH sequences (especially the Unseen sequence) : when opening a MH folder, whatever the unseen sequence is, mutt does not find more than one new message.

I hope the following patch for the functions maildir_parse_dir and mh_read_sequence will solve this.

regards, 
Johan

--------------------------------------------------------------------------------
2009-07-31 11:51:08 UTC JohanD
* Added attachment mutt_1.5.20_MH.patch
* Added comment:
fix MH directories and sequences parsing

--------------------------------------------------------------------------------
2009-07-31 17:00:33 UTC pdmef
* Added comment:
Uh, the first hunk correctly fixes a bug introduced in [0698e8195545]. But why do mess with list of messages in the other 2 hunks? Please help me, I don't see what's broken about it...

* component changed to maildir/mh
* keywords changed to MH unseen sequence patch

--------------------------------------------------------------------------------
2009-08-03 03:12:36 UTC JohanD
* Added comment:
my bad,

the first time I read the code, the bug #3308 was still there, I got lost and couldn't not figure it out what you were trying to do with all the dereferences.
I guess I've still to understand how to use '*' and '&' in C. ,-)

Anyway, you're right, the two last hunks are useless. I've just checked and mutt run fine without them.

sorry for the trouble, 
Johan

--------------------------------------------------------------------------------
2009-08-04 10:08:56 UTC Johan D <djo33@free.fr>
* Added comment:
(In [14bb498c6a1c]) Fix MH parsing, fixes [0698e8195545]. Closes #3312

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2009-08-04 10:49:56 UTC Johan
* Added comment:
{{{
... I'm feeling like I've forgotten something, must be this : 

Thank you for your work on mutt :)
It's good to see that things keep still moving on (even if some
naughty bugs appear). 

Kudos to the mutt devs !

Johan, 
still a bit clumsy with bug reports, but trying to fix this too

On Tue, Aug 04, 2009 at 10:08:57AM -0000, Mutt wrote:
}}}
