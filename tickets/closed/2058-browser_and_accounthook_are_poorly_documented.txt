Ticket:  2058
Status:  closed
Summary: browser and account-hook are poorly documented

Reporter: reich@fas.harvard.edu
Owner:    mutt-dev

Opened:       2005-09-07 06:43:12 UTC
Last Updated: 2009-04-02 10:06:07 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
I have, let's say, two IMAP accounts, imap.fas.harvard.edu and math.harvard.edu.  I manage them both by an extensive system of account-hooks; do not accuse me of forgetting variables, for I have not.  The hooks are:

account-hook . '\
 unset imap_home_namespace; \
 set mbox="=saved-messages"; \
 set postponed="=postponed"; \
 set record="=sent-mail" '

account-hook imap://imap\.fas\.harvard\.edu '\
 set folder="imap://imap.fas.harvard.edu"; \
 set mbox="=saved-messages"; \
 set postponed="=postponed"; \
 set record="=sent-mail"; \
 set spoolfile="=INBOX" '

account-hook imap://math\.harvard\.edu '\
 set folder="imap://math.harvard.edu"; \
 set imap_home_namespace="mail"; \
 set mbox="=saved-messages"; \
 set postponed="=postponed"; \
 set record="=sent-mail" '

I start my session by logging in to imap.fas.harvard.edu and checking the various folders there via the browser (i.e. hitting c-<tab> various times).  Then I switch to math.harvard.edu, but lo, pressiing c-<tab> continues to display the imap.fas.harvard.edu folders.  In fact, it manages to re-activate that account-hook, so that I must re-change folders to get back to math.harvard.edu, signing in again.

It appears that mutt maintains two parallel "current directories": the first, the current IMAP folder, and the second, the current directory in the browser file hierarchy.  Changing the first does not change the second, though changing the second changes the first.  Hence, if I enter an IMAP folder through the browser, then change IMAP accounts via a folder change without using the browser, I can no longer use the browser conveniently.

There are invonvenient workarounds, naturally.  I could type c-=-<tab> to force completing the current $folder, but this has a surprise drawback.  It doesn't bring me to the browser if there is an unambiguous (partial) completion available; instead, it substitutes the completion.  This is true _even_if_the_current_directory_is_the_base_, and if the unambiguous completion would be elided by an application of $imap_home_namespace.  That is, if we have

folder=imap://imap.fas.harvard.edu
imap_home_namespace=mail

and if the only subdirectory of the server's root is mail, then pressing c-=-<tab> will display imap://imap.fas.harvard.edu/mail and the cursor on the editor's line, whereas pressing c-<tab> (before screwing up the browser) will correctly display the _contents_ of mail and not the folder itself.  In other words, the editor ignores settings which are relevant to the browser even though it also precludes the use of the browser in some circumstances, so that these settings are, in fact, unreliable.

This makes a meaningful "display folders" macro nigh-on impossible in a situation with multiple accounts.  c-<tab> is unreliable, and c-=-<tab> is reliable but inaccurate.  The apparently sole solution is to decorate each account-hook with a new macro definition for the desired command, according to the particular nature of the server.  Though this is technically correct, perhaps the more useful solution is to implement a <show-folders> function that does what it's supposed to and displays the root namespace of the current account, with $imap_home_namespace properly interpreted and without indirecting through the editor.  The current browser is little more than a glorified tab-completion mechanism and is apparently not quite in agreement with the editor as to what, exactly, is being completed.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-08 04:17:17 UTC Ryan Reich <reich@fas.harvard.edu>
* Added comment:
{{{
On Wed, Sep 07, 2005 at 11:10:14PM +0200, Alain Bench wrote:
> Synopsis: Folder name completion and browser listing are handled inconsistently
> 
> **** Comment added by ab on Wed, 07 Sep 2005 23:10:14 +0200 ****
>  
>     Hello Ryan. What happens if you use folder-hooks?
> 
> 

Hi Alain,

Quite a quick reply.  I'm not sure I know what you mean: use folder hooks to
redefine a "list folders" command for each folder?  Or to execute <change-dir>
on entering a new folder?  The first one would work, but is cumbersome and
probably not what you meant.  The second one would fail since Mutt executes the
hook AFTER entering the folder, so that <change-dir> is not a valid function in
a folder-hook.

Ryan
}}}

--------------------------------------------------------------------------------
2005-09-08 16:10:14 UTC ab
* Added comment:
{{{
Hello Ryan. What happens if you use folder-hooks?
}}}

--------------------------------------------------------------------------------
2005-09-08 20:58:13 UTC Ryan Reich <reich@fas.harvard.edu>
* Added comment:
{{{
On Thu, Sep 08, 2005 at 12:55:46PM +0200, Alain Bench wrote:
>     I was obviously over-short sub-understandable, sorry. Long version:
> 
>     First thank you for the report. I haven't really analyzed your
> symptoms, because at first stage it seemed to me that you were misusing
> account-hooks to change variables that should be changed in
> folder-hooks. So my question was more: What happens if you replace all
> your account-hooks by folder-hooks (and do possibly needed adaptations)?
> 
>     Also look the recently closed Debian Bug#322040 "account-hook
> doesn't set header with my_hdr for pop" at
> <URL:http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=322040>.
> 
> 
>     Note: Due to annoying BTS to mutt-dev forwarding problems, could you
> please do <group-reply>? We at mutt-dev do not receive even the half of
> bug reports and followups. :-(
> 
> 
> Bye!	Alain.
> -- 
> Give your computer's unused idle processor cycles to a scientific goal:
> The Folding@home project at <URL:http://folding.stanford.edu/>.

You are right that I was misusing account-hook.  Changing them to folder-hooks
does solve the stated browser inconsistency problem.  However, I found another
problem in doing so: even with folder-hooks, the following sequence of actions
does something odd: start mutt in account 1 and do <change-folder><tab>
<select-entry><change-folder>(account 2)<change-folder><tab><select-entry>.  At
this point, and no earlier, the variables all change to the first account's but
I continue to browse the second account correctly (that is, the command
<change-folder><tab> brings up the current account's folders.  Since in my case
$imap_home_namespace is unset, it does display some things incorrectly, but on
the correct account nonetheless).  Interestingly, this only happens if I enter
the folder browser by using the exact command <change-folder><tab>.  If I
complete =, via <change-folder>=<tab><tab><tab> (all three are needed), then
the settings are NOT changed afterwards, even if I use the other command
subsequently.

Still, the second command has its drawbacks as I mentioned in the original
report.  It seems that even with folder-hooks there is an inconsistency
involving the folder browser, in particular <select-entry>.

Ryan
}}}

--------------------------------------------------------------------------------
2005-09-09 05:55:46 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Wednesday, September 7, 2005 at 5:17:17 PM -0400, Ryan Reich wrote:

> On Wed, Sep 07, 2005 at 11:10:14PM +0200, Alain Bench wrote:
>> folder-hooks?
> I'm not sure I know what you mean

    I was obviously over-short sub-understandable, sorry. Long version:

    First thank you for the report. I haven't really analyzed your
symptoms, because at first stage it seemed to me that you were misusing
account-hooks to change variables that should be changed in
folder-hooks. So my question was more: What happens if you replace all
your account-hooks by folder-hooks (and do possibly needed adaptations)?

    Also look the recently closed Debian Bug#322040 "account-hook
doesn't set header with my_hdr for pop" at
<URL:http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=322040>.


    Note: Due to annoying BTS to mutt-dev forwarding problems, could you
please do <group-reply>? We at mutt-dev do not receive even the half of
bug reports and followups. :-(


Bye!	Alain.
-- 
Give your computer's unused idle processor cycles to a scientific goal:
The Folding@home project at <URL:http://folding.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2005-09-14 20:11:02 UTC Ryan Reich <reich@fas.harvard.edu>
* Added comment:
{{{
On Wed, Sep 14, 2005 at 02:27:39PM +0200, Alain Bench wrote:
> Hello Ryan,
> 
>  On Thursday, September 8, 2005 at 9:58:13 AM -0400, Ryan Reich wrote:
> 
> > start mutt in account 1 and do <change-folder><tab><select-entry>
> > <change-folder>(account 2)<change-folder><tab><select-entry>. At this
> > point, and no earlier, the variables all change to the first account's but
> > I continue to browse the second account correctly (that is, the command
> > <change-folder><tab> brings up the current account's folders.
> 
>     More exactly <change-folder><Tab> brings the same directory as browsed
>     last time. This is not influenced by runtime changes to $folder.  Only
>     the first such call to browser is initialised by $folder. AFAICS this is
>     the intended behaviour, permitting to continue navigating at the point
>     where one last left the browser.
> 
>     Start Mutt, ":set folder=~/Mail/<Enter>", then <change-folder><Tab> will
>     bring you in the ~/Mail/ directory. Navigate out of there, towards /tmp/.
>     Quit browsing, either thru <exit>^G or selecting a folder in /tmp/. Call
>     again <change-folder><Tab>: You're in /tmp/.
> 
>     AFAICS the same for local directories and IMAP ones, and the same for
>     <change-folder><complete> and <change-folder>?
 
What you describe in the first two paragraphs is what I see.  <complete> is
bound to <tab> and <change-folder>? seems to be equivalent to
<change-folder><tab> (I wouldn't know for sure; is there specific mention of
this in the manual?  The browser is not much described there) so the last
paragraph as well.  And I don't necessarily see anything wrong with this
behavior, since one does want to pick up browsing where one left off.  Here's
the question, though: if I change my settings with account-hooks then the
folder browser thinks the last account's tree is where I left off, and if I
change them using folder-hooks then it thinks that the current account's tree
(which I have not yet browsed) is where I left off.  I prefer the second
behavior, but I don't see why they should be different.  This is the source of
my criticism of Mutt's separate folder tracking.
 
> > If I complete =, via <change-folder>=<tab><tab><tab> (all three are
> > needed), then the settings are NOT changed afterwards, even if I use the
> > other command subsequently.
> 
>     Completing = explicitly completes $folder. And browsing $folder will
>     count as "last time" for the next <change-folder><Tab>.

Right, which is why I did that.  It forces the current directory to be the
current server's root.  My complaint here was that Mutt seemingly applies
settings from an account I'm no longer browsing when I enter the browser using
one command as opposed to another.

> > Still, the second command has its drawbacks as I mentioned in the original
> > report.
> 
>     You mean <change-folder>=<Tab><Tab><Tab> that could sometimes not bring
>     the browser, but first complete an unambiguous name in $folder?

Yes, that one.  It seems esoteric but it happens to apply to me.  Why this is I
am not sure: although mail/ is the only subfolder of the account root to which
I am subscribed it is explicitly excluded by imap_home_namespace, and the IMAP
folders for the account which are in it do not have an unambiguous completion
from the empty string.

Ryan
}}}

--------------------------------------------------------------------------------
2005-09-15 07:27:39 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Ryan,

 On Thursday, September 8, 2005 at 9:58:13 AM -0400, Ryan Reich wrote:

> start mutt in account 1 and do <change-folder><tab><select-entry>
> <change-folder>(account 2)<change-folder><tab><select-entry>. At this
> point, and no earlier, the variables all change to the first account's
> but I continue to browse the second account correctly (that is, the
> command <change-folder><tab> brings up the current account's folders.

    More exactly <change-folder><Tab> brings the same directory as
browsed last time. This is not influenced by runtime changes to $folder.
Only the first such call to browser is initialised by $folder. AFAICS
this is the intended behaviour, permitting to continue navigating at the
point where one last left the browser.

    Start Mutt, ":set folder=~/Mail/<Enter>", then <change-folder><Tab>
will bring you in the ~/Mail/ directory. Navigate out of there, towards
/tmp/. Quit browsing, either thru <exit>^G or selecting a folder in
/tmp/. Call again <change-folder><Tab>: You're in /tmp/.

    AFAICS the same for local directories and IMAP ones, and the same
for <change-folder><complete> and <change-folder>?


> If I complete =, via <change-folder>=<tab><tab><tab> (all three are
> needed), then the settings are NOT changed afterwards, even if I use
> the other command subsequently.

    Completing = explicitly completes $folder. And browsing $folder will
count as "last time" for the next <change-folder><Tab>.


> Still, the second command has its drawbacks as I mentioned in the
> original report.

    You mean <change-folder>=<Tab><Tab><Tab> that could sometimes not
bring the browser, but first complete an unambiguous name in $folder?


Bye!	Alain.
-- 
set honor_followup_to=yes in muttrc is the default value, and makes your
list replies go where the original author wanted them to go: Only to the
list, or with a private copy.
}}}

--------------------------------------------------------------------------------
2005-09-15 21:25:29 UTC Ryan Reich <reich@fas.harvard.edu>
* Added comment:
{{{
On Thu, Sep 15, 2005 at 10:58:54AM +0200, Alain Bench wrote:
>  On Wednesday, September 14, 2005 at 9:11:02 AM -0400, Ryan Reich wrote:
> > On Wed, Sep 14, 2005 at 02:27:39PM +0200, Alain Bench wrote:
> >> You mean <change-folder>=<Tab><Tab><Tab> that could sometimes not bring
> >> the browser, but first complete an unambiguous name in $folder?
> > Yes, that one.
> 
>     OK: Unless I misundertood something, this seems to me not to be a bug.
>     What about setting $folder="imap://math.harvard.edu/mail/"

Then it's a feature, I guess, and therefore should be better documented.  The
situation with the browser's current folder and the nature of account-hook
versus folder-hook is confusing to say the least.

You haven't addressed the situation where when I change accounts, under a
particular cirumstance the settings revert to the previous account's.

Ryan
}}}

--------------------------------------------------------------------------------
2005-09-16 03:58:54 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Wednesday, September 14, 2005 at 9:11:02 AM -0400, Ryan Reich wrote:

> <change-folder>? seems to be equivalent to <change-folder><tab>

    Well, there is a small difference:

 - <change-folder>?<exit> comes back to index or whatever previous.
 - <change-folder><complete><exit> comes back to <change-folder> prompt.

    But AFAICS no difference about current dir.


> if I change my settings with account-hooks then the folder browser
> thinks the last account's tree is where I left off

    No: It's just random unpredictable. It happens to be the last
account in your precise usage conditions. But could be another one.


> if I change them using folder-hooks then it thinks that the current
> account's tree (which I have not yet browsed) is where I left off.

    Yes. More precisely current $folder. And as you're automatically
setting $folder to current IMAP account, that's the same.


> I prefer the second behavior, but I don't see why they should be
> different. This is the source of my criticism of Mutt's separate
> folder tracking.

    There is no separate folder tracking. In both cases browsing
initiates in current $folder. But account-hook doesn't set $folder
reliably, and is not intended to.


> On Wed, Sep 14, 2005 at 02:27:39PM +0200, Alain Bench wrote:
>> You mean <change-folder>=<Tab><Tab><Tab> that could sometimes not
>> bring the browser, but first complete an unambiguous name in $folder?
> Yes, that one.

    OK: Unless I misundertood something, this seems to me not to be a
bug. What about setting $folder="imap://math.harvard.edu/mail/"


Bye!	Alain.
-- 
When you want to reply to a mailing list, please avoid doing so with
Intertivity Solutions Web.Mail. This lacks necessary references and breaks threads.
}}}

--------------------------------------------------------------------------------
2005-09-18 12:45:31 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Thursday, September 15, 2005 at 10:25:29 AM -0400, Ryan Reich wrote:

> should be better documented. The situation with the browser's current
> folder and the nature of account-hook versus folder-hook is confusing
> to say the least.

    I agree browser is subdocumented, and account-hook would need
explicit statement of what it is not intended to do (even I don't know
if $imap_home_namespace should be set in a-h or f-h). English mother
tongue documentation writers are always welcome.


> You haven't addressed the situation where when I change accounts,
> under a particular cirumstance the settings revert to the previous
> account's.

    I have, indirectly and partially. More precisely:

 On Thursday, September 8, 2005 at 9:58:13 AM -0400, Ryan Reich wrote:

> start mutt in account 1 and do <change-folder><tab><select-entry>
> <change-folder>(account 2)<change-folder><tab><select-entry>. At this
> point, and no earlier, the variables all change to the first account's
> but I continue to browse the second account correctly

    Let's analyse each step:

> start mutt in account 1

    This enters acc1, triggers acc1 folder-hooks, and sets $folder=acc1.

> <change-folder><tab>

    This initiates browsing in current $folder. From now on, you will
always keep browsing acc1.

> <select-entry>

    This enters a folder of acc1. Retriggers acc1 folder-hooks, without
change in variables.

> <change-folder>(account 2)

    This enters acc2, triggers acc2 folder-hooks, changes vars to acc2.
This does *not* change current browsing directory.

> <change-folder><tab>

    You browse acc1, as always.

> <select-entry>

    This enters a folder of acc1, triggers acc1 folder-hooks, and
changes vars to acc1.

> the variables all change to the first account's

    True and normal.

> but I continue to browse the second account correctly

    No: You continue to browse the *first* account. Otherwise please
explain exactly what "(account 2)" means. I assumed it meant
"imap://math.harvard.edu/mail/<Enter>" or such.

    Here all settings in folder-hooks reliably apply when entering a
matching folder. Whatever entrance method: -f, <change-folder> prompt,
browser <select-entry>, ... that's all?


    BTW how can a user in your situation (multiple IMAP accounts, and/or
multiple local directory trees) influence automatically browser's
current dir, out of manual navigation? What about something as
(untested):

| folder-hook ^imap://imap\\.fas\\.harvard\\.edu \
| "push <change-folder>?<change-dir><kill-line>\
| imap://imap.fas.harvard.edu/<Enter><exit>"
|
| folder-hook ^imap://math\\.harvard\\.edu \
| "push <change-folder>?<change-dir><kill-line>\
| imap://math.harvard.edu/mail/<Enter><exit>"


Bye!	Alain.
-- 
Software should be written to deal with every conceivable error
	RFC 1122 / Robustness Principle
}}}

--------------------------------------------------------------------------------
2005-10-08 07:08:36 UTC ab
* Added comment:
{{{
On Wed, Sep 07, 2005 at 11:10:14PM +0200, Alain Bench wrote:
  On Wednesday, September 7, 2005 at 5:17:17 PM -0400, Ryan Reich wrote:
 On Thu, Sep 08, 2005 at 12:55:46PM +0200, Alain Bench wrote:
 Hello Ryan,
 On Wed, Sep 14, 2005 at 02:27:39PM +0200, Alain Bench wrote:
  On Wednesday, September 14, 2005 at 9:11:02 AM -0400, Ryan Reich wrote:
 On Thu, Sep 15, 2005 at 10:58:54AM +0200, Alain Bench wrote:
  On Thursday, September 15, 2005 at 10:25:29 AM -0400, Ryan Reich wrote:
 Retitle from "Folder name completion and browser listing are
handled inconsistently". Refile as doc-bug.

There is here no code bug, but account-hook, prompt
completion, and browser are poorly documented. As always,
English mother tongue documentation writers would be
welcome. Even for limited work on specific sections.

I hesitated to turn this into a wishlist to give user write
access to browser's current directory, maybe thru a
$variable (or 2, see wish/950), so it can be forced in
macros or hooks outside of the browser, but the <change-dir>
pushing folder-hooks I gave seem to do the job.
}}}

--------------------------------------------------------------------------------
2009-04-02 10:06:07 UTC pdmef
* Added comment:
Closing, as documentation with example was added in [22f21b558f57], see http://dev.mutt.org/doc/manual.html#account-hook

* Updated description:
{{{
I have, let's say, two IMAP accounts, imap.fas.harvard.edu and math.harvard.edu.  I manage them both by an extensive system of account-hooks; do not accuse me of forgetting variables, for I have not.  The hooks are:

account-hook . '\
 unset imap_home_namespace; \
 set mbox="=saved-messages"; \
 set postponed="=postponed"; \
 set record="=sent-mail" '

account-hook imap://imap\.fas\.harvard\.edu '\
 set folder="imap://imap.fas.harvard.edu"; \
 set mbox="=saved-messages"; \
 set postponed="=postponed"; \
 set record="=sent-mail"; \
 set spoolfile="=INBOX" '

account-hook imap://math\.harvard\.edu '\
 set folder="imap://math.harvard.edu"; \
 set imap_home_namespace="mail"; \
 set mbox="=saved-messages"; \
 set postponed="=postponed"; \
 set record="=sent-mail" '

I start my session by logging in to imap.fas.harvard.edu and checking the various folders there via the browser (i.e. hitting c-<tab> various times).  Then I switch to math.harvard.edu, but lo, pressiing c-<tab> continues to display the imap.fas.harvard.edu folders.  In fact, it manages to re-activate that account-hook, so that I must re-change folders to get back to math.harvard.edu, signing in again.

It appears that mutt maintains two parallel "current directories": the first, the current IMAP folder, and the second, the current directory in the browser file hierarchy.  Changing the first does not change the second, though changing the second changes the first.  Hence, if I enter an IMAP folder through the browser, then change IMAP accounts via a folder change without using the browser, I can no longer use the browser conveniently.

There are invonvenient workarounds, naturally.  I could type c-=-<tab> to force completing the current $folder, but this has a surprise drawback.  It doesn't bring me to the browser if there is an unambiguous (partial) completion available; instead, it substitutes the completion.  This is true _even_if_the_current_directory_is_the_base_, and if the unambiguous completion would be elided by an application of $imap_home_namespace.  That is, if we have

folder=imap://imap.fas.harvard.edu
imap_home_namespace=mail

and if the only subdirectory of the server's root is mail, then pressing c-=-<tab> will display imap://imap.fas.harvard.edu/mail and the cursor on the editor's line, whereas pressing c-<tab> (before screwing up the browser) will correctly display the _contents_ of mail and not the folder itself.  In other words, the editor ignores settings which are relevant to the browser even though it also precludes the use of the browser in some circumstances, so that these settings are, in fact, unreliable.

This makes a meaningful "display folders" macro nigh-on impossible in a situation with multiple accounts.  c-<tab> is unreliable, and c-=-<tab> is reliable but inaccurate.  The apparently sole solution is to decorate each account-hook with a new macro definition for the desired command, according to the particular nature of the server.  Though this is technically correct, perhaps the more useful solution is to implement a <show-folders> function that does what it's supposed to and displays the root namespace of the current account, with $imap_home_namespace properly interpreted and without indirecting through the editor.  The current browser is little more than a glorified tab-completion mechanism and is apparently not quite in agreement with the editor as to what, exactly, is being completed.
>How-To-Repeat:
>Fix:
}}}
* resolution changed to fixed
* status changed to closed
