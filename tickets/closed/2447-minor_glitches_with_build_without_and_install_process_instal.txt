Ticket:  2447
Status:  closed
Summary: minor glitches with build ("--without") and install process ("install-sh" args)

Reporter: rado@math.uni-hamburg.de
Owner:    mutt-dev

Opened:       2006-08-24 15:28:24 UTC
Last Updated: 2006-09-01 20:31:15 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
install: the Makefile created uses mutt's own "install-sh" with
argument "-m644" which is not parsed correctly by "install-sh":
it requires "-m 644" (i.e. a space inbetween).

build: --without-mixmaster enables it, because currently
"configure" only checks whether any mixmaster related option 
was given, but not which.
>How-To-Repeat:
Just "make install" on the system.

build: ./configure --without-mixmaster
>Fix:
install: either fix the "install-sh" script to accept args without spaces or change Makefile to have spaces.

build: I don't know auto-stuff well enough, but it needs to check for "yes"/"no" explicitely, not just "non-zero".
}}}

--------------------------------------------------------------------------------
2006-08-26 05:01:38 UTC Lars Hecking <lhecking@users.sourceforge.net>
* Added comment:
{{{
> build: ./configure --without-mixmaster
> >Fix:
> install: either fix the "install-sh" script to accept args without spaces or change Makefile to have spaces.

 Correct. It is necessary to change -m644 to "-m 644" in the install-data-local: target in the top level Makefile.am.

> build: I don't know auto-stuff well enough, but it needs to check for "yes"/"no" explicitely, not just "non-zero".

--- configure.in.dist   2006-08-11 13:06:42.000000000 +0100
+++ configure.in        2006-08-25 12:57:28.000000000 +0100
@@ -158,10 +158,12 @@
 fi
  
 AC_ARG_WITH(mixmaster, AC_HELP_STRING([--with-mixmaster[=PATH]], [Include Mixmaster support]),
-        [if test -x "$withval" ; then
-                MIXMASTER="$withval"
-         else
-                MIXMASTER="mixmaster"
+        [if test "$withval" != no ; then
+                 if test -x "$withval" ; then
+                        MIXMASTER="$withval"
+                 else
+                        MIXMASTER="mixmaster"
+                 fi
          fi
          OPS="$OPS \$(srcdir)/OPS.MIX"
          MUTT_LIB_OBJECTS="$MUTT_LIB_OBJECTS remailer.o"
}}}

--------------------------------------------------------------------------------
2006-09-02 14:31:15 UTC brendan
* Added comment:
{{{
> build: ./configure --without-mixmaster
Fixed in CVS (thanks Rado, Lars and Ralf)
}}}

* resolution changed to fixed
* status changed to closed
