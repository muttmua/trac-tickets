Ticket:  2094
Status:  closed
Summary: ESMTP patch: newline conversion and faulty error checking

Reporter: wildenhues@ins.uni-bonn.de
Owner:    mutt-dev

Opened:       2005-09-27 10:33:40 UTC
Last Updated: 2005-10-04 04:00:24 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
The ESMTP patch proposed for mutt writes out the mail
header+body with plain \n instead of \r\n.  qmail does
not accept it.  Snippet from strace output:

write(4, "DATA\r\n", 6)                 = 6
read(4, "354 go ahead\r\n", 1024)       = 14
fstat(5, {st_dev=makedev(8, 5), st_ino=16476, st_mode=S_IFREG|0600, st_nlink=1, st_uid=1032, st_gid=1030, st_blksize=4096, st_blocks=16, st_size=387, st_atime=2005/09/27-10:50:54, st_mtime=2005/09/mmap(NULL, 4096, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_ANONYMOUS, -1, 0) = 0x2aaaad4e5000
read(5, "Date: Tue, 27 Sep 2005 10:50:54 +0200\nFrom: Ralf Wildenhues <wildenhues@ins.uni-bonn.de>\nTo: Ralf Wildenhues <wildenhues@ins.uni-bonn.de>\nSubject: test8\nMessage-ID: <20050927085054.GA8
write(4, "Date: Tue, 27 Sep 2005 10:50:54 +0200\n", 38) = 38
lseek(5, 0, SEEK_CUR)                   = 387
write(4, "From: Ralf Wildenhues <wildenhues@ins.uni-bonn.de>\n", 51) = 51
write(4, "To: Ralf Wildenhues <wildenhues@ins.uni-bonn.de>\n", 49) = 49
write(4, "Subject: test8\n", 15)        = 15
write(4, "Message-ID: <20050927085054.GA832@iam.uni-bonn.de>\n", 51) = 51
write(4, "MIME-Version: 1.0\n", 18)     = 18
write(4, "Content-Type: text/plain; charset=us-ascii\n", 43) = 43
*snip*
write(4, ".\r\n", 3)                    = 3
read(4, "451 See http://pobox.com/~djb/docs/smtplf.html.\r\n", 1024) = 49


After the send failure, I can edit/postpone the message,
but it is also wrongly stored in Fcc.


Is it ok, by the way, to use GNATS for not (yet?) accepted
patches?
>How-To-Repeat:
If desirable or necessary, I can produce a config file +
command sequence to reproduce this.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-09-28 07:41:25 UTC Ralf Wildenhues <wildenhues@ins.uni-bonn.de>
* Added comment:
{{{
* TAKAHASHI Tamotsu wrote on Tue, Sep 27, 2005 at 12:48:07PM CEST:
> > >Number:         2094
> > >Notify-List:    
> > >Category:       mutt
> > >Synopsis:       ESMTP patch: newline conversion and faulty error checking
> 
> > >Release:        CVS HEAD + patch-1.5.11.bc.smtp.10
> 
> > >Description:
> > The ESMTP patch proposed for mutt writes out the mail
> > header+body with plain \n instead of \r\n.  qmail does
> > not accept it.  Snippet from strace output:
> (snip)
> 
> I think Mutt-smtp currently writes exactly what fgets reads.
> Here is an untested patch.

It doesn't work, but can easily be corrected, see below.  Note that even
with this patch, you have an issue if the \r\n does not fit in the buffer.

> > After the send failure, I can edit/postpone the message,
> > but it is also wrongly stored in Fcc.
> 
> Mutt saves FCC _before_ sending a message.
> If FCC fails, mutt does not try to send it.

Oh, I didn't know that, really.  Shows how many failed sends I've had.
:)

Thanks for the quick response!

Cheers,
Ralf

--- orig/smtp.c	2005-09-27 14:24:32.000000000 +0200
+++ mutt/smtp.c	2005-09-27 14:22:35.000000000 +0200
@@ -144,6 +144,7 @@
   progress_t progress;
   struct stat st;
   int r;
+  size_t buflen;
 
   fp = fopen (msgfile, "r");
   if (!fp)
@@ -170,6 +171,10 @@
   
   while (fgets (buf, sizeof (buf), fp))
   {
+    buflen = mutt_strlen (buf);
+    if (buflen && buf[buflen-1] == '\n'
+	&& (buflen == 1 || buf[buflen - 2] != '\r'))
+      snprintf (buf + buflen - 1, sizeof (buf) - buflen + 1, "\r\n");
     if (buf[0] == '.')
     {
       if (mutt_socket_write_d (conn, ".", 3) == -1)
}}}

--------------------------------------------------------------------------------
2005-09-28 12:16:36 UTC Ralf Wildenhues <wildenhues@ins.uni-bonn.de>
* Added comment:
{{{
* Ralf Wildenhues wrote on Tue, Sep 27, 2005 at 02:41:25PM CEST:
> 
> Note that even with this patch, you have an issue if the \r\n does not
> fit in the buffer.

Should be trivially fixed with this patch.

Cheers,
Ralf

--- orig/smtp.c	2005-09-27 14:24:32.000000000 +0200
+++ mutt/smtp.c	2005-09-27 19:14:00.000000000 +0200
@@ -144,6 +144,7 @@
   progress_t progress;
   struct stat st;
   int r;
+  size_t buflen;
 
   fp = fopen (msgfile, "r");
   if (!fp)
@@ -168,8 +169,12 @@
     return r;
   }
   
-  while (fgets (buf, sizeof (buf), fp))
+  while (fgets (buf, sizeof (buf) - 1, fp))
   {
+    buflen = mutt_strlen (buf);
+    if (buflen && buf[buflen-1] == '\n'
+	&& (buflen == 1 || buf[buflen - 2] != '\r'))
+      snprintf (buf + buflen - 1, sizeof (buf) - buflen + 1, "\r\n");
     if (buf[0] == '.')
     {
       if (mutt_socket_write_d (conn, ".", 3) == -1)
}}}

--------------------------------------------------------------------------------
2005-09-28 19:48:07 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
> >Number:         2094
> >Notify-List:    
> >Category:       mutt
> >Synopsis:       ESMTP patch: newline conversion and faulty error checking

> >Release:        CVS HEAD + patch-1.5.11.bc.smtp.10

> >Description:
> The ESMTP patch proposed for mutt writes out the mail
> header+body with plain \n instead of \r\n.  qmail does
> not accept it.  Snippet from strace output:
(snip)

I think Mutt-smtp currently writes exactly what fgets reads.
Here is an untested patch.


> After the send failure, I can edit/postpone the message,
> but it is also wrongly stored in Fcc.

Mutt saves FCC _before_ sending a message.
If FCC fails, mutt does not try to send it.

-- 
tamo

--fdj2RfSjLxBAspz7
Content-Type: text/plain; charset=us-ascii
Content-Disposition: attachment; filename="patch-1.5.11smtp.tamo.eol.1"

diff -r 1506eb0d5f6e smtp.c
--- a/smtp.c	Sat Sep 24 21:57:25 2005
+++ b/smtp.c	Tue Sep 27 19:41:51 2005
@@ -144,6 +144,7 @@
   progress_t progress;
   struct stat st;
   int r;
+  size_t buflen;
 
   fp = fopen (msgfile, "r");
   if (!fp)
@@ -170,6 +171,9 @@
   
   while (fgets (buf, sizeof (buf), fp))
   {
+    buflen = mutt_strlen (buf);
+    if (buflen && buf[buflen] == '\n' && buf[buflen - 1] != '\r')
+      snprintf (buf + buflen, sizeof (buf) - buflen, "\r\n");
     if (buf[0] == '.')
     {
       if (mutt_socket_write_d (conn, ".", 3) == -1)

--fdj2RfSjLxBAspz7--
}}}

--------------------------------------------------------------------------------
2005-10-04 22:00:24 UTC brendan
* Added comment:
{{{
* TAKAHASHI Tamotsu wrote on Tue, Sep 27, 2005 at 12:48:07PM CEST:
 * Ralf Wildenhues wrote on Tue, Sep 27, 2005 at 02:41:25PM CEST:
Applied in patch-1.5.11.bc.smtp.11, thanks.
}}}

* resolution changed to fixed
* status changed to closed
