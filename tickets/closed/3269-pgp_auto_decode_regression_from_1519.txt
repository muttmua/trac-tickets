Ticket:  3269
Status:  closed
Summary: pgp_auto_decode regression from 1.5.19

Reporter: antonio@dyne.org
Owner:    brendan

Opened:       2009-06-16 23:20:11 UTC
Last Updated: 2009-06-17 18:10:42 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/533370

Hi,
using pgp_auto_decode=yes in .muttrc, text outside the signed pgp-inline part is removed, you can try by yourself with the attached mailbox, opening it with mutt -f 533165.mbox

the text "Package:, Version: etc" is not shown; this was working fine on 1.5.18, and it is not working on 1.5.19 and 1.5.20

Cheers
Antonio

--------------------------------------------------------------------------------
2009-06-16 23:21:41 UTC antonio@dyne.org
* Added attachment 533165.mbox

--------------------------------------------------------------------------------
2009-06-16 23:23:17 UTC antonio@dyne.org
* summary changed to pgp_auto_decode regression from 1.5.19
* version changed to 1.5.20

--------------------------------------------------------------------------------
2009-06-17 16:30:53 UTC pdmef
* Added comment:
Bisect says the first bad commit is [e2780a423d96]:

{{{
changeset:   5422:e2780a423d96
branch:      HEAD
user:        Brendan Cully <brendan@kublai.com>
date:        Wed Jun 25 22:43:32 2008 -0700
files:       commands.c copy.c crypt-gpgme.c handler.c mbox.c pgp.c
description:
Make mutt_copy_message distinguish between fatal and non-fatal errors.
Non-fatal errors should prevent moving messages, since they indicate
data loss. But mutt should still attempt to display them, since being
able to see some attachments is better than nothing.
Also stop printing out non-PGP material in application/pgp
attachments. Closes #2545, #2912.
}}}

It removes displaying additional content, maybe brendan can clarify the issue? I think it's harmless to print it if the user can clearly distinct between PGP and non-PGP material.

Assigning it to brendan.

* owner changed to brendan
* status changed to assigned

--------------------------------------------------------------------------------
2009-06-17 18:10:42 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [7f37d0a57d83]) Display unsigned part of traditional PGP messages.
Unbreaks [e2780a423d96], closes #3269

* resolution changed to fixed
* status changed to closed
