Ticket:  3191
Status:  closed
Summary: Manual: Variables should be sorted

Reporter: pdmef
Owner:    pdmef

Opened:       2009-02-20 13:06:02 UTC
Last Updated: 2009-04-02 09:50:26 UTC

Priority:  minor
Component: doc
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Most variables are sorted already, so I assume they should be in general. Attached is a patch adding a 'sortcheck' target to doc/Makefile.am showing unsorted ones.

--------------------------------------------------------------------------------
2009-02-20 13:06:55 UTC pdmef
* Added attachment sortcheck.diff
* Added comment:
Show unsorted variables

--------------------------------------------------------------------------------
2009-03-07 15:49:19 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [ff1906f70b1b]) Sort most variables (except crypto), see #3191.

--------------------------------------------------------------------------------
2009-03-09 10:31:46 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [11cd72da743a]) Sort SSL-related variables, see #3191.

--------------------------------------------------------------------------------
2009-03-09 10:45:50 UTC pdmef
* status changed to assigned

--------------------------------------------------------------------------------
2009-03-09 10:45:58 UTC pdmef
* status changed to started

--------------------------------------------------------------------------------
2009-04-02 09:50:18 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [86faf8f79beb]) Sort $smime_* vars, see #3191.

--------------------------------------------------------------------------------
2009-04-02 09:50:24 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [e8cdc049cf84]) Sort $pgp_* and $crypt_* vars, see #3191.

--------------------------------------------------------------------------------
2009-04-02 09:50:26 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [1759a509dd0a]) Add 'sortcheck' target to doc/Makefile.am

This should be used from time to time to check if vars are sorted.

Closes #3191.

* resolution changed to fixed
* status changed to closed
