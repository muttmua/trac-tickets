Ticket:  2104
Status:  closed
Summary: Core dump reading pgp-signed messages without pgp

Reporter: mills@cc.umanitoba.ca
Owner:    mutt-dev

Opened:       2005-10-05 17:28:14 UTC
Last Updated: 2006-03-24 22:13:15 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
With all PGP variables set to defaults in Muttrc, reading a
PGP-signed message causes a core dump.  Mutt gets a SIGSEGV
while calling rewind() on a NULL stream.  Here's a stack trace
from mdb:

> ::stack
libc.so.1`rewind+4(0, ffbfd744, ffbfbcb0, ffffffff, 9, ffffffff)
pgp_application_pgp_handler+0x898(14ec68, ffbfd744, 8, 0, 0, 0)
crypt_mod_pgp_application_handler+0x18(14ec68, ffbfd744, 0, ff3a2000, ff1e8328, 4)
crypt_pgp_application_pgp_handler+0x60(14ec68, ffbfd744, 8b4, 11a894, 14dd28, 0
)
mutt_body_handler+0x824(14ec68, ffbfd744, ffffffff, fffffff8, ffffffe0, ffbfd75d)
_mutt_copy_message+0x5a4(13e7a0, 13e7b0, 14ecf0, 14ec68, 84c, 96)
mutt_copy_message+0x80(13e7a0, 14fa90, 14ecf0, 84c, 96, a)
mutt_display_message+0x4fc(14ecf0, ffbfdf70, 0, 5, 3, 6)
mutt_index_menu+0x2e74(14fa90, 0, 0, 14fa27, 0, 80808080)
main+0x14b0(1, ffbfeb34, ffbfeb3c, 12e800, ff3a0100, ff3a0140)
_start+0x108(0, 0, 0, 0, 0, 0)

The attached diff file eliminates the core dump.  It now
displays:

[-- Error: unable to create PGP subprocess! --]

>How-To-Repeat:
Read a PGP-signed message with no PGP settings.
>Fix:
See attached diff.  Also needs a configuration change to
avoid the error in the first place.
}}}

--------------------------------------------------------------------------------
2005-10-06 10:07:34 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Wed, Oct 05, 2005 at 06:28:14PM +0200, New Mutt PR wrote:

> PGP-signed message causes a core dump.  Mutt gets a SIGSEGV
> while calling rewind() on a NULL stream.  Here's a stack trace
> from mdb:

That was fixed in 1.5.11. Please upgrade and re-test. :)

Thanks,

-- 
Paul

--jRHKVT23PllUwdXP
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.1 (GNU/Linux)

iD8DBQFDRAhWP9fOqdxRstoRAn8pAJ4mdPvAJJKGJeKMHbyD/IJAYjLs/ACeJLAZ
oQEn1lNWn/trB4a3zgvVtq4=jcD2
-----END PGP SIGNATURE-----

--jRHKVT23PllUwdXP--
}}}

--------------------------------------------------------------------------------
2005-10-07 00:05:55 UTC brendan
* Added comment:
{{{
Awaiting 1.5.11 feedback...
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2006-03-25 16:13:15 UTC paul
* Added comment:
{{{
Fixed in 1.5.11, no feedback in 5 months.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:04 UTC 
* Added attachment pgp.c.diff
* Added comment:
pgp.c.diff
