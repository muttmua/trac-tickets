Ticket:  1908
Status:  closed
Summary: pgp_create_traditional documentation missing

Reporter: hugo@larve.net
Owner:    mutt-dev

Opened:       2004-06-27 02:42:42 UTC
Last Updated: 2005-08-02 16:45:37 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6-20040523
Severity: normal

-- Please type your report below this line

I don't seem to be able to default PGP inline signatures anymore.

I have tried the following:
- set pgp_create_traditional=yes
- set pgp_autoinline=yes

And it still shows "PGP: Sign (PGP/MIME)" in the message send screen.

If I select "PGP: Sign (inline)" manually, then it does the right
thing.

Also, the pgp_create_traditional option exists but isn't documented in
the manual anymore.

Regards,

Hugo

-- System Information
System Version: Linux buena 2.6.7 #1 Wed Jun 23 17:38:24 CEST 2004 i686 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.4/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
Thread model: posix
gcc version 3.3.4 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6+20040523i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.7 (i686) [using ncurses 5.4] [using libidn 0.4.1 (compiled with 0.4.1)]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.3.rr.compressed.1
patch-1.5.5.1.tt.compat.1-cl
patch-1.5.5.1.nt.xtitles.3.ab.1
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.4.fw.maildir_inode_sort
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.5.1.cd.edit_threads.9.2
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.59d


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-06-27 03:12:19 UTC Hugo Haas <hugo@larve.net>
* Added comment:
{{{
retitle 1908 pgp_create_traditional documentation missing
thanks

I have actually realized that pgp_autoinline does work, and that I am
experiencing another problem that I'm about to file a report for
separately.

So the only issue which is real in my report is that the documentation
about pgp_create_traditional has disappeared.

Regards,

Hugo

-- 
Hugo Haas - http://larve.net/people/hugo/
}}}

--------------------------------------------------------------------------------
2005-08-03 10:45:37 UTC brendan
* Added comment:
{{{
pgp_create_traditional is just a compatibility synonym for pgp_autoinline, which 
is documented.
}}}

* resolution changed to fixed
* status changed to closed
