Ticket:  3004
Status:  closed
Summary: Mutt 1.5.17 segfaults on debian etch

Reporter: vegard
Owner:    brendan

Opened:       2007-12-21 11:04:32 UTC
Last Updated: 2008-12-10 10:00:18 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
I'm running mutt 1.5.17 (from backports; 1.5.17-1~bpo40+2) on debian etch. Mutt segfaults several times a day when reading e-mail over IMAP. It dies when something happens; like when new e-mail arrives. This is the gdb output:

{{{
$ gdb mutt core
GNU gdb 6.4.90-debian
Copyright (C) 2006 Free Software Foundation, Inc.
GDB is free software, covered by the GNU General Public License, and you are
welcome to change it and/or distribute copies of it under certain conditions.
Type "show copying" to see the conditions.
There is absolutely no warranty for GDB.  Type "show warranty" for details.
This GDB was configured as "i486-linux-gnu"...(no debugging symbols found)
Using host libthread_db library "/lib/tls/i686/cmov/libthread_db.so.1".


warning: Can't read pathname for load map: Input/output error.
Reading symbols from /lib/libncursesw.so.5...(no debugging symbols found)...done.
Loaded symbols for /lib/libncursesw.so.5
Reading symbols from /usr/lib/libgnutls.so.13...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libgnutls.so.13
Reading symbols from /usr/lib/libsasl2.so.2...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libsasl2.so.2
Reading symbols from /usr/lib/libgdbm.so.3...
(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libgdbm.so.3
Reading symbols from /usr/lib/libidn.so.11...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libidn.so.11
Reading symbols from /lib/tls/i686/cmov/libc.so.6...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libc.so.6
Reading symbols from /lib/tls/i686/cmov/libdl.so.2...
(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libdl.so.2
Reading symbols from /usr/lib/libtasn1.so.3...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libtasn1.so.3
Reading symbols from /usr/lib/libz.so.1...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libz.so.1
Reading symbols from /usr/lib/libgcrypt.so.11...
(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libgcrypt.so.11
Reading symbols from /usr/lib/libgpg-error.so.0...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libgpg-error.so.0
Reading symbols from /lib/tls/i686/cmov/libresolv.so.2...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libresolv.so.2
Reading symbols from /lib/ld-linux.so.2...
(no debugging symbols found)...done.
Loaded symbols for /lib/ld-linux.so.2
Reading symbols from /lib/tls/i686/cmov/libnsl.so.1...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libnsl.so.1
Reading symbols from /lib/tls/i686/cmov/libnss_compat.so.2...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libnss_compat.so.2
Reading symbols from /lib/tls/i686/cmov/libnss_nis.so.2...
(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libnss_nis.so.2
Reading symbols from /lib/tls/i686/cmov/libnss_files.so.2...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libnss_files.so.2
Reading symbols from /usr/lib/gconv/ISO8859-1.so...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/gconv/ISO8859-1.so
Reading symbols from /usr/lib/sasl2/libsasldb.so.2...
(no debugging symbols found)...done.
Loaded symbols for /usr/lib/sasl2/libsasldb.so.2
Reading symbols from /usr/lib/libdb-4.2.so...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libdb-4.2.so
Loaded symbols for /usr/lib/libdb-4.2.so
(no debugging symbols found)
Core was generated by `mutt'.
Program terminated with signal 11, Segmentation fault.
#0  0x08089a6c in ?? ()
(gdb) bt
#0  0x08089a6c in ?? ()
#1  0x0814a9c0 in ?? ()
#2  0x08ea2ba8 in ?? ()
#3  0x00000000 in ?? ()
}}}
Not much to go on, I believe. What to do? :)

--------------------------------------------------------------------------------
2007-12-21 11:06:40 UTC vegard
* Added attachment coredump.txt
* Added comment:
gdb output

--------------------------------------------------------------------------------
2007-12-21 15:17:40 UTC Kyle Wheeler
* Added comment:
{{{
On Friday, December 21 at 11:04 AM, quoth Mutt:
> Not much to go on, I believe. What to do? :)

If you have debug support compiled in, you could run it with the -d2 
flag and provide the ~/.muttdebug0 file. Or you could try compiling 
your own version (that doesn't have the debug symbols stripped out). 
Short of that, the best you can do is complain to the guy who made the 
package.

~Kyle
}}}

--------------------------------------------------------------------------------
2008-05-18 01:52:30 UTC brendan
* Added comment:
This needs help from downstream -- I can't debug it with this backtrace.

* component changed to IMAP
* Updated description:
I'm running mutt 1.5.17 (from backports; 1.5.17-1~bpo40+2) on debian etch. Mutt segfaults several times a day when reading e-mail over IMAP. It dies when something happens; like when new e-mail arrives. This is the gdb output:

{{{
$ gdb mutt core
GNU gdb 6.4.90-debian
Copyright (C) 2006 Free Software Foundation, Inc.
GDB is free software, covered by the GNU General Public License, and you are
welcome to change it and/or distribute copies of it under certain conditions.
Type "show copying" to see the conditions.
There is absolutely no warranty for GDB.  Type "show warranty" for details.
This GDB was configured as "i486-linux-gnu"...(no debugging symbols found)
Using host libthread_db library "/lib/tls/i686/cmov/libthread_db.so.1".


warning: Can't read pathname for load map: Input/output error.
Reading symbols from /lib/libncursesw.so.5...(no debugging symbols found)...done.
Loaded symbols for /lib/libncursesw.so.5
Reading symbols from /usr/lib/libgnutls.so.13...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libgnutls.so.13
Reading symbols from /usr/lib/libsasl2.so.2...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libsasl2.so.2
Reading symbols from /usr/lib/libgdbm.so.3...
(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libgdbm.so.3
Reading symbols from /usr/lib/libidn.so.11...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libidn.so.11
Reading symbols from /lib/tls/i686/cmov/libc.so.6...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libc.so.6
Reading symbols from /lib/tls/i686/cmov/libdl.so.2...
(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libdl.so.2
Reading symbols from /usr/lib/libtasn1.so.3...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libtasn1.so.3
Reading symbols from /usr/lib/libz.so.1...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libz.so.1
Reading symbols from /usr/lib/libgcrypt.so.11...
(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libgcrypt.so.11
Reading symbols from /usr/lib/libgpg-error.so.0...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libgpg-error.so.0
Reading symbols from /lib/tls/i686/cmov/libresolv.so.2...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libresolv.so.2
Reading symbols from /lib/ld-linux.so.2...
(no debugging symbols found)...done.
Loaded symbols for /lib/ld-linux.so.2
Reading symbols from /lib/tls/i686/cmov/libnsl.so.1...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libnsl.so.1
Reading symbols from /lib/tls/i686/cmov/libnss_compat.so.2...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libnss_compat.so.2
Reading symbols from /lib/tls/i686/cmov/libnss_nis.so.2...
(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libnss_nis.so.2
Reading symbols from /lib/tls/i686/cmov/libnss_files.so.2...(no debugging symbols found)...done.
Loaded symbols for /lib/tls/i686/cmov/libnss_files.so.2
Reading symbols from /usr/lib/gconv/ISO8859-1.so...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/gconv/ISO8859-1.so
Reading symbols from /usr/lib/sasl2/libsasldb.so.2...
(no debugging symbols found)...done.
Loaded symbols for /usr/lib/sasl2/libsasldb.so.2
Reading symbols from /usr/lib/libdb-4.2.so...(no debugging symbols found)...done.
Loaded symbols for /usr/lib/libdb-4.2.so
Loaded symbols for /usr/lib/libdb-4.2.so
(no debugging symbols found)
Core was generated by `mutt'.
Program terminated with signal 11, Segmentation fault.
#0  0x08089a6c in ?? ()
(gdb) bt
#0  0x08089a6c in ?? ()
#1  0x0814a9c0 in ?? ()
#2  0x08ea2ba8 in ?? ()
#3  0x00000000 in ?? ()
}}}
Not much to go on, I believe. What to do? :)
* milestone changed to 1.6
* owner changed to brendan
* version changed to 1.5.17

--------------------------------------------------------------------------------
2008-05-18 21:47:55 UTC Paul Walker
* Added comment:
{{{
Hi,

> If you have debug support compiled in, you could run it with the -d2 flag
> and provide the ~/.muttdebug0 file. Or you could try compiling your own
> version (that doesn't have the debug symbols stripped out). Short of that,
> the best you can do is complain to the guy who made the package.

Kylie made that suggestion in late December (just after the bug was filed).
Do you still see the problem, and (if so) were you able to try building and
running mutt with debug enabled or with debug symbols?
}}}

--------------------------------------------------------------------------------
2008-05-27 09:41:36 UTC Vegard Svanberg
* Added comment:
{{{
* Mutt <fleas@mutt.org> [2008-05-18 23:48]:

[snip]
>  Kylie made that suggestion in late December (just after the bug was
>  filed).
>  Do you still see the problem, and (if so) were you able to try building
>  and
>  running mutt with debug enabled or with debug symbols?

My apologies for not updating the ticket. This was a problem I had
running against Archiveopteryx as IMAP server. A new version of ao was
released a while after I opened this ticket, and the segfault problem
disappeared. So, while the segfault by itself is a Mutt problem, I'm
unfortunately unable to reproduce the problem. In other words, I believe
this ticket could be closed (for my part -- I don't know if anyone else
is having similar trouble).

Thanks!
}}}

--------------------------------------------------------------------------------
2008-05-29 05:31:34 UTC brendan
* Added comment:
Ok, I'm closing this ticket as unreproducible. It's probably the same bug as one of the other open IMAP segfault tickets anyway.

* resolution changed to worksforme
* status changed to closed

--------------------------------------------------------------------------------
2008-12-09 07:25:49 UTC vegard
* resolution changed to 
* status changed to reopened

--------------------------------------------------------------------------------
2008-12-09 07:27:47 UTC vegard
* Added comment:
It started crashing again a few weeks ago. Since the package from backports is stripped, I recompiled 1.5.18 from source so I could run gdb on it. Here's a backtrace:


{{{
warning: Lowest section in system-supplied DSO at 0xffffe000 is .hash at ffffe0b4
Core was generated by `mutt'.
Program terminated with signal 11, Segmentation fault.
#0  mx_update_context (ctx=0xc262990, new_messages=57) at mx.c:1561
1561          h->security = crypt_query (h->content);
(gdb) bt
#0  mx_update_context (ctx=0xc262990, new_messages=57) at mx.c:1561
#1  0x080ce21c in imap_read_headers (idata=0x8102398, msgbegin=72195, msgend=72250) at message.c:347
#2  0x080c7351 in imap_cmd_finish (idata=0x8102398) at command.c:265
#3  0x080c8fa8 in imap_check_mailbox (ctx=0xc262990, index_hint=0xffd13930, force=0) at imap.c:1417
#4  0x08062008 in mutt_index_menu () at curs_main.c:484
#5  0x0807e5ce in main (argc=Cannot access memory at address 0x468ec
) at main.c:1019

}}}

* version changed to 1.5.18

--------------------------------------------------------------------------------
2008-12-09 13:23:53 UTC vegard
* Added comment:
Crashed again, new backtrace below. Let me know if you need more -- it's crashing frequently here (I won't post more bt's after this, unless requested). 

{{{
warning: Lowest section in system-supplied DSO at 0xffffe000 is .hash at ffffe0b4
Core was generated by `mutt'.
Program terminated with signal 11, Segmentation fault.
#0  mx_update_context (ctx=0x812d810, new_messages=3) at mx.c:1561
1561          h->security = crypt_query (h->content);
(gdb) bt
#0  mx_update_context (ctx=0x812d810, new_messages=3) at mx.c:1561
#1  0x080ce21c in imap_read_headers (idata=0x8102398, msgbegin=72316, msgend=72317) at message.c:347
#2  0x080c7351 in imap_cmd_finish (idata=0x8102398) at command.c:265
#3  0x080c8fa8 in imap_check_mailbox (ctx=0x812d810, index_hint=0xffe20a00, force=0) at imap.c:1417
#4  0x08062008 in mutt_index_menu () at curs_main.c:484
#5  0x0807e5ce in main (argc=Cannot access memory at address 0x469f8
) at main.c:1019

}}}

--------------------------------------------------------------------------------
2008-12-09 16:47:09 UTC erik
* Added attachment mutt-header-bounds-checking.patch
* Added comment:
Does more bounds checking on the header for loop

--------------------------------------------------------------------------------
2008-12-09 16:48:12 UTC erik
* Added comment:
Could you give the patch a try. It does a more rigorous job of checking bounds before dereferencing the header pointer.

--------------------------------------------------------------------------------
2008-12-09 18:37:39 UTC vegard
* Added comment:
Sorry, it's still crashing. Note that it only happens when I have other e-mail clients (TB/Evolution) accessing the same mailbox simultaneously. It never happens (as far as I can remember) when Mutt is alone. It usually crashes when:

a) I'm starting Mutt, it has to open a lot (currently >70k) of messages, and new messages are injected to the mailbox while Mutt is opening it. 

b) I'm reading a message, and while I'm reading, new messages arrive in the mailbox. I press 'i' to go back to the message index. Crash.

{{{
warning: Lowest section in system-supplied DSO at 0xffffe000 is .hash at ffffe0b4
Core was generated by `mutt'.
Program terminated with signal 11, Segmentation fault.
#0  mx_update_context (ctx=0x812d810, new_messages=72374) at mx.c:1564
1564          h->security = crypt_query (h->content);
(gdb) bt
#0  mx_update_context (ctx=0x812d810, new_messages=72374) at mx.c:1564
#1  0x080ce22c in imap_read_headers (idata=0x8102398, msgbegin=0, msgend=72372) at message.c:347
#2  0x080cbce9 in imap_open_mailbox (ctx=0x812d810) at imap.c:751
#3  0x08088a95 in mx_open_mailbox (path=0xfff3ae50 "imap://vegard@127.0.0.1:144/INBOX", 
    flags=<value optimized out>, pctx=0x11ab5) at mx.c:692
#4  0x080654ef in mutt_index_menu () at curs_main.c:1138
#5  0x0807e5ce in main (argc=Cannot access memory at address 0x46ad4
) at main.c:1019

}}}

--------------------------------------------------------------------------------
2008-12-09 19:19:27 UTC erik
* Added attachment mutt-mx.c-check-header-pointer.patch
* Added comment:
Just check the header pointer for validity

--------------------------------------------------------------------------------
2008-12-09 19:20:01 UTC erik
* Added comment:
That is interesting. Try the new patch that just moves on if the header pointer is not valid.

--------------------------------------------------------------------------------
2008-12-10 07:01:25 UTC vegard
* Added comment:
It survived a while longer this time. Backtrace is slightly different:

{{{
Program terminated with signal 11, Segmentation fault.
#0  mutt_sort_threads (ctx=0x812d810, init=0) at thread.c:787
787         if (!cur->thread)
(gdb) bt
#0  mutt_sort_threads (ctx=0x812d810, init=0) at thread.c:787
#1  0x080ab7ae in mutt_sort_headers (ctx=0x812d810, init=0) at sort.c:297
#2  0x08061c25 in update_index (menu=0x8101828, ctx=0x812d810, check=1, oldcount=72496, 
    index_hint=72435) at curs_main.c:323
#3  0x080620f0 in mutt_index_menu () at curs_main.c:497
#4  0x0807e5ce in main (argc=135612280, argv=0xff940764) at main.c:1019

}}}

--------------------------------------------------------------------------------
2008-12-10 07:12:56 UTC vegard
* Added comment:
Segfaulted when I restarted mutt after the above crash :) Slightly different bt, but seems to be the same kind of segfault:

{{{
Program terminated with signal 11, Segmentation fault.
#0  mutt_sort_threads (ctx=0x812d300, init=1) at thread.c:787
787         if (!cur->thread)
(gdb) bt
#0  mutt_sort_threads (ctx=0x812d300, init=1) at thread.c:787
#1  0x080ab7ae in mutt_sort_headers (ctx=0x812d300, init=1) at sort.c:297
#2  0x08088c2b in mx_open_mailbox (path=0xffcefc00 "imap://vegard@127.0.0.1:144/INBOX", 
    flags=<value optimized out>, pctx=0xc205318) at mx.c:715
#3  0x080654ef in mutt_index_menu () at curs_main.c:1138
#4  0x0807e5ce in main (argc=135451512, argv=0xffcf0b14) at main.c:1019

}}}

--------------------------------------------------------------------------------
2008-12-10 10:00:18 UTC pdmef
* Added comment:
Closing as this looks like a duplicate of #2935. Please continue the discussion there or reopen if this case is different.

* resolution changed to duplicate
* status changed to closed
