Ticket:  2820
Status:  closed
Summary: Mutt doesn't build with --without-wc-funcs

Reporter: vincent@vinc17.org
Owner:    mutt-dev

Opened:       2007-03-04 11:31:35 UTC
Last Updated: 2007-04-03 01:22:23 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
When I configure Mutt using --without-wc-funcs (this should be better under Mac OS X as its wcwidth function is buggy), I get the following error when running make:

gcc -DHAVE_CONFIG_H -I. -I..  -I.. -I../intl -I../intl  -Wall -pedantic -g -O2 -MT auth.o -MD -MP -MF .deps/auth.Tpo -c -o auth.o auth.c
In file included from ../protos.h:24,
                 from ../mutt.h:956,
                 from auth.c:27:
../mbyte.h:16: error: parse error before '(' token
../mbyte.h:17: error: parse error before '(' token
../mbyte.h:18: error: parse error before '(' token
make[2]: *** [auth.o] Error 1
make[1]: *** [all-recursive] Error 1
make: *** [all] Error 2
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2007-03-05 06:15:35 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2007-03-04 12:31:36 +0100, Vincent Lefevre wrote:
> When I configure Mutt using --without-wc-funcs (this should be
> better under Mac OS X as its wcwidth function is buggy), I get the
> following error when running make:
> 
> gcc -DHAVE_CONFIG_H -I. -I..  -I.. -I../intl -I../intl  -Wall -pedantic -g -O2 -MT auth.o -MD -MP -MF .deps/auth.Tpo -c -o auth.o auth.c
> In file included from ../protos.h:24,
>                  from ../mutt.h:956,
>                  from auth.c:27:
> ../mbyte.h:16: error: parse error before '(' token
> ../mbyte.h:17: error: parse error before '(' token
> ../mbyte.h:18: error: parse error before '(' token

I could fix the build problem by adding #undef's in mbyte.h:

Index: mbyte.h
===================================================================
RCS file: /home/roessler/cvs/mutt/mbyte.h,v
retrieving revision 3.1
diff -d -u -r3.1 mbyte.h
--- mbyte.h	5 Feb 2005 14:11:36 -0000	3.1
+++ mbyte.h	4 Mar 2007 12:09:38 -0000
@@ -11,13 +11,21 @@
 # endif
 
 # ifndef HAVE_WC_FUNCS
+#  undef wcrtomb
 size_t wcrtomb (char *s, wchar_t wc, mbstate_t *ps);
+#  undef mbrtowc
 size_t mbrtowc (wchar_t *pwc, const char *s, size_t n, mbstate_t *ps);
+#  undef iswprint
 int iswprint (wint_t wc);
+#  undef iswspace
 int iswspace (wint_t wc);
+#  undef iswalnum
 int iswalnum (wint_t wc);
+#  undef towupper
 wint_t towupper (wint_t wc);
+#  undef towlower
 wint_t towlower (wint_t wc);
+#  undef wcwidth
 int wcwidth (wchar_t wc);
 # endif /* !HAVE_WC_FUNCS */
 
but combining characters aren't displayed correctly. I get:

e\01.1 e\01.2 e\01.3 e\01.4 e\01.5 e\01.6 e\01.7 e\01.8 e\01.9+e\01.10 e\01.11 e
\01.12 e\01.13 e\01.14 e\01.15 e\01.16 e\01.17+e\01.18 e\01.19 e\01.20

for the attached mail message.

-- 
Vincent Lefèvre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / Arenaire project (LIP, ENS-Lyon)

--AhhlLboLdkugWU4S
Content-Type: text/plain; charset=utf-8
Content-Disposition: attachment; filename=combining-wcwidth
Content-Transfer-Encoding: quoted-printable

From vincent@vinc17.org Sun Mar  4 11:58:35 2007
Date: Sun, 4 Mar 2007 11:58:31 +0100
From: Vincent Lefevre <vincent@vinc17.org>
To: Vincent Lefevre <vincent@vinc17.org>
Subject: test of combining-character width
Message-ID: <20070304105831.GC5012@prunille.vinc17.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Disposition: inline
Content-Transfer-Encoding: 8bit
Status: RO
Content-Length: 131
Lines: 1

eÌ.1 eÌ.2 eÌ.3 eÌ.4 eÌ.5 eÌ.6 eÌ.7 eÌ.8 eÌ.9 eÌ.10 eÌ.11 eÌ.12 eÌ.13 eÌ.14 eÌ.15 eÌ.16 eÌ.17 eÌ.18 eÌ.19 eÌ.20


--AhhlLboLdkugWU4S--
}}}

--------------------------------------------------------------------------------
2007-03-15 06:37:05 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Sunday, 04 March 2007 at 13:15, Vincent Lefevre wrote:
> I could fix the build problem by adding #undef's in mbyte.h:
> =20
> but combining characters aren't displayed correctly. I get:
>=20
> e\01.1 e\01.2 e\01.3 e\01.4 e\01.5 e\01.6 e\01.7 e\01.8 e\01.9+e\01.10 =
e\01.11 e
> \01.12 e\01.13 e\01.14 e\01.15 e\01.16 e\01.17+e\01.18 e\01.19 e\01.20
>=20
> for the attached mail message.

Works for me here, almost. I get:

=C3=A9.1 =C3=A9.2 =C3=A9.3 =C3=A9.4 =C3=A9.5 =C3=A9.6 =C3=A9.7 =C3=A9.8 =C3=
=A9.9 =C3=A9.10 =C3=A9.11 =C3=A9.12 =C3=A9.13 =C3=A9.14 =C3=A9.15 =C3=A9.=
16 =C3=A9.17 =C3=A9.18 e
=CC=81.19 =C3=A9.20

(they all combine except for 19, due to some wrap bug)
}}}

--------------------------------------------------------------------------------
2007-03-27 18:07:34 UTC 
* Added attachment mailbox-testcase
* Added comment:
mailbox-testcase

--------------------------------------------------------------------------------
2007-04-03 01:22:23 UTC brendan
* Added comment:
Build error fixed. Defects in the included wc functions may need a separate bug, or perhaps this one should be reopened and retitled.

* Updated description:
{{{
When I configure Mutt using --without-wc-funcs (this should be better under Mac OS X as its wcwidth function is buggy), I get the following error when running make:

gcc -DHAVE_CONFIG_H -I. -I..  -I.. -I../intl -I../intl  -Wall -pedantic -g -O2 -MT auth.o -MD -MP -MF .deps/auth.Tpo -c -o auth.o auth.c
In file included from ../protos.h:24,
                 from ../mutt.h:956,
                 from auth.c:27:
../mbyte.h:16: error: parse error before '(' token
../mbyte.h:17: error: parse error before '(' token
../mbyte.h:18: error: parse error before '(' token
make[2]: *** [auth.o] Error 1
make[1]: *** [all-recursive] Error 1
make: *** [all] Error 2
>How-To-Repeat:
>Fix:
Unknown
}}}
* resolution changed to fixed
* status changed to closed
