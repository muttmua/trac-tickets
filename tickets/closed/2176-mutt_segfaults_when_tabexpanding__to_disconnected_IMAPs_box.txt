Ticket:  2176
Status:  closed
Summary: mutt segfaults when tab-expanding '=' to disconnected IMAPs box

Reporter: holecek@fi.muni.cz
Owner:    mutt-dev

Opened:       2006-02-01 22:17:57 UTC
Last Updated: 2006-04-04 03:46:40 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Mutt segfaults during folder name completion under certain conditions: typed string should expand to a list of IMAPs folders but the IMAPs connection is not established yet.
>How-To-Repeat:
I have in my .muttrc these lines:
set mbox_type   = mbox
set folder      = imaps://host.name//path/to/maildir"
set spoolfile   = "/var/mail/user"
set mbox        = "/var/mail/user"

I start mutt. It opens /var/mail/user (usually empty). I type 'm' to change mailbox. I type '=' to access an IMAPs folder. I press tabulator to get list of my IMAPs folders. Mutt segfaults as IMAPs connection is not established yet (I guess).

Alternatively, you can type just 'imaps://' and then the <TAB> key to start completion. The mutt will segfault too.
>Fix:
I have tracked the segfault down to case-block
line 443: case OP_EDITOR_COMPLETE:
in function _mutt_enter_string in file enter.c.

I got lost there, sorry.
}}}

--------------------------------------------------------------------------------
2006-04-04 21:46:40 UTC brendan
* Added comment:
{{{
fixed; duplicate of 2188
}}}

* resolution changed to fixed
* status changed to closed
