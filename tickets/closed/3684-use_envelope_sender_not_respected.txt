Ticket:  3684
Status:  closed
Summary: use_envelope_sender not respected

Reporter: michael.orlitzky
Owner:    mutt-dev

Opened:       2014-05-01 23:40:42 UTC
Last Updated: 2014-05-02 13:26:16 UTC

Priority:  major
Component: mutt
Keywords:  sendmail, use_envelope_from

--------------------------------------------------------------------------------
Description:
Background: I'd like to switch to a real MTA on my workstations and laptop. I've chosen Postfix for this, and it allows me to choose an SMTP server, username, and password all based on the envelope sender of the message. So it's important that the envelope sender be correct; otherwise, the message will go through the wrong server and be rejected (at best) or possibly fail SPF/DKIM somewhere down the line.

Here's what happens if I invoke sendmail directly, using the default value of the '''sendmail''' option:

{{{
$ /usr/sbin/sendmail -oem -oi -f michael@orlitzky.com michael@orlitzky.com
To: michael@orlitzky.com
From: michael@orlitzky.com
Subject: Envelope sender test.

This will have the correct envelope sender, michael@orlitzky.com.
}}}

In my logs, everything looks great:

{{{
May 01 19:15:29 [postfix/pickup] EA186205CC: uid=1000 from=<michael@orlitzky.com>
May 01 19:15:29 [postfix/cleanup] EA186205CC: message-id=<20140501231529.EA186205CC@gantu>
May 01 19:15:29 [postfix/qmgr] EA186205CC: from=<michael@orlitzky.com>, size=352, nrcpt=1 (queue active)
May 01 19:15:30 [postfix/smtp] EA186205CC: to=<michael@orlitzky.com>, relay=mail2.viabit.com[65.246.80.16]:587, delay=29, delays=28/0.08/0.42/0.07, dsn=2.0.0, status=sent (250 2.0.0 Ok: queued as 3gKXWQ3gC9z3N1B)
May 01 19:15:30 [postfix/qmgr] EA186205CC: removed
}}}

The correct envelope sender is used, and so the server, user, and password all get chosen correctly. This is what I'd like to reproduce with Mutt.

In my ''muttrc'', I have '''set use_envelope_from=yes'''. This should infer the envelope sender from the "From:" address and append it to the sendmail command with '''-f'''. I'm running the latest hg version, configured with '''./configure --with-ssl --enable-imap --with-sasl'''. 

When I send a message using Mutt ("From: michael@orlitzky.com"), I get the following in the logs:

{{{
May 01 19:23:09 [postfix/pickup] 96694205CC: uid=1000 from=<mjo>
May 01 19:23:09 [postfix/cleanup] 96694205CC: message-id=<20140501232309.GA29921@gantu>
May 01 19:23:09 [postfix/qmgr] 96694205CC: from=<mjo@gantu.localdomain>, size=440, nrcpt=1 (queue active)
May 01 19:23:10 [postfix/smtp] 96694205CC: to=<michael@orlitzky.com>, relay=mail2.viabit.com[65.246.80.16]:587, delay=0.47, delays=0.08/0.01/0.29/0.08, dsn=5.7.1, status=bounced (host mail2.viabit.com[65.246.80.16] said: 554 5.7.1 <michael@orlitzky.com>: Recipient address rejected: Access denied (in reply to RCPT TO command))
}}}

With no envelope sender, my hostname ''gantu.localdomain'' is appended to my local username, and Postfix doesn't know what to do with that. I see the same behaviour even if I define an '''envelope_from_address'''. That fact led me to investigate, by placing debug (log) statements in ''sendlib.c''. It appears as if the envelope sender will only be appended with '''-f''' if '''option (OPTENVFROM)''' evaluates to '''true''' in '''mutt_invoke_sendmail'''. But nothing I do changes that value! To demonstrate, here's my silly patch:

{{{
diff -r 8f62001989cc sendlib.c
--- a/sendlib.c	Sat Feb 08 10:24:22 2014 -0800
+++ b/sendlib.c	Thu May 01 19:28:26 2014 -0400
@@ -2343,6 +2343,7 @@
   char **args = NULL;
   size_t argslen = 0, argsmax = 0;
   int i;
+  FILE* logfile = NULL;
 
   /* ensure that $sendmail is set to avoid a crash. http://dev.mutt.org/trac/ticket/3548 */
   if (!s)
@@ -2377,6 +2378,10 @@
   if (eightbit && option (OPTUSE8BITMIME))
     args = add_option (args, &argslen, &argsmax, "-B8BITMIME");
 
+  logfile = fopen("muttlog.txt", "a");
+  fprintf(logfile, "optenvfrom: %d\n", (option (OPTENVFROM)));
+  fclose(logfile);
+
   if (option (OPTENVFROM))
   {
     if (EnvFrom)
}}}

Running this with '''use_envelope_from=yes''' and '''use_envelope_from=no''' both produce the same result:

{{{
$ cat muttlog.txt 
optenvfrom: 0
optenvfrom: 0
}}}

I am rather perplexed, but at least that explains why the envelope sender is not set correctly! I will note that '''use_envelope_from''' is defined in the ''same'' muttrc as my IMAP server, so I'm fairly sure it's being read and processed; otherwise, Mutt wouldn't know where to find my inbox.


--------------------------------------------------------------------------------
2014-05-02 10:33:22 UTC barsnick
* Added comment:
{{{
On Thu, May 01, 2014 at 23:40:43 -0000, Mutt wrote:

You can check this by invoking
mutt -D

Got this from mutt's help:
barsnick@sunshine:~ > mutt -h | grep -- -D
  mutt [<options>] -D
  -D            print the value of all variables to stdout

Moritz
}}}

--------------------------------------------------------------------------------
2014-05-02 13:26:16 UTC michael.orlitzky
* Added comment:
Thanks, I actually started out logging the exact sendmail command that was being used (which isn't stored in a variable). Then when I saw no '''-f''' was being passed, I just modified it to log the value of that variable.

After a night of '''grep -r use_envelope_from /''', it turns out the cause of this is what it usually is: I'm an idiot. There's another config file redefining '''use_envelope_from = no''' outside of my muttrc. Removing that makes it work (surprise).

I'll open a feature request to warn about duplicate variable assignments instead, to prevent other idiots from doing the same thing.

* resolution changed to invalid
* status changed to closed
