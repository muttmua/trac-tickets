Ticket:  3966
Status:  closed
Summary: IMAP delete unread message

Reporter: anton
Owner:    brendan

Opened:       2017-09-11 07:12:03 UTC
Last Updated: 2017-09-13 23:20:26 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi,
While invoking delete-message on a unread message and then syncing the
IMAP mailbox the message ends up being copied to the trash but not
deleted from my local cache until I issue a second sync. Running `mutt
-d5` reveals the following communication between the client and server:

{{{
Copying 1 messages to Trash...
4> a0018 UID STORE 90 +FLAGS.SILENT (\Seen)
a0019 UID COPY 90 "Trash"
4< * 6 FETCH (FLAGS (\Seen $X-ME-Annot-2) UID 90)
Handling FETCH
Message UID 90 updated
4< a0016 OK Completed
4< a0017 OK [COPYUID 1503661657 90 184] Completed
IMAP queue drained
}}}

I initially thought something was wrong with the IMAP-server since
according to RFC 3501:

> +FLAGS.SILENT <flag list>
>   Equivalent to +FLAGS, but without returning a new value.

... but it also states:

> Note: Regardless of whether or not the ".SILENT" suffix was used, the
> server SHOULD send an untagged FETCH response if a change to a
> message's flags from an external source is observed.

After all this looks like a documented and valid behavior.

According to my understanding of the source code, the following is
triggering the behavior I'm seeing:

1. The unanticipated FETCH is handled by cmd_parse_fetch(). Since
   `h->changed` is true (due to the deleted flag being set and yet not
   synced) the IMAP context is flagged as IMAP_EXPUNGE_PENDING.

2. imap_sync_mailbox() will not end up deleting the message since
   imap_check_mailbox() doesn't return 0 due to the
   IMAP_EXPUNGE_PENDING flag being set.

3. Issuing a second sync will cause the message to be deleted since the
   local flags are in sync with the server. But this will copy the
   message to the trash again so I will end up with two copies of the
   same message in the trash.

I've been tinkering with different approaches on how to solve this
issue. Here's a couple of insights and ideas:

1. Ignoring the unanticipated FETCH inside cmd_parse_fetch() if
   `compare_flags() == 0` doesn't work since the local flags and server
   flags differs.

2. Setting `h->active = 0` prior issuing the STORE/COPY IMAP-commands
   will cause the unanticipated FETCH to be ignored similar to what's
   done at the end of the imap_sync_mailbox(). With my limited knowledge
   of the mutt source code I cant' deduce the implications of setting
   active at an earlier stage.

3. Adding a flag ignore_fetch (just as an example) to the IMAP_DATA structure
   and setting it 1 prior issuing the STORE/COPY could inform
   cmd_parse_fetch() that any FETCH should be ignored. Not the cleanest
   solution since it adds more state handling.

Any thoughts and ideas on how to solve this would be much appreciated.

{{{
$ mutt -v
Mutt 1.9+10 (0e0a4b7e9dd1) (2017-09-02)
Copyright (C) 1996-2016 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: OpenBSD 6.2 (amd64)
ncurses: ncurses 5.7.20081102 (compiled with 5.7)

Compiler:
Reading specs from /usr/lib/gcc-lib/amd64-unknown-openbsd6.2/4.2.1/specs
Target: amd64-unknown-openbsd6.2
Configured with: OpenBSD/amd64 system compiler
Thread model: posix
gcc version 4.2.1 20070719 

Configure options: '--enable-debug' '--enable-imap' '--enable-sidebar' '--with-ssl' 'CFLAGS=-g'

Compilation CFLAGS: -Wall -pedantic -Wno-long-long -g

Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
-USE_POP  +USE_IMAP  -USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
-HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE  +USE_SIDEBAR  -USE_COMPRESSED  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.
}}}

--------------------------------------------------------------------------------
2017-09-11 18:57:49 UTC kevin8t8
* Added comment:
Thank you for the excellent bug report.  This is reproducible in 1.8.3 and is the result of the "fix" in changeset:323e3d6e5e4c for ticket 3860.  I swear I remember testing that better, but I can easily reproduce the problem.  My apologies.

I'll take a look at a few possible fixes.  At the worst I'll just remove the "/Seen" syncing for 1.9.1 if other fixes are too involved, and try to fix it properly for the next major release.

--------------------------------------------------------------------------------
2017-09-11 19:22:00 UTC anton
* Added comment:
> Thank you for the excellent bug report.  This is reproducible in 1.8.3 and
> is the result of the "fix" in changeset:323e3d6e5e4c for ticket 3860.  I
> swear I remember testing that better, but I can easily reproduce the
> problem.  My apologies.

No worries, my current work-around is to issue a sync of the mailbox
prior deleting. That we the read flag will already be in sync.

> I'll take a look at a few possible fixes.  At the worst I'll just remove
> the "/Seen" syncing for 1.9.1 if other fixes are too involved, and try to
> fix it properly for the next major release.

Great! If you need any help or me trying out diffs just let me know. I'm
willing to look further into this with some guidance.

Another related gotcha: the communication enclosed in my initial report
was from FastMail. I tried to reproduce the same bug using Gmail without
luck (guess it's actually a good thing :-)). Turns out the unanticipated
FETCH is ignored since the format of response is differs:

{{{
# FastMail
4< * 6 FETCH (FLAGS (\Seen $X-ME-Annot-2) UID 80)
Handling FETCH
Message UID 80 updated

# Gmail
4< * 2 FETCH (UID 85046 FLAGS (\Seen))
Handling FETCH
Message UID 85046 updated
Only handle FLAGS updates
}}}

Since `FETCH (` is not immediately followed by `FLAGS` the response is
ignored. My knowledge of the IMAP-protocol is limited but if the server
is free to arrange the items in the response I guess a check like one
below would be more appropriate:

{{{
diff -r 0e0a4b7e9dd1 imap/command.c
--- a/imap/command.c	Tue Sep 05 17:33:45 2017 +0200
+++ b/imap/command.c	Mon Sep 11 21:18:43 2017 +0200
@@ -680,7 +680,7 @@
   }
   s++;
 
-  if (ascii_strncasecmp ("FLAGS", s, 5) != 0)
+  if (strstr (s, "FLAGS") == NULL)
   {
     dprint (2, (debugfile, "Only handle FLAGS updates\n"));
     return;
}}}

--------------------------------------------------------------------------------
2017-09-13 03:07:44 UTC kevin8t8
* Added comment:
I'm attaching a two patches that I hope will solve the syncing issue.  The patches are very lightly tested - I still need to review and clean them up - but I would appreciate if you wouldn't mind testing them out.

--------------------------------------------------------------------------------
2017-09-13 03:08:18 UTC kevin8t8
* Added attachment ticket-3966-part-01.patch

--------------------------------------------------------------------------------
2017-09-13 03:08:31 UTC kevin8t8
* Added attachment ticket-3966-part-02.patch

--------------------------------------------------------------------------------
2017-09-13 07:00:51 UTC anton
* Added comment:
> I'm attaching a two patches that I hope will solve the syncing issue.  The
> patches are very lightly tested - I still need to review and clean them up
> - but I would appreciate if you wouldn't mind testing them out.

Deleting an unread message works after applying your patches.

--------------------------------------------------------------------------------
2017-09-13 22:57:08 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"19597bb7baa6e1e4ce4006a994df9a0e56dee8a7" 7152:19597bb7baa6]:
{{{
#!CommitTicketReference repository="" revision="19597bb7baa6e1e4ce4006a994df9a0e56dee8a7"
Remove \Seen flag setting for imap trash.  (see #3966) (see #3860)

Commit 323e3d6e5e4c has a side effect where spurious FETCH flag
updates after setting the \Seen flag can cause a sync to abort.

Remove manually setting \Seen for all trashed message before copying.

The next commit will change the imap trash function to use the same
code as the imap copy/save function for syncing the message before
server-side copying.
}}}

--------------------------------------------------------------------------------
2017-09-13 22:57:11 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"f90712538cd9198a1fecbca362eecefe5541e4a5" 7153:f90712538cd9]:
{{{
#!CommitTicketReference repository="" revision="f90712538cd9198a1fecbca362eecefe5541e4a5"
Change imap copy/save and trash to sync flags, excluding deleted. (closes #3966) (closes #3860)

imap_copy_messages() uses a helper to sync the flags before performing
a server-side copy.  However, it had a bug that the "deleted" flag on
a local message, if set, will be propagated to the copy too.

Change the copy sync helper to ignore the deleted flag.  Then, change
the imap trash function to use the same helper.

Thanks to Anton Lindqvist for his excellent bug report, suggested
fixes, and help testing.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2017-09-13 23:20:26 UTC kevin8t8
* Added comment:
Thanks again for all your help.  I'm opening a different ticket for the Gmail FETCH flags issue: #3969.
