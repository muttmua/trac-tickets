Ticket:  2086
Status:  closed
Summary: MIME-Version: header case

Reporter: Alain Bench <veronatif@free.fr>
Owner:    mutt-dev

Opened:       2005-09-24 12:55:16 UTC
Last Updated: 2005-09-24 20:37:04 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
    RFC 2045 says MIME-Version field name is case
insensitive, but it should preferably be generated with
full uppercase MIME acronym. Mutt generates Mime-Version.
>How-To-Repeat:
>Fix:
    patch-1.5.11.ab.mime_version_case.1 corrects this, together with 2 miscased Message-Id left.
}}}

--------------------------------------------------------------------------------
2005-09-25 14:37:04 UTC brendan
* Added comment:
{{{
Applied, thanks.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:03 UTC 
* Added attachment patch-1.5.11.ab.mime_version_case.1.gz
* Added comment:
patch-1.5.11.ab.mime_version_case.1.gz
