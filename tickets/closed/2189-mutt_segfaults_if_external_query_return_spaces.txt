Ticket:  2189
Status:  closed
Summary: mutt segfaults if external query return spaces

Reporter: fraff@free.fr
Owner:    mutt-dev

Opened:       2006-03-03 01:41:40 UTC
Last Updated: 2006-03-09 05:03:32 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
mutt segfault when selecting mail if query_command is set to:

cat << EOF
#!/bin/sh

echo "first line"
echo -e " \t \tsomething not important"
EOF
>How-To-Repeat:
set query_command="provided-script.sh '%s'"
m<Ctrl-T>
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-03-03 01:48:08 UTC owner@bugs.debian.org (Debian Bug Tracking System)
* Added comment:
{{{
Thank you for the additional information you have supplied regarding
this problem report.  It has been forwarded to the package maintainer(s)
and to other interested parties to accompany the original report.

Your message has been sent to the package maintainer(s):
 Adeodato Simó <dato@net.com.org.es>

If you wish to continue to submit further information on your problem,
please send it to 351258@bugs.debian.org, as before.

Please do not reply to the address at the top of this message,
unless you wish to report a problem with the Bug-tracking system.

Debian bug tracking system administrator
(administrator, Debian Bugs database)
}}}

--------------------------------------------------------------------------------
2006-03-04 03:56:42 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2006-03-03 02:41:41 +0100, fraff@free.fr wrote:

> mutt segfault when selecting mail if query_command is set to:

I'm committing this to CVS:

diff -u -r3.10 query.c
--- query.c	17 Sep 2005 20:46:11 -0000	3.10
+++ query.c	3 Mar 2006 09:55:05 -0000
@@ -64,7 +64,8 @@
 {
   static ADDRESS *tmp;
   
-  tmp = rfc822_cpy_adr (r->addr);
+  if (!(tmp = rfc822_cpy_adr (r->addr)))
+    return NULL;
   
   if(!tmp->next && !tmp->personal)
     tmp->personal = safe_strdup (r->name);



-- 
Thomas Roessler			      <roessler@does-not-exist.org>
}}}

--------------------------------------------------------------------------------
2006-03-09 23:03:32 UTC brendan
* Added comment:
{{{
Thank you for the additional information you have supplied regarding
 On 2006-03-03 02:41:41 +0100, fraff@free.fr wrote:
Fixed in CVS
}}}

* resolution changed to fixed
* status changed to closed
