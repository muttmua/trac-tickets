Ticket:  3819
Status:  closed
Summary: imap_mailbox_rename uses an uninitialized string

Reporter: tamo
Owner:    brendan

Opened:       2016-03-20 09:31:05 UTC
Last Updated: 2016-03-23 22:33:20 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
Renaming an imap mailbox sometimes surprises you:
http://permalink.gmane.org/gmane.os.openbsd.ports/28230

The buffer "newname" should be initialized before being used here
https://dev.mutt.org/hg/mutt/file/default/imap/browse.c#l324

Probably "char newname[SHORT_STRING]={'\0'};" is sufficient,
but OpenBSD has another patch to use the old name.

current: "Rename mailbox foo to: ??##$GBG" (if the memory is not zeroed)
nullstr: "Rename mailbox foo to: "
openbsd: "Rename mailbox foo to: foo"

I prefer the last one
because in most cases I'll choose a similar name,
e.g. mutt-devel to mutt-dev.

http://cvsweb.openbsd.org/cgi-bin/cvsweb/ports/mail/mutt/patches/patch-imap_browse_c

{{{
$OpenBSD: patch-imap_browse_c,v 1.2 2015/09/03 20:41:57 sthen Exp $

Use the old mailbox name, rather than uninitialized memory, as a suggestion
for new mailbox name when renaming an IMAP folder.

--- imap/browse.c.orig	Sun Aug 30 18:06:38 2015
+++ imap/browse.c	Thu Sep  3 21:05:49 2015
@@ -322,6 +322,7 @@ int imap_mailbox_rename(const char* mailbox)
   }
 
   snprintf(buf, sizeof (buf), _("Rename mailbox %s to: "), mx.mbox);
+  strfcpy (newname, NONULL (mx.mbox), sizeof (newname));
   
  if (mutt_get_field (buf, newname, sizeof (newname), M_FILE) < 0)
     goto fail;
}}}

--------------------------------------------------------------------------------
2016-03-23 22:33:20 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"27cde0a2fe32f9cb58531f098b81fb9cc1b2ddb8"]:
{{{
#!CommitTicketReference repository="" revision="27cde0a2fe32f9cb58531f098b81fb9cc1b2ddb8"
Fix uninitialized prompt buffer in imap_mailbox_rename(). (closes #3819)

This is a patch from OpenBSD to initialize the newname buffer in
imap_mailbox_rename().  Previously it would generate random garbage in
the prompt.

This version of the patch initializes the prompt to the old mailbox name.

Thanks to TAKAHASHI Tamotsu for finding this patch.
}}}

* resolution changed to fixed
* status changed to closed
