Ticket:  3017
Status:  closed
Summary: change-folder: tab-completion results in "Out of memory!", return value = 1 if many files for the menu

Reporter: GregorZattler
Owner:    mutt-dev

Opened:       2008-01-17 13:35:57 UTC
Last Updated: 2008-01-22 09:01:03 UTC

Priority:  minor
Component: mutt
Keywords:  file browser

--------------------------------------------------------------------------------
Description:
With 1975 subdirectories and 50721 ordinary files in ~/Mail tab-completion on empty input line in <change-folder> results in "Out of memory!", return value 1.

How to reproduce: Have thousands of files in ~/Mail and in mutt index or pager: 

{{{
hit "c" (<change-folder>), "^a" (<bol>), "^k" (<kill-eol>), "<tab>", voila: "Out of memory!", return value is 1.
}}}

This is not a configuration problem nor an alien patch problem since this happens with  vanilla mutt-6e6e6c4bee59 started like this:

/usr/local/stow/bin/mutt -nF /dev/null



The problem occured first with recent mutt from debian unstable (1.5.17-2) which includes a patch which enables a debug-option "-d", last 3 lines of debug output are:

   Updating progress: 0
   mutt_index_menu[633]: Got op 99
   Out of memory!


Workaround: Delete ~50000 files which did not belong there.  But I think mutt should handle a such a situation with thousands of filenames to complete.


Thank you for this great MUA,
Gregor Zattler

--------------------------------------------------------------------------------
2008-01-17 16:05:42 UTC vinc17
* Added comment:
I can reproduce this bug when I try to complete on the "cur" subdirectory of my archive mailbox. This wasn't an empty tab-completion; the problem comes from the fact that there are many files to be taken into account for the menu. But I have more than 3 GB free memory!

--------------------------------------------------------------------------------
2008-01-17 16:10:35 UTC vinc17
* Added comment:
The summary should now be more accurate. Note that in my case, when I first type [Tab], a "1" is displayed as all the files start with a "1". The failure occurs only if Mutt needs to display a menu.

* summary changed to change-folder: tab-completion results in "Out of memory!", return value = 1 if many files for the menu

--------------------------------------------------------------------------------
2008-01-17 16:26:39 UTC vinc17
* Added comment:
This may be due to an integer overflow. In browser.h:
{{{
struct browser_state
{
  struct folder_file *entry;
  short entrylen; /* number of real entries */
  short entrymax;  /* max entry */
[...]
}}}
These two short's should be int's (or long's). In browser.c:
{{{
    safe_realloc (&state->entry,
                  sizeof (struct folder_file) * (state->entrymax += 256));
}}}
Possible integer overflow on state->entrymax.

--------------------------------------------------------------------------------
2008-01-17 16:32:50 UTC pdmef
* Added comment:
(In [039d939d8c3e]) Use 'unsigned int' for file browser state to browse huge dirs (closes #3017).

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2008-01-17 16:38:35 UTC vinc17
* Added comment:
I haven't tried yet with the fix, but I've recompiled Mutt with -ftrapv in CFLAGS, and I get the "Out of memory" error before the integer overflow,  perhaps of all these realloc's? (Instead of adding 256, I think that multiplying by some factor would be better.)

--------------------------------------------------------------------------------
2008-01-17 17:26:28 UTC pdmef
* Added comment:

> I haven't tried yet with the fix, but I've recompiled Mutt with -ftrapv in CFLAGS, and I get the "Out of memory" error before the integer overflow,  perhaps of all these realloc's? (Instead of adding 256, I think that multiplying by some factor would be better.)

Hmm, that doesn't work here.

Increasing by a (constant?) factor means the table grows exponentially, e.g. n^2 for
a factor of 2? That would waste quite a lot of space for browsing larger dirs, e.g. when
you have a table size of 16k, need just a few more files you'd double it to 32k entries.
In such a case, such a table is twice as large as needed.

Though I don't have any hard numbers, I doubt just a couple of realloc/memset make
the situation much better for the average case (I'm not sure, but browsing a dir from
within mutt containing several 10k files is not what the regular mutt user does, I guess).

I tested such a scheme for maildir speedup using a table incremented by a factor 1.5 instead
of a single-linked list, it made nearly no difference speed-wise.

Rocco

* keywords changed to file browser

--------------------------------------------------------------------------------
2008-01-18 13:33:27 UTC vinc17
* Added comment:
Perhaps you can waste some memory with a geometric progression compared to an arithmetic progression (but this is not obvious, as the fragmentation may be lower), but the time complexity should be better. I'd say O(n) instead of O(n^2^). See [http://en.wikipedia.org/wiki/Dynamic_array]. But all this depends on the malloc routine and how blocks are allocated.

--------------------------------------------------------------------------------
2008-01-18 16:03:31 UTC Rocco Rutte
* Added comment:
{{{
Hi,

* Mutt wrote:

> Perhaps you can waste some memory with a geometric progression compared to
> an arithmetic progression (but this is not obvious, as the fragmentation
> may be lower), but the time complexity should be better. I'd say O(n)
> instead of O(n^2^). See [http://en.wikipedia.org/wiki/Dynamic_array]. But
> all this depends on the malloc routine and how blocks are allocated.

Even 2000 files in a directory you browse with mutt are quite a lot, and 
that would be 1 calloc() and 7 realloc() calls. I think there're areas 
in mutt where memory management needs to be improved first than this 
one... what gains do you expect from such an optimization?

Rocco
}}}

--------------------------------------------------------------------------------
2008-01-22 09:01:03 UTC vinc17
* Added comment:
Replying to [comment:8 Rocco Rutte]:
> Even 2000 files in a directory you browse with mutt are quite a lot, [...]

2000 is not a lot. I have 66000 files in a directory. But anyway, there's no difference between both methods: immediate in both cases under Linux, about 17 seconds in both cases under Mac OS X. I assume that most of the time is taken at the file system level. Also, it is not clear that memory is moved after the realloc here. More tests would need to be done...
