Ticket:  3753
Status:  closed
Summary: dev.mutt.org: redirect http to https

Reporter: ilf
Owner:    mutt-dev

Opened:       2015-04-30 18:52:45 UTC
Last Updated: 2017-04-29 15:57:22 UTC

Priority:  major
Component: doc
Keywords:  

--------------------------------------------------------------------------------
Description:
dev.mutt.org is available as both http and https.

Having both available can reveal login information and session cookies from https over an incidential http connection.

Also, [https://citizenlab.org/2014/08/cat-video-and-the-death-of-clear-text/ plaintext is actively used as an attack vector]:

> Thus far we have provided two examples of commercial tools that have widely proliferated and that enable purchasers (for a fee) to exploit clear-text traffic in some of the most popular sites on the web.

> In order for network injection appliances to function, they rely on the fact that popular websites will not encrypt all of their traffic. In order to mitigate these types of attacks, we suggest that providers serve all content over TLS, and provide end-to-end encryption wherever possible. The use of HSTS and certificate pinning is also strongly recommended.

So let's just default to HTTPS and let HTTP redirect to it.

According to the HTTP header, the webserver is "Apache/2.2.22 (Ubuntu)". THe !BetterCrypto project recommends the following config for that:

{{{
<VirtualHost *:80>
 Redirect permanent / https://SERVER_NAME/
</VirtualHost>
}}}

https://git.bettercrypto.org/ach-master.git/blob/HEAD:/src/configuration/Webservers/Apache/hsts-vhost

(While on it, www.mutt.org could use some HTTPS, too :)

--------------------------------------------------------------------------------
2015-05-02 17:58:43 UTC ilf
* Added comment:
Mozilla just announced their intent to phase out non-secure HTTP: https://blog.mozilla.org/security/2015/04/30/deprecating-non-secure-http/

--------------------------------------------------------------------------------
2015-05-09 16:49:26 UTC vinc17
* Added comment:
dev.mutt.org would need to get a verifiable certificate first.

--------------------------------------------------------------------------------
2015-05-10 15:53:42 UTC ilf
* Added comment:
One can get free (beer) certs at http://www.startssl.com/
And hopefully free (beer and speech) ones soon at https://letsencrypt.org/

--------------------------------------------------------------------------------
2017-04-29 15:57:22 UTC ilf
* resolution changed to fixed
* status changed to closed
