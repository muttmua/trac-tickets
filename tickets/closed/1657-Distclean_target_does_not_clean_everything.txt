Ticket:  1657
Status:  closed
Summary: Distclean target does not clean everything

Reporter: sam@rfc1149.net
Owner:    mutt-dev

Opened:       2003-10-04 09:22:15 UTC
Last Updated: 2005-10-04 05:37:28 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4i
Severity: normal

-- Please type your report below this line

Running make distclean does not let the tree in a clean state. The
patch at the end of this report fixes it (at least partially).

-- System Information
N/A

-- Build environment information
N/A

-- Mutt Version Information
Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: FreeBSD 4.9-PRERELEASE (i386) [using ncurses 5.1]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
-HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/local/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

-- Patch

Apply with -p1 within the source tree.
Patch name is patch-1.5.4.st.cleantarget.1

--- orig/Makefile.am
+++ mod/Makefile.am
@@ -90,7 +90,7 @@
 	cp $(srcdir)/dotlock.c mutt_dotlock.c
 
 CLEANFILES = mutt_dotlock.c stamp-doc-rc makedoc \
-	keymap_alldefs.h keymap_defs.h patchlist.c
+	keymap_alldefs.h keymap_defs.h patchlist.c flea
 
 ACLOCAL_AMFLAGS = -I m4
 


--- orig/PATCHES
+++ mod/PATCHES
@@ -0,0 +1 @@
+patch-1.5.4.st.cleantarget.1


--- orig/configure.in
+++ mod/configure.in
@@ -507,6 +507,7 @@
                 need_imap="yes"
                 need_socket="yes"
                 need_md5="yes"
+                imap_Makefile="imap/Makefile"
         fi
 ])
 AM_CONDITIONAL(BUILD_IMAP, test x$need_imap = xyes)
@@ -975,6 +976,6 @@
 AC_OUTPUT(Makefile intl/Makefile m4/Makefile
         po/Makefile.in doc/Makefile contrib/Makefile
         muttbug.sh
-        imap/Makefile
+        $imap_Makefile
         Muttrc.head
         doc/instdoc.sh)


--- orig/doc/Makefile.in
+++ mod/doc/Makefile.in
@@ -114,7 +114,8 @@
 	sgml2latex --output=ps manual || true
 
 clean: 
-	rm -f *~ *.html *.orig *.rej  stamp-doc-sgml stamp-doc-man *.ps
+	rm -f *~ *.html *.orig *.rej  stamp-doc-sgml stamp-doc-man *.ps \
+		instdoc.sh
 
 clean-real:
 	rm -f manual.txt





>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-10-04 09:22:15 UTC sam@rfc1149.net
* Added comment:
{{{
Package: mutt
Version: 1.5.4i
Severity: normal

-- Please type your report below this line

Running make distclean does not let the tree in a clean state. The
patch at the end of this report fixes it (at least partially).

-- System Information
N/A

-- Build environment information
N/A

-- Mutt Version Information
Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: FreeBSD 4.9-PRERELEASE (i386) [using ncurses 5.1]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
-HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/local/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

-- Patch

Apply with -p1 within the source tree.
Patch name is patch-1.5.4.st.cleantarget.1

--- orig/Makefile.am
+++ mod/Makefile.am
@@ -90,7 +90,7 @@
	cp $(srcdir)/dotlock.c mutt_dotlock.c

CLEANFILES = mutt_dotlock.c stamp-doc-rc makedoc \
-	keymap_alldefs.h keymap_defs.h patchlist.c
+	keymap_alldefs.h keymap_defs.h patchlist.c flea

ACLOCAL_AMFLAGS = -I m4



--- orig/PATCHES
+++ mod/PATCHES
@@ -0,0 +1 @@
+patch-1.5.4.st.cleantarget.1


--- orig/configure.in
+++ mod/configure.in
@@ -507,6 +507,7 @@
                need_imap="yes"
                need_socket="yes"
                need_md5="yes"
+                imap_Makefile="imap/Makefile"
        fi
])
AM_CONDITIONAL(BUILD_IMAP, test x$need_imap = xyes)
@@ -975,6 +976,6 @@
AC_OUTPUT(Makefile intl/Makefile m4/Makefile
        po/Makefile.in doc/Makefile contrib/Makefile
        muttbug.sh
-        imap/Makefile
+        $imap_Makefile
        Muttrc.head
        doc/instdoc.sh)


--- orig/doc/Makefile.in
+++ mod/doc/Makefile.in
@@ -114,7 +114,8 @@
	sgml2latex --output=ps manual || true

clean: 
-	rm -f *~ *.html *.orig *.rej  stamp-doc-sgml stamp-doc-man *.ps
+	rm -f *~ *.html *.orig *.rej  stamp-doc-sgml stamp-doc-man *.ps \
+		instdoc.sh

clean-real:
	rm -f manual.txt
}}}

--------------------------------------------------------------------------------
2005-09-05 13:14:39 UTC brendan
* Added comment:
{{{
Does this still apply to 1.5.10? Seems to work here.
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-10-04 23:37:28 UTC brendan
* Added comment:
{{{
Believed fixed; no feedback.
}}}

* resolution changed to fixed
* status changed to closed
