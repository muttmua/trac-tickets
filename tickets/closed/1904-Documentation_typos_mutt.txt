Ticket:  1904
Status:  closed
Summary: Documentation typos (mutt)

Reporter: "Brent J. Nordquist" <brent@nordist.net>
Owner:    mutt-dev

Opened:       2004-06-20 05:54:34 UTC
Last Updated: 2005-07-26 07:14:59 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.2.1

Spotted two problems in the HTML manual for mutt 1.4.2.1 (available on
the mutt.org web site):

(1)  Typo, the word "veritical" should be "vertical" (section 4.2)

(2)  After "separated by a hyphen" a literal element "&hyphen;" is
appearing instead of an actual hyphen character.  Presumably that
means the element "&hyphen;" isn't legal -- should be "&dash;" or
something? (section 4.2)

-- 
Brent J. Nordquist <brent@nordist.net> N0BJN
Other contact information: http://www.nordist.net/contact.html


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-07-27 01:14:59 UTC brendan
* Added comment:
{{{
Fixed in CVS, thanks.
}}}

* resolution changed to fixed
* status changed to closed
