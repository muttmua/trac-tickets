Ticket:  3720
Status:  closed
Summary: Integer Overflow

Reporter: MegaManSec
Owner:    mutt-dev

Opened:       2014-12-22 10:23:52 UTC
Last Updated: 2014-12-22 22:56:19 UTC

Priority:  major
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi,

In pgppacket.c, in the pgp_read_packet function, an integer overflow may occur with the variable 'material'.

Add operation overflows on operands material and b + 192. Example values for operands: b + 192 = 192, material = 18446744073709551424.
127        material += b + 192;



Thanks

--------------------------------------------------------------------------------
2014-12-22 22:32:46 UTC kevin8t8
* Added comment:
Thank you for the bug report.  I currently can't see the overflow at line 127.  Here's the snippet surrounding it:

{{{
119      else if (192 <= b && b <= 223)
120      {
121        material = (b - 192) * 256;
122        if (fread (&b, 1, 1, fp) < 1)
123        {
124          perror ("fread");
125	     goto bail;
126        }
127        material += b + 192;
128        partial = 0;
129        /* material -= 2; */
130     }
}}}

It looks like the value for material is constrained by line 121, so it should be between 0 and 7936.

If I've misunderstood or missed something (which is quite possible, as I'm not very familiar with this file), please reply and let us know with more details.

--------------------------------------------------------------------------------
2014-12-22 22:56:19 UTC MegaManSec
* Added comment:
Yes, my bad, I missed that.

* resolution changed to invalid
* status changed to closed
