Ticket:  2922
Status:  closed
Summary: Couldn't lock /sent

Reporter: agostinho
Owner:    mutt-dev

Opened:       2007-06-25 16:19:33 UTC
Last Updated: 2008-05-13 05:24:13 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When I try send a messsage with mutt the message below is displayed. My SO is HP-UX 11.11 on PA-Risc. I find this file (sent) with size 0 (zero).

Couldn't lock /sent




--------------------------------------------------------------------------------
2007-07-16 17:48:58 UTC brendan
* Added comment:
what is $record set to? And where do you find the file? It sounds like you're trying to FCC to a mailbox you don't have write permission for.

--------------------------------------------------------------------------------
2008-05-13 05:24:13 UTC brendan
* Added comment:
No followup--sounds like pilot error.

* resolution changed to worksforme
* status changed to closed
