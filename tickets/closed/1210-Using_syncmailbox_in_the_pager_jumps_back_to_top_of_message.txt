Ticket:  1210
Status:  closed
Summary: Using <sync-mailbox> in the pager jumps back to top of message

Reporter: <dkcombs@panix.com>
Owner:    mutt-dev

Opened:       2002-05-27 17:03:01 UTC
Last Updated: 2005-09-06 06:36:30 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.28i
Severity: normal

-- Please type your report below this line




-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Using builtin specs.
gcc version egcs-2.91.66 19990314 (egcs-1.1.2 release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.28i (2002-03-13)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: NetBSD 1.5.2 (i386) [using ncurses 5.2]
Compile options:
DOMAIN="panix.com"
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
-USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  -HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/pkg/mutt-1.3.28/share/mutt"
SYSCONFDIR="/pkg/mutt-1.3.28/libdata/mutt-1.3.28"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.







Here is a short of emails about this problem,
the final one was someone advising me to
send this stuff to you guys.

The emails:




From dkcombs@panix.com Mon May 27 05:46:04 EDT 2002
Article: 13866 of comp.mail.mutt
Path: reader1.panix.com!panix!panix1.panix.com!not-for-mail
From: dkcombs@panix.com (David Combs)
Newsgroups: comp.mail.mutt
Subject: "$" cmd while reading article goes back to start.  Why?
Date: 22 May 2002 22:43:47 -0400
Organization: PANIX -- Public Access Networks Corp.
...

I'm in mutt, looking through that
list of articles, marking "delete"
some.  So I've got, say, 10 of them
marked "D".

Now, I *could* give a "$", to actually
delete them, and update my mail-file (HUGE!),
and when that's finished, I can continue
down the list of emails.

But, *while* the deleting was happening,
I could do nothing useful (except gaze
at the list of emails), but because
I have *so many* emails, it takes maybe
fifteen seconds (these days, that's important :-).

So, what I do is click on a message that
I actually want to read, and while reading
the text, I hit "$" then.

That way the fifteen seconds happens, but meanwhile
I am doing something, uh, "worthwhile" -- reading
through a screenfull of that article.

(Well, maybe *not* so worthwhile!)

Works fine.

HOWEVER -- maybe the actual text of the
article doesn't start until page 2 --
or as with some newspaper articles, page 5 or 10
(the earlier pages being this huge list of links).

So I'm halfway down that page 10 when I
hit the $ -- OOPS -- as an *unwelcome*
side-effect of the $, it flips the screen
back up to the top of the article.

So I've got to page down to page 10 again,
to continue reading where it rudely interrupted
my reading.

No big deal -- but WHY does this happen?

Is there some reason it MUST happen?

Is there a way to turn that behavior OFF?

Or will you add it to your to-do list, for fixing,
perhaps.

Thanks!

David




From lool@via.ecp.fr Mon May 27 05:51:03 EDT 2002
Article: 13868 of comp.mail.mutt
From: lool@via.ecp.fr (=?iso-8859-15?Q?Lo=EFc?= Minier)
Newsgroups: comp.mail.mutt
Subject: Re: "$" cmd while reading article goes back to start.  Why?
Date: 23 May 2002 10:35:26 GMT
Organization: Ecole Centrale Paris : Grde Voie des Vignes, 92295 Chatenay-Malabry Cdx
...

* David Combs <dkcombs@panix.com>,
  22 May 2002 22:43:47 -0400:
> So I'm halfway down that page 10 when I
> hit the $ -- OOPS -- as an *unwelcome*
> side-effect of the $, it flips the screen
> back up to the top of the article.
> No big deal -- but WHY does this happen?

  $ is bound to "goto top" in pager by default.

> Is there some reason it MUST happen?
> Is there a way to turn that behavior OFF?

  Bind $ to whatever you want it to do.

-- 
Loïc Minier <lool@via.ecp.fr>


From lool@via.ecp.fr Mon May 27 05:51:26 EDT 2002
Article: 13871 of comp.mail.mutt
From: lool@via.ecp.fr (=?iso-8859-15?Q?Lo=EFc?= Minier)
Newsgroups: comp.mail.mutt
Subject: Re: "$" cmd while reading article goes back to start.  Why?
Date: 23 May 2002 11:46:28 GMT
Organization: Ecole Centrale Paris : Grde Voie des Vignes, 92295 Chatenay-Malabry Cdx
...

* Holger Lillqvist <lillqvis@venus.ling.helsinki.fi>,
  23 May 2002 11:22:05 GMT:
> Really? This is what the pager help says:
> $             sync-mailbox              save changes to mailbox

  This is mine :
$           bottom                 jump to the bottom of the message

  And in index :
$           sync-mailbox           save changes to mailbox


  Maybe you already rebound it, then it may be a bug, or you're using
another Mutt version. I'm using Mutt 1.2.5i (2000-07-28) (Debian
potato).

  If you redefined $ in pager, how did you achieve this ? May it
conflict with a "generic" binding or with a "macro" ?

-- 
Loïc Minier <lool@via.ecp.fr>


From lillqvis@venus.ling.helsinki.fi Mon May 27 05:51:38 EDT 2002
Article: 13876 of comp.mail.mutt
From: Holger Lillqvist <lillqvis@venus.ling.helsinki.fi>
Newsgroups: comp.mail.mutt
Subject: Re: "$" cmd while reading article goes back to start.  Why?
Date: 23 May 2002 17:47:32 GMT
...

Loïc Minier wrote:
> * Holger Lillqvist <lillqvis@venus.ling.helsinki.fi>,
>   23 May 2002 11:22:05 GMT:
>> Really? This is what the pager help says:
>> $             sync-mailbox              save changes to mailbox
> 
>   This is mine :
> $           bottom                 jump to the bottom of the message
> 
>   And in index :
> $           sync-mailbox           save changes to mailbox
> 
> 
>   Maybe you already rebound it, then it may be a bug, or you're using
> another Mutt version. I'm using Mutt 1.2.5i (2000-07-28) (Debian
> potato).

No, I haven't rebound it. I'm using Mutt 1.3.28i. I also checked
1.2.5 - this really has been changed.

Holger


From wv@xs2mail.com Mon May 27 05:51:48 EDT 2002
Article: 13869 of comp.mail.mutt
From: "W. Verheijen" <(\>)w(nospam)v@xs2mail.com>
Newsgroups: comp.mail.mutt
Subject: Re: "$" cmd while reading article goes back to start.  Why?
Date: Thu, 23 May 2002 10:46:00 +0000 (UTC)
Organization: XS2Mail
...

In article <achl13$5hc$1@panix1.panix.com>, David Combs wrote:
> So I'm halfway down that page 10 when I
> hit the $ -- OOPS -- as an *unwelcome*
> side-effect of the $, it flips the screen
> back up to the top of the article.
[...]
> Or will you add it to your to-do list, for fixing,
> perhaps.

I think you'd best submit this as a bug (priority wishlist) with the
"flea" utility.

-- 
Wouter Verheijen,                          <W.J.A.Verheijen@xs2mail.com>
  XS2Mail: Feature-rich webbased access to any POP3 and IMAP mailbox.
http://www.xs2mail.com/









Thanks much!

David




>How-To-Repeat:
	
Eg: am 5-screenfulls down, reading email; I hit "$" (flush deleteds) ==> screen-goes-to-top-of-that-email!  Very bad -- I must page way back down & find where I had been reading, to resume.
>Fix:
}}}

--------------------------------------------------------------------------------
2002-05-23 09:43:47 UTC dkcombs@panix.com (David Combs)
* Added comment:
{{{
...

I'm in mutt, looking through that
list of articles, marking "delete"
some.  So I've got, say, 10 of them
marked "D".

Now, I *could* give a "$", to actually
delete them, and update my mail-file (HUGE!),
and when that's finished, I can continue
down the list of emails.

But, *while* the deleting was happening,
I could do nothing useful (except gaze
at the list of emails), but because
I have *so many* emails, it takes maybe
fifteen seconds (these days, that's important :-).

So, what I do is click on a message that
I actually want to read, and while reading
the text, I hit "$" then.

That way the fifteen seconds happens, but meanwhile
I am doing something, uh, "worthwhile" -- reading
through a screenfull of that article.

(Well, maybe *not* so worthwhile!)

Works fine.

HOWEVER -- maybe the actual text of the
article doesn't start until page 2 --
or as with some newspaper articles, page 5 or 10
(the earlier pages being this huge list of links).

So I'm halfway down that page 10 when I
hit the $ -- OOPS -- as an *unwelcome*
side-effect of the $, it flips the screen
back up to the top of the article.

So I've got to page down to page 10 again,
to continue reading where it rudely interrupted
my reading.

No big deal -- but WHY does this happen?

Is there some reason it MUST happen?

Is there a way to turn that behavior OFF?

Or will you add it to your to-do list, for fixing,
perhaps.

Thanks!

David
}}}

--------------------------------------------------------------------------------
2002-05-24 01:46:00 UTC "W. Verheijen" <(\>)w(nospam)v@xs2mail.com>
* Added comment:
{{{
...

In article <achl13$5hc$1@panix1.panix.com>, David Combs wrote:
> So I'm halfway down that page 10 when I
> hit the $ -- OOPS -- as an *unwelcome*
> side-effect of the $, it flips the screen
> back up to the top of the article.
[...]
> Or will you add it to your to-do list, for fixing,
> perhaps.

I think you'd best submit this as a bug (priority wishlist) with the
"flea" utility.

-- 
Wouter Verheijen,                          <W.J.A.Verheijen@xs2mail.com>
 XS2Mail: Feature-rich webbased access to any POP3 and IMAP mailbox.
http://www.xs2mail.com/









Thanks much!

David
}}}

--------------------------------------------------------------------------------
2002-05-24 02:35:26 UTC lool@via.ecp.fr ( Loïc Minier)
* Added comment:
{{{
...

* David Combs <dkcombs@panix.com>,
 22 May 2002 22:43:47 -0400:
> So I'm halfway down that page 10 when I
> hit the $ -- OOPS -- as an *unwelcome*
> side-effect of the $, it flips the screen
> back up to the top of the article.
> No big deal -- but WHY does this happen?

 $ is bound to "goto top" in pager by default.

> Is there some reason it MUST happen?
> Is there a way to turn that behavior OFF?

 Bind $ to whatever you want it to do.

-- 
Loïc Minier <lool@via.ecp.fr>
}}}

--------------------------------------------------------------------------------
2002-05-24 03:46:28 UTC lool@via.ecp.fr ( Loïc Minier)
* Added comment:
{{{
...

* Holger Lillqvist <lillqvis@venus.ling.helsinki.fi>,
 23 May 2002 11:22:05 GMT:
> Really? This is what the pager help says:
> $             sync-mailbox              save changes to mailbox

 This is mine :
$           bottom                 jump to the bottom of the message

 And in index :
$           sync-mailbox           save changes to mailbox


 Maybe you already rebound it, then it may be a bug, or you're using
another Mutt version. I'm using Mutt 1.2.5i (2000-07-28) (Debian
potato).

 If you redefined $ in pager, how did you achieve this ? May it
conflict with a "generic" binding or with a "macro" ?

-- 
Loïc Minier <lool@via.ecp.fr>
}}}

--------------------------------------------------------------------------------
2002-05-24 09:47:32 UTC Holger Lillqvist <lillqvis@venus.ling.helsinki.fi>
* Added comment:
{{{
...

Loïc Minier wrote:
> * Holger Lillqvist <lillqvis@venus.ling.helsinki.fi>,
>   23 May 2002 11:22:05 GMT:
>> Really? This is what the pager help says:
>> $             sync-mailbox              save changes to mailbox
> 
>   This is mine :
> $           bottom                 jump to the bottom of the message
> 
>   And in index :
> $           sync-mailbox           save changes to mailbox
> 
> 
>   Maybe you already rebound it, then it may be a bug, or you're using
> another Mutt version. I'm using Mutt 1.2.5i (2000-07-28) (Debian
> potato).

No, I haven't rebound it. I'm using Mutt 1.3.28i. I also checked
1.2.5 - this really has been changed.

Holger
}}}

--------------------------------------------------------------------------------
2002-05-27 17:03:01 UTC <dkcombs@panix.com>
* Added comment:
{{{
Package: mutt
Version: 1.3.28i
Severity: normal

-- Please type your report below this line




-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Using builtin specs.
gcc version egcs-2.91.66 19990314 (egcs-1.1.2 release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.28i (2002-03-13)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: NetBSD 1.5.2 (i386) [using ncurses 5.2]
Compile options:
DOMAIN="panix.com"
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
-USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  -HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/pkg/mutt-1.3.28/share/mutt"
SYSCONFDIR="/pkg/mutt-1.3.28/libdata/mutt-1.3.28"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.







Here is a short of emails about this problem,
the final one was someone advising me to
send this stuff to you guys.

The emails:
}}}

--------------------------------------------------------------------------------
2002-05-30 14:36:13 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Wed, May 29, 2002 at 09:21:05AM -0700, Michael Elkins wrote:

> Please state the problem clearly in proper English.

I think he's complaining that when you sync a mailbox using $ while you're
partway down a mail, the pager returns to the top. It was also covered on
the newsgroup recently IIRC.

-- 
Paul

"/net is just an interface to NFS, not a Magic Fairy Directory of Power."
-- Mike Sphar, in asr
}}}

--------------------------------------------------------------------------------
2005-09-07 00:36:30 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed
