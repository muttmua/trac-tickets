Ticket:  2042
Status:  closed
Summary: Mutt crashes horribly verifying signatures

Reporter: elfchief-muttbug@lupine.org
Owner:    mutt-dev

Opened:       2005-08-17 18:34:49 UTC
Last Updated: 2005-08-30 22:39:04 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Attempting to read a PGP-signed message gives the following:

Invoking PGP...zsh: segmentation fault (core dumped)  TERM=screen /var/tmp/mutt/mutt

Stack trace for core file:

#0  0xd1940d95 in _fflush_u () from /usr/lib/libc.so.1
#1  0xd1942ff8 in _rewind_unlocked () from /usr/lib/libc.so.1
#2  0xd1942fc3 in rewind () from /usr/lib/libc.so.1
#3  0x080b5b47 in pgp_application_pgp_handler (m=0x8520d78, s=0x80460d0)
    at pgp.c:384
#4  0x080bb571 in crypt_mod_pgp_application_handler (m=0x8520d78, s=0x80460d0)
    at crypt-mod-pgp-classic.c:46
#5  0x08067b32 in crypt_pgp_application_pgp_handler (m=0x8520d78, s=0x80460d0)
    at cryptglue.c:153
#6  0x080820fe in mutt_body_handler (b=0x8520d78, s=0x80460d0)
    at handler.c:1935
#7  0x0806ec0e in _mutt_copy_message (fpout=0x810da90, fpin=0x810da80,
    hdr=0x8520cc0, body=0x8520d78, flags=2124, chflags=134504672) at copy.c:661
#8  0x0806eeaa in mutt_copy_message (fpout=0x810da90, src=0x811d4e8,
    hdr=0x8520cc0, flags=2124, chflags=150) at copy.c:741
#9  0x080685f2 in mutt_display_message (cur=0x8520cc0) at commands.c:146
#10 0x08072d40 in mutt_index_menu () at curs_main.c:1155
#11 0x0808800d in main (argc=1, argv=0x8047544) at main.c:948


An email message that has this problem has been attached.
>How-To-Repeat:
Read attached file, crash? :) I'm assuming this is specific to something about my setup (or Solaris, or similar) so may not be easily reproducible. Happy to provide any further information needed, though.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-08-31 16:39:04 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:00 UTC 
* Added attachment muttpgpcrash.mbox
* Added comment:
muttpgpcrash.mbox
