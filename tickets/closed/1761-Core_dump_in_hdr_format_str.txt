Ticket:  1761
Status:  closed
Summary: Core dump in hdr_format_str

Reporter: "James R. O'Kane" <jo2y@cs.pitt.edu>
Owner:    mutt-dev

Opened:       2004-01-16 01:15:37 UTC
Last Updated: 2005-10-04 05:33:04 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.1i
Severity: normal

-- Please type your report below this line

I can't give you the core file yet until I find out if the message in question was sensetive or not. I can include the stack trace. 
Looking at the line in question, 
mutt_format_s (dest, destlen, prefix, mutt_get_name (hdr->env->from));
env is NULL. I traced back up the stack, but I couldn't find the place that env should be set. 
This is within the case 'n': block. The author of the email as listed in NIS is Daniel Mosse' (including the '). I suspect that the ' is involved, but when I never had problems when I used mutt frequently. 
Mutt is also not this user's primary client, so it's possible this is the first time he's used it since we last updated versions.



(gdb) bt
#0  0x08068852 in hdr_format_str (dest=0xbfffccf0 "", destlen=1024, 
    op=110 'n', src=0x80ce484 "   %s", prefix=0xbfffd0f0 "-20.20", 
    ifstring=0xbfffcc70 "\020", 
    elsestring=0xbfffcbf0 "l���<���h���\f\202\023B`�\016\b\002", 
    data=3221213616, flags=M_FORMAT_MAKEPRINT) at hdrline.c:484
#1  0x08093096 in mutt_FormatString (
    dest=0xbfffd510 "- D*- 191/0: : Takashi Okumura        Netnice: the trial", destlen=81, src=0x80ce470 "-%Z- %C/%m: %-20.20n   %s", 
    callback=0x80680f8 <hdr_format_str>, data=3221213616, 
    flags=M_FORMAT_MAKEPRINT) at muttlib.c:1040
#2  0x080690ae in _mutt_make_string (
    dest=0xbfffd510 "- D*- 191/0: : Takashi Okumura        Netnice: the trial", destlen=82, s=0x80ce470 "-%Z- %C/%m: %-20.20n   %s", ctx=0x80ced10, 
    hdr=0x8111048, flags=M_FORMAT_MAKEPRINT) at hdrline.c:708
#3  0x0807793d in mutt_pager (banner=0x0, 
    fname=0xbfffdd80 "/tmp/mutt-cachaca-31864-5", flags=66, extra=0xbfffd960)
    at pager.c:1705
#4  0x0805318e in mutt_display_message (cur=0x8111048) at commands.c:181
#5  0x0805b543 in mutt_index_menu () at curs_main.c:1070
#6  0x0806cf74 in main (argc=1, argv=0xbfffec94) at main.c:841
#7  0x420158f7 in __libc_start_main () from /lib/i686/libc.so.6



-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/3.2/specs
Configured with: ../configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --enable-shared --enable-threads=posix --disable-checking --host=i386-redhat-linux --with-system-zlib --enable-__cxa_atexit
Thread model: posix
gcc version 3.2 20020903 (Red Hat Linux 8.0 3.2-7)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4.1i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20-19.8 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
-HAVE_REGCOMP  +USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/afs/.cs.pitt.edu/system/@sys/usr/local/share/mutt"
SYSCONFDIR="/afs/.cs.pitt.edu/system/@sys/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /afs/.cs.pitt.edu/system/@sys/usr/local/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
set hidden_host=yes
set imap_force_ssl=yes
set mail_check=600
set certificate_file="~/.mutt-certificates"
set spoolfile="{imap}inbox"
--- End /afs/.cs.pitt.edu/system/@sys/usr/local/etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-04 18:35:30 UTC brendan
* Added comment:
{{{
We believe this bug is fixed in 1.5.10. Care to give it a try?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-10-04 23:33:04 UTC brendan
* Added comment:
{{{
Unreproducible and believed fixed; no feedback.
}}}

* resolution changed to fixed
* status changed to closed
