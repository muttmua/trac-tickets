Ticket:  893
Status:  closed
Summary: Mailbox size not updated when attachments are deleted

Reporter: kromJx <kromJx@myrealbox.com>
Owner:    mutt-dev

Opened:       2001-11-28 15:34:28 UTC
Last Updated: 2005-08-01 18:17:43 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.23.2i
Severity: normal

-- Please type your report below this line
After one (or more) attachments are deleted (by pressing 'd'
while in the attachments menu), the message size shown on the
status line is not updated to reflect this change, even after
the mailbox sync key ('$') is pressed. 

Since mutt doesn't see the change, it will happily report
'Mailbox is unchanged' and will not try to compute the new mailbox size.


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/2.95.2/specs
gcc version 2.95.2 19991024 (release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.23.2i (2001-11-08)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.16 (i586) [using slang 10401]
Compile options:
-DOMAIN
+DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
-ENABLE_NLS  +LOCALES_HACK  +HAVE_WC_FUNCS  -HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  +ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Mailbox"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/usr/share/mutt"
EXECSHELL="/bin/sh"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 12:17:43 UTC brendan
* Added comment:
{{{
Fixed in 2002.
}}}

* resolution changed to fixed
* status changed to closed
