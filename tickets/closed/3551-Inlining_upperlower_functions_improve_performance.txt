Ticket:  3551
Status:  closed
Summary: Inlining upper/lower functions improve performance

Reporter: danf
Owner:    mutt-dev

Opened:       2011-11-30 03:11:39 UTC
Last Updated: 2011-12-03 19:14:21 UTC

Priority:  trivial
Component: display
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Several often-used function can be inlined to noticeably improve mutt's performance. When I first sent this patch to the mailing list in 2008, my benchmarking found a reduction in startup time of 15%.

--------------------------------------------------------------------------------
2011-11-30 03:12:05 UTC danf
* Added attachment mutt-1.5.21-inline.patch
* Added comment:
Inline upper/lower functions to improve performance

--------------------------------------------------------------------------------
2011-12-03 19:14:21 UTC Dan Fandrich <dan@coneharvesters.com>
* Added comment:
(In [1ed2657f6e24]) Inline some small, often-used functions (closes #3551)

When I first sent this patch to the mailing list in 2008, my benchmarking found a
reduction in startup time of 15%.

* resolution changed to fixed
* status changed to closed
