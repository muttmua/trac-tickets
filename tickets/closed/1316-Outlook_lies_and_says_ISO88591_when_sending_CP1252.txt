Ticket:  1316
Status:  closed
Summary: Outlook lies and says ISO-8859-1 when sending CP1252

Reporter: Tony Leneis <tony@cvr.ds.adp.com>
Owner:    mutt-dev

Opened:       2002-08-17 04:57:44 UTC
Last Updated: 2005-08-02 22:14:12 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: wishlist

-- Please type your report below this line

Outlook lies and marks text attachments as "text/plain; charset=iso-8859-1"
when it's really using cp-1252 encoding (or whatever that particular PC has
been set to use.)  What we need is a way to override charset encodings from
certain untrustworthy e-mail clients, as this problem is getting worse and
worse as more people use Word to edit their e-mail (Word likes to use open
and close quotes, em dashes, elipses, and other nasties that are illegal
ISO-8859-1 characters.)  One way to do it would be to have a third argument
for charset-hook - for example:

  charset-hook [!]pattern alias charset

Of course, that would probably break everybody's .muttrc file.  The idea
being that I could, for example, replace Outlook's iso-8859-1 with cp1252
(in my case - it would probably vary depending on what country the people
sending you e-mail are in) just as if I'd gone to the attachment menu and
hit control-e and changed the charset manually (which works, BTW.)
Meanwhile, it would leave e-mail from other MUAs alone.

-Tony

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc


cc: Error: no source, object or ucode file specified

- CFLAGS
-std1 -fast -O4 -tune host -arch host -D_XOPEN_SOURCE_EXTENDED

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: OSF1 V4.0 (alpha)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-08-17 04:57:44 UTC Tony Leneis <tony@cvr.ds.adp.com>
* Added comment:
{{{
Package: mutt
Version: 1.4i
Severity: wishlist

-- Please type your report below this line

Outlook lies and marks text attachments as "text/plain; charset=iso-8859-1"
when it's really using cp-1252 encoding (or whatever that particular PC has
been set to use.)  What we need is a way to override charset encodings from
certain untrustworthy e-mail clients, as this problem is getting worse and
worse as more people use Word to edit their e-mail (Word likes to use open
and close quotes, em dashes, elipses, and other nasties that are illegal
ISO-8859-1 characters.)  One way to do it would be to have a third argument
for charset-hook - for example:

 charset-hook [!]pattern alias charset

Of course, that would probably break everybody's .muttrc file.  The idea
being that I could, for example, replace Outlook's iso-8859-1 with cp1252
(in my case - it would probably vary depending on what country the people
sending you e-mail are in) just as if I'd gone to the attachment menu and
hit control-e and changed the charset manually (which works, BTW.)
Meanwhile, it would leave e-mail from other MUAs alone.

-Tony

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc


cc: Error: no source, object or ucode file specified

- CFLAGS
-std1 -fast -O4 -tune host -arch host -D_XOPEN_SOURCE_EXTENDED

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: OSF1 V4.0 (alpha)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.
}}}

--------------------------------------------------------------------------------
2003-09-25 03:42:15 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Tony, and thanks for your suggestion.

On Friday, August 16, 2002 at 8:57:44 PM -0700, Tony Leneis wrote:

> Outlook lies and marks text attachments as "text/plain;
> charset=iso-8859-1" when it's really using cp-1252 encoding

   MSO is not the only such liar.


> [...] or whatever that particular PC has been set to use.

   Hum... AFAICS the confusion is only between Latin-1 and CP-1252, not
other such charsets. I mean there is no say CP-1250 commonly labelled
Latin-2 in Poland: The first beeing not an exact superset of the second,
confusing them is harder. OTOH ICBW.


> What we need is a way to override charset encodings from certain
> untrustworthy e-mail clients [...] a third argument for charset-hook
>| charset-hook [!]pattern alias charset

   An unconditional "charset-hook ^iso-8859-1$ cp1252" can do the trick
without too much bad effects. Please look at subthread « strange chars
from Outlook users » beginning at <20030315133221.GA14916@oreka.com> on
mutt-users archives for pros and cons of conditionning this to mailer.

   Important note: Mutt 1.4 has a nasty bug preventing correct use of
this charset-hook. It needs a patch from Edmund Grimley-Evans, or an
upgrade to 1.5.4.


Bye!	Alain.
-- 
Mutt muttrc tip for mailing lists: set followup_to=yes and declare the list as
- subscribe list@ddress if you are subscribed and don't want courtesy copy.
- lists list@ddress     if you are not subscribed or want a courtesy copy.
}}}

--------------------------------------------------------------------------------
2005-08-02 08:53:50 UTC cb
* Added comment:
{{{
Get info.
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-02 08:53:51 UTC cb
* Added comment:
{{{
Does using iconv-hook (maybe in combination with message-hooks) solve this (non-mutt) problem?
}}}

--------------------------------------------------------------------------------
2005-08-02 13:28:02 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Monday, August 1, 2005 at 3:55:52 PM +0200, Gnats notified:

> Synopsis: Outlook lies and says ISO-8859-1 when sending CP1252
> **** Comment added by cb on Mon, 01 Aug 2005 15:55:52 +0200 ****

    Some of the notifications are... short. Unusefull. And the other
didn't reach mutt-dev?


 On Monday, August 1, 2005 at 3:53:50 PM +0200, Christoph Berg wrote:

| Does using iconv-hook (maybe in combination with message-hooks) sol=
ve
| this (non-mutt) problem?

    *charset*-hook, yes: The problem is solved totally, without harm.=
 No
need for an additional condition (this mutt/1316 wish) nor massage-ho=
ok.


Bye!=09Alain.
--=20
=AB=A0if you believe the Content-Type header, I've got a bridge to se=
ll you.=A0=BB
}}}

--------------------------------------------------------------------------------
2005-08-03 16:14:12 UTC ab
* Added comment:
{{{
On Monday, August 1, 2005 at 3:55:52 PM +0200, Gnats notified:

    Mutt already has a perfect workaround for the problem. 
No need to wait OP feedback, he bounces "Illegal 
host/domain name found". Thanks for your suggestions Tony.
}}}

* resolution changed to fixed
* status changed to closed
