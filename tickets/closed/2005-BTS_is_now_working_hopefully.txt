Ticket:  2005
Status:  closed
Summary: BTS is now working (hopefully)

Reporter: cb@df7cb.de
Owner:    mutt-dev

Opened:       2005-07-27 17:03:23 UTC
Last Updated: 2006-03-24 23:39:28 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
>How-To-Repeat:
go to http://bugs.mutt.org/ and check.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-07-31 05:23:00 UTC cb
* Added comment:
{{{
Use this procmail snippet and the already attached gnatsfix.pl to get sane attachments.
}}}

--------------------------------------------------------------------------------
2005-09-15 22:56:25 UTC brendan
* Added comment:
{{{
Refiled as support to hide it from sw-bug queries.
}}}

--------------------------------------------------------------------------------
2006-03-25 17:39:28 UTC paul
* Added comment:
{{{
BTS found to be working. :-)
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:42:58 UTC 
* Added attachment gnatsfix
* Added attachment gnatsfix.pl
* Added comment:
gnatsfix

* Added comment:
gnatsfix.pl
