Ticket:  3585
Status:  closed
Summary: Updated Italian translation

Reporter: marco
Owner:    mutt-dev

Opened:       2012-05-25 20:36:32 UTC
Last Updated: 2012-07-07 06:05:24 UTC

Priority:  minor
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
Translations have been fixed and converted to UTF-8; some of them still need to be checked further.

--------------------------------------------------------------------------------
2012-05-25 20:37:00 UTC marco
* Added attachment italian_translation.patch

--------------------------------------------------------------------------------
2012-07-07 06:05:24 UTC brendan
* Added comment:
Applied, thanks. Typically translations go through the mutt-po@mutt.org list though.

* resolution changed to fixed
* status changed to closed
