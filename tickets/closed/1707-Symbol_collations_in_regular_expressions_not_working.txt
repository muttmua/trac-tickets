Ticket:  1707
Status:  closed
Summary: Symbol collations in regular expressions not working

Reporter: mj@zope.com
Owner:    mutt-dev

Opened:       2003-11-14 01:45:19 UTC
Last Updated: 2007-11-20 13:32:10 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4+20031024
Severity: normal

-- Please type your report below this line

Mutt uses extended POSIX regular expressions, including support for
symbol collations (collating elements enclosed in [. and .]); the Mutt
documentation gives an explicit example of a symbol collation element.

However, I cannot create any regular expression using a valid symbol
collation, including the example cited in the documentation; invariably
the error 'invalid collation character' is returned.

To reproduce:

- Start a search ('/')

- Type '[[.ch.]]' and hit enter

- Mutt prints the error message 'invalid collation character'.

Expected results:

- A search (matching or not) for emails with 'ch' in the subject or
  sender.

This problem could conceivably be a bug in the libc regex implementation;
I didn't verify this. Running "echo chchc | grep -E '[[.ch.]]*c'" on the
command line does however return the same error message. I have libc6
version 2.3.2.ds1-10 (Debian package), FWIW.

-- System Information
System Version: Linux viper.zope.com 2.4.22-1-k7 #1 Sat Sep 6 02:13:04 EST 2003 i686 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.2/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
Thread model: posix
gcc version 3.3.2 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.22-1-k7 (i686) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.14)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.3.rr.compressed.1
patch-1.5.4.helmersson.incomplete_multibyte
patch-1.5.4.fw.maildir_inode_sort
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.5.1.cd.edit_threads.9.2
patch-1.3.27.bse.xtitles.1
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.56d


>How-To-Repeat:
	
>Fix:
}}}

--------------------------------------------------------------------------------
2007-11-20 13:32:10 UTC pdmef
* Added comment:
If the underlying libc has bugs or doesn't work as expected, there's little mutt can do  about it. You try to build with the included regex engine though.

* resolution changed to invalid
* status changed to closed
