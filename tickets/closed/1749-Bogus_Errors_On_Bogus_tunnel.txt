Ticket:  1749
Status:  closed
Summary: Bogus Errors On Bogus $tunnel

Reporter: lists+mutt_bugs@bigfatdave.com
Owner:    mutt-dev

Opened:       2004-01-07 21:49:42 UTC
Last Updated: 2005-08-06 23:21:06 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.5.1i
Severity: normal

-- Please type your report below this line

If you misspell the value of $tunnel or if you delete the binary by
mistake (what happened in my particular case), Mutt says it received an
unrecognized response from the server (which it declines to actually
show, since there is none), and then complains that the connection to
the server was closed.  It doesn't, however, tell you that your $tunnel
simply doesn't exist ... any ideas why?

-- System Information
System Version: Linux dave2 2.4.22 #2 SMP Mon Oct 27 06:48:20 EST 2003 i686 unknown unknown GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Leyendo especificaciones de /usr/local/lib/gcc-lib/i686-pc-linux-gnu/3.2.3/specs
Configurado con: ./configure --enable-version-specific-runtime-libs --enable-languages=c,c++
Modelo de hilos: posix
gcc versión 3.2.3

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.5.1i (2003-11-05)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.22 (i686) [using ncurses 5.3]
Opciones especificadas al compilar:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/beta/mutt_cvs/usr/share/mutt"
SYSCONFDIR="/beta/mutt_cvs/usr/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Para contactar a los desarrolladores mande un mensaje a <mutt-dev@mutt.org>.
Para reportar un fallo use la utilería flea(1) por favor.


--- Begin /home/dave/.muttrc
source ~/.mutt/muttrc
--- End /home/dave/.muttrc


--- Begin /beta/mutt_cvs/usr/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /beta/mutt_cvs/usr/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /beta/mutt_cvs/usr/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /beta/mutt_cvs/usr/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /beta/mutt_cvs/usr/etc/Muttrc




>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-07 17:21:06 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed
