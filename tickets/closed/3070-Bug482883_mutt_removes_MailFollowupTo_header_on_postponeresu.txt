Ticket:  3070
Status:  closed
Summary: Bug#482883: mutt: removes Mail-Followup-To header on postpone+resume (fwd)

Reporter: myon
Owner:    kevin8t8

Opened:       2008-06-07 22:46:41 UTC
Last Updated: 2013-10-23 22:22:21 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:


{{{
Mutt drops a manually added M-F-T header on postpone/resume, confirmed
here for 1.5.18:

----- Forwarded message from Bas Wijnen <wijnen@debian.org> -----

Date: Sun, 25 May 2008 20:08:01 +0200
From: Bas Wijnen <wijnen@debian.org>
Reply-To: Bas Wijnen <wijnen@debian.org>, 482883@bugs.debian.org
To: submit@bugs.debian.org
Subject: Bug#482883: mutt: removes custom headers on postpone+resume

Package: mutt
Version: 1.5.17+20080114-1
Severity: minor

When composing a message with edit_headers=yes, adding a custom header
(such as Mail-Followup-To:) and postponing, then this header is nicely
sent with the message.

However, if the message is postponed and then resumed, it is not shown
anymore.  When the message is then sent, it is indeed not present.

Thanks,
Bas

----- End forwarded message -----
}}}

--------------------------------------------------------------------------------
2010-02-06 18:04:21 UTC antonio@dyne.org
* keywords changed to 
* version changed to 1.5.20

--------------------------------------------------------------------------------
2013-04-20 21:47:55 UTC kevin8t8
* Added comment:
To clarify, this bug is specifically for the Mail-Followup-To header.  Other custom headers are properly restored after a postpone/recall.

This is caused by the line
{{{
FREE (&newhdr->env->mail_followup_to); /* really? */
}}}
in mutt_prepare_template() [postpone.c]

This function is called by both mutt_get_postponed() [postpone.c] and mutt_resend_message() [send.c] for the <recall-message> and <resend-message> commands.

The line was originally added in changeset 840:b39dbb28b760 for some <resend-message> cleanup by Thomas Roessler.

Later on, the comment was added in changeset 1402:08ee0a190bb7 for an unrelated cleanup patch, also by Thomas.

My guess was that the FREE was added initially without considering the overlapping use by postpone/recall.

Two obvious solutions I can see to the bug are:

1. Move the FREE to mutt_resend_message(), so it only takes place for a resend.  It needs to be investigated whether this is feasible or if there is code below in mutt_prepare_template() that makes this difficult. 

2. Add a flag to mutt_prepare_template() to differentiate usage between resend and postpone.  However, if there are no other differences, this is fairly awkward.



--------------------------------------------------------------------------------
2013-04-20 21:49:28 UTC kevin8t8
* cc changed to kevin@8t8.us
* summary changed to Bug#482883: mutt: removes Mail-Followup-To header on postpone+resume (fwd)

--------------------------------------------------------------------------------
2013-04-21 01:58:31 UTC kevin8t8
* Added comment:
Another approach is to test whether message_id is null or not.  This will basically tell us whether we are doing a postpone/recall or resend message.  I'm attaching a patch for that and would appreciate comments.

--------------------------------------------------------------------------------
2013-04-22 20:44:46 UTC kevin8t8
* owner changed to kevin8t8
* status changed to assigned

--------------------------------------------------------------------------------
2013-04-26 19:13:30 UTC kevin8t8
* Added attachment check-messageid-null.patch
* Added comment:
Add closes to patch summary

--------------------------------------------------------------------------------
2013-10-22 22:02:35 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [d7d83298011a6e7aa31bde49e0b6b21209a85fd2]:
{{{
#!CommitTicketReference repository="" revision="d7d83298011a6e7aa31bde49e0b6b21209a85fd2"
Fix postpone/resume to not remove a Mail-Followup-To header  (closes #3070)

This solution changes mutt_prepare_template() to check whether the
message_id field is NULL to decide whether to wipe the message-id and
mail-followup-to headers when instantiating the message.

If we are resending a message, we don't want the previous message-id
and mail-followup-to headers.  If we are resuming a postponed message,
however, we want to keep the mail-followup-to header if any was set
before the postpone.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2013-10-23 22:22:21 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [914e13a3694db39eda56b1f596f4c9fcb9d6d81e]:
{{{
#!CommitTicketReference repository="" revision="914e13a3694db39eda56b1f596f4c9fcb9d6d81e"
Fix postpone/resume to not remove a Mail-Followup-To header  (closes #3070)

This solution changes mutt_prepare_template() to check whether the
message_id field is NULL to decide whether to wipe the message-id and
mail-followup-to headers when instantiating the message.

If we are resending a message, we don't want the previous message-id
and mail-followup-to headers.  If we are resuming a postponed message,
however, we want to keep the mail-followup-to header if any was set
before the postpone.
(grafted from d7d83298011a6e7aa31bde49e0b6b21209a85fd2)
}}}
