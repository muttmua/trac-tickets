Ticket:  3875
Status:  closed
Summary: Mutt with imap exits line editor when resized

Reporter: nicop
Owner:    mutt-dev

Opened:       2016-09-13 23:03:49 UTC
Last Updated: 2016-09-22 22:58:37 UTC

Priority:  major
Component: user interface
Keywords:  imap editor resize

--------------------------------------------------------------------------------
Description:
To reproduce, you need to configure mutt with {{{--enable-imap}}} before compiling. Then, just open the line editor, for instance by pressing {{{m}}} to start composing an email (assuming default key bindings). Now, if you resize the terminal, mutt will exit the line editor. If you directly start mutt with a mailto url, e.g. {{{mutt me@example.com}}}, resizing the terminal will exit mutt.

This only affects mutt with imap support as the code handling key input behaves differently in case of resize with imap enabled. I attached a (mq) patch that solves the problem.

--------------------------------------------------------------------------------
2016-09-13 23:05:10 UTC nicop
* Added attachment fix-resize.patch
* Added comment:
Patch to fix the resize issue

--------------------------------------------------------------------------------
2016-09-20 03:19:15 UTC kevin8t8
* Added comment:
Thanks for the bug report and patch.  I will take a closer look at this soon, but the patch looks alright to me.

--------------------------------------------------------------------------------
2016-09-22 21:10:11 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"87911ba95daed80b9edb4aa0622379f01e0589ff" 6798:87911ba95dae]:
{{{
#!CommitTicketReference repository="" revision="87911ba95daed80b9edb4aa0622379f01e0589ff"
Don't abort the menu editor on sigwinch. (closes #3875)

getch() will return ERR on sigwinch when timeout() is called with a
positive value.  mutt_getch() will therefore return ch==-2 for both a
timeout and a sigwinch in this case.

The imap code in km_dokey() exits out of the ImapKeepalive loop for a
SigWinch, and was skipping past the check for MENU_EDITOR and
tmp.ch==-2.  Move this check below the gotkey: label so the
ImapKeepalive loop behaves the same as the Timeout code.

Thanks to nicop for reporting the problem and for the initial patch!
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2016-09-22 22:58:37 UTC nicop
* Added comment:
Thanks to you for this changeset, this is a much cleaner solution. I thought about moving gotkey label too but I wasn't sure about the implications, as this would restart the ImapKeepalive loop, so I went for the safer fix.

Also, it seems that mutt_getch is already waiting for a key other than KEY_RESIZE, which is supposed to be sent when the terminal is resized. However, mutt overrides the ncurse SigWinch signal handling with his own in ''signal.c''. Disabling this does solve the problem, so maybe we could temporary disable it during the call to getch?

Otherwise, this ''while (ch == KEY_RESIZE)'' loop seems useless. Also, in this specific case, it may also delay the imap synchronization as each getch would reset the timeout, so it might need to be removed at least when compiled with --enable-imap.

This is just some thought I had when investigating this problem that I wanted to share. I hope it may be useful.
