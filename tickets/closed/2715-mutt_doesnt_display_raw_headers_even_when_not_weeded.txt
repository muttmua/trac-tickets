Ticket:  2715
Status:  closed
Summary: mutt doesn't display raw headers even when not weeded

Reporter: mutt-bug@nospam.pz.podzone.net
Owner:    mutt-dev

Opened:       2007-01-27 15:30:14 UTC
Last Updated: 2008-05-26 21:51:46 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
In the example below, mutt does not display the same header information as seen in the raw mailbox.  Notably To:, From: & Return-path: are all modified in some way.

How can mutt be made to show raw headers?

>How-To-Repeat:
Note From: Return-Path: are not raw!
{{{
~$ cat ~/grummund.mbx
From root@duke.grummund.net Thu Jan 18 13:59:02 2007
Return-path: <root@duke.grummund.net>
Envelope-to: root@duke.grummund.net
Delivery-date: Thu, 18 Jan 2007 13:59:02 +0000
Received: from root by aa.dnsdojo.com with local (Exim 4.50)
        id 1H7XnB-00076V-TJ
        for root@duke.grummund.net; Thu, 18 Jan 2007 13:59:02 +0000
From: root@duke.grummund.net (Cron Daemon)
To: root@duke.grummund.net
Subject: Cron <root@duke> date
X-Cron-Env: <SHELL=/bin/sh>
X-Cron-Env: <PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin>
X-Cron-Env: <HOME=/root>
X-Cron-Env: <LOGNAME=root>
Message-Id: <E1H7XnB-00076V-TJ@aa.dnsdojo.com>
Date: Thu, 18 Jan 2007 13:59:01 +0000
Status: RO
Content-Length: 29
Lines: 1

Thu Jan 18 13:59:01 GMT 2007
}}}
~$ mutt -f ~/grummund.mbx

In mutt, press 'h' to display full headers:
{{{
From root@duke.grummund.net Thu Jan 18 13:59:02 2007
Return-path: root@duke.grummund.net
Envelope-to: root@duke.grummund.net
Delivery-date: Thu, 18 Jan 2007 13:59:02 +0000
Received: from root by aa.dnsdojo.com with local (Exim 4.50)
        id 1H7XnB-00076V-TJ
        for root@duke.grummund.net; Thu, 18 Jan 2007 13:59:02 +0000
From: Cron Daemon <root@duke.grummund.net>
To: root@duke.grummund.net
Subject: Cron <root@duke> date
X-Cron-Env: <SHELL=/bin/sh>
X-Cron-Env: <PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin>
X-Cron-Env: <HOME=/root>
X-Cron-Env: <LOGNAME=root>
Message-Id: <E1H7XnB-00076V-TJ@aa.dnsdojo.com>
Date: Thu, 18 Jan 2007 13:59:01 +0000
Status: RO
Content-Length: 29
Lines: 1

Thu Jan 18 13:59:01 GMT 2007
}}}

--------------------------------------------------------------------------------
2007-03-31 22:19:11 UTC brendan
* Updated description:
In the example below, mutt does not display the same header information as seen in the raw mailbox.  Notably To:, From: & Return-path: are all modified in some way.

How can mutt be made to show raw headers?

>How-To-Repeat:
Note From: Return-Path: are not raw!
{{{
~$ cat ~/grummund.mbx
From root@duke.grummund.net Thu Jan 18 13:59:02 2007
Return-path: <root@duke.grummund.net>
Envelope-to: root@duke.grummund.net
Delivery-date: Thu, 18 Jan 2007 13:59:02 +0000
Received: from root by aa.dnsdojo.com with local (Exim 4.50)
        id 1H7XnB-00076V-TJ
        for root@duke.grummund.net; Thu, 18 Jan 2007 13:59:02 +0000
From: root@duke.grummund.net (Cron Daemon)
To: root@duke.grummund.net
Subject: Cron <root@duke> date
X-Cron-Env: <SHELL=/bin/sh>
X-Cron-Env: <PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin>
X-Cron-Env: <HOME=/root>
X-Cron-Env: <LOGNAME=root>
Message-Id: <E1H7XnB-00076V-TJ@aa.dnsdojo.com>
Date: Thu, 18 Jan 2007 13:59:01 +0000
Status: RO
Content-Length: 29
Lines: 1

Thu Jan 18 13:59:01 GMT 2007
}}}
~$ mutt -f ~/grummund.mbx

In mutt, press 'h' to display full headers:
{{{
From root@duke.grummund.net Thu Jan 18 13:59:02 2007
Return-path: root@duke.grummund.net
Envelope-to: root@duke.grummund.net
Delivery-date: Thu, 18 Jan 2007 13:59:02 +0000
Received: from root by aa.dnsdojo.com with local (Exim 4.50)
        id 1H7XnB-00076V-TJ
        for root@duke.grummund.net; Thu, 18 Jan 2007 13:59:02 +0000
From: Cron Daemon <root@duke.grummund.net>
To: root@duke.grummund.net
Subject: Cron <root@duke> date
X-Cron-Env: <SHELL=/bin/sh>
X-Cron-Env: <PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin>
X-Cron-Env: <HOME=/root>
X-Cron-Env: <LOGNAME=root>
Message-Id: <E1H7XnB-00076V-TJ@aa.dnsdojo.com>
Date: Thu, 18 Jan 2007 13:59:01 +0000
Status: RO
Content-Length: 29
Lines: 1

Thu Jan 18 13:59:01 GMT 2007
}}}
* priority changed to minor

--------------------------------------------------------------------------------
2008-05-26 21:51:46 UTC myon
* Added comment:
This is the same bug as #1702, I'm closing this copy.

* resolution changed to duplicate
* status changed to closed
