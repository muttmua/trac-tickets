Ticket:  2947
Status:  closed
Summary: mutt-1.5.16: Parameter in gpg_list_pubring_command truncated

Reporter: karlheinz.eckmeier@t-online.de
Owner:    mutt-dev

Opened:       2007-09-04 23:31:14 UTC
Last Updated: 2009-06-12 12:44:01 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:

{{{
Package: mutt
Version: 1.5.16
Severity: important

-- Please type your report below this line

I've configured pgp_list_pubring_command to use a custom shell script to find gpg group entries: 
set pgp_list_pubring_command="~/bin/myfetchkeyid %r"

This worked perfectly for years and stopped working after upgrading to 1.5.16.
Now the value which the parameter %r inserts into the command is truncated. 
Some examples:
To=abc@domain.com      results in   %r=abc@domain
To=abc@domain.sub.com  results in   %r=abc@domain

It looks like the parameter is cut of at the first '.' character.
 


-- System Information
System Version: FreeBSD abakus 6.2-STABLE FreeBSD 6.2-STABLE #2: Thu Apr 26 17:14:35 CEST 2007     toor@abakus:/usr/src/sys/i386/compile/ABAKUS  i386

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc -I/usr/local/include
Using built-in specs.
Configured with: FreeBSD/i386 system compiler
Thread model: posix
gcc version 3.4.6 [FreeBSD] 20060305

- CFLAGS
-O2 -fno-strict-aliasing -pipe

-- Mutt Version Information

Mutt 1.5.16 (2007-06-09)
Copyright (C) 1996-2007 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: FreeBSD 6.2-STABLE (i386)
slang: 10409
libiconv: 1.9
libidn: 0.6.14 (compiled with 0.6.14)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
-USE_FCNTL  +USE_FLOCK   -USE_INODESORT   
+USE_POP  +USE_NNTP  +USE_IMAP  +USE_SMTP  +USE_GSS  +USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

vvv.quote
patch-1.5.0.ats.date_conditional.1
dgc.deepif.1
vvv.initials
vvv.nntp
rr.compressed
}}}

--------------------------------------------------------------------------------
2009-06-12 12:44:01 UTC pdmef
* Added comment:
Actually this is a feature so that mutt doesn't need to rely on exact string matching. This is supposed to help finding matches in uids.

* Updated description:

{{{
Package: mutt
Version: 1.5.16
Severity: important

-- Please type your report below this line

I've configured pgp_list_pubring_command to use a custom shell script to find gpg group entries: 
set pgp_list_pubring_command="~/bin/myfetchkeyid %r"

This worked perfectly for years and stopped working after upgrading to 1.5.16.
Now the value which the parameter %r inserts into the command is truncated. 
Some examples:
To=abc@domain.com      results in   %r=abc@domain
To=abc@domain.sub.com  results in   %r=abc@domain

It looks like the parameter is cut of at the first '.' character.
 


-- System Information
System Version: FreeBSD abakus 6.2-STABLE FreeBSD 6.2-STABLE #2: Thu Apr 26 17:14:35 CEST 2007     toor@abakus:/usr/src/sys/i386/compile/ABAKUS  i386

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc -I/usr/local/include
Using built-in specs.
Configured with: FreeBSD/i386 system compiler
Thread model: posix
gcc version 3.4.6 [FreeBSD] 20060305

- CFLAGS
-O2 -fno-strict-aliasing -pipe

-- Mutt Version Information

Mutt 1.5.16 (2007-06-09)
Copyright (C) 1996-2007 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: FreeBSD 6.2-STABLE (i386)
slang: 10409
libiconv: 1.9
libidn: 0.6.14 (compiled with 0.6.14)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
-USE_FCNTL  +USE_FLOCK   -USE_INODESORT   
+USE_POP  +USE_NNTP  +USE_IMAP  +USE_SMTP  +USE_GSS  +USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

vvv.quote
patch-1.5.0.ats.date_conditional.1
dgc.deepif.1
vvv.initials
vvv.nntp
rr.compressed
}}}
* resolution changed to invalid
* status changed to closed
