Ticket:  3370
Status:  closed
Summary: [INTL:sv] mutt: fixes to Swedish translation

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2010-01-31 11:06:36 UTC
Last Updated: 2010-02-02 07:40:21 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/548494

The Swedish translation has a suboptimal translation of the word 
"Bad" in several places. The proposed patch substitutes Dålig 
(which implies bad "as in quality", and not bad "as in error") 
for Felaktig (erroneous).

The attached patch fixes the problem

--------------------------------------------------------------------------------
2010-01-31 11:06:50 UTC antonio@dyne.org
* Added attachment mutt-1.5.20-l10n_sv.patch

--------------------------------------------------------------------------------
2010-01-31 11:20:28 UTC antonio@dyne.org
* Added attachment 548494-swedish-intl.patch

--------------------------------------------------------------------------------
2010-01-31 11:21:00 UTC antonio@dyne.org
* Added comment:
Attaching a new patch that does the same thing, this one cleanly applies to the 548494-swedish-intl.patch tree 

--------------------------------------------------------------------------------
2010-01-31 15:47:11 UTC antonio@dyne.org
* version changed to 1.5.20

--------------------------------------------------------------------------------
2010-02-02 07:40:21 UTC Antonio Radici <antonio@dyne.org>
* Added comment:
(In [e9965b78e92d]) The Swedish translation has a suboptimal translation of the word "Bad"
in several places. The proposed patch substitutes Dålig (which implies
bad "as in quality", and not bad "as in error") for Felaktig
(erroneous).

Closes #3370.

* resolution changed to fixed
* status changed to closed
