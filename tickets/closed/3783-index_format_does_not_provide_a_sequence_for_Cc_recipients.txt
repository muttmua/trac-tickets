Ticket:  3783
Status:  closed
Summary: index_format does not provide a sequence for Cc: recipients

Reporter: derekschrock
Owner:    mutt-dev

Opened:       2015-09-13 02:59:08 UTC
Last Updated: 2017-10-18 11:02:21 UTC

Priority:  minor
Component: mutt
Keywords:  index_format

--------------------------------------------------------------------------------
Description:
I need to pass the Cc: recipients to my EDITOR.  [[BR]]

Using '''attribution''' I can't do this since '''index_format''' does not provide a sequence for the Cc: header.

--------------------------------------------------------------------------------
2015-10-17 03:20:49 UTC kevin8t8
* Added comment:
Derek, is this covered by your %r/%R patch?

--------------------------------------------------------------------------------
2017-10-18 11:02:21 UTC kevin8t8
* Added comment:
I believe this was addressed in changeset:aec82c4dd826 by the reporter, so I am closing this out.

* resolution changed to fixed
* status changed to closed
