Ticket:  3293
Status:  closed
Summary: attachments menu broken/inconsistent

Reporter: pdmef
Owner:    pdmef

Opened:       2009-07-07 12:25:13 UTC
Last Updated: 2013-01-17 23:08:27 UTC

Priority:  minor
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
When saving attachments from the attachments menu, they're recoded from their charset to local `$charset` if they're of type `text/*`. This is (1) unexpected and (2) wrong (e.g. for `text/html` which likely has a charset declaration on its own). We should at least provide save and decode-save.

It's inconsistent compared to pager/index, where saving is copy + delete. In the attachments menu, saving means copying only.

--------------------------------------------------------------------------------
2011-01-01 19:16:51 UTC antonio@dyne.org
* Added comment:
Hi,
can you please add 537061@bugs.debian.org to CC?

This seems to be a side effect of http://dev.mutt.org/trac/changeset/392e945dfba7 and we are thinking of reverting it for squeeze since it breaks so many attachments.

A MUA should not modify (in this case recode) the attachments when they are saved, but they should be saved as is; please consider reverting that commit or to find a proper fix for #3234.

Thanks!

Cheers
Antonio

--------------------------------------------------------------------------------
2011-01-03 14:36:17 UTC vinc17
* cc changed to vincent@vinc17.org

--------------------------------------------------------------------------------
2013-01-17 23:08:23 UTC Michael Elkins <me@sigpipe.org>
* Added comment:
(In [b9f9e3147eb4]) when falling back to viewing an attachment as text in the internal pager, perform charset conversion

this change is meant to replace [392e945dfba7]

see #3234
see #3293

--------------------------------------------------------------------------------
2013-01-17 23:08:27 UTC Michael Elkins <me@sigpipe.org>
* Added comment:
(In [7fcc0049f250]) backout [392e945dfba7]

closes #3293
see #3234

* resolution changed to fixed
* status changed to closed
