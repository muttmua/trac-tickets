Ticket:  3609
Status:  closed
Summary: Crash (segmentation fault) in mutt_substrdup when viewing a message with an empty "Cc: "

Reporter: vinc17
Owner:    mutt-dev

Opened:       2012-12-18 09:08:05 UTC
Last Updated: 2012-12-18 10:20:18 UTC

Priority:  critical
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
With the trunk (6232) and some patches, after viewing some message with en empty "Cc: " (4 characters), I get a direct crash:
{{{
Program received signal SIGSEGV, Segmentation fault.
__memcpy_ssse3 () at ../sysdeps/x86_64/multiarch/memcpy-ssse3.S:2914
2914    ../sysdeps/x86_64/multiarch/memcpy-ssse3.S: No such file or directory.
(gdb) backtrace 
#0  __memcpy_ssse3 () at ../sysdeps/x86_64/multiarch/memcpy-ssse3.S:2914
#1  0x000000000046d58b in mutt_substrdup (begin=begin@entry=0xd047c6 "", 
    end=end@entry=0xd047c5 "\n") at lib.c:823
#2  0x0000000000462285 in write_one_header (fp=fp@entry=0x75a050, 
    pfxw=pfxw@entry=0, max=max@entry=4, wraplen=wraplen@entry=80, 
    pfx=pfx@entry=0x0, start=start@entry=0xd047c0 "Cc: \n\n", 
    end=end@entry=0xd047c5 "\n", flags=flags@entry=262294) at sendlib.c:1818
#3  0x0000000000465549 in mutt_write_one_header (fp=fp@entry=0x75a050, 
    tag=tag@entry=0x0, value=<optimized out>, pfx=pfx@entry=0x0, wraplen=80, 
    flags=flags@entry=262294) at sendlib.c:1894
#4  0x000000000041ab4c in mutt_copy_hdr (in=<optimized out>, 
    out=out@entry=0x75a050, off_start=<optimized out>, off_end=262294, 
    flags=flags@entry=262294, prefix=prefix@entry=0x0) at copy.c:289
#5  0x000000000041afee in mutt_copy_header (in=in@entry=0xcf8d70, 
    h=h@entry=0x96cd00, out=out@entry=0x75a050, flags=262294, prefix=0x0)
    at copy.c:350
#6  0x000000000041b5e2 in _mutt_copy_message (fpout=fpout@entry=0x75a050, 
    fpin=0xcf8d70, hdr=hdr@entry=0x96cd00, body=0x9e3840, 
    flags=flags@entry=76, chflags=<optimized out>, chflags@entry=262294)
    at copy.c:570
#7  0x000000000041ba35 in mutt_copy_message (fpout=0x75a050, 
    src=<optimized out>, hdr=hdr@entry=0x96cd00, flags=flags@entry=76, 
    chflags=262294) at copy.c:687
#8  0x0000000000414c03 in mutt_display_message (cur=0x96cd00) at commands.c:148
#9  0x00000000004216ea in mutt_index_menu () at curs_main.c:1199
#10 0x0000000000408894 in main (argc=1, argv=<optimized out>) at main.c:1048
}}}
Removing the "Cc: " line no longer makes Mutt crash.

--------------------------------------------------------------------------------
2012-12-18 09:34:44 UTC vinc17
* Added comment:
I can reproduce it without patches and an empty muttrc. This is due to r6225.

--------------------------------------------------------------------------------
2012-12-18 10:00:00 UTC vinc17
* Added attachment 3609.patch
* Added comment:
patch

--------------------------------------------------------------------------------
2012-12-18 10:03:27 UTC vinc17
* Added comment:
The patch is based on the fact that the newline character was taken into account previously. I think that the goal of r6225 was to reject other space characters.

--------------------------------------------------------------------------------
2012-12-18 10:18:06 UTC me
* Added comment:
{{{
On Tue, Dec 18, 2012 at 09:34:44AM -0000, Mutt wrote:

SKIPWS is redefined inside of rfc822.c.  The problem you reported 
existed prior to that change.
}}}

--------------------------------------------------------------------------------
2012-12-18 10:20:18 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [897dcc62e4aa]) fix problem where mutt_substrdup() was called with the start
pointer > end pointer, as a result of using SKIPWS().  This
occurred because the header field had an empty body, so the
trailing newline was skipped over.  The fix is to just skip over
ascii space and htab.

closes #3609

* resolution changed to fixed
* status changed to closed
