Ticket:  2048
Status:  closed
Summary: mutt requires pop or imap to use smime

Reporter: 
Owner:    mutt-dev

Opened:       2005-08-28 18:07:52 UTC
Last Updated: 2005-09-02 19:04:43 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
It should be possible to use mutt with fetchmail or similar apps, but in the latest ebuild in the gentoo portage package-system, mutt-1.5.10-r1.ebuild, this is not possible.
I would like to be able to use smime, without having to use pop or imap, as neither is useful for me.
The error is:
configure: error: SSL support is only useful with POP or IMAP support


In the ebuild it has:
 # there's no need for gnutls, ssl or sasl without either pop or imap.
 # in fact mutt's configure will bail if you do:
 #   --without-pop --without-imap --with-ssl
 if use pop || use imap; then

This is not correct. I use fetchmail for delivering mail locally, and then send it with postfix, using SMIME and pgp.

The correct test should be:
 if use pop || use imap || use smime; then

But trying to fix this, the compilation of mutt fails. Unfortunately I have already filed a bug report with Gentoo, but they should change their ebuild once this change has been committed to the mutt configure scripts.
http://bugs.gentoo.org/show_bug.cgi?id=104047
>How-To-Repeat:
>Fix:
Make mutt compile if smime is desired, not only is pop/imap and ssl/gnutls.
}}}

--------------------------------------------------------------------------------
2005-09-01 23:00:17 UTC brendan
* Added comment:
{{{
What exactly is the bug? You don't need --with-ssl to use smime, since it's not 
linked in. SMIME should work fine without POP or IMAP. Sounds like a gentoo 
issue if anything.
}}}

* status changed to new

--------------------------------------------------------------------------------
2005-09-03 13:04:43 UTC brendan
* Added comment:
{{{
Not a mutt bug.
}}}

* resolution changed to fixed
* status changed to closed
