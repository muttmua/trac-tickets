Ticket:  1017
Status:  closed
Summary: ^C (SIGINT) signal only caught once

Reporter: Eric Joanis <joanis@cs.toronto.edu>
Owner:    mutt-dev

Opened:       2002-02-01 23:35:45 UTC
Last Updated: 2005-10-19 18:52:32 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.2.5.1i
Severity: wishlist

-- Please type your report below this line

Hitting ^C to cancel any operation within mutt will bring the prompt "Exit
Mutt? ([y]/n):".  When I say "n", the current operation is cancelled (as
desired), and mutt doesn't quit (as desired).  However, if I hit ^C a
second time (after having done any number of intervening operations), the
signal appears not to be caught, and mutt terminates without saving
anything or moving the cursor to the bottom of the page as it does when
the q or x commands are used.  Hence, it is not possible to use ^C to
cancel an operation more than once without first restarting mutt.

According to a friend of mine who knows how to handle signals fairly well,
the problem would be that when the trap for the SIGINT signal is
registered with the OS, it is only registered for a single occurence.
When it is caught and handled, it should be reregistered with the OS so
that it can be caught again the next time the user hits ^C.

(Note:  doing "kill -INT <pid>" from a different shell yields exactly
the same behavior.)

PS:  I first made this a normal severity report, but after reading the
     manual, I realized that the "abort" command is ^G, and not ^C.
     However, this behaviour still bothers me, and I still think ^C should
     be caugth every time it is hit, and that the user should be given the
     "Exit Mytt?" prompt every time.  Hence this is most appropriately a
     wishlist item.

-- Mutt Version Information

Mutt 1.2.5.1i (2000-07-28)
Copyright (C) 1996-2000 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: SunOS 5.8
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  +USE_FCNTL  -USE_FLOCK
-USE_IMAP  -USE_GSS  -USE_SSL  -USE_POP  +HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  +ENABLE_NLS
SENDMAIL="/usr/lib/sendmail"
MAILPATH="/var/mail"
SHAREDIR="/u/joanis/lib/mutt"
SYSCONFDIR="/u/joanis/etc"
ISPELL="/local/bin/ispell"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the muttbug utility.


--- Begin /h/34/joanis/.muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b '
macro index \cb |urlview\n
macro pager \cb |urlview\n
set abort_nosubject=ask-no
set abort_unmodified=no
set alternates="joanis@.*"
unset beep
unset confirmappend
set copy=yes
set delete=yes
set editor="vi_ispell"
set forward_format="Fwd: %s"
set hidden_host
set include=yes
set index_format="%3C %Z %[%b %d] %-15.15F (%3l) %s"
unset mark_old
unset markers
set metoo
set move=no
set pager_context=1
set postponed="~/Mail/postponed"
set print_command="lpr -h"
set record="=sent"
unset save_empty
set save_name
set sort=reverse-date-sent
set to_chars="L +C<"
save-hook WebNotice =webnotice
save-hook '~s E-Job' =elsnet
--- End /h/34/joanis/.muttrc


--- Begin /u/joanis/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /u/joanis/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /u/joanis/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /u/joanis/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /u/joanis/etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-02-02 21:34:22 UTC Aaron Schrab <aaron+mutt@schrab.com>
* Added comment:
{{{
At 12:35 -0500 01 Feb 2002, Eric Joanis <joanis@cs.toronto.edu> wrote:
> Package: mutt
> Version: 1.2.5.1i

> System: SunOS 5.8

> Hitting ^C to cancel any operation within mutt will bring the prompt "Exit
> Mutt? ([y]/n):".  When I say "n", the current operation is cancelled (as
> desired), and mutt doesn't quit (as desired).  However, if I hit ^C a
> second time (after having done any number of intervening operations), the
> signal appears not to be caught, and mutt terminates without saving

I tried this using 1.2.5 (+ security patch) on a SunOS 5.8 system, and
couldn't reproduce it.  Mutt caught all occurrences of SIGINT.  One
noteable difference is that your build is apparently using the system
curses library, but the version I tested uses ncurses (5.2).

> According to a friend of mine who knows how to handle signals fairly well,
> the problem would be that when the trap for the SIGINT signal is
> registered with the OS, it is only registered for a single occurence.

That would be true (for some systems) if mutt used the signal(2) system
call to set signal handlers.  But mutt uses sigaction(2) instead, which
allows the application to choose whether the handler should be reset
when it's triggered; mutt does not request that the signal behaviour be
reset.

My guess is that the system curses library is playing around with
signals itself, and messing things up.  I recommend compiling against
ncurses or slang instead.

-- 
Aaron Schrab     aaron@schrab.com      http://www.execpc.com/~aarons/
It should be illegal to yell "Y2K" in a crowded economy.  :-) -- Larry Wall
}}}

--------------------------------------------------------------------------------
2005-10-20 06:59:29 UTC ab
* Added comment:
{{{
Do you still have this problem with current Mutt 1.5.11?
Have you tried ncurses as Aaron suggested?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-10-20 06:59:30 UTC ab
* Added comment:
{{{
deduppe unform
}}}

--------------------------------------------------------------------------------
2005-10-20 07:18:43 UTC Eric Joanis <eric.joanis@gmail.com>
* Added comment:
{{{
Wow, this is coming back from a long time ago!  I'm not even using
mutt anymore (I still haven't found a way to get all the Gmail
functionality from non-browser interface).

Anyway, the last version I used was 1.4.2.1i (2004-02-12) and, as I
just tested now, the problem does not occur with that version, so I
guess it has been fixed or has disappeared of its own.  Not sure if
this is the version I was using when I reported the bug, but I guess
that's no longer relevant.

Thanks for following up!

Eric

2005/10/19, Alain Bench <veronatif@free.fr>:
> Synopsis: ^C (SIGINT) signal only caught once
>
> State-Changed-From-To: open->feedback
> State-Changed-By: ab
> State-Changed-When: Wed, 19 Oct 2005 13:59:29 +0200
> State-Changed-Why:
>
>     Do you still have this problem with current Mutt 1.5.11?
> Have you tried ncurses as Aaron suggested?
>
>
>
> **** Comment added by ab on Wed, 19 Oct 2005 13:59:29 +0200 ****
>  deduppe unform
>
>
>
>
>
}}}

--------------------------------------------------------------------------------
2005-10-20 12:52:32 UTC ab
* Added comment:
{{{
Wow, this is coming back from a long time ago!  I'm not even using

    Thank you for checking, Eric, and for reporting at
first. Nobody can reproduce the problem anymore: Let's close
the flea/1017.

    Your report was about Mutt 1.2.5.1i. About Gmail probs,
you can look at comp.mail.mutt or mutt-users: Many people
seem to use (happily?) Gmail with Mutt.
}}}

* resolution changed to fixed
* status changed to closed
