Ticket:  1495
Status:  closed
Summary: macro with ë

Reporter: Mickaël Leducq <micled@free.fr>
Owner:    mutt-dev

Opened:       2003-03-15 14:25:13 UTC
Last Updated: 2005-09-02 19:20:01 UTC

Priority:  minor
Component: mutt
Keywords:  patch, #2023

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: wishlist

-- Please type your report below this line


When I am using :

macro compose <f2> "<edit-from><kill-line>Mickaël LEDUCQ <micled@free.fr>\n"

And press F2 when compose, the result is :

Mickal LEDUCQ <micled@free.fr>

The "ë" is missing

in my config, i have :

set charset="iso-8859-15"
set send_charset="iso-8859-15"

Can you help me.

Thank
Mickaël LEDUCQ
A French User of MUTT

Escuse for my english





-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i586-mandrake-linux-gnu/3.2/specs
Configured with: ../configure --prefix=/usr --libdir=/usr/lib --with-slibdir=/lib --mandir=/usr/share/man --infodir=/usr/share/info --enable-shared --enable-threads=posix --disable-checking --enable-long-long --enable-__cxa_atexit --enable-languages=c,c++,ada,f77,objc,java --host=i586-mandrake-linux-gnu --with-system-zlib
Thread model: posix
gcc version 3.2 (Mandrake Linux 9.0 3.2-1mdk)

- CFLAGS
-O3 -fomit-frame-pointer -pipe -mcpu=pentiumpro -march=i586 -ffast-math -fno-strength-reduce

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins et autres.
Mutt ne fournit ABSOLUMENT AUCUNE GARANTIE ; pour les détails tapez `mutt -vv'.
Mutt est un logiciel libre, et vous êtes libre de le redistribuer
sous certaines conditions ; tapez `mutt -vv' pour les détails.

System: Linux 2.4.20-2mdk (i686) [using ncurses 5.2]
Options de compilation :
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Pour contacter les développeurs, veuillez écrire à <mutt-dev@mutt.org>.
Pour signaler un bug, veuillez utiliser l'utilitaire flea(1).

patch-1.4.rr.compressed.1

>How-To-Repeat:
>Fix:
See patch in report #2023
}}}

--------------------------------------------------------------------------------
2003-03-28 16:35:21 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Micha[eë]ls,

On Tuesday, March 25, 2003 at 2:00:07 PM -0800, Michael R. Elkins wrote:

> On 2003-03-14, Mickaël LEDUCQ <micled@free.fr> wrote:
>> macro compose <f2> "<edit-from><kill-line>Mickaël LEDUCQ <micled@free.fr>\n"
>> the result is : Mickal LEDUCQ <micled@free.fr> The "ë" is missing
> Are you able to input the 8-bit character in prompts in Mutt if you
> type it manually?

   I get the same problem in macro as Mickaël, but can input the umlaut
manually at ESC f prompt.


>> set send_charset="iso-8859-15"

   Mickaël, no link with your prob, but can I suggest use better:

| set send_charset="us-ascii:iso-8859-1:iso-8859-15:windows-1252:utf-8"


Bye!	Alain.
}}}

--------------------------------------------------------------------------------
2003-03-29 04:49:48 UTC Mickaël LEDUCQ <micled@free.fr>
* Added comment:
{{{
Le mardi 25 mars 2003 à 14:00, Michael Elkins écrivait :
> On 2003-03-14, Mickael LEDUCQ <micled@free.fr> wrote:
> > When I am using :
> > 
> > macro compose <f2> "<edit-from><kill-line>Mickaël LEDUCQ <micled@free.fr>\n"
> > 
> > And press F2 when compose, the result is :
> > 
> > Mickal LEDUCQ <micled@free.fr>
> > 
> > The "ë" is missing
> > 
> > in my config, i have :
> > 
> > set charset="iso-8859-15"
> > set send_charset="iso-8859-15"
> 
> Are you able to input the 8-bit character in prompts in Mutt if you type
> it manually?

Yes, manually it's ok

and if i set
my_hdr From: Mickaël LEDUCQ <micled@free.fr>

The from it's ok.

The problem it is just when i using the macro

I have test the lastest version of mutt (mutt-1.5.4i) and i have the problem
("ë" is missing)

In reality, i have 3 macro with the same name and not the same email.

-- 
Mickaël LEDUCQ                               GnuPG KeyID : 0xAD483D96
Ingénieur Systèmes & Réseaux            Registered Linux User #308711
                                             @ http://counter.li.org
Pour obtenir la clé publique,
envoyer un mail vide avec comme object : GET 0xAD483D96
}}}

--------------------------------------------------------------------------------
2003-09-15 16:21:55 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# one problem one solution
merge 965 1178 1228
merge 1406 1505
# no usable reports, no followups to inquieries
close 1057
close 1408
# not a wishlist but a bug
severity 1495 normal
# contains a selfsolving patch
tags 1062 patch
}}}

--------------------------------------------------------------------------------
2005-08-25 12:50:32 UTC rado
* Added comment:
{{{
retry with 1.5.9+ and "config_charset"
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-25 12:50:33 UTC rado
* Added comment:
{{{
despam
+ request for validity with 1.5.9 + "config_charset"
}}}

--------------------------------------------------------------------------------
2005-09-02 02:52:39 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Wednesday, August 24, 2005 at 7:50:33 PM +0200, Rado Smiljanic wr=
ote:

> Synopsis: macro with =EB
> retry with 1.5.9+ and "config_charset"

    Problem reproducable with 1.5.10 and with both $config_charset=
=3Dl1
and unset. The key condition seems to be type char is signed by defau=
lt.
If char is unsigned by default, then no more bug^W symptoms. See bug
mutt/2023 for a casting patch.


Bye!=09Alain.
--=20
Everything about locales on Sven Mascheck's excellent site at new
location <URL:http://www.in-ulm.de/~mascheck/locale/>. The little tes=
ter
utility is at <URL:http://www.in-ulm.de/~mascheck/locale/checklocale.=
c>.
}}}

--------------------------------------------------------------------------------
2005-09-02 03:36:51 UTC ab
* Added comment:
{{{
crossreferencing related bugs 1495 and 2023.
}}}

--------------------------------------------------------------------------------
2005-09-03 13:20:01 UTC brendan
* Added comment:
{{{
On Wednesday, August 24, 2005 at 7:50:33 PM +0200, Rado Smiljanic wr=
Fixed in CVS, merci Alain.
}}}

* resolution changed to fixed
* status changed to closed
