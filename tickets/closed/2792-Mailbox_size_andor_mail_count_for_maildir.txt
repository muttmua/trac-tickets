Ticket:  2792
Status:  closed
Summary: Mailbox size and/or mail count for maildir

Reporter: cl@isbd.net
Owner:    mutt-dev

Opened:       2007-02-21 08:43:05 UTC
Last Updated: 2007-04-06 05:06:44 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When using maildir there's no indication whether there is mail in a maildir, you just get the size of the directory (if it's configured that way).  Some sort of indication (count of E-Mails, disk space used) would be extremely useful.
>How-To-Repeat:
Simply look at an index of maildir mailboxes, no indication of size or presence of mail (except N[ew] mail).

--------------------------------------------------------------------------------
2007-03-15 08:06:37 UTC cb
* Added comment:
{{{
Fix notify list.
}}}

--------------------------------------------------------------------------------
2007-03-27 23:12:38 UTC brendan
* Updated description:
When using maildir there's no indication whether there is mail in a maildir, you just get the size of the directory (if it's configured that way).  Some sort of indication (count of E-Mails, disk space used) would be extremely useful.
>How-To-Repeat:
Simply look at an index of maildir mailboxes, no indication of size or presence of mail (except N[ew] mail).

--------------------------------------------------------------------------------
2007-04-06 05:06:44 UTC me
* Added comment:
This is a duplicate report of #2476.

* resolution changed to duplicate
* status changed to closed
