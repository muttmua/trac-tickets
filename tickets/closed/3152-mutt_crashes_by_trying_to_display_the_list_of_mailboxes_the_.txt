Ticket:  3152
Status:  closed
Summary: mutt crashes by trying to display the list of mailboxes the second time

Reporter: Ulrich
Owner:    mutt-dev

Opened:       2009-01-12 12:43:24 UTC
Last Updated: 2009-06-10 15:03:37 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Program received signal SIGSEGV, Segmentation fault.
[Switching to Thread 0xb7c318e0 (LWP 14231)] 
0x080e17b4 in ?? ()-  2 scholzuh emld 
(gdb) backtrace-xr-x 11 scholzuh emld 
#0  0x080e17b4 in ?? () scholzuh emld 
#1  0x08052c2c in ?? () scholzuh emld 
#2  0x08053272 in ?? () scholzuh emld 
#3  0x08070d26 in ?? () scholzuh emld 
#4  0x08064ec8 in ?? () scholzuh emld  
#5  0x080650bf in ?? () scholzuh emld  
#6  0x08069337 in ?? () scholzuh emld  
#7  0x08085600 in ?? () scholzuh emld 
#8  0xb7d1d685 in __libc_start_main () from /lib/tls/i686/cmov/libc.so.6/
#9  0x0804ce71 in ?? () scholzuh emld    
}}}
{{{
[scholzuh@emld-desktop:bin] >mutt -v
Mutt 1.5.18 (2008-05-17)
Copyright (C) 1996-2008 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.27-9-generic (i686)
ncurses: ncurses 5.6.20071124 (compiled with 5.6)
libidn: 1.8 (compiled with 1.8)
hcache backend: GDBM version 1.8.3. 10/15/2002 (built Jun 15 2006 21:19:27)
Einstellungen bei der Compilierung:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +USE_SMTP  +USE_GSS  -USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, besuchen Sie bitte http://bugs.mutt.org/.

patch-1.5.13.cd.ifdef.2
patch-1.5.13.cd.purge_message.3.4
patch-1.5.13.nt+ab.xtitles.4
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.6.dw.maildir-mtime.1
patch-1.5.8.hr.sensible_browser_position.3
}}}

--------------------------------------------------------------------------------
2009-01-12 18:31:46 UTC brendan
* Added comment:
I'm afraid that backtrace isn't useful -- it seems you're missing debug symbols. This may also have been fixed recently, so I'd recommend giving 1.5.19 a try.

* component changed to IMAP
* Updated description:
{{{
Program received signal SIGSEGV, Segmentation fault.
[Switching to Thread 0xb7c318e0 (LWP 14231)] 
0x080e17b4 in ?? ()-  2 scholzuh emld 
(gdb) backtrace-xr-x 11 scholzuh emld 
#0  0x080e17b4 in ?? () scholzuh emld 
#1  0x08052c2c in ?? () scholzuh emld 
#2  0x08053272 in ?? () scholzuh emld 
#3  0x08070d26 in ?? () scholzuh emld 
#4  0x08064ec8 in ?? () scholzuh emld  
#5  0x080650bf in ?? () scholzuh emld  
#6  0x08069337 in ?? () scholzuh emld  
#7  0x08085600 in ?? () scholzuh emld 
#8  0xb7d1d685 in __libc_start_main () from /lib/tls/i686/cmov/libc.so.6/
#9  0x0804ce71 in ?? () scholzuh emld    
}}}
{{{
[scholzuh@emld-desktop:bin] >mutt -v
Mutt 1.5.18 (2008-05-17)
Copyright (C) 1996-2008 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.27-9-generic (i686)
ncurses: ncurses 5.6.20071124 (compiled with 5.6)
libidn: 1.8 (compiled with 1.8)
hcache backend: GDBM version 1.8.3. 10/15/2002 (built Jun 15 2006 21:19:27)
Einstellungen bei der Compilierung:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +USE_SMTP  +USE_GSS  -USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, besuchen Sie bitte http://bugs.mutt.org/.

patch-1.5.13.cd.ifdef.2
patch-1.5.13.cd.purge_message.3.4
patch-1.5.13.nt+ab.xtitles.4
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.6.dw.maildir-mtime.1
patch-1.5.8.hr.sensible_browser_position.3
}}}
* milestone changed to 1.6
* status changed to infoneeded_new

--------------------------------------------------------------------------------
2009-01-13 09:04:11 UTC Ulrich
* Added comment:
After some testing, I found that the problem more probably lies in the email folder list: The crash occurs when trying to cange to an IMAP folder the second time, even if the first attempt failed.

* status changed to new
* summary changed to mutt crashes by trying to display the list of mailboxes the second time

--------------------------------------------------------------------------------
2009-06-01 04:42:53 UTC brendan
* Updated description:
{{{
Program received signal SIGSEGV, Segmentation fault.
[Switching to Thread 0xb7c318e0 (LWP 14231)] 
0x080e17b4 in ?? ()-  2 scholzuh emld 
(gdb) backtrace-xr-x 11 scholzuh emld 
#0  0x080e17b4 in ?? () scholzuh emld 
#1  0x08052c2c in ?? () scholzuh emld 
#2  0x08053272 in ?? () scholzuh emld 
#3  0x08070d26 in ?? () scholzuh emld 
#4  0x08064ec8 in ?? () scholzuh emld  
#5  0x080650bf in ?? () scholzuh emld  
#6  0x08069337 in ?? () scholzuh emld  
#7  0x08085600 in ?? () scholzuh emld 
#8  0xb7d1d685 in __libc_start_main () from /lib/tls/i686/cmov/libc.so.6/
#9  0x0804ce71 in ?? () scholzuh emld    
}}}
{{{
[scholzuh@emld-desktop:bin] >mutt -v
Mutt 1.5.18 (2008-05-17)
Copyright (C) 1996-2008 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.27-9-generic (i686)
ncurses: ncurses 5.6.20071124 (compiled with 5.6)
libidn: 1.8 (compiled with 1.8)
hcache backend: GDBM version 1.8.3. 10/15/2002 (built Jun 15 2006 21:19:27)
Einstellungen bei der Compilierung:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +USE_SMTP  +USE_GSS  -USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, besuchen Sie bitte http://bugs.mutt.org/.

patch-1.5.13.cd.ifdef.2
patch-1.5.13.cd.purge_message.3.4
patch-1.5.13.nt+ab.xtitles.4
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.6.dw.maildir-mtime.1
patch-1.5.8.hr.sensible_browser_position.3
}}}
* version changed to 1.5.18

--------------------------------------------------------------------------------
2009-06-10 02:00:35 UTC brendan
* Added comment:
I don't suppose you could give a more recent version a try? A backtrace with symbols and a debug file would also be very helpful.

* Updated description:
{{{
Program received signal SIGSEGV, Segmentation fault.
[Switching to Thread 0xb7c318e0 (LWP 14231)] 
0x080e17b4 in ?? ()-  2 scholzuh emld 
(gdb) backtrace-xr-x 11 scholzuh emld 
#0  0x080e17b4 in ?? () scholzuh emld 
#1  0x08052c2c in ?? () scholzuh emld 
#2  0x08053272 in ?? () scholzuh emld 
#3  0x08070d26 in ?? () scholzuh emld 
#4  0x08064ec8 in ?? () scholzuh emld  
#5  0x080650bf in ?? () scholzuh emld  
#6  0x08069337 in ?? () scholzuh emld  
#7  0x08085600 in ?? () scholzuh emld 
#8  0xb7d1d685 in __libc_start_main () from /lib/tls/i686/cmov/libc.so.6/
#9  0x0804ce71 in ?? () scholzuh emld    
}}}
{{{
[scholzuh@emld-desktop:bin] >mutt -v
Mutt 1.5.18 (2008-05-17)
Copyright (C) 1996-2008 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.27-9-generic (i686)
ncurses: ncurses 5.6.20071124 (compiled with 5.6)
libidn: 1.8 (compiled with 1.8)
hcache backend: GDBM version 1.8.3. 10/15/2002 (built Jun 15 2006 21:19:27)
Einstellungen bei der Compilierung:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +USE_SMTP  +USE_GSS  -USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, besuchen Sie bitte http://bugs.mutt.org/.

patch-1.5.13.cd.ifdef.2
patch-1.5.13.cd.purge_message.3.4
patch-1.5.13.nt+ab.xtitles.4
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.6.dw.maildir-mtime.1
patch-1.5.8.hr.sensible_browser_position.3
}}}
* status changed to infoneeded_new

--------------------------------------------------------------------------------
2009-06-10 15:03:22 UTC brendan
* Added comment:
From the submitter:

Well, the problem went away after an upgrade of kubuntu.  I don't have the problem anymore.  So, I guess, it was a problem outside of mutt.  You can close the bug.

* status changed to new

--------------------------------------------------------------------------------
2009-06-10 15:03:37 UTC brendan
* resolution changed to worksforme
* status changed to closed
